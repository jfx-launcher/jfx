# English
## Project structure
The project is split into several modules.
Some modules are split into two modules: API and Implementation.
Other modules are not separated and contain API and implementation.
Modules that are split into API and Implementation parts were split based on the likelihood of whether a different implementation might be required.
Various modules like launcher and launcher-impl should be merged because the chances of different implementations are very small, but I don't have time to merge them.

Most of the modules are very small and self-contained.  
For example, `gui-common` depends on `injector`. `gui-common` implements most basic GUI workflow, like Window management (`StageManager`), Overlays and Views.  
`gui-fxml` only brings fxml support. This is made this way, so other modules that don't need fxml will not have `javafx-fxml` as a transitive dependency.  
`gui-core` implements most of the bootstrapping and loading logic and some management logic, it wires various modules.
`core` module starts an application, implements most views and controllers (`auth-openid` ships its own auth view and controller)

`authlib-remapper` implements authlib library patching, it patches URLs so different auth server could be used.
Only V1 and V2 libraries are supported. Some limited support for V3 is present, but it's broken on recent game versions. (21w38a/21w39a or newer)

`user-info` and `user-info-impl` implements game-specific profile representation  
`account-info` and `account-info-impl` implements lookup to find some information about accounts, like Account ID.

`injector` implements some very crude dependency injecting mechanism.
It was made because I wanted to use GraalVM and compile the whole launcher into native binary using `native-image`.
I was not able to find an easy to use `jakarta.inject` implementation that supported Java Module System and did not have a lot of dependencies.
I found `avaje-inject`, but it looked like it had a lot of boilerplate if module system was enabled.

`launcher` and `launcher-impl` implement game download and launch logic.

`auth` contains some primitives to implement authentication service and get basic info like user claims.  
`auth-openid` contains OAuth 2.0 Authorization Code Flow with PKCE client and authentication logic.

`downloader` implements multithreaded HTTP file downloader  
`file` contains an interface that represents some file in a File System  
`network` contains some abstraction to monitor IO work

`httpclient-utils` contains some utilities for Java 11 HttpClient, like logic to handle input differently based on returned error codes.
It would be nice if it supported throwing exceptions on some codes, like 403 code, but I'm tired of this project and don't want to spend time on it.

`platform` contains an interface that represents some platform-specific information, like operating system, it's version, it's bitness.

`modpack-manager` contains mod pack management logic, like download mod pack, upload mod pack, etc.
`skin-manager` contains skin upload system
`update-manager` contains client for Update Service. It can download or upload entries from Update Service.
Currently, Update Service supports 3 types of entries: Updates for Launcher, Updater and Java.
Java entries are used by this launcher to download default or mod pack specific java versions.
Launcher and Updater entries are used by Updater. Updater is a bootstrap program that downloads or updates this launcher or itself.

`Settings` is some abstraction to store configuration files.

## How to build
To make JLink image, run these maven commands:
1. `mvn install -Djavafx.version=17.0.1`
2. `mvn jlink:jlink -Djavafx.version=17 -pl core` (not a typo, `17` is a right version)  
Jlink image zip path is: `core/target/core-*.zip`

Reasons:  
Different JavaFX versions are required due to JavaFX packaging issues.  
In Maven JavaFX plugin, we can't pass --add-modules arguments, so some modules will not be present in JLink image.  
`--bind-services` can be used, but it produces a lot larger modules for no reason and looks like these builds contain a lot of JDK stuff that is not required.  
Maven JLink plugin supports everything we need, but it does not support faulty JavaFX packaging.  
JavaFX ships real modules and empty jars with Automatic-Module-Name, and JLink fails because it finds these modules.  
With JavaFX 17, maven can't build project, compiler can't find JavaFX classes.  
To work around these limitations, it should be built with JavaFX 17.0.1 and then jlink'ed with JavaFX 17, because this version does not ship JARs with Automatic-Module-Name.