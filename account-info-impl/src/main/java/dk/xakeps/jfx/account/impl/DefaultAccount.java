package dk.xakeps.jfx.account.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.account.Account;

public record DefaultAccount(@JsonProperty("id") String id,
                             @JsonProperty("email") String email) implements Account {
    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
