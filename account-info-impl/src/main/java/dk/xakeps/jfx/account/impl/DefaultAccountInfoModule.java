package dk.xakeps.jfx.account.impl;

import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import com.github.mizosoft.methanol.TypeRef;
import dk.xakeps.jfx.account.Account;
import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.account.AccountModuleException;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class DefaultAccountInfoModule implements AccountInfoModule {
    private final Methanol methanol;
    private final URI serviceUri;

    public DefaultAccountInfoModule(URI serviceUri, String accessToken) {
        Objects.requireNonNull(accessToken, "accessToken");
        this.methanol = Methanol.newBuilder()
                .defaultHeader("Authorization", "Bearer " + accessToken)
                .build();
        this.serviceUri = Objects.requireNonNull(serviceUri, "serviceUri");
    }

    @Override
    public Optional<Account> findAccountByEmail(String email) {
        URI endpoint = serviceUri.resolve("find?email=" + URLEncoder.encode(email, StandardCharsets.UTF_8));
        HttpRequest request = HttpRequest.newBuilder(endpoint).GET().build();
        try {
            HttpResponse<List<DefaultAccount>> response = methanol.send(request,
                    MoreBodyHandlers.ofObject(new TypeRef<List<DefaultAccount>>() {
                    }));
            List<DefaultAccount> body = response.body();
            if (body.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(body.get(0));
        } catch (IOException | InterruptedException e) {
            throw new AccountModuleException("Can't receive", e);
        }
    }

    @Override
    public List<Account> findAccountsByIds(List<String> ids) {
        StringJoiner joiner = new StringJoiner("&");
        for (String id : ids) {
            joiner.add("id=" + URLEncoder.encode(id, StandardCharsets.UTF_8));
        }
        URI endpoint = serviceUri.resolve("find?" + joiner);
        HttpRequest request = HttpRequest.newBuilder(endpoint).GET().build();
        try {
            HttpResponse<List<DefaultAccount>> response = methanol.send(request,
                    MoreBodyHandlers.ofObject(new TypeRef<List<DefaultAccount>>() {}));
            return Collections.unmodifiableList(response.body());
        } catch (IOException | InterruptedException e) {
            throw new AccountModuleException("Can't receive", e);
        }
    }
}
