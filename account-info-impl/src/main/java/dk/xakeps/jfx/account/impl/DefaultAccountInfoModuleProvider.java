package dk.xakeps.jfx.account.impl;

import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.account.AccountInfoModuleProvider;

import java.net.URI;
import java.util.Map;

public class DefaultAccountInfoModuleProvider implements AccountInfoModuleProvider {
    @Override
    public String getId() {
        return "DEFAULT";
    }

    @Override
    public AccountInfoModule createModule(Map<String, String> settings) {
        URI serviceUri = URI.create(settings.get("serviceUri"));
        String accessToken = settings.get("accessToken");
        return new DefaultAccountInfoModule(serviceUri, accessToken);
    }
}
