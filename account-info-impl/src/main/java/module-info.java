import com.github.mizosoft.methanol.BodyAdapter;
import dk.xakeps.jfx.account.AccountInfoModuleProvider;
import dk.xakeps.jfx.account.impl.DefaultAccountInfoModuleProvider;
import dk.xakeps.jfx.account.impl.JacksonAdapters;

module dk.xakeps.jfx.account.impl {
    requires methanol;
    requires methanol.adapter.jackson;

    requires dk.xakeps.jfx.account;

    opens dk.xakeps.jfx.account.impl to com.fasterxml.jackson.databind;

    provides BodyAdapter.Encoder with JacksonAdapters.JacksonEncoder;
    provides BodyAdapter.Decoder with JacksonAdapters.JacksonDecoder;

    provides AccountInfoModuleProvider with DefaultAccountInfoModuleProvider;
}