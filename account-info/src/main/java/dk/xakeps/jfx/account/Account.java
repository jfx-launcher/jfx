package dk.xakeps.jfx.account;

public interface Account {
    String getId();
    String getEmail();
}
