package dk.xakeps.jfx.account;

import java.util.List;
import java.util.Optional;

public interface AccountInfoModule {
    Optional<Account> findAccountByEmail(String email);
    List<Account> findAccountsByIds(List<String> ids);
}
