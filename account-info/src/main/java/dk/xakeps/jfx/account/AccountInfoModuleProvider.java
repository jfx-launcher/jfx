package dk.xakeps.jfx.account;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

public interface AccountInfoModuleProvider {
    String getId();

    default AccountInfoModule createModule() {
        return createModule(Collections.emptyMap());
    }

    AccountInfoModule createModule(Map<String, String> settings);

    static Optional<AccountInfoModuleProvider> findAny() {
        return ServiceLoader.load(AccountInfoModuleProvider.class).findFirst();
    }

    static Optional<AccountInfoModuleProvider> find(String id) {
        return ServiceLoader.load(AccountInfoModuleProvider.class).stream()
                .map(ServiceLoader.Provider::get)
                .filter(provider -> provider.getId().equals(id))
                .findAny();
    }
}
