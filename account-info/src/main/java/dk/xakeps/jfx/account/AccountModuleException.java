package dk.xakeps.jfx.account;

public class AccountModuleException extends RuntimeException {
    public AccountModuleException() {
    }

    public AccountModuleException(String message) {
        super(message);
    }

    public AccountModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountModuleException(Throwable cause) {
        super(cause);
    }

    public AccountModuleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
