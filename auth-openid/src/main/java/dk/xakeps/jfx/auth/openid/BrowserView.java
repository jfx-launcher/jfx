package dk.xakeps.jfx.auth.openid;

import dk.xakeps.jfx.gui.common.view.View;
import javafx.application.Platform;
import javafx.scene.web.WebView;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class BrowserView implements View {
    private final WebView browserNode;

    private BrowserView(WebView browserNode) {
        this.browserNode = Objects.requireNonNull(browserNode, "browserNode");
    }

    @Override
    public WebView getContent() {
        return browserNode;
    }

    public static CompletableFuture<BrowserView> load() {
        return CompletableFuture.supplyAsync(WebView::new, Platform::runLater)
                .thenApplyAsync(BrowserView::new);
    }
}
