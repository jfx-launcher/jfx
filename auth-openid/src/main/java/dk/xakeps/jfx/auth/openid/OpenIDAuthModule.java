package dk.xakeps.jfx.auth.openid;

import com.fasterxml.jackson.core.type.TypeReference;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.auth.AuthModule;
import dk.xakeps.jfx.auth.openid.model.LoginData;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayHandle;
import dk.xakeps.jfx.settings.Settings;
import dk.xakeps.jfx.settings.SettingsLoader;
import javafx.application.Platform;
import javafx.util.Pair;

import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class OpenIDAuthModule implements AuthModule {
    private static final System.Logger LOGGER = System.getLogger(OpenIDAuthModule.class.getName());

    private final OpenIDAuthService authService;
    private final StageManager stageManager;

    public OpenIDAuthModule(String issuer, String clientId, String rolesClaims, String rolesSeparator,
                            SettingsLoader loader, StageManager stageManager) {
        Settings<LoginData> loginData = loader.load(Path.of("login"), null, new TypeReference<>() {});
        this.authService = new OpenIdAuthServiceImpl(loginData, issuer, clientId, rolesClaims, rolesSeparator);
        this.stageManager = stageManager;
    }

    @Override
    public CompletableFuture<AuthInfo> authenticate() {
        LOGGER.log(System.Logger.Level.DEBUG, "Running authentication sequence");
        return BrowserView.load()
                .thenApplyAsync(this::enableWaitOverlay, Platform::runLater)
                .thenApplyAsync(this::changeView, Platform::runLater)
                .thenApplyAsync(this::authenticate)
                .thenApplyAsync(this::disableWaitOverlay, Platform::runLater);
    }

    private Pair<BrowserView, OverlayHandle> enableWaitOverlay(BrowserView browserView) {
        LOGGER.log(System.Logger.Level.TRACE, "Wait overlay enabled");
        OverlayHandle overlayHandle = stageManager.getOverlayManager().addOverlay(IndicatorOverlay.INSTANCE);
        return new Pair<>(browserView, overlayHandle);
    }

    private Pair<BrowserView, OverlayHandle> changeView(Pair<BrowserView, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.TRACE, "Changing view to the browser view");
        stageManager.setCurrentView(pair.getKey());
        return pair;
    }

    private Pair<AuthInfo, OverlayHandle> authenticate(Pair<BrowserView, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.DEBUG, "Authenticating...");
        AuthInfo authInfo = authService.authenticate(url -> onAuthenticate(url, pair));
        LOGGER.log(System.Logger.Level.DEBUG, "Authenticated");
        return new Pair<>(authInfo, pair.getValue());
    }

    private void onAuthenticate(String url, Pair<BrowserView, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.DEBUG, "Manual authentication required");
        Platform.runLater(() -> {
            stageManager.getOverlayManager().disableOverlay(pair.getValue());
            pair.getKey().getContent().getEngine().load(url);
        });
    }

    private AuthInfo disableWaitOverlay(Pair<AuthInfo, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.TRACE, "Wait overlay disabled");
        stageManager.getOverlayManager().disableOverlay(pair.getValue());
        return pair.getKey();
    }
}
