package dk.xakeps.jfx.auth.openid;

import dk.xakeps.jfx.auth.AuthModule;
import dk.xakeps.jfx.auth.AuthModuleProvider;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.settings.SettingsLoaderAware;
import dk.xakeps.jfx.settings.SettingsLoader;

import java.util.Map;
import java.util.Objects;

public class OpenIDAuthModuleProvider implements AuthModuleProvider, SettingsLoaderAware, StageManagerAware {
    private SettingsLoader settingsLoader;
    private StageManager stageManager;

    @Override
    public String getId() {
        return "OPENID";
    }

    @Override
    public AuthModule createModule(Map<String, String> settings) {
        String issuer = settings.get("issuer");
        Objects.requireNonNull(issuer, "'issuer' setting containing openid issuer is required");

        String clientId = settings.get("clientId");
        Objects.requireNonNull(clientId, "'clientId' setting containing openid client id is required");

        String rolesClaims = settings.get("rolesClaims");
        Objects.requireNonNull(rolesClaims, "rolesClaims");

        String rolesSeparator = Objects.requireNonNullElse(settings.get("rolesSeparator"), " ");

        Objects.requireNonNull(settingsLoader, "settingsLoader");
        return new OpenIDAuthModule(issuer, clientId, rolesClaims, rolesSeparator, settingsLoader, stageManager);
    }

    @Override
    public void setSettingsLoader(SettingsLoader settingsLoader) {
        this.settingsLoader = Objects.requireNonNull(settingsLoader, "settingsLoader");
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = Objects.requireNonNull(stageManager, "stageManager");
    }
}
