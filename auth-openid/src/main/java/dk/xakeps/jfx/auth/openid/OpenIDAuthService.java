package dk.xakeps.jfx.auth.openid;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.auth.openid.client.BrowseCallback;

public interface OpenIDAuthService {
    AuthInfo authenticate(BrowseCallback browseCallback);
}
