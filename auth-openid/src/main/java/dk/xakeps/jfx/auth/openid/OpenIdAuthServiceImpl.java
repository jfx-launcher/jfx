package dk.xakeps.jfx.auth.openid;

import com.github.mizosoft.methanol.*;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.auth.AuthenticatorException;
import dk.xakeps.jfx.auth.openid.client.AuthorizationCodeResponse;
import dk.xakeps.jfx.auth.openid.client.BrowseCallback;
import dk.xakeps.jfx.auth.openid.client.HttpAuthorizationCodeListener;
import dk.xakeps.jfx.auth.openid.client.WebResponse;
import dk.xakeps.jfx.auth.openid.client.config.*;
import dk.xakeps.jfx.auth.openid.client.model.AccessTokenResponse;
import dk.xakeps.jfx.auth.openid.client.model.ErrorResponse;
import dk.xakeps.jfx.auth.openid.client.model.OpenIdSettings;
import dk.xakeps.jfx.auth.openid.http.ErrorHandler;
import dk.xakeps.jfx.auth.openid.http.Response;
import dk.xakeps.jfx.auth.openid.model.AuthInfoModel;
import dk.xakeps.jfx.auth.openid.model.LoginData;
import dk.xakeps.jfx.settings.Settings;
import io.fusionauth.jwks.JSONWebKeySetHelper;
import io.fusionauth.jwks.domain.JSONWebKey;
import io.fusionauth.jwt.domain.JWT;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.regex.Pattern;

public class OpenIdAuthServiceImpl implements OpenIDAuthService {
    /**
     * This pattern uses a positive lookahead to split an expression around the forward slashes
     * ignoring those which are located inside a pair of the double quotes.
     */
    private static final Pattern CLAIM_PATH_PATTERN = Pattern.compile("/(?=(?:(?:[^\"]*\"){2})*[^\"]*$)");
    /**
     * This pattern uses a positive lookahead to split an expression around the semicolon
     * ignoring those which are located inside a pair of the double quotes.
     */
    private static final Pattern CLAIM_GROUP_PATTERN = Pattern.compile(";(?=(?:(?:[^\"]*\"){2})*[^\"]*$)");

    private final Settings<LoginData> loginDataSettings;
    private final String issuer;
    private final String clientId;
    private final String rolesClaims;
    private final String rolesSeparator; // nullable

    private boolean loggedId;

    public OpenIdAuthServiceImpl(Settings<LoginData> loginDataSettings,
                                 String issuer, String clientId, String rolesClaims, String rolesSeparator) {
        this.loginDataSettings = loginDataSettings;
        this.issuer = issuer;
        this.clientId = clientId;
        this.rolesClaims = rolesClaims;
        this.rolesSeparator = rolesSeparator;
        this.loggedId = false;
    }

    private Optional<VerifiedTokens> checkAndSaveToken() {
        try {
            loginDataSettings.reload();
        } catch (IOException e) {
            // log this TODO:
        }

        LoginData object = loginDataSettings.getObject();
        if (object == null) {
            return Optional.empty();
        }

        String refreshToken = object.getRefreshToken();
        VerifiedTokens verifiedTokens = refreshToken(refreshToken).orElse(null);
        if (verifiedTokens == null) {
            return Optional.empty();
        }
        object = new LoginData(verifiedTokens.accessTokenString(), verifiedTokens.refreshTokenString());
        loginDataSettings.setObject(object);
        try {
            loginDataSettings.save();
        } catch (IOException e) {
            // todo: failed to save, log
            e.printStackTrace();
        }
        loggedId = true;
        return Optional.of(verifiedTokens);
    }

    @Override
    public AuthInfo authenticate(BrowseCallback browseCallback) {
        if (loggedId) {
            throw new AuthenticatorException("Already logged in");
        }

        VerifiedTokens verifiedTokens = checkAndSaveToken().orElse(null);
        if (verifiedTokens != null) {
            loggedId = true;
            return new AuthInfoModel(
                    verifiedTokens.accessToken().subject,
                    loginDataSettings.getObject().getAccessToken(),
                    getGroups(verifiedTokens.accessToken())
            );
        }

        verifiedTokens = performDesktopLogin(browseCallback);

        return new AuthInfoModel(verifiedTokens.accessToken().subject, verifiedTokens.accessTokenString(),
                getGroups(verifiedTokens.accessToken()));
    }

    private Set<String> getGroups(JWT jwt) {
        return getGroups(rolesClaims, rolesSeparator, jwt);
    }

    static Set<String> getGroups(String rolesClaims, String rolesSeparator, JWT jwt) {
        Set<String> roles = new HashSet<>();
        String[] claimPaths = splitClaimGroup(rolesClaims);
        for (String claimGroup : claimPaths) {
            Map<?, ?> otherClaims = jwt.getOtherClaims();
            String[] keys = splitClaimPath(claimGroup);
            for (int i = 0; i < keys.length; i++) {
                String key = keys[i];
                Object o = otherClaims.get(key);
                if (i == keys.length - 1) {
                    if (o instanceof Collection<?> l) {
                        l.stream().map(Object::toString).forEach(roles::add);
                    } else if (o instanceof String s) {
                        if (rolesSeparator != null) {
                            roles.addAll(Arrays.asList(s.split(rolesSeparator)));
                        } else {
                            roles.add(s);
                        }
                    } else {
//                        throw new AuthenticatorException(
//                                String.format("Unknown node type. Groups path '%s', node type: '%s'",
//                                        claimGroup, o != null ? o.getClass() : "null"));
                        // todo: log warning if no roles found by provided path
                    }
                    continue;
                }

                if (o instanceof Map) {
                    otherClaims = (Map<?, ?>) o;
                } else {
//                    throw new AuthenticatorException("Bad roles path. Groups path not found: " + claimGroup);
                    // todo: log warning if no roles found by provided path
                }
            }
        }
        return roles;
    }

    private static String[] splitClaimPath(String claimPath) {
        return claimPath.indexOf('/') > 0 ? CLAIM_PATH_PATTERN.split(claimPath) : new String[] { claimPath };
    }

    private static String[] splitClaimGroup(String claimPath) {
        return claimPath.indexOf(';') > 0 ? CLAIM_GROUP_PATTERN.split(claimPath) : new String[] { claimPath };
    }

    private VerifiedTokens performDesktopLogin(BrowseCallback browseCallback) {
        try {
            OpenIdSettings openIdSettings = OpenIdSettings.fetchFrom(issuer);
            OpenIdInfo openIdInfo = new OpenIdInfo(issuer, clientId, openIdSettings);
            String successRedirectUrl = openIdSettings.getTokenEndpoint()
                    .replace("/token", "/delegated");
            RedirectInfo redirectInfo = new RedirectInfo(successRedirectUrl,
                    successRedirectUrl + "?error=true");
            Config config = new Config(openIdInfo, redirectInfo, null);
            HttpAuthorizationCodeListener httpAuthorizationCodeListener = new HttpAuthorizationCodeListener(config);
            WebResponse webResponse = httpAuthorizationCodeListener.start(browseCallback).join();
            VerifiedTokens verifiedTokens = exchangeAuthCodeToToken(config, webResponse);

            loggedId = true;
            LoginData loginData = new LoginData(verifiedTokens.accessTokenString(),
                    verifiedTokens.refreshTokenString());
            loginDataSettings.setObject(loginData);
            try {
                loginDataSettings.save();
            } catch (IOException e) {
                // failed to save TODO: log
                e.printStackTrace();
            }
            return verifiedTokens;
        } catch (IOException | URISyntaxException e) {
            throw new AuthenticatorException(e);
        }
    }

    private VerifiedTokens exchangeAuthCodeToToken(Config config, WebResponse webResponse) {
        AuthorizationCodeResponse authorizationCodeResponse = webResponse.getAuthorizationCodeResponse();
        AuthParams authParams = webResponse.getAuthParams();
        if (authorizationCodeResponse.getError().isPresent()) {
            throw new AuthenticatorException("Error: " + authorizationCodeResponse.getError().orElse("")
                    + ", Description: " + authorizationCodeResponse.getErrorDescription().orElse(""));
        }

        if (!authParams.getState().equals(authorizationCodeResponse.getState().orElse(null))) {
            throw new AuthenticatorException("Invalid state");
        }

        String redirectUri = authParams.getRedirectUri();
        URI redirectUriDirty = URI.create(redirectUri);
        String cleanQuery = QueryUtil.removeParam(redirectUriDirty.getRawQuery(), "code", "state");
        URI redirectUriClean = replaceQuery(redirectUriDirty, cleanQuery);

        String tokenEndpoint = config.getOpenIdInfo().getOpenIdSettings().getTokenEndpoint();

        Methanol httpClient = Methanol.create();
        FormBodyPublisher body = FormBodyPublisher.newBuilder()
                .query("grant_type", "authorization_code")
                .query("code", authorizationCodeResponse.getCode().orElseThrow())
                .query("redirect_uri", redirectUriClean.toString())
                .query("code_verifier", authParams.getPkce().getCodeVerifier())
                .query("client_id", config.getOpenIdInfo().getClientId())
                .build();
        HttpRequest request = HttpRequest.newBuilder(URI.create(tokenEndpoint))
                .POST(body)
                .build();

        AccessTokenResponse accessTokenResponse;
        try {
            HttpResponse<Response<AccessTokenResponse>> response = httpClient.send(request,
                    ErrorHandler.withBodyHandler(MoreBodyHandlers.ofObject(AccessTokenResponse.class)));
            accessTokenResponse = response.body().response();
            ErrorResponse errorResponse = response.body().errorResponse();
            if (errorResponse != null) {
                throw new AuthenticatorException(String.format(
                        "Error reading AccessTokenResponse. Response code: %d, Error response: %s",
                        response.statusCode(), errorResponse));
            }
        } catch (IOException | InterruptedException e) {
            throw new AuthenticatorException("Can't exchange authorization code to access token", e);
        }
        return verifyToken(accessTokenResponse);
    }

    private Optional<VerifiedTokens> refreshToken(String refreshToken) {
        OpenIdSettings openIdSettings;
        try {
            openIdSettings = OpenIdSettings.fetchFrom(issuer);
        } catch (OpenIdSettingsFetchException e) {
            throw new AuthenticatorException("Can't fetch openid settings", e);
        }

        String tokenEndpoint = openIdSettings.getTokenEndpoint();

        Methanol httpClient = Methanol.create();
        FormBodyPublisher body = FormBodyPublisher.newBuilder()
                .query("grant_type", "refresh_token")
                .query("refresh_token", refreshToken)
                .query("client_id", clientId)
                .build();
        HttpRequest request = HttpRequest.newBuilder(URI.create(tokenEndpoint))
                .POST(body)
                .build();

        AccessTokenResponse accessTokenResponse;
        try {
            HttpResponse<Response<AccessTokenResponse>> response = httpClient.send(request,
                    ErrorHandler.withBodyHandler(MoreBodyHandlers.ofObject(AccessTokenResponse.class)));
            accessTokenResponse = response.body().response();
            ErrorResponse errorResponse = response.body().errorResponse();
            if (errorResponse != null) {
                System.getLogger(getClass().getName()).log(System.Logger.Level.TRACE,
                        "Error reading AccessTokenResponse. Response code: %d, Error response: %s",
                        response.statusCode(), errorResponse); // todo: fix logging
                return Optional.empty();
            }
        } catch (IOException | InterruptedException e) {
            // todo: logging;
            return Optional.empty();
        }
        return Optional.of(verifyToken(accessTokenResponse));
    }

    private VerifiedTokens verifyToken(AccessTokenResponse response) {
        List<JSONWebKey> keys = JSONWebKeySetHelper.retrieveKeysFromIssuer(issuer);
        JWT accessToken = TokenVerifier.newVerifier(issuer, keys)
                .checkIssuer()
                .checkSubjectExist()
                .checkTokenIsBearer()
                .verify(response.getToken());

        JWT idToken = TokenVerifier.newVerifier(issuer, keys)
                .checkAudience(clientId)
                .checkIssuedFor(clientId)
                .verify(response.getIdToken());

        return new VerifiedTokens(accessToken, idToken, response.getToken(), response.getIdToken(),
                response.getRefreshToken());
    }

    private static URI replaceQuery(URI uri, String query) {
        try {
            return new URI(
                    uri.getScheme(),
                    uri.getRawAuthority(),
                    uri.getPath(),
                    query,
                    uri.getRawFragment()
            );
        } catch (URISyntaxException e) {
            throw new AuthenticatorException("Can't recreate URI", e); // todo: fix
        }
    }
}
