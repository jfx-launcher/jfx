package dk.xakeps.jfx.auth.openid;

public class PkceException extends RuntimeException {
    public PkceException() {
    }

    public PkceException(String message) {
        super(message);
    }

    public PkceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PkceException(Throwable cause) {
        super(cause);
    }

    public PkceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
