package dk.xakeps.jfx.auth.openid;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class QueryUtil {
    /**
     * Removes params from query
     * @param query encoded query, for example {@link java.net.URI#getRawQuery}
     * @param params unencoded param names
     * @return filtered query string
     */
    public static String removeParam(String query, String... params) {
        if (query == null) {
            return null;
        }
        List<String> paramsList = Arrays.stream(params)
                .map(s -> URLEncoder.encode(s, StandardCharsets.UTF_8) + "=").collect(Collectors.toList());
        StringBuilder newQuery = new StringBuilder();
        String[] split = query.split("&");
        for (String s : split) {
            if (paramsList.stream().anyMatch(s::startsWith)) {
                continue;
            }
            if (!newQuery.isEmpty()) {
                newQuery.append("&");
            }
            newQuery.append(s);
        }
        return newQuery.toString();
    }
}
