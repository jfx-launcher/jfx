package dk.xakeps.jfx.auth.openid;

import io.fusionauth.jwks.JSONWebKeySetHelper;
import io.fusionauth.jwks.domain.JSONWebKey;
import io.fusionauth.jwt.JWTUtils;
import io.fusionauth.jwt.Verifier;
import io.fusionauth.jwt.domain.Algorithm;
import io.fusionauth.jwt.domain.Header;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.ec.ECVerifier;
import io.fusionauth.jwt.rsa.RSAPSSVerifier;
import io.fusionauth.jwt.rsa.RSAVerifier;

import java.security.PublicKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

public class TokenVerifier {
    private final String issuer;
    private final List<JSONWebKey> keys;
    private final List<Predicate<JWT>> checks;

    private TokenVerifier(String issuer, List<JSONWebKey> keys) {
        this.issuer = Objects.requireNonNull(issuer, "issuer");
        this.keys = Objects.requireNonNull(keys, "keys");
        this.checks = new ArrayList<>();
    }

    public JWT verify(String token) {
        Verifier verifier = createVerifier(token);
        JWT jwt = JWT.getDecoder().decode(token, verifier);

        for (Predicate<JWT> check : checks) {
            if (!check.test(jwt)) {
                throw new TokenVerifierException("Token verification failed");
            }
        }

        return jwt;
    }

    public TokenVerifier withCheck(Predicate<JWT> predicate) {
        Objects.requireNonNull(predicate, "predicate");
        this.checks.add(predicate);
        return this;
    }

    public TokenVerifier checkIssuer() {
        return withCheck(jwt -> issuer.equals(jwt.issuer));
    }

    public TokenVerifier checkSubjectExist() {
        return withCheck(jwt -> jwt.subject != null && !jwt.subject.isBlank());
    }

    public TokenVerifier checkTokenType(String tokenType) {
        return withCheck(jwt -> tokenType.equals(jwt.getString("typ")));
    }

    public TokenVerifier checkTokenIsBearer() {
        return checkTokenType("Bearer");
    }

    public TokenVerifier checkAudience(String audience) {
        return withCheck(jwt -> audience.equals(jwt.audience));
    }

    public TokenVerifier checkIssuedFor(String issuedFor) {
        return withCheck(jwt -> issuedFor.equals(jwt.getString("azp")));
    }

    public TokenVerifier clearChecks() {
        this.checks.clear();
        return this;
    }

    private Verifier createVerifier(String jwt) {
        Header header = JWTUtils.decodeHeader(jwt);
        String kid = header.getString("kid");
        JSONWebKey accessTokenKey = getKey(kid);
        return createVerifier(accessTokenKey);
    }

    private JSONWebKey getKey(String kid) {
        for (JSONWebKey key : keys) {
            if (kid.equals(key.kid)) {
                return key;
            }
        }
        throw new TokenVerifierException("Can't find key");
    }

    private Verifier createVerifier(JSONWebKey key) {
        if (key.alg == Algorithm.HS256
                || key.alg == Algorithm.HS384
                || key.alg == Algorithm.HS512) {
            throw new IllegalArgumentException("HMAC unsupported");
        }
        PublicKey parse = JSONWebKey.parse(key);
        return switch (key.alg) {
            case ES256, ES384, ES512 -> ECVerifier.newVerifier((ECPublicKey) parse);
            case HS256, HS384, HS512 -> throw new IllegalArgumentException("HMAC unsupported");
            case PS256, PS384, PS512 -> RSAPSSVerifier.newVerifier((RSAPublicKey) parse);
            case RS256, RS384, RS512 -> RSAVerifier.newVerifier((RSAPublicKey) parse);
            case none -> null;
        };
    }

    public static TokenVerifier newVerifier(String issuer) {
        return new TokenVerifier(issuer, JSONWebKeySetHelper.retrieveKeysFromIssuer(issuer));
    }

    public static TokenVerifier newVerifier(String issuer, List<JSONWebKey> keys) {
        return new TokenVerifier(issuer, keys);
    }
}
