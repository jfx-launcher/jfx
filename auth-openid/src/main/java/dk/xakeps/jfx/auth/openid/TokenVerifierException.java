package dk.xakeps.jfx.auth.openid;

public class TokenVerifierException extends RuntimeException {
    public TokenVerifierException() {
    }

    public TokenVerifierException(String message) {
        super(message);
    }

    public TokenVerifierException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenVerifierException(Throwable cause) {
        super(cause);
    }

    public TokenVerifierException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
