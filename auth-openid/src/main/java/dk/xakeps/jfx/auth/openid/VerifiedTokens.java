package dk.xakeps.jfx.auth.openid;

import io.fusionauth.jwt.domain.JWT;

public record VerifiedTokens(JWT accessToken, JWT idToken,
                             String accessTokenString, String idTokenString,
                             String refreshTokenString) {
}
