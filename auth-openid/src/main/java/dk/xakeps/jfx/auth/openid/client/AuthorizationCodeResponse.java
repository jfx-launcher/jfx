package dk.xakeps.jfx.auth.openid.client;

import com.sun.net.httpserver.HttpExchange;

import java.util.*;

public class AuthorizationCodeResponse {
    private final String code;
    private final String error;
    private final String errorDescription;
    private final String state;

    public AuthorizationCodeResponse(Map<String, List<String>> params) {
        this.code = firstOrNull("code", params);
        this.error = firstOrNull("error", params);
        this.errorDescription = firstOrNull("error_description", params);
        this.state = firstOrNull("state", params);
    }

    public Optional<String> getCode() {
        return Optional.ofNullable(code);
    }

    public Optional<String> getError() {
        return Optional.ofNullable(error);
    }

    public Optional<String> getErrorDescription() {
        return Optional.ofNullable(errorDescription);
    }

    public Optional<String> getState() {
        return Optional.ofNullable(state);
    }

    public static AuthorizationCodeResponse read(HttpExchange exchange) {
        Map<String, List<String>> map = splitQueries(exchange.getRequestURI().getQuery());
        return new AuthorizationCodeResponse(map);
    }

    private static String firstOrNull(String key, Map<String, List<String>> params) {
        List<String> list = params.getOrDefault(key, Collections.emptyList());
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    private static Map<String, List<String>> splitQueries(String query) {
        Map<String, List<String>> params = new HashMap<>();
        String[] split = query.split("&");
        for (String qParams : split) {
            String[] splitParams = qParams.split("=");
            List<String> strings = params.computeIfAbsent(splitParams[0], k -> new ArrayList<>(1));
            strings.add(splitParams[1]);
        }
        return params;
    }
}
