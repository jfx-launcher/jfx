package dk.xakeps.jfx.auth.openid.client;

import java.util.function.Consumer;

public interface BrowseCallback extends Consumer<String> {
    default void browse(String url) {
        accept(url);
    }
}
