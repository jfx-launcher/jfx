package dk.xakeps.jfx.auth.openid.client;

import com.sun.net.httpserver.HttpServer;
import dk.xakeps.jfx.auth.openid.QueryBuilder;
import dk.xakeps.jfx.auth.openid.client.config.AuthParams;
import dk.xakeps.jfx.auth.openid.client.config.Config;
import dk.xakeps.jfx.auth.openid.client.config.OpenIdInfo;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;

public class HttpAuthorizationCodeListener {
    private static final String LOCALHOST = "localhost";
    private final Config config;
    private final AuthParams authParams;
    private final HttpServer httpServer;
    private final HttpHandlerImpl httpHandler;

    public HttpAuthorizationCodeListener(Config config) throws IOException {
        this.config = config;
        this.httpServer = HttpServer.create(new InetSocketAddress(LOCALHOST, 2048), 0); // todo: random port? try to create it many times?
        this.httpHandler = new HttpHandlerImpl(config.getRedirectInfo());
        this.httpServer.createContext("/", httpHandler);

        InetSocketAddress address = httpServer.getAddress();
        String redirectUri = String.format("http://%s:%s", LOCALHOST, address.getPort());
        this.authParams = AuthParams.newParams(redirectUri);
    }

    public CompletableFuture<WebResponse> start(BrowseCallback browseCallback)
            throws IOException, URISyntaxException {
        String authUrl = genAuthUrl(config, authParams);
        httpServer.start();
        browseCallback.browse(authUrl);
        return httpHandler.getResponseFuture()
                .whenComplete((responseParams, throwable) -> httpServer.stop(0))
                .thenApply(responseParams -> new WebResponse(responseParams, authParams));
    }

    private String genAuthUrl(Config config, AuthParams authParams) {
        OpenIdInfo openIdInfo = config.getOpenIdInfo();
        QueryBuilder queryBuilder = QueryBuilder.get(openIdInfo.getOpenIdSettings().getAuthorizationEndpoint())
                .addQuery("response_type", "code")
                .addQuery("client_id", openIdInfo.getClientId())
                .addQuery("redirect_uri", authParams.getRedirectUri())
                .addQuery("scope", "openid")
                .addQuery("state", authParams.getState());
        if (authParams.getPkce() != null) {
            queryBuilder.addQuery("code_challenge", authParams.getPkce().getCodeChallenge())
                    .addQuery("code_challenge_method", "S256");
        }
        if (config.getLocale() != null) {
            queryBuilder.addQuery("ui_locales", config.getLocale().getLanguage());
        }

        return queryBuilder.toUriString();
    }

}
