package dk.xakeps.jfx.auth.openid.client;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import dk.xakeps.jfx.auth.openid.client.config.RedirectInfo;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class HttpHandlerImpl implements HttpHandler {
    private static final int CODE_METHOD_NOT_ALLOWED = 405; // http return code for Method Not Allowed
    private static final int CODE_FOUND = 302; // http return code for Found
    private static final String HEADER_LOCATION = "Location";

    private final RedirectInfo redirectInfo;
    private final CompletableFuture<AuthorizationCodeResponse> responseFuture;

    public HttpHandlerImpl(RedirectInfo redirectInfo) {
        this.redirectInfo = redirectInfo;
        this.responseFuture = new CompletableFuture<>();
    }

    @Override
    public void handle(HttpExchange exchange) {
        try (exchange) {
            handleImpl(exchange);
        } catch (Throwable t) {
            responseFuture.completeExceptionally(t);
        }
    }

    private void handleImpl(HttpExchange exchange) throws IOException {
        if (!"GET".equalsIgnoreCase(exchange.getRequestMethod())) {
            exchange.sendResponseHeaders(CODE_METHOD_NOT_ALLOWED, -1);
            return;
        }

        AuthorizationCodeResponse authorizationCodeResponse = AuthorizationCodeResponse.read(exchange);
        if (authorizationCodeResponse.getError().isEmpty()) {
            exchange.getResponseHeaders().add(HEADER_LOCATION, redirectInfo.getSuccessRedirectUri());
        } else {
            exchange.getResponseHeaders().add(HEADER_LOCATION, redirectInfo.getFailedRedirectUri());
        }
        exchange.sendResponseHeaders(CODE_FOUND, -1);
        responseFuture.complete(authorizationCodeResponse);
    }

    public CompletableFuture<AuthorizationCodeResponse> getResponseFuture() {
        return responseFuture;
    }
}
