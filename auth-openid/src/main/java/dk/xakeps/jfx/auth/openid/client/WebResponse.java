package dk.xakeps.jfx.auth.openid.client;

import dk.xakeps.jfx.auth.openid.client.config.AuthParams;

public class WebResponse { // todo: rename?
    private final AuthorizationCodeResponse authorizationCodeResponse;
    private final AuthParams authParams;

    public WebResponse(AuthorizationCodeResponse authorizationCodeResponse, AuthParams authParams) {
        this.authorizationCodeResponse = authorizationCodeResponse;
        this.authParams = authParams;
    }

    public AuthorizationCodeResponse getAuthorizationCodeResponse() {
        return authorizationCodeResponse;
    }

    public AuthParams getAuthParams() {
        return authParams;
    }
}
