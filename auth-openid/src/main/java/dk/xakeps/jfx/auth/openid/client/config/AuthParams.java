package dk.xakeps.jfx.auth.openid.client.config;

import java.util.UUID;

public class AuthParams {
    private final String state;
    private final Pkce pkce;
    private final String redirectUri;

    public AuthParams(Pkce pkce, String redirectUri) {
        this.redirectUri = redirectUri;
        this.state = UUID.randomUUID().toString();
        this.pkce = pkce;
    }

    public String getState() {
        return state;
    }

    public Pkce getPkce() {
        return pkce;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public static AuthParams newParams(String redirectUri) {
        return new AuthParams(Pkce.generate(), redirectUri);
    }
}