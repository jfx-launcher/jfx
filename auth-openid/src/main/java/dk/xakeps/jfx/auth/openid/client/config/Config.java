package dk.xakeps.jfx.auth.openid.client.config;

import java.util.Locale;

public class Config {
    private final OpenIdInfo openIdInfo;
    private final RedirectInfo redirectInfo;
    private final Locale locale;

    public Config(OpenIdInfo openIdInfo, RedirectInfo redirectInfo, Locale locale) {
        this.openIdInfo = openIdInfo;
        this.redirectInfo = redirectInfo;
        this.locale = locale;
    }

    public OpenIdInfo getOpenIdInfo() {
        return openIdInfo;
    }

    public RedirectInfo getRedirectInfo() {
        return redirectInfo;
    }

    public Locale getLocale() {
        return locale;
    }
}
