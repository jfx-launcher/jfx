package dk.xakeps.jfx.auth.openid.client.config;

import dk.xakeps.jfx.auth.openid.client.model.OpenIdSettings;

public class OpenIdInfo {
    private final String issuer;
    private final String clientId;
    private final OpenIdSettings openIdSettings;

    public OpenIdInfo(String issuer, String clientId, OpenIdSettings openIdSettings) {
        this.issuer = issuer;
        this.clientId = clientId;
        this.openIdSettings = openIdSettings;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getClientId() {
        return clientId;
    }

    public OpenIdSettings getOpenIdSettings() {
        return openIdSettings;
    }
}
