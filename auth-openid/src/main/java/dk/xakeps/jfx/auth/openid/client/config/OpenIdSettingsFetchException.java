package dk.xakeps.jfx.auth.openid.client.config;

import java.io.IOException;

public class OpenIdSettingsFetchException extends IOException {
    public OpenIdSettingsFetchException() {
    }

    public OpenIdSettingsFetchException(String message) {
        super(message);
    }

    public OpenIdSettingsFetchException(String message, Throwable cause) {
        super(message, cause);
    }

    public OpenIdSettingsFetchException(Throwable cause) {
        super(cause);
    }
}
