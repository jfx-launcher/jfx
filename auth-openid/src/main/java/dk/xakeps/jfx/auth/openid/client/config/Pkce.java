package dk.xakeps.jfx.auth.openid.client.config;

import dk.xakeps.jfx.auth.openid.PkceException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Pkce {
    private static final int MAX_SIZE = 128;

    private final String codeChallenge;
    private final String codeVerifier;

    public Pkce(String codeChallenge, String codeVerifier) {
        this.codeChallenge = codeChallenge;
        this.codeVerifier = codeVerifier;
    }

    public String getCodeChallenge() {
        return codeChallenge;
    }

    public String getCodeVerifier() {
        return codeVerifier;
    }

    public static Pkce generate() {
        String codeVerifier = PkceStringGenerator.generate(MAX_SIZE);
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new PkceException(e);
        }
        md.update(codeVerifier.getBytes(StandardCharsets.ISO_8859_1));
        String codeChallenge = Base64.getUrlEncoder().withoutPadding().encodeToString(md.digest());
        return new Pkce(codeChallenge, codeVerifier);
    }
}