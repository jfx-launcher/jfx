package dk.xakeps.jfx.auth.openid.client.config;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

public class PkceStringGenerator {
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LOWER = UPPER.toLowerCase(Locale.ROOT);
    private static final String DIGITS = "0123456789";
    private static final String AUX = "-._~";

    private static final String CHARS_STR = UPPER + LOWER + DIGITS + AUX;
    private static final char[] CHARS = CHARS_STR.toCharArray();

    public static String generate(int length, Random random) {
        char[] buf = new char[length];
        for (int i = 0; i < length; i++) {
            buf[i] = CHARS[random.nextInt(CHARS.length)];
        }
        return new String(buf);
    }

    public static String generate(int length) {
        return generate(length, new SecureRandom());
    }
}