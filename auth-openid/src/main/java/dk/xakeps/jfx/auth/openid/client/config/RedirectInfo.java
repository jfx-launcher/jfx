package dk.xakeps.jfx.auth.openid.client.config;

public class RedirectInfo {
    private final String successRedirectUri;
    private final String failedRedirectUri;

    public RedirectInfo(String successRedirectUri, String failedRedirectUri) {
        this.successRedirectUri = successRedirectUri;
        this.failedRedirectUri = failedRedirectUri;
    }

    public String getSuccessRedirectUri() {
        return successRedirectUri;
    }

    public String getFailedRedirectUri() {
        return failedRedirectUri;
    }
}
