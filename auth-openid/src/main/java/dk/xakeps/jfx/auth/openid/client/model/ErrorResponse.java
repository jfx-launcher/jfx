package dk.xakeps.jfx.auth.openid.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public record ErrorResponse(@JsonProperty("error") String error,
                            @JsonProperty("error_description") String errorDescription) {
    @JsonCreator
    public ErrorResponse {
        error = error == null ? "" : error;
        errorDescription = errorDescription == null ? "" : errorDescription;
    }
}
