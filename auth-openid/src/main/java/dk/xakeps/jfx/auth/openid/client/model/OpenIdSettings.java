package dk.xakeps.jfx.auth.openid.client.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.auth.openid.client.config.OpenIdSettingsFetchException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenIdSettings {
    @JsonProperty("authorization_endpoint")
    private final String authorizationEndpoint;
    @JsonProperty("token_endpoint")
    private final String tokenEndpoint;

    @JsonCreator
    public OpenIdSettings(@JsonProperty("authorization_endpoint") String authorizationEndpoint,
                          @JsonProperty("token_endpoint") String tokenEndpoint) {
        this.authorizationEndpoint = authorizationEndpoint;
        this.tokenEndpoint = tokenEndpoint;
    }

    public String getAuthorizationEndpoint() {
        return authorizationEndpoint;
    }

    public String getTokenEndpoint() {
        return tokenEndpoint;
    }

    public static OpenIdSettings fetchFrom(String issuer) throws OpenIdSettingsFetchException {
        try {
            if (!issuer.endsWith("/")) {
                issuer = issuer + "/";
            }
            Methanol httpClient = Methanol.create();
            HttpRequest request = HttpRequest.newBuilder(URI.create(issuer + ".well-known/openid-configuration"))
                    .GET().build();
            return httpClient.send(request, MoreBodyHandlers.ofObject(OpenIdSettings.class)).body();
        } catch (IOException | InterruptedException e) {
            throw new OpenIdSettingsFetchException(e);
        }
    }
}
