package dk.xakeps.jfx.auth.openid.http;

import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.auth.openid.client.model.ErrorResponse;

import java.net.http.HttpResponse;

public class ErrorHandler<T> implements HttpResponse.BodyHandler<Response<T>> {
    private final HttpResponse.BodyHandler<T> responseHandler;

    private ErrorHandler(HttpResponse.BodyHandler<T> responseHandler) {
        this.responseHandler = responseHandler;
    }

    @Override
    public HttpResponse.BodySubscriber<Response<T>> apply(HttpResponse.ResponseInfo responseInfo) {
        if (responseInfo.statusCode() == 200) {
            HttpResponse.BodySubscriber<T> subscriber = responseHandler.apply(responseInfo);
            return HttpResponse.BodySubscribers.mapping(subscriber, response -> new Response<>(response, null));
        } else {
            HttpResponse.BodySubscriber<ErrorResponse> subscriber = MoreBodyHandlers.ofObject(ErrorResponse.class)
                    .apply(responseInfo);
            return HttpResponse.BodySubscribers.mapping(subscriber,
                    errorResponse -> new Response<>(null, errorResponse));
        }
    }

    public static <T> HttpResponse.BodyHandler<Response<T>> withBodyHandler(HttpResponse.BodyHandler<T> responseHandler) {
        return new ErrorHandler<>(responseHandler);
    }
}
