package dk.xakeps.jfx.auth.openid.http;

import dk.xakeps.jfx.auth.openid.client.model.ErrorResponse;

public record Response<T>(T response, ErrorResponse errorResponse) {
}
