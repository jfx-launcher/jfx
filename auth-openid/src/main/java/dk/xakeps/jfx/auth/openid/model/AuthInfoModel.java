package dk.xakeps.jfx.auth.openid.model;

import dk.xakeps.jfx.auth.AuthInfo;

import java.util.Objects;
import java.util.Set;

public record AuthInfoModel(String userId, String accessToken, Set<String> groups) implements AuthInfo {
    public AuthInfoModel {
        Objects.requireNonNull(userId, "userId");
        Objects.requireNonNull(accessToken, "accessToken");
        Objects.requireNonNull(groups, "groups");
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getAccessToken() {
        return accessToken;
    }

    @Override
    public Set<String> getGroups() {
        return null;
    }
}
