package dk.xakeps.jfx.auth.openid.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginData {
    @JsonProperty("accessToken")
    private final String accessToken;
    @JsonProperty("refreshToken")
    private final String refreshToken;

    @JsonCreator
    public LoginData(@JsonProperty("accessToken") String accessToken,
                     @JsonProperty("refreshToken") String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
