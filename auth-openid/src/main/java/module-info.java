import com.github.mizosoft.methanol.BodyAdapter;
import dk.xakeps.jfx.auth.AuthModuleProvider;
import dk.xakeps.jfx.auth.openid.JacksonAdapters;
import dk.xakeps.jfx.auth.openid.OpenIDAuthModuleProvider;

module dk.xakeps.jfx.auth.openid {
    requires com.fasterxml.jackson.databind;
    requires methanol;
    requires methanol.adapter.jackson;

    requires dk.xakeps.jfx.auth;
    requires dk.xakeps.jfx.settings;
    requires dk.xakeps.jfx.gui.common;

    requires javafx.web;

    requires io.fusionauth;

    requires jdk.httpserver;

    opens dk.xakeps.jfx.auth.openid.model to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.auth.openid.client.model to com.fasterxml.jackson.databind;

    provides AuthModuleProvider with OpenIDAuthModuleProvider;

    provides BodyAdapter.Encoder with JacksonAdapters.JacksonEncoder;
    provides BodyAdapter.Decoder with JacksonAdapters.JacksonDecoder;
}