package dk.xakeps.jfx.auth.openid;

import io.fusionauth.jwt.domain.JWT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class OpenIdAuthServiceImplTest {

    @Test
    public void singleClaimGroupList() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRoles));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles", " ", jwt);
        Assertions.assertEquals(realmRoles, groups);
    }

    @Test
    public void multipleClaimGroupList() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        Set<String> resourceRoles = Set.of("resource_role_a", "resource_role_b");
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRoles));
        otherClaims.put("resource_access", Map.of("client_id", Map.of("roles", resourceRoles)));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles;resource_access/client_id/roles", " ", jwt);

        Set<String> allRoles = new HashSet<>();
        allRoles.addAll(realmRoles);
        allRoles.addAll(resourceRoles);
        Assertions.assertEquals(allRoles, groups);
    }

    @Test
    public void singleClaimGroupString() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        String realmRolesString = String.join(" ", realmRoles);
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRolesString));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles", " ", jwt);
        Assertions.assertEquals(realmRoles, groups);
    }

    @Test
    public void multipleClaimGroupString() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        String realmRolesString = String.join(" ", realmRoles);
        Set<String> resourceRoles = Set.of("resource_role_a", "resource_role_b");
        String resourceRolesString = String.join(" ", resourceRoles);
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRolesString));
        otherClaims.put("resource_access", Map.of("client_id", Map.of("roles", resourceRolesString)));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles;resource_access/client_id/roles", " ", jwt);

        Set<String> allRoles = new HashSet<>();
        allRoles.addAll(realmRoles);
        allRoles.addAll(resourceRoles);
        Assertions.assertEquals(allRoles, groups);
    }

    @Test
    public void multipleClaimGroupListString() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        Set<String> resourceRoles = Set.of("resource_role_a", "resource_role_b");
        String resourceRolesString = String.join(" ", resourceRoles);
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRoles));
        otherClaims.put("resource_access", Map.of("client_id", Map.of("roles", resourceRolesString)));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles;resource_access/client_id/roles", " ", jwt);

        Set<String> allRoles = new HashSet<>();
        allRoles.addAll(realmRoles);
        allRoles.addAll(resourceRoles);
        Assertions.assertEquals(allRoles, groups);
    }

    @Test
    public void singleClaimGroupStringSlashSeparator() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        String realmRolesString = String.join("/", realmRoles);
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRolesString));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles", "/", jwt);
        Assertions.assertEquals(realmRoles, groups);
    }

    @Test
    public void singleClaimGroupStringSemicolonSeparator() {
        JWT jwt = new JWT();
        Set<String> realmRoles = Set.of("realm_role_a", "realm_role_b");
        String realmRolesString = String.join(";", realmRoles);
        Map<String, Object> otherClaims = jwt.getOtherClaims();
        otherClaims.put("realm_access", Map.of("roles", realmRolesString));
        Set<String> groups = OpenIdAuthServiceImpl.getGroups("realm_access/roles", ";", jwt);
        Assertions.assertEquals(realmRoles, groups);
    }
}