package dk.xakeps.jfx.auth.openid;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueryBuilderTest {

    @Test
    public void postEmpty() {
        String queryString = QueryBuilder.post().toQueryString();
        assertTrue(queryString.isEmpty(), "Query string must be empty");

        String uriString = QueryBuilder.post().toUriString();
        assertTrue(uriString.isEmpty(), "Uri string must be empty");
    }

    @Test
    public void getEmpty() {
        String uri = "https://localhost";
        QueryBuilder builder = QueryBuilder.get(uri);
        String queryString = builder.toQueryString();
        assertTrue(queryString.isEmpty(), "Query string must be empty");

        String uriString = builder.toUriString();
        assertEquals(uri, uriString, "Uri string must be equal to uri");
    }

    @Test
    public void testEncodeKeyAndValue() {
        String postQuery = QueryBuilder.post()
                .addQuery("=", "=")
                .toQueryString();
        assertEquals("%3D=%3D", postQuery, "Query key and value must be encoded!");

        String uri = "https://test";
        String getUri = QueryBuilder.get(uri)
                .addQuery("=", "=")
                .toUriString();
        assertEquals(uri + "?%3D=%3D", getUri, "Uri key and value must be encoded!");
    }
}
