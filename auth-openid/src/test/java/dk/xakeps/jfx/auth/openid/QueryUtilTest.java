package dk.xakeps.jfx.auth.openid;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class QueryUtilTest {

    @Test
    public void removeParam() {
        String query = QueryBuilder.post()
                .addQuery("param_test", "param_test1")
                .addQuery("param_test", "param_test2")
                .addQuery("param_test1", "param_test3")
                .toQueryString();


        String filteredQuery = QueryUtil.removeParam(query, "param_test");

        String resultQuery = QueryBuilder.post()
                .addQuery("param_test1", "param_test3")
                .toQueryString();

        assertEquals(resultQuery, filteredQuery, "Params weren't removed");
    }

    @Test
    public void removeParamEncoded() {
        String query = QueryBuilder.post()
                .addQuery("=", "=")
                .addQuery("test_key", "test_val")
                .toQueryString();

        String filteredQuery = QueryUtil.removeParam(query, "=");

        String resultQuery = QueryBuilder.post()
                .addQuery("test_key", "test_val")
                .toQueryString();

        assertEquals(resultQuery, filteredQuery, "Params weren't removed");
    }
}