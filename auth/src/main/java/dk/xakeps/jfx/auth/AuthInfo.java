package dk.xakeps.jfx.auth;

import java.util.Set;

public interface AuthInfo {
    String getUserId();
    String getAccessToken();
    Set<String> getGroups();
}
