/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.auth;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

public interface AuthModuleProvider {
    String getId();

    default AuthModule createModule() {
        return createModule(Collections.emptyMap());
    }

    AuthModule createModule(Map<String, String> settings);

    static Optional<AuthModuleProvider> findAny() {
        return ServiceLoader.load(AuthModuleProvider.class).findFirst();
    }

    static Optional<AuthModuleProvider> find(String id) {
        return ServiceLoader.load(AuthModuleProvider.class).stream()
                .map(ServiceLoader.Provider::get)
                .filter(provider -> provider.getId().equals(id))
                .findAny();
    }
}
