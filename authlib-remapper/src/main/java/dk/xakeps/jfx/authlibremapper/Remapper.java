package dk.xakeps.jfx.authlibremapper;

import dk.xakeps.jfx.authlibremapper.impl.JarRemapper;
import dk.xakeps.jfx.authlibremapper.impl.V1JarRemapper;
import dk.xakeps.jfx.authlibremapper.impl.V2JarRemapper;

import java.io.IOException;
import java.nio.file.*;

public class Remapper {
    private final RemapperConfig remapperConfig;

    public Remapper(RemapperConfig remapperConfig) {
        this.remapperConfig = remapperConfig;
    }

    public Path remap(Path inputFile) throws IOException {
        String fileName = inputFile.getFileName().toString();
        Path remappedFile = remapperConfig.getOutputPath().resolve(fileName + "-remapped.jar");
        Files.copy(inputFile, remappedFile, StandardCopyOption.REPLACE_EXISTING);
        JarRemapper jarRemapper;
        try(FileSystem fileSystem = FileSystems.newFileSystem(inputFile)) {
            Path path = fileSystem.getPath("/", "com", "mojang", "authlib", "Environment.class");
            if (!Files.exists(path)) {
                jarRemapper = new V1JarRemapper(remapperConfig);
            } else {
                jarRemapper = new V2JarRemapper(remapperConfig);
            }
        }
        try (FileSystem fileSystem = FileSystems.newFileSystem(remappedFile)) {
            Files.write(fileSystem.getPath("/", "yggdrasil_session_pubkey.der"), remapperConfig.getPubKey());
        }
        jarRemapper.remap(inputFile, remappedFile);
        return remappedFile;
    }
}
