package dk.xakeps.jfx.authlibremapper;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class RemapperConfig {
    private final String joinUrl;
    private final String hasJoinedUrl;
    private final String profilesUrl;
    private final List<String> allowedDomains;
    private final List<String> blockedDomains;
    private final String profilesRepoUrl;
    private final String privilegesUrl;
    private final String blockListUrl;
    private final String legacyJoinUrl;
    private final String legacyHasJoinedUrl;
    private final byte[] pubKey;
    private final Path outputPath;

    private RemapperConfig(Builder builder) {
        this.joinUrl = Objects.requireNonNull(builder.joinUrl, "joinUrl");
        this.hasJoinedUrl = Objects.requireNonNull(builder.hasJoinedUrl, "hasJoinedUrl");
        this.profilesUrl = Objects.requireNonNull(builder.profilesUrl, "profilesUrl");
        this.allowedDomains = List.copyOf(Objects.requireNonNull(builder.allowedDomains, "allowedDomains"));
        this.blockedDomains = List.copyOf(Objects.requireNonNull(builder.blockedDomains, "blockedDomains"));
        this.profilesRepoUrl = Objects.requireNonNull(builder.profilesRepoUrl, "profilesRepoUrl");
        this.privilegesUrl = Objects.requireNonNull(builder.privilegesUrl, "privilegesUrl");
        this.blockListUrl = Objects.requireNonNull(builder.blockListUrl, "blockListUrl");
        this.legacyJoinUrl = Objects.requireNonNull(builder.legacyJoinUrl, "legacyJoinUrl");
        this.legacyHasJoinedUrl = Objects.requireNonNull(builder.legacyHasJoinedUrl, "legacyHasJoinedUrl");
        this.pubKey = Arrays.copyOf(Objects.requireNonNull(builder.pubKey, "pubKey"), builder.pubKey.length);
        this.outputPath = Objects.requireNonNull(builder.outputPath, "outputPath");
    }

    public String getJoinUrl() {
        return joinUrl;
    }

    public String getHasJoinedUrl() {
        return hasJoinedUrl;
    }

    public String getProfilesUrl() {
        return profilesUrl;
    }

    public List<String> getAllowedDomains() {
        return allowedDomains;
    }

    public List<String> getBlockedDomains() {
        return blockedDomains;
    }

    public String getProfilesRepoUrl() {
        return profilesRepoUrl;
    }

    public String getPrivilegesUrl() {
        return privilegesUrl;
    }

    public String getBlockListUrl() {
        return blockListUrl;
    }

    public String getLegacyJoinUrl() {
        return legacyJoinUrl;
    }

    public String getLegacyHasJoinedUrl() {
        return legacyHasJoinedUrl;
    }

    public byte[] getPubKey() {
        return Arrays.copyOf(pubKey, pubKey.length);
    }

    public Path getOutputPath() {
        return outputPath;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private String joinUrl;
        private String hasJoinedUrl;
        private String profilesUrl;
        private List<String> allowedDomains;
        private List<String> blockedDomains;
        private String profilesRepoUrl;
        private String privilegesUrl;
        private String blockListUrl;
        private String legacyJoinUrl;
        private String legacyHasJoinedUrl;
        private byte[] pubKey;
        private Path outputPath;

        private Builder() {
        }

        public Builder joinUrl(String joinUrl) {
            this.joinUrl = Objects.requireNonNull(joinUrl, "joinUrl");
            return this;
        }

        public Builder hasJoinedUrl(String hasJoinedUrl) {
            this.hasJoinedUrl = Objects.requireNonNull(hasJoinedUrl, "hasJoinedUrl");
            return this;
        }

        public Builder profilesUrl(String profilesUrl) {
            this.profilesUrl = Objects.requireNonNull(profilesUrl, "profilesUrl");
            return this;
        }

        public Builder allowedDomains(List<String> allowedDomains) {
            this.allowedDomains = Objects.requireNonNull(allowedDomains, "allowedDomains");
            return this;
        }

        public Builder blockedDomains(List<String> blockedDomains) {
            this.blockedDomains = Objects.requireNonNull(blockedDomains, "blockedDomains");
            return this;
        }

        public Builder profilesRepoUrl(String profilesRepoUrl) {
            this.profilesRepoUrl = Objects.requireNonNull(profilesRepoUrl, "profilesRepoUrl");
            return this;
        }

        public Builder privilegesUrl(String privilegesUrl) {
            this.privilegesUrl = Objects.requireNonNull(privilegesUrl, "privilegesUrl");
            return this;
        }

        public Builder blockListUrl(String blockListUrl) {
            this.blockListUrl = Objects.requireNonNull(blockListUrl, "blockListUrl");
            return this;
        }

        public Builder legacyJoinUrl(String legacyJoinUrl) {
            this.legacyJoinUrl = Objects.requireNonNull(legacyJoinUrl, "legacyJoinUrl");
            return this;
        }

        public Builder legacyHasJoinedUrl(String legacyHasJoinedUrl) {
            this.legacyHasJoinedUrl = Objects.requireNonNull(legacyHasJoinedUrl, "legacyHasJoinedUrl");
            return this;
        }

        public Builder pubKey(byte[] pubKey) {
            this.pubKey = Objects.requireNonNull(pubKey, "pubKey");
            return this;
        }

        public Builder outputPath(Path outputPath) {
            this.outputPath = Objects.requireNonNull(outputPath, "outputPath");
            return this;
        }

        public RemapperConfig build() {
            return new RemapperConfig(this);
        }
    }
}
