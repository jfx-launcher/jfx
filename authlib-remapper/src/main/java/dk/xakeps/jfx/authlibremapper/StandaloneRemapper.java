package dk.xakeps.jfx.authlibremapper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Properties;

public class StandaloneRemapper {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.println("First argument must be input jar name or path");
            return;
        }
        String jarPathStr = args[0];
        Path jarPath = Path.of(jarPathStr);
        if (Files.notExists(jarPath)) {
            System.out.println("Input jar not found in path: " + jarPath.toAbsolutePath());
            return;
        }

        Path props = Path.of("config.properties");
        if (Files.notExists(props)) {
            Files.createFile(props);
        }

        Properties properties = new Properties();
        try (BufferedReader reader = Files.newBufferedReader(props, StandardCharsets.UTF_8)) {
            properties.load(reader);
        }

        String pubKeyPathStr = getOrPut(properties, "pubKeyPath", "pubKey.der");
        Path pubKeyPath = Path.of(pubKeyPathStr);
        byte[] pubKey;
        if (Files.notExists(pubKeyPath)) {
            pubKey = new byte[0];
        } else {
            pubKey = Files.readAllBytes(pubKeyPath);
        }

        RemapperConfig config = RemapperConfig.newBuilder()
                .joinUrl(getOrPut(properties, "joinUrl", "http://localhost:8180/session-service/join"))
                .hasJoinedUrl(getOrPut(properties, "hasJoinedUrl", "http://localhost:8180/session-service/hasJoined"))
                .profilesUrl(getOrPut(properties, "profilesUrl", "http://localhost:8180/session-service/profile/"))
                .allowedDomains(Arrays.asList(getOrPut(properties, "privilegesUrl", "localhost").split(" ")))
                .blockedDomains(Arrays.asList(getOrPut(properties, "privilegesUrl", "").split(" ")))
                .profilesRepoUrl(getOrPut(properties, "profilesRepoUrl", "http://localhost:8180/profile-service/profiles"))
                .privilegesUrl(getOrPut(properties, "privilegesUrl", "http://localhost:8180/"))
                .blockListUrl(getOrPut(properties, "blockListUrl", "http://localhost:8180/"))
                .legacyJoinUrl(getOrPut(properties, "legacyJoinUrl", "http://localhost:8180/"))
                .legacyHasJoinedUrl(getOrPut(properties, "legacyHasJoinedUrl", "http://localhost:8180/"))
                .pubKey(pubKey)
                .outputPath(Path.of(""))
                .build();

        try (BufferedWriter writer = Files.newBufferedWriter(props, StandardCharsets.UTF_8)) {
            properties.store(writer, "Standalone configuration");
        }

        Remapper remapper = new Remapper(config);
        remapper.remap(jarPath);
        System.out.println("Remapped");
    }

    private static String getOrPut(Properties properties, String key, String def) {
        String property = properties.getProperty(key);
        if (property != null) {
            return property;
        }
        properties.setProperty(key, def);
        return def;
    }
}
