package dk.xakeps.jfx.authlibremapper.impl;

public interface ClassRemapper {
    byte[] remap(byte[] input);
}
