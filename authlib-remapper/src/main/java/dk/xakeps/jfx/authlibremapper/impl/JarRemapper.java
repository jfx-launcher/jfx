package dk.xakeps.jfx.authlibremapper.impl;

import java.io.IOException;
import java.nio.file.Path;

public interface JarRemapper {
    void remap(Path inFile, Path outFile) throws IOException;
}
