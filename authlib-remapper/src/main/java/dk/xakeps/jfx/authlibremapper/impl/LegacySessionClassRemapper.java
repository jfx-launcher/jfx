package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

public class LegacySessionClassRemapper implements ClassRemapper {
    private final String legacyJoinUrl;
    private final String legacyHasJoinedUrl;

    public LegacySessionClassRemapper(RemapperConfig remapperConfig) {
        this.legacyJoinUrl = remapperConfig.getLegacyJoinUrl();
        this.legacyHasJoinedUrl = remapperConfig.getLegacyHasJoinedUrl();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("<clinit>".equals(name)) {
                return new LegacyUrlsRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class LegacyUrlsRemapper extends MethodVisitor {
        public LegacyUrlsRemapper(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("http://session.minecraft.net/game/joinserver.jsp".equals(value)) {
                super.visitLdcInsn(legacyJoinUrl);
                return;
            }
            if ("http://session.minecraft.net/game/checkserver.jsp".equals(value)) {
                super.visitLdcInsn(legacyHasJoinedUrl);
                return;
            }
            super.visitLdcInsn(value);
        }
    }
}
