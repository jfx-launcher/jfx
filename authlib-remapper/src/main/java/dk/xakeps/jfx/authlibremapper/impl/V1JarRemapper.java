package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class V1JarRemapper implements JarRemapper {
    private final ClassRemapper sessionClassRemapper;
    private final ClassRemapper repoClassRemapper;
    private final ClassRemapper legacySessionClassRemapper;

    public V1JarRemapper(RemapperConfig remapperConfig) {
        this.sessionClassRemapper = new V1SessionClassRemapper(remapperConfig);
        this.repoClassRemapper = new V1ProfileRepoClassRemapper(remapperConfig);
        this.legacySessionClassRemapper = new LegacySessionClassRemapper(remapperConfig);
    }

    @Override
    public void remap(Path inFile, Path outFile) throws IOException {
        try(FileSystem fileSystem = FileSystems.newFileSystem(inFile)) {
            Path sessionClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilMinecraftSessionService.class");
            Path repositoryClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilGameProfileRepository.class");
            Path legacySessionClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "legacy", "LegacyMinecraftSessionService.class");

            byte[] sessionClass = Files.readAllBytes(sessionClassPath);
            byte[] remappedSessionClass = sessionClassRemapper.remap(sessionClass);

            byte[] repositoryClass = Files.readAllBytes(repositoryClassPath);
            byte[] remappedRepositoryClass = repoClassRemapper.remap(repositoryClass);

            byte[] legacySessionClass = Files.readAllBytes(legacySessionClassPath);
            byte[] remappedLegacySessionClass = legacySessionClassRemapper.remap(legacySessionClass);

            try(FileSystem remappedFs = FileSystems.newFileSystem(outFile)) {
                Path remappedSessionClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilMinecraftSessionService.class");
                Files.write(remappedSessionClassPath, remappedSessionClass);

                Path remappedRepositoryClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilGameProfileRepository.class");
                Files.write(remappedRepositoryClassPath, remappedRepositoryClass);

                Path remappedLegacySessionClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "legacy", "LegacyMinecraftSessionService.class");
                Files.write(remappedLegacySessionClassPath, remappedLegacySessionClass);
            }
        }
    }
}
