package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

public class V1ProfileRepoClassRemapper implements ClassRemapper {
    private final String profileRepoUrl;

    public V1ProfileRepoClassRemapper(RemapperConfig remapperConfig) {
        this.profileRepoUrl = remapperConfig.getProfilesRepoUrl();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("findProfilesByNames".equals(name)) {
                return new ProfilesRepoUrlRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class ProfilesRepoUrlRemapper extends MethodVisitor {
        public ProfilesRepoUrlRemapper(int api, MethodVisitor mv) {
            super(api, mv);
        }

        int numSkips;
        boolean ldcHappened;
        boolean beginRemap;
        boolean profilesRepoUrlRemapped;

        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            if ("com/mojang/authlib/Agent".equals(owner) && "getName".equals(name) && "()Ljava/lang/String;".equals(descriptor)) {
                beginRemap = true;
            }
            if (opcode == Opcodes.INVOKEVIRTUAL && ldcHappened && beginRemap && numSkips < 3) {
                if (numSkips == 0) {
                    super.visitInsn(Opcodes.POP);
                }
                numSkips++;
                return;
            }
            if (numSkips >= 3) {
                profilesRepoUrlRemapped = true;
            }
            super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("https://api.mojang.com/profiles/".equals(value)) {
                super.visitLdcInsn(profileRepoUrl);
                ldcHappened = true;
                return;
            }
            super.visitLdcInsn(value);
        }
    }
}
