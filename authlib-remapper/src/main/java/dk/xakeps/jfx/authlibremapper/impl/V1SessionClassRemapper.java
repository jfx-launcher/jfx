package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

import java.util.List;

public class V1SessionClassRemapper implements ClassRemapper {
    private final String joinUrl;
    private final String hasJoinedUrl;
    private final String profilesUrl;
    private final List<String> whitelistedDomains;

    public V1SessionClassRemapper(RemapperConfig remapperConfig) {
        this.joinUrl = remapperConfig.getJoinUrl();
        this.hasJoinedUrl = remapperConfig.getHasJoinedUrl();
        this.profilesUrl = remapperConfig.getProfilesUrl();
        this.whitelistedDomains = remapperConfig.getAllowedDomains();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("<clinit>".equals(name)) {
                return new FullRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            } else if ("fillGameProfile".equals(name)) {
                return new FullRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class FullRemapper extends MethodVisitor {
        public FullRemapper(int api, MethodVisitor mv) {
            super(api, mv);
        }

        boolean baseUrlRemapped;
        boolean joinUrlRemapped;
        boolean hasJoinedUrlRemapped;
        boolean profilesUrlRemapped;
        @Override
        public void visitLdcInsn(Object value) {
            if ("https://sessionserver.mojang.com/session/minecraft/".equals(value)) {
                super.visitLdcInsn("");
                baseUrlRemapped = true;
                return;
            } else if ("https://sessionserver.mojang.com/session/minecraft/join".equals(value)) {
                super.visitLdcInsn(joinUrl);
                joinUrlRemapped = true;
                return;
            } else if ("https://sessionserver.mojang.com/session/minecraft/hasJoined".equals(value)) {
                super.visitLdcInsn(hasJoinedUrl);
                hasJoinedUrlRemapped = true;
                return;
            } else if ("https://sessionserver.mojang.com/session/minecraft/profile/".equals(value)) {
                super.visitLdcInsn(profilesUrl);
                profilesUrlRemapped = true;
                return;
            }
            super.visitLdcInsn(value);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            if ("WHITELISTED_DOMAINS".equals(name)) {
                super.visitInsn(Opcodes.POP);
                writeStringArray(whitelistedDomains);
            }
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }

        private void writeStringArray(List<String> array) {
            super.visitLdcInsn(array.size());
            super.visitTypeInsn(Opcodes.ANEWARRAY, "java/lang/String");
            for (int i = 0; i < array.size(); i++) {
                storeString(i, array.get(i));
            }
        }

        private void storeString(int pos, String val) {
            super.visitInsn(Opcodes.DUP);
            super.visitLdcInsn(pos);
            super.visitLdcInsn(val);
            super.visitInsn(Opcodes.AASTORE);
        }
    }
}
