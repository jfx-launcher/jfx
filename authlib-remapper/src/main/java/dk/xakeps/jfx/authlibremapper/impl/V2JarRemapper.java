package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class V2JarRemapper implements JarRemapper {
    private final ClassRemapper sessionClassRemapper;
    private final ClassRemapper repoClassRemapper;
    private final ClassRemapper socialClassRemapper;
    private final ClassRemapper legacySessionClassRemapper;

    public V2JarRemapper(RemapperConfig remapperConfig) {
        this.sessionClassRemapper = new V2SessionClassRemapper(remapperConfig);
        this.repoClassRemapper = new V2ProfileRepoClassRemapper(remapperConfig);
        this.socialClassRemapper = new V2SocialIntegrationsRemapper(remapperConfig);
        this.legacySessionClassRemapper = new LegacySessionClassRemapper(remapperConfig);
    }

    @Override
    public void remap(Path inFile, Path outFile) throws IOException {
        try(FileSystem fileSystem = FileSystems.newFileSystem(inFile)) {
            Path sessionClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilMinecraftSessionService.class");
            Path repositoryClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilGameProfileRepository.class");
            Path socialClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilSocialInteractionsService.class");
            Path legacySessionClassPath = fileSystem.getPath("/", "com", "mojang", "authlib", "legacy", "LegacyMinecraftSessionService.class");

            byte[] sessionClass = Files.readAllBytes(sessionClassPath);
            byte[] remappedSessionClass = sessionClassRemapper.remap(sessionClass);

            byte[] repositoryClass = Files.readAllBytes(repositoryClassPath);
            byte[] remappedRepositoryClass = repoClassRemapper.remap(repositoryClass);

            byte[] socialClass = Files.readAllBytes(socialClassPath);
            byte[] remappedSocialClass = socialClassRemapper.remap(socialClass);

            byte[] legacySessionClass = Files.readAllBytes(legacySessionClassPath);
            byte[] remappedLegacySessionClass = legacySessionClassRemapper.remap(legacySessionClass);

            try(FileSystem remappedFs = FileSystems.newFileSystem(outFile)) {
                Path remappedSessionClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilMinecraftSessionService.class");
                Files.write(remappedSessionClassPath, remappedSessionClass);

                Path remappedRepositoryClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilGameProfileRepository.class");
                Files.write(remappedRepositoryClassPath, remappedRepositoryClass);

                Path remappedSocialClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "yggdrasil", "YggdrasilSocialInteractionsService.class");
                Files.write(remappedSocialClassPath, remappedSocialClass);

                Path remappedLegacySessionClassPath = remappedFs.getPath("/", "com", "mojang", "authlib", "legacy", "LegacyMinecraftSessionService.class");
                Files.write(remappedLegacySessionClassPath, remappedLegacySessionClass);
            }
        }
    }
}
