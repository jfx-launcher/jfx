package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

public class V2ProfileRepoClassRemapper implements ClassRemapper {
    private final String profileRepoUrl;

    public V2ProfileRepoClassRemapper(RemapperConfig remapperConfig) {
        this.profileRepoUrl = remapperConfig.getProfilesRepoUrl();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("<init>".equals(name)) {
                return new ProfilesUrlRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            } else if ("findProfilesByNames".equals(name)) {
                return new AgentNameRemover(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class ProfilesUrlRemapper extends MethodVisitor {
        public ProfilesUrlRemapper(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        int numSkips;
        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            if ("com/mojang/authlib/Environment".equals(owner) && "getAccountsHost".equals(name) && "()Ljava/lang/String;".equals(descriptor)) {
                super.visitInsn(Opcodes.POP);
                numSkips++;
                return;
            }
            if (numSkips == 1) {
                numSkips++;
                return;
            }
            super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("/profiles/".equals(value)) {
                super.visitLdcInsn(profileRepoUrl);
                return;
            }
            super.visitLdcInsn(value);
        }
    }

    private static class AgentNameRemover extends MethodVisitor {
        public AgentNameRemover(int api, MethodVisitor mv) {
            super(api, mv);
        }

        int numSkips;
        boolean beginRemap;
        boolean agentNameRemoved;
        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            if ("com/mojang/authlib/Agent".equals(owner) && "getName".equals(name) && "()Ljava/lang/String;".equals(descriptor)) {
                beginRemap = true;
            }
            if (opcode == Opcodes.INVOKEVIRTUAL && beginRemap && numSkips < 3) {
                if (numSkips == 0) {
                    super.visitInsn(Opcodes.POP);
                }
                numSkips++;
                return;
            }
            if (numSkips >= 3) {
                agentNameRemoved = true;
            }
            super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
        }
    }
}
