package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

import java.util.List;

public class V2SessionClassRemapper implements ClassRemapper {
    private final String joinUrl;
    private final String hasJoinedUrl;
    private final String profilesUrl;
    private final List<String> allowedDomains;
    private final List<String> blockedDomains;

    public V2SessionClassRemapper(RemapperConfig remapperConfig) {
        this.joinUrl = remapperConfig.getJoinUrl();
        this.hasJoinedUrl = remapperConfig.getHasJoinedUrl();
        this.profilesUrl = remapperConfig.getProfilesUrl();
        this.allowedDomains = remapperConfig.getAllowedDomains();
        this.blockedDomains = remapperConfig.getBlockedDomains();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("<init>".equals(name)) {
                return new JoinAndCheckUrlsRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            } else if ("<clinit>".equals(name)) {
                return new AllowedAndBlockedDomainsRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            } else if ("fillGameProfile".equals(name)) {
                return new ProfilesUrlRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }

            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class JoinAndCheckUrlsRemapper extends MethodVisitor {
        public JoinAndCheckUrlsRemapper(int api, MethodVisitor mv) {
            super(api, mv);
        }

        boolean baseUrlRemapped;
        boolean joinUrlRemapped;
        boolean hasJoinedUrlRemapped;
        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            if ("baseUrl".equals(name) && opcode == Opcodes.PUTFIELD && !baseUrlRemapped) {
                baseUrlRemapped = true;
                super.visitInsn(Opcodes.POP);
                super.visitLdcInsn("");
            } else if ("baseUrl".equals(name) && opcode == Opcodes.GETFIELD) {
                if (!joinUrlRemapped) {
                    joinUrlRemapped = true;
                    super.visitInsn(Opcodes.POP);
                    super.visitLdcInsn(joinUrl);
                    return;
                }
                if (!hasJoinedUrlRemapped) {
                    hasJoinedUrlRemapped = true;
                    super.visitInsn(Opcodes.POP);
                    super.visitLdcInsn(hasJoinedUrl);
                    return;
                }
            }
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("join".equals(value) || "hasJoined".equals(value)) {
                super.visitLdcInsn("");
                return;
            }
            super.visitLdcInsn(value);
        }
    }

    private class AllowedAndBlockedDomainsRemapper extends MethodVisitor {
        public AllowedAndBlockedDomainsRemapper(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            if ("ALLOWED_DOMAINS".equals(name)) {
                super.visitInsn(Opcodes.POP);
                writeStringArray(allowedDomains);
            } else if ("BLOCKED_DOMAINS".equals(name)) {
                super.visitInsn(Opcodes.POP);
                writeStringArray(blockedDomains);
            }
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }

        private void writeStringArray(List<String> array) {
            super.visitLdcInsn(array.size());
            super.visitTypeInsn(Opcodes.ANEWARRAY, "java/lang/String");
            for (int i = 0; i < array.size(); i++) {
                storeString(i, array.get(i));
            }
        }

        private void storeString(int pos, String val) {
            super.visitInsn(Opcodes.DUP);
            super.visitLdcInsn(pos);
            super.visitLdcInsn(val);
            super.visitInsn(Opcodes.AASTORE);
        }
    }

    private class ProfilesUrlRemapper extends MethodVisitor {
        public ProfilesUrlRemapper(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        @Override
        public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
            if ("baseUrl".equals(name) && opcode == Opcodes.GETFIELD) {
                super.visitInsn(Opcodes.POP);
                super.visitLdcInsn(profilesUrl);
                return;
            }

            super.visitFieldInsn(opcode, owner, name, descriptor);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("profile/".equals(value)) {
                super.visitLdcInsn("");
                return;
            }
            super.visitLdcInsn(value);
        }
    }
}
