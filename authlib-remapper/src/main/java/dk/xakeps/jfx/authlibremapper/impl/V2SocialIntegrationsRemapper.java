package dk.xakeps.jfx.authlibremapper.impl;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import org.objectweb.asm.*;

public class V2SocialIntegrationsRemapper implements ClassRemapper {
    private final String privilegesUrl;
    private final String blocklistUrl;

    public V2SocialIntegrationsRemapper(RemapperConfig remapperConfig) {
        this.privilegesUrl = remapperConfig.getPrivilegesUrl();
        this.blocklistUrl = remapperConfig.getBlockListUrl();
    }

    @Override
    public byte[] remap(byte[] input) {
        ClassReader reader = new ClassReader(input);
        ClassWriter writer = new ClassWriter(reader, 0);

        ClassEditor editor = new ClassEditor(Opcodes.ASM9, writer);
        reader.accept(editor, 0);
        return writer.toByteArray();
    }

    private class ClassEditor extends ClassVisitor {
        public ClassEditor(int api, ClassVisitor classVisitor) {
            super(api, classVisitor);
        }

        @Override
        public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
            if ("<init>".equals(name)) {
                return new SocialUrlRemapper(api, super.visitMethod(access, name, descriptor, signature, exceptions));
            }
            return super.visitMethod(access, name, descriptor, signature, exceptions);
        }
    }

    private class SocialUrlRemapper extends MethodVisitor {
        public SocialUrlRemapper(int api, MethodVisitor methodVisitor) {
            super(api, methodVisitor);
        }

        int numSkips;
        @Override
        public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
            if ("com/mojang/authlib/Environment".equals(owner) && "getServicesHost".equals(name) && "()Ljava/lang/String;".equals(descriptor)) {
                super.visitInsn(Opcodes.POP);
                numSkips++;
                return;
            }
            if (numSkips == 1) {
                numSkips++;
                return;
            }
            super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
        }

        @Override
        public void visitLdcInsn(Object value) {
            if ("/privileges".equals(value)) {
                super.visitLdcInsn(privilegesUrl);
                numSkips = 0;
                return;
            }
            if ("/privacy/blocklist".equals(value)) {
                super.visitLdcInsn(blocklistUrl);
                return;
            }
            super.visitLdcInsn(value);
        }
    }
}
