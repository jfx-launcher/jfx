package dk.xakeps.jfx.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.LogManager;

public class LoggerConfig {
    public LoggerConfig() throws IOException {
        URL resource = getClass().getResource("/logger.properties");
        if (resource == null) {
            System.out.println("Logger config not found!");
            return;
        }
        try (InputStream inputStream = resource.openStream()) {
            LogManager.getLogManager().readConfiguration(inputStream);
        }
    }
}
