package dk.xakeps.jfx.core;

import java.util.Map;

public class ModuleConfig {
    private final Map<String, String> userInfoConfig;
    private final Map<String, String> accountInfoConfig;
    private final Map<String, String> authConfig;

    public ModuleConfig(Map<String, String> userInfoConfig,
                        Map<String, String> accountInfoConfig,
                        Map<String, String> authConfig) {
        this.userInfoConfig = userInfoConfig;
        this.accountInfoConfig = accountInfoConfig;
        this.authConfig = authConfig;
    }

    public Map<String, String> getUserInfoConfig() {
        return userInfoConfig;
    }

    public Map<String, String> getAccountInfoConfig() {
        return accountInfoConfig;
    }

    public Map<String, String> getAuthConfig() {
        return authConfig;
    }
}
