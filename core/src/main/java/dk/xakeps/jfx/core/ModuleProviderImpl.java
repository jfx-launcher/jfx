package dk.xakeps.jfx.core;

import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.account.AccountInfoModuleProvider;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.auth.AuthModule;
import dk.xakeps.jfx.auth.AuthModuleProvider;
import dk.xakeps.jfx.gui.core.ModuleProvider;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.user.UserInfoModule;
import dk.xakeps.jfx.user.UserInfoModuleProvider;

import java.util.Map;
import java.util.Objects;

public class ModuleProviderImpl implements ModuleProvider {
    private final ModuleConfig moduleConfig;

    public ModuleProviderImpl(ModuleConfig moduleConfig) {
        this.moduleConfig = Objects.requireNonNull(moduleConfig, "moduleConfig");
    }

    @Override
    public AuthModule loadAuthModule(Injector injector) {
        AuthModuleProvider provider = AuthModuleProvider.findAny().orElseThrow();
        injector.inject(provider);
        return provider.createModule(moduleConfig.getAuthConfig());
    }

    @Override
    public UserInfoModule loadUserInfoModule() {
        return UserInfoModuleProvider.findAny().orElseThrow()
                .createModule(moduleConfig.getUserInfoConfig());
    }

    @Override
    public AccountInfoModule loadAccountInfoModule(AuthInfo authInfo) {
        AccountInfoModuleProvider accountInfoModuleProvider = AccountInfoModuleProvider.findAny()
                .orElseThrow();
        Map<String, String> config = moduleConfig.getAccountInfoConfig();
        config.put("accessToken", authInfo.getAccessToken());
        return accountInfoModuleProvider.createModule(config);
    }
}
