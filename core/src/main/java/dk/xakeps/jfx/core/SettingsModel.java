package dk.xakeps.jfx.core;

import dk.xakeps.jfx.gui.core.profile.LauncherProfileModel;
import dk.xakeps.jfx.platform.PlatformInfo;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.nio.file.Path;

public class SettingsModel {
    private final ObjectProperty<Path> javaExecutablePath = new SimpleObjectProperty<>(this, "javaExecutablePath");
    private final ObjectProperty<Path> gameDirectory = new SimpleObjectProperty<>(this, "gameDirectory");
    private final ObservableList<String> enabledReleaseTypes = FXCollections.observableArrayList();

    public SettingsModel(LauncherProfileModel profileModel) {
        Path gameDirectory = profileModel.getGameDirectory();
        if (gameDirectory != null) {
            this.gameDirectory.set(gameDirectory);
        }
        Path javaExecutable = profileModel.getJavaExecutable();
        if (javaExecutable != null) {
            javaExecutablePath.set(javaExecutable);
        } else {
            PlatformInfo info = new PlatformInfo() {};
            javaExecutablePath.set(info.getJavaExecutable());
        }
        enabledReleaseTypes.setAll(profileModel.getEnabledReleaseTypes());
    }

    public Path getJavaExecutablePath() {
        return javaExecutablePath.get();
    }

    public ObjectProperty<Path> javaExecutablePathProperty() {
        return javaExecutablePath;
    }

    public void setJavaExecutablePath(Path javaExecutablePath) {
        this.javaExecutablePath.set(javaExecutablePath);
    }

    public Path getGameDirectory() {
        return gameDirectory.get();
    }

    public ObjectProperty<Path> gameDirectoryProperty() {
        return gameDirectory;
    }

    public void setGameDirectory(Path gameDirectory) {
        this.gameDirectory.set(gameDirectory);
    }

    public ObservableList<String> getEnabledReleaseTypes() {
        return enabledReleaseTypes;
    }
}
