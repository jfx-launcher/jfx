package dk.xakeps.jfx.core;

import java.net.URI;

public record SkinManagerConfig(URI skinServiceUri) {
}
