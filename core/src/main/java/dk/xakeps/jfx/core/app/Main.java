/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.app;

import dk.xakeps.jfx.core.ModuleProviderImpl;
import dk.xakeps.jfx.core.controller.MainController;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.view.View;
import dk.xakeps.jfx.gui.core.Core;
import dk.xakeps.jfx.gui.core.GuiEntryPoint;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.InjectorAware;
import javafx.application.Application;
import javafx.application.Preloader;
import javafx.stage.Stage;

import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class Main extends Application {
    public static final boolean DEV = true;
    private static final String LAUNCHER_ID = "jfx.launcher";
    private final Core core;

    static {
//        System.setProperty("javafx.preloader", AppPreloader.class.getName());
        if (System.getProperty("java.util.logging.config.class", "").isBlank()) {
            System.setProperty("java.util.logging.config.class", "dk.xakeps.jfx.core.LoggerConfig");
        }
    }


    public Main() {
        this.core = new Core(Util.createConfigHolder(LAUNCHER_ID, DEV),
                new ModuleProviderImpl(Util.loadModuleConfig(DEV)), new GuiEntryPointImpl());
    }

    public static void main(String[] args) {
        Application.launch(Main.class, args);
    }

    @Override
    public void start(Stage stage) {
        Util.loadIcon(stage);

        core.start(new StageManager(stage, 640, 480));
        stage.centerOnScreen();

        notifyPreloader(new Preloader.PreloaderNotification() {});
        stage.show();
    }

    private static class GuiEntryPointImpl implements GuiEntryPoint, InjectorAware {
        private Injector injector;

        @Override
        public CompletableFuture<View> loadMainGui() {
            return FXMLView.load(
                    Util.class.getResource("/fxml/main.fxml"),
                    ResourceBundle.getBundle("fxml.main"),
                    injector)
                    .thenApply(fxmlView -> {
                        MainController controller = (MainController) fxmlView.getController();
                        controller.setTabs(List.of(new WebTab(),
                                new ModPackAdminTab(injector),
                                new ManageJavaTab(injector),
                                new VersionAdminTab(injector),
                                new ChangeSkinTab(injector),
                                new ChangeCapeTab(injector)));
                        return fxmlView;
                    });
        }

        @Override
        public void setInjector(Injector injector) {
            this.injector = injector;
        }
    }
}
