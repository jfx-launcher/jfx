package dk.xakeps.jfx.core.app;

import dk.xakeps.jfx.core.view.mainview.MainViewTab;
import dk.xakeps.jfx.gui.common.view.View;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;

import java.util.concurrent.CompletableFuture;

public class ModPackAdminTab implements MainViewTab {
    private final CompletableFuture<? extends View> view;

    public ModPackAdminTab(Injector injector) {
        this.view = FXMLView.load(ModPackAdminTab.class.getResource("/fxml/modpack_admin.fxml"), null, injector);
    }

    @Override
    public String getId() {
        return "Mod Pack Admin";
    }

    @Override
    public CompletableFuture<? extends View> getView() {
        return view;
    }
}
