package dk.xakeps.jfx.core.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.core.ModuleConfig;
import dk.xakeps.jfx.core.SkinManagerConfig;
import dk.xakeps.jfx.gui.core.config.Config;
import dk.xakeps.jfx.gui.core.config.ConfigHolder;
import dk.xakeps.jfx.gui.core.config.RemapperConfigDto;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;
import java.util.Objects;

public class Util {
    static void loadIcon(Stage stage) {
        try (InputStream logo = Main.class.getResourceAsStream("/logo.png")) {
            if (logo != null) {
                stage.getIcons().add(new Image(logo));
            }
        } catch (IOException e) {
            e.printStackTrace(); // todo: handle
        }
    }

    static ModuleConfig loadModuleConfig(boolean dev) {
        return new ModuleConfig(loadUserInfoConfig(dev), loadAccountInfoConfig(dev), loadOpenIdConfig(dev));
    }

    private static byte[] loadAuthlibPubKey() {
        try(InputStream pubKeyStream = Util.class.getResourceAsStream("/public.der")) {
            if (pubKeyStream == null) {
                throw new RuntimeException("Public key not found");
            }
            return pubKeyStream.readAllBytes();
        } catch (IOException e) {
            throw new RuntimeException("Can't load public key", e);
        }
    }

    private static Map<String, String> loadUserInfoConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev ? "/user_info_config_dev.json" : "/user_info_config.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, new TypeReference<>() {});
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }

    private static Map<String, String> loadOpenIdConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev? "/openid_config_dev.json" : "/openid_config.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, new TypeReference<>() {});
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }

    private static Map<String, String> loadAccountInfoConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev ? "/account_info_config_dev.json" : "/account_info_config.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, new TypeReference<>() {});
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }

    private static RemapperConfigDto loadRemapperConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev ? "/remapper_config_dev.json" : "/remapper_config.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, RemapperConfigDto.class);
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }

    private static Config loadConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev ? "/config_dev.json" : "/config.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, Config.class);
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }

    static ConfigHolder createConfigHolder(String launcherId, boolean dev) {
        return ConfigHolder.newBuilder()
                .launcherId(launcherId)
                .remapperConfigDto(loadRemapperConfig(dev))
                .config(loadConfig(dev))
                .authlibPubKey(loadAuthlibPubKey())
                .build();
    }

    public static SkinManagerConfig getSkinManagerConfig(boolean dev) {
        URL configUrl = Util.class.getResource(dev ? "/skin_manager_dev.json" : "/skin_manager.json");
        Objects.requireNonNull(configUrl, "configUrl");
        try {
            return new ObjectMapper().readValue(configUrl, SkinManagerConfig.class);
        } catch (IOException e) {
            throw new RuntimeException("Can't read config", e);
        }
    }
}
