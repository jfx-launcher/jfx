package dk.xakeps.jfx.core.app;

import dk.xakeps.jfx.core.view.mainview.MainViewTab;
import dk.xakeps.jfx.gui.common.view.View;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;

import java.util.concurrent.CompletableFuture;

public class VersionAdminTab implements MainViewTab {
    private final CompletableFuture<? extends View> view;

    public VersionAdminTab(Injector injector) {
        this.view = FXMLView.load(VersionAdminTab.class.getResource("/fxml/upload_forge.fxml"), null, injector);
    }

    @Override
    public String getId() {
        return "Install forge";
    }

    @Override
    public CompletableFuture<? extends View> getView() {
        return view;
    }
}
