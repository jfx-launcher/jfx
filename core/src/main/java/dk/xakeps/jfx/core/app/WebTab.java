package dk.xakeps.jfx.core.app;

import dk.xakeps.jfx.core.view.mainview.MainViewTab;
import dk.xakeps.jfx.gui.common.view.View;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;

import java.util.concurrent.CompletableFuture;

public class WebTab implements MainViewTab {
    @Override
    public String getId() {
        return "News";
    }

    @Override
    public CompletableFuture<? extends View> getView() {
        return CompletableFuture.supplyAsync(() ->
                new BrowserView("https://jfx-launcher.gitlab.io/launcher-news/"), Platform::runLater);
    }

    private static final class BrowserView implements View {
        private final AnchorPane browser;

        public BrowserView(String url) {
            browser = new AnchorPane();
            WebView webView = new WebView();
            webView.getEngine().load(url);
            browser.getChildren().add(webView);
            AnchorPane.setTopAnchor(webView, 0d);
            AnchorPane.setRightAnchor(webView, 0d);
            AnchorPane.setBottomAnchor(webView, 0d);
            AnchorPane.setLeftAnchor(webView, 0d);
        }

        @Override
        public Node getContent() {
            return browser;
        }
    }
}
