package dk.xakeps.jfx.core.component;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class TextProgressBar extends StackPane {
    private static final int LABEL_PADDING = 5;

    private final Text textComponent;
    private final StringProperty text;
    private final ProgressBar progressBar;

    public TextProgressBar() {
        this.progressBar = new ProgressBar();
        this.textComponent = new Text();
        this.text = new SimpleStringProperty();

        progressBar.setMaxWidth(Double.MAX_VALUE);
        text.addListener((observable, oldValue, newValue) -> {
            textComponent.setText(newValue);
        });

        progressBar.setMinHeight(textComponent.getBoundsInLocal().getHeight() + LABEL_PADDING * 2);
        progressBar.setMinWidth(textComponent.getBoundsInLocal().getWidth()  + LABEL_PADDING * 2);

        progressBar.progressProperty().addListener((observable, oldValue, newValue) -> {
            progressBar.setMinHeight(textComponent.getBoundsInLocal().getHeight() + LABEL_PADDING * 2);
            progressBar.setMinWidth (textComponent.getBoundsInLocal().getWidth()  + LABEL_PADDING * 2);
        });

        getChildren().addAll(progressBar, textComponent);
    }

    public Text getTextComponent() {
        return textComponent;
    }

    public String getText() {
        return text.get();
    }

    public StringProperty textProperty() {
        return text;
    }

    public void setText(String text) {
        this.text.set(text);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }
}
