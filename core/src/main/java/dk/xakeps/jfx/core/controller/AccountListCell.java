package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.account.Account;
import javafx.scene.control.ListCell;

public class AccountListCell extends ListCell<Account> {
    @Override
    protected void updateItem(Account item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setText(null);
        } else {
            setText(item.getEmail());
        }
    }
}
