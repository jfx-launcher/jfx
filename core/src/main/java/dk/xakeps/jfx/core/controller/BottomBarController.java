package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.overlay.DownloadOverlay;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManagerInjector;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorAndLabelOverlay;
import dk.xakeps.jfx.gui.core.DirectoryManager;
import dk.xakeps.jfx.gui.core.inject.*;
import dk.xakeps.jfx.gui.core.launcher.GameLauncherBean;
import dk.xakeps.jfx.gui.core.launcher.GameRunnable;
import dk.xakeps.jfx.gui.core.profile.LauncherProfileModel;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.InjectorAware;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class BottomBarController implements ProfileManagerAware, ModPackManagerWrapperAware,
        DirectoryManagerAware, StageManagerAware, InjectorAware, GameLauncherBeanAware,
        VersionManagerWrapperAware, Initializable {
    private ProfileManager profileManager;
    private ModPackManagerWrapper modPackManagerWrapper;
    private VersionManagerWrapper versionManagerWrapper;
    private StageManager stageManager;
    private Injector injector;
    private GameLauncherBean gameLauncherBean;

    @FXML
    private ComboBox<GameVersion> versionsList;
    @FXML
    public ComboBox<ModPack> modPackList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LauncherProfileModel selectedProfile = profileManager.getSelectedProfile();

        modPackList.itemsProperty().set(new FilteredList<>(modPackManagerWrapper.getRemoteRegistry().modPacksProperty()));
        modPackList.setConverter(new ModPackConverter(modPackManagerWrapper));

        versionsList.itemsProperty().set(new FilteredList<>(versionManagerWrapper.getRemoteRegistry().versionsProperty(),
                gameVersion -> selectedProfile.getEnabledReleaseTypes().contains(gameVersion.getReleaseType())));
        versionsList.setConverter(new GameVersionConverter(versionManagerWrapper));

        versionManagerWrapper.getRemoteRegistry().versionsProperty().addListener((observable, oldValue, newValue) -> {
            versionsList.itemsProperty().set(new FilteredList<>(newValue, gameVersion ->
                    selectedProfile.getEnabledReleaseTypes().contains(gameVersion.getReleaseType())));
        });

        ObservableList<String> enabledVersionTypes = selectedProfile.getEnabledReleaseTypes();
        if (enabledVersionTypes.isEmpty()) {
            enabledVersionTypes.add("release");
        }
        enabledVersionTypes.addListener((ListChangeListener<? super String>) c -> {
            FilteredList<GameVersion> gameVersions = (FilteredList<GameVersion>) versionsList.itemsProperty().get();
            gameVersions.setPredicate(gameVersion -> selectedProfile.getEnabledReleaseTypes().contains(gameVersion.getReleaseType()));
        });
        updateSelectedVersion();
        versionsList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                selectedProfile.setLastPlayedVersion(newValue.getId());
            }
        });
    }

    private void updateSelectedVersion() {
        LauncherProfileModel selectedProfile = profileManager.getSelectedProfile();
        String lastPlayedVersion = selectedProfile.getLastPlayedVersion();
        GameVersion gameVersion = versionManagerWrapper.getVersion(lastPlayedVersion).orElse(null);
        if (gameVersion != null) {
            versionsList.getSelectionModel().select(gameVersion);
            selectedProfile.setLastPlayedVersion(gameVersion.getId());
        } else {
            versionsList.getSelectionModel().select(0);
            GameVersion selectedItem = versionsList.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                selectedProfile.setLastPlayedVersion(selectedItem.getId());
            }
        }
    }

    public void onPlay(ActionEvent actionEvent) {
        ModPack modPack = modPackList.getValue();
        GameVersion gameVersion;
        if (modPack == null) {
            gameVersion = versionsList.getValue();
        } else {
            gameVersion = versionManagerWrapper.getVersion(
                    modPack.getEnabledVersion()
                            .map(ModPackVersion::getMinecraftVersion)
                            .orElseThrow())
                    .orElseThrow();
        }

        DownloadOverlay downloadOverlay = new DownloadOverlay();
        CompletableFuture<GameRunnable> gameRunnable;
        if (modPack == null) {
            gameRunnable = gameLauncherBean.prepareVersion(gameVersion, downloadOverlay.getProgressListenerFactory());
        } else {
            gameRunnable = gameLauncherBean.prepareModPack(modPack, downloadOverlay.getProgressListenerFactory());
        }

        CompletableFuture<GameRunnable> future = CompletableFuture.runAsync(() -> {
            try {
                profileManager.save();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).thenComposeAsync(unused -> gameRunnable);
        stageManager.runWithOverlay(downloadOverlay, future).thenComposeAsync(gameRunnable1 -> {
            IndicatorAndLabelOverlay overlay = new IndicatorAndLabelOverlay();
            overlay.getLabel().setText("Game started");
            overlay.getLabel().setTextFill(Color.GREEN);
            return stageManager.runWithOverlay(overlay, CompletableFuture.runAsync(() -> {
                try {
                    gameRunnable1.start();
                } catch (IOException e) {
                    throw new RuntimeException("Error during game start", e);
                }
            }));
        }, Platform::runLater).exceptionally(throwable -> {
            System.getLogger(BottomBarController.class.getName())
                    .log(System.Logger.Level.WARNING, "Can't start the game", throwable);
            return null;
        });
    }

    public void onShowSettings(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.setTitle("Settings"); // todo: locale
        stage.initModality(Modality.APPLICATION_MODAL);
        StageManager stageManager = new StageManager(stage, 640, 480);
        FXMLView.load(getClass().getResource("/fxml/settings.fxml"), null,
                injector.copy()
                        .addInjector(new StageManagerInjector(stageManager))
                        .build())
                .thenApplyAsync(stageManager::setCurrentView, Platform::runLater)
                .whenCompleteAsync((o, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                    stage.showAndWait();
                }, Platform::runLater);
    }

    @Override
    public void setProfileManager(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public void setDirectoryManager(DirectoryManager directoryManager) {
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = modPackManagerWrapper;
    }

    @Override
    public void setVersionManagerWrapper(VersionManagerWrapper versionManagerWrapper) {
        this.versionManagerWrapper = versionManagerWrapper;
    }

    @Override
    public void setInjector(Injector injector) {
        this.injector = injector;
    }

    @Override
    public void setGameLauncherBean(GameLauncherBean gameLauncherBean) {
        this.gameLauncherBean = gameLauncherBean;
    }
}
