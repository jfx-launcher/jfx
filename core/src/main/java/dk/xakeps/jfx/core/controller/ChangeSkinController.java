package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.app.Main;
import dk.xakeps.jfx.core.app.Util;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.core.inject.ProfileManagerAware;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.skinmanager.SkinManager;
import dk.xakeps.jfx.skinmanager.SkinType;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class ChangeSkinController implements StageManagerAware, ProfileManagerAware, Initializable {
    public Label skinName;
    public ComboBox<SkinType> skinTypeBox;

    private StageManager stageManager;
    private ProfileManager profileManager;

    private Path skinPath;

    public void onSelectSkin(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(
                new FileChooser.ExtensionFilter("PNG file", "*.png"));
        File file = fileChooser.showOpenDialog(stageManager.getStage());
        if (file == null || !file.exists()) {
            return;
        }
        skinPath = file.toPath();
        skinName.setText(skinPath.getFileName().toString());
    }

    public void onUploadSkin(ActionEvent actionEvent) {
        if (skinPath == null || !Files.exists(skinPath)) {
            return;
        }

        SkinType skinType = skinTypeBox.getSelectionModel().getSelectedItem();
        if (skinType == null) {
            return;
        }

        SkinManager skinManager = SkinManager.newBuilder()
                .accessToken(profileManager.getAuthInfo().getAccessToken())
                .serviceUri(Util.getSkinManagerConfig(Main.DEV).skinServiceUri())
                .build();
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, CompletableFuture.runAsync(() -> {
            skinManager.changeSkin(
                    profileManager.getAccountModel().getSelectedProfile(),
                    skinType, skinPath);
        }));
    }

    @Override
    public void setProfileManager(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        skinTypeBox.getItems().addAll(SkinType.values());
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }
}
