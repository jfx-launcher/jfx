package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.launcher.GameVersion;
import javafx.util.StringConverter;

import java.util.Objects;

public class GameVersionConverter extends StringConverter<GameVersion> {
    private final VersionManagerWrapper versionManagerHolder;

    public GameVersionConverter(VersionManagerWrapper versionManagerHolder) {
        this.versionManagerHolder = Objects.requireNonNull(versionManagerHolder, "versionManagerHolder");
    }

    @Override
    public String toString(GameVersion object) {
        if (object == null) {
            return null;
        }
        return object.getId();
    }

    @Override
    public GameVersion fromString(String string) {
        if (string == null) {
            return null;
        }
        return versionManagerHolder.getVersion(string).orElse(null);
    }
}
