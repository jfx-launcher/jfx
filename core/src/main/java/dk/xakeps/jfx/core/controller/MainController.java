/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.TabButton;
import dk.xakeps.jfx.core.view.mainview.MainViewTab;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayHandle;
import dk.xakeps.jfx.gui.core.inject.ProfileManagerAware;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.util.Pair;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements StageManagerAware, ProfileManagerAware, Initializable {
    private final ToggleGroup tabsPaneToggleGroup = new ToggleGroup();

    private StageManager stageManager;
    private ProfileManager profileManager;

    @FXML
    private BorderPane borderPane;
    @FXML
    private FlowPane tabsButtonsPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void setProfileManager(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    public void setTabs(List<MainViewTab> tabs) {
        for (MainViewTab tab : tabs) {
            TabButton tabButton = new TabButton();
            tabButton.setToggleGroup(tabsPaneToggleGroup);
            tabsButtonsPane.getChildren().add(tabButton);
            tabButton.setText(tab.getId());
            tabButton.setOnAction(event1 -> {
                tab.getView()
                        .thenApplyAsync(view -> {
                            OverlayHandle overlayHandle = stageManager.getOverlayManager().addOverlay(IndicatorOverlay.INSTANCE);
                            return new Pair<>(view, overlayHandle);
                        }, Platform::runLater)
                        .thenApplyAsync(pair -> {
                            borderPane.setCenter(pair.getKey().getContent());
                            return pair;
                        }, Platform::runLater)
                        .thenAcceptAsync(pair -> stageManager.getOverlayManager().disableOverlay(pair.getValue()),
                                Platform::runLater)
                        .exceptionally(throwable -> {
                            throwable.printStackTrace();
                            return null;
                        });
            });
        }
        Iterator<Toggle> iterator = tabsPaneToggleGroup.getToggles().iterator();
        if (iterator.hasNext()) {
            TabButton next = (TabButton) iterator.next();
            Platform.runLater(next::fire);
        }

        Label loginId = new Label(profileManager.getSelectedProfile().getName());
        tabsButtonsPane.getChildren().add(loginId);
    }
}
