package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.core.inject.ModPackManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class ModPackAddController implements ModPackManagerWrapperAware,
        StageManagerAware, Initializable {
    @FXML
    private TextField modPackIdText;

    private ModPackManagerWrapper modPackManagerWrapper;
    private StageManager stageManager;

    public void onAddModPack(ActionEvent actionEvent) {
        String modPackId = modPackIdText.getText();
        if (modPackId == null || modPackId.isBlank()) {
            return;
        }

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            modPackManagerWrapper.getModPackAdmin().create(modPackId);
        });
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, future)
                .whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                    stageManager.close();
                }, Platform::runLater);
    }

    public void onCancelModPackAdd(ActionEvent actionEvent) {
        stageManager.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = modPackManagerWrapper;
    }
}
