package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.account.Account;
import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorAndLabelOverlay;
import dk.xakeps.jfx.gui.core.inject.AccountInfoModuleAware;
import dk.xakeps.jfx.gui.core.inject.ModPackManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.modpackmanager.ModPack;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class ModPackAddManagerController implements ModPackManagerWrapperAware, StageManagerAware, AccountInfoModuleAware {
    @FXML
    private TextField managerEmailField;

    private ModPackManagerWrapper modPackManagerWrapper;
    private StageManager stageManager;
    private AccountInfoModule accountInfoModule;
    private ModPack modPack;

    public void onAddManager(ActionEvent actionEvent) {
        String email = managerEmailField.getText();
        if (email == null || email.isBlank()) {
            return;
        }

        IndicatorAndLabelOverlay overlay = new IndicatorAndLabelOverlay();
        CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
            Account account = accountInfoModule.findAccountByEmail(email).orElse(null);
            if (account == null) {
                Platform.runLater(() -> overlay.getLabel().setText("User not found"));
                return Result.NO_ACCOUNT;
            } else {
                Platform.runLater(() -> overlay.getLabel().setText("User found, adding manager"));
                modPackManagerWrapper.getModPackAdmin().addAdmin(modPack, account.getId());
                return Result.OK;
            }
        }).whenCompleteAsync((result, throwable) -> {
            if (throwable != null) {
                overlay.getLabel().setText("Error");
                throwable.printStackTrace();
            }
            if (result == Result.OK) {
                overlay.getLabel().setText("Manager added");
            }
        }, Platform::runLater)
                .thenRunAsync(() -> {
                    try {
                        TimeUnit.MILLISECONDS.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).thenRunAsync(() -> stageManager.close(), Platform::runLater);
        overlay.getLabel().setText("Looking for user");
        stageManager.runWithOverlay(overlay, future);
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setAccountInfoModule(AccountInfoModule accountInfoModule) {
        this.accountInfoModule = accountInfoModule;
    }

    public void setModPack(ModPack modPack) {
        this.modPack = modPack;
    }

    @Override
    public void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = modPackManagerWrapper;
    }

    private enum Result {
        OK, NO_ACCOUNT
    }
}
