package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.account.Account;
import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManagerInjector;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.core.inject.AccountInfoModuleAware;
import dk.xakeps.jfx.gui.core.inject.ModPackManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.InjectorAware;
import dk.xakeps.jfx.modpackmanager.Admin;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackAdmin;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class ModPackAdminController implements ModPackManagerWrapperAware,
        StageManagerAware, AccountInfoModuleAware, InjectorAware, Initializable {
    @FXML
    private ListView<Account> managerList;
    @FXML
    private ComboBox<ModPack> modPackList;
    @FXML
    private ListView<ModPackVersion> modPackVersionList;

    private ModPackManagerWrapper modPackManagerWrapper;
    private ModPackAdmin modPackAdmin;
    private StageManager stageManager;
    private AccountInfoModule accountInfoModule;
    private Injector injector;

    public void onAddModPack(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.setTitle("Create ModPack");
        stage.initModality(Modality.APPLICATION_MODAL);
        StageManager stageManager = new StageManager(stage);
        FXMLView.load(getClass().getResource("/fxml/modpack_add.fxml"), null,
                injector.copy().addInjector(new StageManagerInjector(stageManager)).build())
                .thenApplyAsync(stageManager::setCurrentView, Platform::runLater)
                .thenAcceptAsync(v -> {
                    stage.sizeToScene();
                    stage.showAndWait();
                    }, Platform::runLater)
                .thenAcceptAsync(manager -> {
                    modPackManagerWrapper.getRemoteRegistry().reload();
                }).whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                    }, Platform::runLater);
    }

    public void onRemoveModPack(ActionEvent actionEvent) {
        ModPack selectedItem = modPackList.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            modPackAdmin.delete(selectedItem);
            modPackManagerWrapper.getRemoteRegistry().reload();
        });
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, future)
                .whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                }, Platform::runLater);
    }

    public void onEnableVersion(ActionEvent actionEvent) {
        ModPackVersion selectedItem = modPackVersionList.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            modPackAdmin.enable(selectedItem);
            modPackManagerWrapper.getRemoteRegistry().reload();
        });
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, future)
                .whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                }, Platform::runLater);
    }

    public void onUploadVersion(ActionEvent actionEvent) {
        Stage stage = new Stage();
        stage.setTitle("Upload ModPack");
        stage.initModality(Modality.APPLICATION_MODAL);
        StageManager stageManager = new StageManager(stage);
        FXMLView.load(getClass().getResource("/fxml/modpack_upload.fxml"), null,
                injector.copy().addInjector(new StageManagerInjector(stageManager)).build())
                .thenApplyAsync(stageManager::setCurrentView, Platform::runLater)
                .thenAcceptAsync(v -> {
                    stage.sizeToScene();
                    stage.showAndWait();
                }, Platform::runLater)
                .thenAcceptAsync(manager -> {
                    modPackManagerWrapper.getRemoteRegistry().reload();
                }).whenCompleteAsync((unused, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            }
        }, Platform::runLater);
    }

    public void onAddManager(ActionEvent actionEvent) {
        ModPack modPack = modPackList.getSelectionModel().getSelectedItem();
        if (modPack == null) {
            return;
        }
        Stage stage = new Stage();
        stage.setTitle("Add ModPack Manager");
        stage.initModality(Modality.APPLICATION_MODAL);
        StageManager stageManager = new StageManager(stage);
        FXMLView.load(getClass().getResource("/fxml/modpack_add_manager.fxml"), null,
                injector.copy().addInjector(new StageManagerInjector(stageManager)).build())
                .thenApplyAsync(fxmlView -> {
                    ModPackAddManagerController controller = (ModPackAddManagerController) fxmlView.getController();
                    controller.setModPack(modPack);
                    return fxmlView;
                }).thenApplyAsync(stageManager::setCurrentView, Platform::runLater)
                .thenAcceptAsync(o -> {
                    stage.sizeToScene();
                    stage.showAndWait();
                    }, Platform::runLater)
                .thenAcceptAsync(unused -> {
                    modPackList.getSelectionModel().select(null); // fire reload event
                    modPackList.getSelectionModel().select(modPack); // fire reload event
                }).whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                });
    }

    public void onRemoveManager(ActionEvent actionEvent) {
        Account account = managerList.getSelectionModel().getSelectedItem();
        if (account == null) {
            return;
        }
        ModPack modPack = modPackList.getSelectionModel().getSelectedItem();
        if (modPack == null) {
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,
                "Remove manager: " + account.getEmail() + "?");
        ButtonType buttonType = alert.showAndWait().orElse(ButtonType.CANCEL);
        if (buttonType == ButtonType.OK) {
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
                modPackAdmin.removeAdmin(modPack, account.getId());
            }).whenCompleteAsync((unused, throwable) -> {
                if (throwable != null) {
                    throwable.printStackTrace();
                }
                modPackList.getSelectionModel().select(null); // fire reload event
                modPackList.getSelectionModel().select(modPack); // fire reload event
            }, Platform::runLater);
            stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, future);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.modPackAdmin = modPackManagerWrapper.getModPackAdmin();
        managerList.setCellFactory(param -> new AccountListCell());
        modPackList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                modPackVersionList.getItems().clear();
                modPackVersionList.getItems().addAll(newValue.getVersions());
                CompletableFuture.supplyAsync(() -> modPackAdmin.getAdmins(newValue))
                        .thenApplyAsync(admins -> accountInfoModule.findAccountsByIds(
                                admins.stream().map(Admin::getName).collect(Collectors.toList())))
                .whenCompleteAsync((accounts, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                    if (accounts != null) {
                        managerList.getItems().setAll(accounts);
                    }
                }, Platform::runLater);
            }
        });
        modPackList.setCellFactory(param -> new ModPackListCell());
        modPackVersionList.setCellFactory(param -> new ModPackVersionListCell());
        modPackList.itemsProperty().bind(modPackManagerWrapper.getRemoteRegistry().modPacksProperty());
        modPackList.itemsProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null || newValue.isEmpty()) {
                managerList.getItems().clear();
                modPackVersionList.getItems().clear();
            }
        });
    }

    @Override
    public void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = modPackManagerWrapper;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setAccountInfoModule(AccountInfoModule accountInfoModule) {
        this.accountInfoModule = accountInfoModule;
    }

    @Override
    public void setInjector(Injector injector) {
        this.injector = injector;
    }
}
