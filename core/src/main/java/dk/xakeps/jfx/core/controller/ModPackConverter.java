package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.modpackmanager.ModPack;
import javafx.util.StringConverter;

public class ModPackConverter extends StringConverter<ModPack> {
    private final ModPackManagerWrapper modPackManagerHolder;

    public ModPackConverter(ModPackManagerWrapper modPackManagerHolder) {
        this.modPackManagerHolder = modPackManagerHolder;
    }

    @Override
    public String toString(ModPack object) {
        if (object == null) {
            return null;
        }
        return object.getModPackId();
    }

    @Override
    public ModPack fromString(String string) {
        if (string == null) {
            return null;
        }
        return modPackManagerHolder.getModPack(string).orElse(null);
    }
}
