package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.modpackmanager.ModPack;
import javafx.scene.control.ListCell;

public class ModPackListCell extends ListCell<ModPack> {
    @Override
    protected void updateItem(ModPack item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setText(null);
        } else {
            setText(item.getModPackId());
        }
    }
}
