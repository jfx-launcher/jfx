package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.overlay.DownloadOverlay;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.core.Constants;
import dk.xakeps.jfx.gui.core.inject.ModPackManagerWrapperAware;
import dk.xakeps.jfx.gui.core.inject.UpdateManagerWrapperAware;
import dk.xakeps.jfx.gui.core.inject.VersionManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.updatemanager.UpdateVersion;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class ModPackUploadController implements ModPackManagerWrapperAware, VersionManagerWrapperAware,
        UpdateManagerWrapperAware, StageManagerAware, Initializable {
    @FXML
    private Label modPackFilesLabel;
    @FXML
    private ComboBox<ModPack> modPackList;
    @FXML
    private ComboBox<GameVersion> gameVersionList;
    @FXML
    private ComboBox<String> javaVersionList;
    @FXML
    private TextArea ignoredFilesText;

    private ModPackManagerWrapper modPackManagerWrapper;
    private VersionManagerWrapper versionManagerWrapper;
    private UpdateManagerWrapper updateManagerWrapper;
    private StageManager stageManager;

    private Path modPackFilesPath;

    public void onUpload(ActionEvent actionEvent) {
        ModPack modPack = modPackList.getSelectionModel().getSelectedItem();
        GameVersion gameVersion = gameVersionList.getSelectionModel().getSelectedItem();
        String javaVersion = javaVersionList.getSelectionModel().getSelectedItem();
        if (modPack == null || gameVersion == null || javaVersion == null) {
            return;
        }

        if (modPackFilesPath == null || !Files.isRegularFile(modPackFilesPath)) {
            modPackFilesLabel.setText("Mod pack files");
            return;
        }

        Set<String> ignoredFiles = ignoredFilesText.getParagraphs().stream().map(CharSequence::toString)
                .map(s -> s.replace('\\', '/'))
                .collect(Collectors.toSet());
        DownloadOverlay overlay = new DownloadOverlay();
        stageManager.runWithOverlay(overlay, CompletableFuture.runAsync(() -> {
            modPackManagerWrapper.getModPackAdmin().upload(overlay.getProgressListenerFactory(),
                    modPack, gameVersion.getId(), javaVersion, ignoredFiles, modPackFilesPath);
            modPackManagerWrapper.getRemoteRegistry().reload();
        })).whenCompleteAsync((unused, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            }
            stageManager.close();
        }, Platform::runLater);
    }

    public void onCancel(ActionEvent actionEvent) {
        stageManager.close();
    }

    public void onSelectModPackFiles(ActionEvent actionEvent) {
        ModPack modPack = modPackList.getSelectionModel().getSelectedItem();
        if (modPack == null) {
            return;
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("Zip files", "*.zip"));
        File file = fileChooser.showOpenDialog(stageManager.getStage());
        if (file == null) {
            return;
        }
        if (!file.exists()) {
            return;
        }
        this.modPackFilesPath = file.toPath();
        this.modPackFilesLabel.setText(modPackFilesPath.getFileName().toString());
    }

    @Override
    public void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = modPackManagerWrapper;
    }

    @Override
    public void setVersionManagerWrapper(VersionManagerWrapper versionManagerWrapper) {
        this.versionManagerWrapper = versionManagerWrapper;
    }

    @Override
    public void setUpdateManagerWrapper(UpdateManagerWrapper updateManagerWrapper) {
        this.updateManagerWrapper = updateManagerWrapper;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gameVersionList.setConverter(new GameVersionConverter(versionManagerWrapper));
        gameVersionList.itemsProperty().bind(versionManagerWrapper.getRemoteRegistry().versionsProperty());
        modPackList.setConverter(new ModPackConverter(modPackManagerWrapper));
        modPackList.itemsProperty().bind(modPackManagerWrapper.getRemoteRegistry().modPacksProperty());
        updateManagerWrapper.getRemoteRegistry(Constants.JAVA_UPDATE_PACKAGE_ID).updateVersionsProperty()
                .addListener((observable, oldValue, newValue) -> {
            setJavaVersions(newValue);
        });
        setJavaVersions(updateManagerWrapper.getRemoteRegistry(Constants.JAVA_UPDATE_PACKAGE_ID).getUpdateVersions());
    }

    private void setJavaVersions(List<UpdateVersion> updateVersions) {
        if (updateVersions == null) {
            return;
        }
        Set<String> versions = updateVersions.stream().map(UpdateVersion::getVersion).collect(Collectors.toSet());
        javaVersionList.getItems().setAll(versions);
    }
}
