package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import javafx.scene.control.ListCell;

public class ModPackVersionListCell extends ListCell<ModPackVersion> {
    @Override
    protected void updateItem(ModPackVersion item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setText(null);
        } else {
            setText(item.getMinecraftVersion() + " " + item.getModPackId() + " " + item.getVersion());
        }
    }
}
