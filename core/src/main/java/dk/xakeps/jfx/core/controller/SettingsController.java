package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.SettingsModel;
import dk.xakeps.jfx.core.overlay.DownloadOverlay;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.core.inject.GameLauncherBeanAware;
import dk.xakeps.jfx.gui.core.inject.ProfileManagerAware;
import dk.xakeps.jfx.gui.core.inject.VersionManagerWrapperAware;
import dk.xakeps.jfx.gui.core.launcher.GameLauncherBean;
import dk.xakeps.jfx.gui.core.profile.LauncherProfileModel;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.launcher.GameVersion;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SettingsController implements ProfileManagerAware, StageManagerAware,
        VersionManagerWrapperAware, GameLauncherBeanAware,
        Initializable {
    private SettingsModel settingsModel;

    @FXML
    private TextField javaPathTextField;
    @FXML
    private TextField gameDirectoryTextField;
    @FXML
    private FlowPane selectedReleaseTypesPane;
    @FXML
    private ComboBox<GameVersion> remoteVersionList;

    private final ObservableList<String> installedVersions = FXCollections.observableList(new ArrayList<>());

    private ProfileManager profileManager;
    private StageManager stageManager;
    private GameLauncherBean gameLauncherBean;
    private VersionManagerWrapper versionManagerWrapper;

    public void onSelectJavaPath(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        Path javaExecutablePath = settingsModel.getJavaExecutablePath();
        if (javaExecutablePath != null) {
            fileChooser.setInitialDirectory(javaExecutablePath.getParent().toFile());
            fileChooser.setInitialFileName(javaExecutablePath.getFileName().toString());
        }
        File file = fileChooser.showOpenDialog(stageManager.getStage());
        if (file != null) {
            settingsModel.setJavaExecutablePath(file.getAbsoluteFile().toPath());
        }
    }

    public void onSelectGameDirectory(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        if (settingsModel.getGameDirectory() != null) {
            directoryChooser.setInitialDirectory(settingsModel.getGameDirectory().toFile());
        }
        File file = directoryChooser.showDialog(stageManager.getStage());
        if (file != null) {
            settingsModel.setGameDirectory(file.getAbsoluteFile().toPath());
        }
    }

    public void onSave(ActionEvent actionEvent) {
        LauncherProfileModel selectedProfile = profileManager.getSelectedProfile();
        selectedProfile.setGameDirectory(settingsModel.getGameDirectory());
        selectedProfile.setJavaExecutable(settingsModel.getJavaExecutablePath());
        selectedProfile.getEnabledReleaseTypes().setAll(settingsModel.getEnabledReleaseTypes());
        try {
            profileManager.save();
        } catch (IOException e) {
            e.printStackTrace(); // todo: log error
        }
        stageManager.close();
    }

    public void onCancel(ActionEvent actionEvent) {
        stageManager.close();
    }

    public SettingsModel getSettingsModel() {
        return settingsModel;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.settingsModel = new SettingsModel(profileManager.getSelectedProfile());
        javaPathTextField.textProperty().bindBidirectional(settingsModel.javaExecutablePathProperty(), new PathConverter());
        gameDirectoryTextField.textProperty().bindBidirectional(settingsModel.gameDirectoryProperty(), new PathConverter());

        remoteVersionList.setConverter(new GameVersionConverter(versionManagerWrapper));
        Set<String> releaseTypes = new LinkedHashSet<>();
        releaseTypes.addAll(getReleaseTypes(versionManagerWrapper.getLocalRegistry().getVersions()));
        releaseTypes.addAll(getReleaseTypes(versionManagerWrapper.getRemoteRegistry().getVersions()));
        for (String releaseType : releaseTypes) {
            Node node = releaseTypeCheckBox(releaseType);
            selectedReleaseTypesPane.getChildren().add(node);
        }
        versionManagerWrapper.getLocalRegistry().versionsProperty().addListener((observable, oldValue, newValue) -> {
            installedVersions.setAll(newValue.stream().map(GameVersion::getId).collect(Collectors.toList()));
        });
        installedVersions.setAll(versionManagerWrapper.getLocalRegistry().getVersions()
                .stream()
                .map(GameVersion::getId)
                .collect(Collectors.toList()));

        remoteVersionList.itemsProperty().set(new FilteredList<>(
                versionManagerWrapper.getRemoteRegistry().getVersions(),
                gameVersion -> {
                    if (installedVersions.contains(gameVersion.getId())) {
                        return false;
                    } else {
                        return settingsModel.getEnabledReleaseTypes().contains(gameVersion.getReleaseType());
                    }
                }));

        versionManagerWrapper.getRemoteRegistry().versionsProperty().addListener((observable, oldValue, newValue) -> {
            remoteVersionList.itemsProperty().set(new FilteredList<>(newValue, gameVersion -> {
                if (installedVersions.contains(gameVersion.getId())) {
                    return false;
                } else {
                    return settingsModel.getEnabledReleaseTypes().contains(gameVersion.getReleaseType());
                }
            }));
        });
    }

    public void onInstall(ActionEvent actionEvent) {
        GameVersion gameVersion = remoteVersionList.getValue();
        if (gameVersion == null) {
            return;
        }
        DownloadOverlay downloadOverlay = new DownloadOverlay();
        stageManager.runWithOverlay(downloadOverlay,
                gameLauncherBean.prepareVersion(gameVersion, downloadOverlay.getProgressListenerFactory()));
    }

    private List<String> getReleaseTypes(List<GameVersion> gameVersions) {
        return gameVersions.stream()
                .map(GameVersion::getReleaseType)
                .distinct()
                .collect(Collectors.toList());
    }

    private Node releaseTypeCheckBox(String id) {
        CheckBox checkBox = new CheckBox(id);
        if (settingsModel.getEnabledReleaseTypes().contains(id)) {
            checkBox.setSelected(true);
        }
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && newValue) {
                settingsModel.getEnabledReleaseTypes().add(id);
            } else {
                settingsModel.getEnabledReleaseTypes().remove(id);
            }
        });
        return checkBox;
    }

    @Override
    public void setProfileManager(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setVersionManagerWrapper(VersionManagerWrapper versionManagerWrapper) {
        this.versionManagerWrapper = versionManagerWrapper;
    }

    @Override
    public void setGameLauncherBean(GameLauncherBean gameLauncherBean) {
        this.gameLauncherBean = gameLauncherBean;
    }

    private static final class PathConverter extends StringConverter<Path> {
        @Override
        public String toString(Path object) {
            if (object == null) {
                return null;
            }
            return object.toAbsolutePath().toString();
        }

        @Override
        public Path fromString(String string) {
            if (string == null) {
                return null;
            }
            return Paths.get(string);
        }
    }
}
