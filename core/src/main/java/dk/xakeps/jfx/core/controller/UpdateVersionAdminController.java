package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManagerInjector;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayHandle;
import dk.xakeps.jfx.gui.core.inject.UpdateManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateRegistryWrapper;
import dk.xakeps.jfx.gui.fxml.FXMLView;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.InjectorAware;
import dk.xakeps.jfx.updatemanager.UpdateVersion;
import dk.xakeps.jfx.updatemanager.PlatformSupportInfo;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class UpdateVersionAdminController implements UpdateManagerWrapperAware,
        StageManagerAware, InjectorAware, Initializable {
    private final ObservableMap<String, Set<String>> systemsAndArchs = FXCollections.observableHashMap();
    @FXML
    private ComboBox<String> updatePackageTypeList;
    @FXML
    private ComboBox<String> systemsList;
    @FXML
    private ComboBox<String> archsList;
    @FXML
    private ListView<UpdateVersion> versionsList;

    private final ObjectProperty<UpdateRegistryWrapper> currentRegistry = new SimpleObjectProperty<>();
    private UpdateManagerWrapper updateManagerWrapper;
    private StageManager stageManager;
    private Injector injector;

    public void onUploadVersion(ActionEvent actionEvent) {
        String updatePackageType = updatePackageTypeList.getSelectionModel().getSelectedItem();
        if (updatePackageType == null || updatePackageType.isBlank()) {
            return;
        }

        String arch = archsList.getSelectionModel().getSelectedItem();
        if (arch == null || arch.isBlank()) {
            return;
        }

        String system = systemsList.getSelectionModel().getSelectedItem();
        if (system == null || system.isBlank()) {
            return;
        }

        Stage stage = new Stage();
        stage.setTitle("Upload Update version");
        stage.initModality(Modality.APPLICATION_MODAL);
        StageManager stageManager = new StageManager(stage);
        FXMLView.load(getClass().getResource("/fxml/update_version_upload.fxml"), null,
                injector.copy().addInjector(new StageManagerInjector(stageManager)).build())
                .thenApplyAsync(stageManager::setCurrentView, Platform::runLater)
                .thenAcceptAsync(v -> {
                    UpdateVersionUploadController controller = (UpdateVersionUploadController) v.getController();
                    controller.setArch(arch);
                    controller.setSystem(system);
                    controller.setUpdatePackageType(updatePackageType);
                    stage.sizeToScene();
                    stage.showAndWait();
                }, Platform::runLater)
                .thenAcceptAsync(manager -> {
                    updateManagerWrapper.getRemoteRegistry(updatePackageType).reload();
                }).whenCompleteAsync((unused, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            }
        }, Platform::runLater);
    }

    public void onDeleteVersion(ActionEvent actionEvent) {
        UpdateVersion selectedItem = versionsList.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }
        OverlayHandle overlayHandle = stageManager.getOverlayManager().addOverlay(IndicatorOverlay.INSTANCE);
        CompletableFuture.runAsync(() -> {
            updateManagerWrapper.getUpdateManagerAdmin()
                    .delete(selectedItem);
        }).thenComposeAsync(unused -> updateManagerWrapper.getRemoteRegistry(selectedItem.getUpdatePackageType()).reload())
                .whenCompleteAsync((unused, throwable) -> {
                    if (throwable != null) {
                        throwable.printStackTrace();
                    }
                    overlayHandle.close();
                }, Platform::runLater);
    }

    public void onEnableVersion(ActionEvent actionEvent) {
        UpdateVersion selectedItem = versionsList.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            return;
        }

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            updateManagerWrapper.getUpdateManagerAdmin().enable(selectedItem);
        });
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, future)
                .exceptionally(throwable -> {
                    throwable.printStackTrace();
                    return null;
                });
    }

    @Override
    public void setUpdateManagerWrapper(UpdateManagerWrapper updateManagerWrapper) {
        this.updateManagerWrapper = updateManagerWrapper;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setInjector(Injector injector) {
        this.injector = injector;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        updatePackageTypeList.itemsProperty().bind(updateManagerWrapper.updatePackageTypesProperty());
        updatePackageTypeList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                return;
            }
            currentRegistry.setValue(updateManagerWrapper.getRemoteRegistry(newValue));
        });
        versionsList.setCellFactory(param -> new UpdateVersionListCell());
        systemsList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                return;
            }
            String selectedArch = archsList.getSelectionModel().getSelectedItem();
            archsList.getItems().setAll(systemsAndArchs.get(newValue));
            archsList.getSelectionModel().select(selectedArch);
        });
        systemsAndArchs.addListener((MapChangeListener<String, Set<String>>) change -> {
            systemsList.getItems().setAll(change.getMap().keySet());
        });

        currentRegistry.addListener((observable, oldValue, newValue) -> {
            onUpdate(newValue);
        });
        archsList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            onUpdate(currentRegistry.get());
        });

        CompletableFuture.supplyAsync(() -> {
            Set<PlatformSupportInfo> platformSupportInfos = updateManagerWrapper.getUpdateManagerAdmin().getPlatformSupportInfos();
            return platformSupportInfos.stream()
                    .collect(Collectors.groupingBy(PlatformSupportInfo::getSystem,
                            Collectors.flatMapping(i -> i.getArchitectures().stream(), Collectors.toSet())));
        }).whenCompleteAsync((stringSetMap, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            }
            systemsAndArchs.clear();
            systemsAndArchs.putAll(stringSetMap);
        }, Platform::runLater);
    }

    private void onUpdate(UpdateRegistryWrapper registryWrapper) {
        String selectedArch = archsList.getSelectionModel().getSelectedItem();
        String selectedSystem = systemsList.getSelectionModel().getSelectedItem();
        if (registryWrapper == null || selectedArch == null || selectedSystem == null) {
            versionsList.setItems(FXCollections.emptyObservableList());
            return;
        }

        FilteredList<UpdateVersion> filtered = registryWrapper.updateVersionsProperty()
                .filtered(updateVersion -> updateVersion.getArch().equals(selectedArch)
                        && updateVersion.getSystem().equals(selectedSystem));
        versionsList.setItems(filtered);
    }
}
