package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.updatemanager.UpdateVersion;
import javafx.scene.control.ListCell;

public class UpdateVersionListCell extends ListCell<UpdateVersion> {
    @Override
    protected void updateItem(UpdateVersion item, boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setText(null);
        } else {
            setText(item.getVersion());
        }
    }
}
