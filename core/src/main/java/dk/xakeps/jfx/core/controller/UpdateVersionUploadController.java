package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.core.overlay.DownloadOverlay;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.core.inject.UpdateManagerWrapperAware;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class UpdateVersionUploadController implements UpdateManagerWrapperAware, StageManagerAware {
    @FXML
    private Label systemLabel;
    @FXML
    private Label archLabel;
    @FXML
    private TextField updateVersionField;
    @FXML
    private Label updateArchiveLabel;

    private Path versionArchive;

    private UpdateManagerWrapper updateManagerWrapper;
    private StageManager stageManager;

    private String arch;
    private String system;
    private String updatePackageType;

    public void onUpload(ActionEvent actionEvent) {
        if (system == null || system.isBlank()) {
            return;
        }
        if (arch == null || arch.isBlank()) {
            return;
        }
        String updateVersion = updateVersionField.getText();
        if (updateVersion == null || updateVersion.isBlank()) {
            return;
        }

        if (versionArchive == null || Files.notExists(versionArchive)) {
            updateArchiveLabel.setText("Update archive:");
            return;
        }

        DownloadOverlay overlay = new DownloadOverlay();
        stageManager.runWithOverlay(overlay, CompletableFuture.runAsync(() -> {
            updateManagerWrapper.getUpdateManagerAdmin().upload(overlay.getProgressListenerFactory(),
                    updatePackageType, arch, system, updateVersion, versionArchive);
            updateManagerWrapper.getRemoteRegistry(updatePackageType).reload();
        })).whenCompleteAsync((unused, throwable) -> {
            if (throwable != null) {
                throwable.printStackTrace();
            }
            stageManager.close();
        }, Platform::runLater);
    }

    public void onCancel(ActionEvent actionEvent) {
        stageManager.close();
    }

    @Override
    public void setUpdateManagerWrapper(UpdateManagerWrapper updateManagerWrapper) {
        this.updateManagerWrapper = updateManagerWrapper;
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    public void onSelectJavaArchive(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("ZIP or TAR.GZ", "*.zip", "*.tar.gz"));
        File file = fileChooser.showOpenDialog(stageManager.getStage());
        if (file == null) {
            return;
        }
        if (!file.exists()) {
            return;
        }
        this.versionArchive = file.toPath();
        updateArchiveLabel.setText(versionArchive.getFileName().toString());
    }

    void setArch(String arch) {
        this.arch = arch;
        archLabel.setText(arch);
    }

    void setSystem(String system) {
        this.system = system;
        systemLabel.setText(system);
    }

    void setUpdatePackageType(String updatePackageType) {
        this.updatePackageType = updatePackageType;
    }
}
