package dk.xakeps.jfx.core.controller;

import dk.xakeps.jfx.gui.common.scene.StageManagerAware;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.core.inject.ProfileManagerAware;
import dk.xakeps.jfx.gui.core.inject.VersionManagerWrapperAware;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.launcher.VersionManagerAdmin;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public class UploadForgeController implements StageManagerAware, VersionManagerWrapperAware,
        ProfileManagerAware, Initializable {
    @FXML
    private Label selectedFileLabel;

    private StageManager stageManager;
    private VersionManagerWrapper versionManagerWrapper;
    private ProfileManager profileManager;

    private VersionManagerAdmin versionManagerAdmin;

    private Path selectedFile;

    public void onChoiceFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("JAR files", "*.jar"));
        File file = fileChooser.showOpenDialog(stageManager.getStage());
        if (file == null) {
            return;
        }
        if (!file.exists()) {
            return;
        }

        selectedFile = file.toPath();
        selectedFileLabel.setText(selectedFile.getFileName().toString());
    }

    public void onUpload(ActionEvent actionEvent) {
        if (selectedFile == null) {
            return;
        }
        stageManager.runWithOverlay(IndicatorOverlay.INSTANCE, CompletableFuture.runAsync(() -> {
            versionManagerAdmin.uploadForge(selectedFile);
            versionManagerWrapper.getRemoteRegistry().reload();
        })).whenCompleteAsync((unused, throwable) -> {
            selectedFile = null;
            if (throwable != null) {
                throwable.printStackTrace();
                selectedFileLabel.setText("Error uploading");
                return;
            }
            selectedFileLabel.setText("Upload complete");
        }, Platform::runLater);
    }

    @Override
    public void setStageManager(StageManager stageManager) {
        this.stageManager = stageManager;
    }

    @Override
    public void setVersionManagerWrapper(VersionManagerWrapper versionManagerWrapper) {
        this.versionManagerWrapper = versionManagerWrapper;
    }

    @Override
    public void setProfileManager(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.versionManagerAdmin = versionManagerWrapper.getVersionManagerAdmin();
    }
}
