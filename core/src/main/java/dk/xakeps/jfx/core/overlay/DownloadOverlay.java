package dk.xakeps.jfx.core.overlay;

import dk.xakeps.jfx.core.component.TextProgressBar;
import dk.xakeps.jfx.gui.common.scene.overlay.Overlay;
import dk.xakeps.jfx.network.ProgressInfo;
import dk.xakeps.jfx.network.ProgressListener;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DownloadOverlay implements Overlay {
    private final VBox content = new VBox();
    private final TextProgressBar textProgressBar = new TextProgressBar();
    private final ProgressListenerFactory progressListenerFactory = new ProgressListenerFactoryImpl();

    public DownloadOverlay() {
        content.setAlignment(Pos.CENTER);
        content.getChildren().add(textProgressBar);
    }

    @Override
    public Node getContent() {
        return content;
    }

    public ProgressListenerFactory getProgressListenerFactory() {
        return progressListenerFactory;
    }

    private final class ProgressListenerFactoryImpl implements ProgressListenerFactory {
        private static final long SPEED_RESOLUTION = TimeUnit.SECONDS.toMillis(1);

        private final Lock lock = new ReentrantLock();

        private long totalListeners;
        private long listenersClosed;
        private long totalBytes;
        private long bytesRead;

        private long previousUpdateTime;
        private long bytesReadAtPrevCheck;
        private String speed = "";

        @Override
        public ProgressListener newListener(String listenedId, long size) {
            try {
                lock.lock();
                this.totalListeners++;
                this.totalBytes += size;
                return new ProgressListenerImpl(size);
            } finally {
                lock.unlock();
            }
        }

        private void onSizeNotification(ProgressInfo progressInfo) {
            try {
                lock.lock();
                totalBytes += progressInfo.contentLength();
            } finally {
                lock.unlock();
            }
        }

        private void onNotification(ProgressInfo progressInfo) {
            try {
                lock.lock();
                bytesRead += progressInfo.bytesTransferred();
                update();
            } finally {
                lock.unlock();
            }
        }

        private void update() {
            long currentTime = System.currentTimeMillis();
            if (currentTime - previousUpdateTime >= SPEED_RESOLUTION) {
                previousUpdateTime = currentTime;
                long bytesReadAtThisCheck = bytesRead - bytesReadAtPrevCheck;
                bytesReadAtPrevCheck = bytesRead;
                speed = toReadableBytes(bytesReadAtThisCheck) + "ps";
            }
            Platform.runLater(() -> {
                textProgressBar.getProgressBar().setProgress(bytesRead / (double) totalBytes);
                textProgressBar.setText("%s / %s; %d / %d; %s".formatted(
                        toReadableBytes(bytesRead),
                        toReadableBytes(totalBytes),
                        listenersClosed,
                        totalListeners,
                        speed));
            });
        }

        private void onListenerClosed(long leftoverBytes) {
            try {
                lock.lock();
                listenersClosed++;
                bytesRead += leftoverBytes;
            } finally {
                lock.unlock();
            }
        }

        private final class ProgressListenerImpl implements ProgressListener {
            private long totalBytes;
            private long downloadedBytes;
            private boolean sizeNotified;

            public ProgressListenerImpl(long totalBytes) {
                this.totalBytes = totalBytes;
                this.sizeNotified = totalBytes != 0;
            }

            @Override
            public void notify(ProgressInfo progressInfo) {
                if (!sizeNotified) {
                    sizeNotified = true;
                    onSizeNotification(progressInfo);
                    this.totalBytes = progressInfo.contentLength();
                }
                this.downloadedBytes = progressInfo.totalBytesTransferred();
                onNotification(progressInfo);
            }

            @Override
            public void close() {
                long amountToAdd = 0;
                if (totalBytes > 0) {
                    amountToAdd = totalBytes - downloadedBytes;
                }
                onListenerClosed(amountToAdd);
            }
        }
    }

    private static final String[] PREFIXES = {"kB", "MB", "GB"};
    private static String toReadableBytes(long bytes) {
        int v = (int) Math.floor(Math.log10(Math.abs(bytes)) / 3);
        double speed = bytes * Math.pow(1000, -v);
        int max = Math.max(v - 1, 0);
        int min = Math.min(max, PREFIXES.length - 1);
        return "%.2f %s".formatted(speed, PREFIXES[min]);
    }
}
