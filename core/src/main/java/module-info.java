/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module dk.xakeps.jfx.core {
    requires transitive javafx.base;
    requires transitive com.fasterxml.jackson.databind;
    requires transitive dk.xakeps.jfx.settings;
    requires transitive dk.xakeps.jfx.gui.common;
    requires transitive dk.xakeps.jfx.gui.fxml;
    requires transitive dk.xakeps.jfx.gui.core;
    requires transitive dk.xakeps.jfx.auth;
    requires transitive dk.xakeps.jfx.modpackmanager;
    requires transitive dk.xakeps.jfx.account;
    requires transitive dk.xakeps.jfx.skinmanager;

    requires dk.xakeps.jfx.launcher;
    requires dk.xakeps.jfx.authlibremapper;

    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires java.prefs;
    requires java.xml;
    requires java.logging;

    opens dk.xakeps.jfx.core.app to javafx.graphics;
    opens dk.xakeps.jfx.core.controller to javafx.fxml;
    opens dk.xakeps.jfx.core to com.fasterxml.jackson.databind;

    exports dk.xakeps.jfx.core;
    exports dk.xakeps.jfx.core.app;
    exports dk.xakeps.jfx.core.controller;
    exports dk.xakeps.jfx.core.view.mainview;

    uses dk.xakeps.jfx.auth.AuthModuleProvider;
    uses dk.xakeps.jfx.launcher.VersionManager.Builder;
    uses dk.xakeps.jfx.settings.SettingsLoaderFactory;
    uses dk.xakeps.jfx.user.UserInfoModuleProvider;
    uses dk.xakeps.jfx.modpackmanager.ModPackManager.Builder;
    uses dk.xakeps.jfx.account.AccountInfoModuleProvider;
}