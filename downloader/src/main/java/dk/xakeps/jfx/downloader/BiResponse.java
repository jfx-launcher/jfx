package dk.xakeps.jfx.downloader;

public record BiResponse<F, S>(F first, S second) {
}
