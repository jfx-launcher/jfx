/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.downloader;

import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.ProgressTracker;
import dk.xakeps.jfx.file.NotEnoughSpaceException;
import dk.xakeps.jfx.network.ProgressInfo;
import dk.xakeps.jfx.network.ProgressListener;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Used to download files
 */
public class Downloader implements AutoCloseable {
    private static final System.Logger LOGGER = System.getLogger(Downloader.class.getName());

    private final ProgressListenerFactory listenerFactory;
    private final HttpClient httpClient = Methanol.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .followRedirects(HttpClient.Redirect.NORMAL)
            .build();
    private final ProgressTracker progressTracker = ProgressTracker.create();
    private final ExecutorService executorService = Executors.newWorkStealingPool(2);
    // since we may receive multiple batches of downloads
    // without waiting previous downloads to be finished
    // we should account pending downloads size too
    private final Map<FileStore, Long> totalSpaceUsed = new HashMap<>(1);

    /**
     * @param listenerFactory {@link ProgressListenerFactory} used to monitor downloads
     */
    public Downloader(ProgressListenerFactory listenerFactory) {
        LOGGER.log(System.Logger.Level.TRACE, "Downloader created");
        this.listenerFactory = listenerFactory;
    }

    /**
     * All file and hash checks should be done later
     * If disk space is enough or not should be checked here
     * @param name name of this download batch
     * @param fileInfos files to download
     * @return future, that is finished after all downloads finished
     */
    public CompletableFuture<Void> download(String name, Set<DownloadFileInfo> fileInfos) {
        LOGGER.log(System.Logger.Level.TRACE, "Download request {0}, items: {1}", name, fileInfos.size());
        return CompletableFuture.runAsync(() -> {
            List<CompletableFuture<Void>> futures = new ArrayList<>(fileInfos.size());
            try {
                checkDiskSpace(fileInfos);
            } catch (IOException e) {
                LOGGER.log(System.Logger.Level.ERROR, "Can't check disk space", e);
                throw new DownloaderException("Can't check disk space", e);
            }
            for (DownloadFileInfo fileInfo : fileInfos) {
                ProgressListener listener = listenerFactory.newListener(name, fileInfo.getSize());
                futures.add(CompletableFuture.runAsync(
                        () -> downloadUnchecked(fileInfo, listener), executorService));
            }
            waitForAll(futures);

        }, executorService);
    }

    /**
     * Checks available space
     * @param fileInfos files
     * @throws IOException if errored
     * @throws NotEnoughSpaceException if there is not enough space to download all files
     */
    void checkDiskSpace(Set<DownloadFileInfo> fileInfos) throws IOException {
        LOGGER.log(System.Logger.Level.TRACE, "Checking disk space");
        for (DownloadFileInfo fileInfo : fileInfos) {
            if (Files.exists(fileInfo.getFile())) {
                continue;
            }
            Path file = fileInfo.getFile();
            while (Files.notExists(file)) {
                file = file.getParent();
            }
            FileStore fileStore = Files.getFileStore(file);
            Long orDefault = totalSpaceUsed.getOrDefault(fileStore, 0L);
            totalSpaceUsed.put(fileStore, orDefault + getDownloadSize(fileInfo));
        }
        for (Map.Entry<FileStore, Long> entry : totalSpaceUsed.entrySet()) {
            FileStore key = entry.getKey();
            Long value = entry.getValue();
            if (key.getUsableSpace() < value) {
                LOGGER.log(
                        System.Logger.Level.ERROR,
                        "Not enough disk space, store: {0}, avail: {1}, required: {2}, need: {3}",
                        key.name(),
                        key.getUsableSpace(),
                        value,
                        value - key.getUsableSpace());
                throw new NotEnoughSpaceException(key.name(), key.getUsableSpace(), value);
            }
        }
    }

    /**
     * Calculates if local filesystem space usage
     * will increase or decrease after file downloaded
     * @param fileInfo file info
     * @return local filesystem used space change
     * @throws IOException if error happens
     */
    long getDownloadSize(DownloadFileInfo fileInfo) throws IOException {
        if (Files.notExists(fileInfo.getFile())) {
            return fileInfo.getSize();
        }
        long fsSize = Files.size(fileInfo.getFile());
        long dlSize = fileInfo.getSize();
        return dlSize - fsSize;
    }

    /**
     * Waits list of futures
     * @param futures list of futures to wait
     */
    private void waitForAll(List<CompletableFuture<Void>> futures) {
        LOGGER.log(System.Logger.Level.TRACE, "Waiting for {0} futures", futures.size());
        for (CompletableFuture<Void> future : futures) {
            future.join();
        }
    }

    /**
     * Downloads single file
     * @param downloadFileInfo file to download
     * @param listener download listener
     */
    private void downloadUnchecked(DownloadFileInfo downloadFileInfo, ProgressListener listener) {
        try(listener) {
            download(downloadFileInfo, listener);
        } catch (IOException | InterruptedException | NoSuchAlgorithmException e) {
            LOGGER.log(System.Logger.Level.ERROR, "Can't download", e);
            throw new DownloaderException("Can't download", e);
        }
    }

    /**
     * Downloads single file
     * @param downloadFileInfo file to download
     * @param listener download listener
     * @throws IOException if error happens
     */
    private void download(DownloadFileInfo downloadFileInfo, ProgressListener listener)
            throws IOException, InterruptedException, NoSuchAlgorithmException {
        LOGGER.log(System.Logger.Level.ALL, "Downloading file: {0}", downloadFileInfo.getFile());
        // if file exists, check it's size
        // if size is ok, then continue, else error
        // Size is ok if downloadFileInfo is > 0 and filesystem file size equals to downloadFileInfo file size
        if (Files.exists(downloadFileInfo.getFile())) {
            LOGGER.log(System.Logger.Level.ALL, "File exists: {0}", downloadFileInfo.getFile());
            if (downloadFileInfo.getSize() <= 0 || Files.size(downloadFileInfo.getFile()) == downloadFileInfo.getSize()) {
                LOGGER.log(System.Logger.Level.ALL, "File is fine: {0}", downloadFileInfo.getFile());
                return;
            }
            LOGGER.log(System.Logger.Level.TRACE, "File is not fine: {0}", downloadFileInfo.getFile());
            // file size is wrong, re-download it
            Files.deleteIfExists(downloadFileInfo.getFile());
        }

        // if file does not exist and no source provided, we should fail, since we can't download this file and file is not downloaded
        if (Files.notExists(downloadFileInfo.getFile()) && downloadFileInfo.getSource() == null) {
            LOGGER.log(System.Logger.Level.ERROR,
                    "File has not source and it's not present on filesystem, {0}", downloadFileInfo.getFile());
            throw new DownloaderException("URI is empty. Artifact: " + downloadFileInfo.getFile());
        }

        runDownload(downloadFileInfo, listener);
    }

    /**
     * @param downloadFileInfo file to save
     * @param listener download listener
     * @throws IOException if error happens
     * @throws InterruptedException if error happens
     */
    private void runDownload(DownloadFileInfo downloadFileInfo, ProgressListener listener)
            throws IOException, InterruptedException, NoSuchAlgorithmException {
        Path target = downloadFileInfo.getFile();
        LOGGER.log(System.Logger.Level.TRACE, "Downloading file {0}", target);
        String hash = downloadFileInfo.getHash();
        String hashAlgorithm = downloadFileInfo.getHashAlgorithm();
        Files.createDirectories(downloadFileInfo.getFile().getParent());

        HttpResponse.BodyHandler<Path> trackingHandler = progressTracker.tracking(
                HttpResponse.BodyHandlers.ofFile(downloadFileInfo.getFile()),
                item -> listener.notify(new ProgressInfoImpl(downloadFileInfo, item)));

        HttpRequest request = HttpRequest.newBuilder(downloadFileInfo.getSource()).GET().build();
        if (hashAlgorithm.isEmpty() || hash.isEmpty()) { // we don't have hash or hashing algorithm, just copy file
            LOGGER.log(System.Logger.Level.TRACE, "Downloading without hash check, file {0}", target);
            httpClient.send(request, trackingHandler);
            LOGGER.log(System.Logger.Level.TRACE, "File downloaded, {0}", target);
            return;
        }

        HttpResponse.BodyHandler<BiResponse<MessageDigest, Path>> bodyHandler =
                MessageDigestBodySubscriber.ofAlgorithm(trackingHandler,
                        downloadFileInfo.getHashAlgorithm());
        HttpResponse<BiResponse<MessageDigest, Path>> response = httpClient.send(request, bodyHandler);
        byte[] digest = response.body().first().digest();
        if (!toHex(digest).equalsIgnoreCase(hash)) {
            LOGGER.log(System.Logger.Level.TRACE, "Wong SHA1, file {0}", target);
            throw new DownloaderException("Wrong SHA1, Path: " + target);
        }
        LOGGER.log(System.Logger.Level.TRACE, "File downloaded, {0}", target);
    }

    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

    /**
     * @param bytes byte array
     * @return HEX string of bytes
     */
    static String toHex(byte[] bytes) {
        StringBuilder builder = new StringBuilder(bytes.length * 2);
        for (byte aByte : bytes) {
            builder.append(HEX_CHARS[(aByte & 0xF0) >>> 4]);
            builder.append(HEX_CHARS[(aByte & 0x0F)]);
        }
        return builder.toString();
    }

    @Override
    public void close() {
        executorService.shutdown();
    }

    private static class ProgressInfoImpl implements ProgressInfo {
        private final DownloadFileInfo downloadFileInfo;
        private final ProgressTracker.Progress progress;

        public ProgressInfoImpl(DownloadFileInfo downloadFileInfo, ProgressTracker.Progress progress) {
            this.downloadFileInfo = Objects.requireNonNull(downloadFileInfo, "downloadFileInfo");
            this.progress = Objects.requireNonNull(progress, "progress");
        }

        @Override
        public long bytesTransferred() {
            return progress.bytesTransferred();
        }

        @Override
        public long totalBytesTransferred() {
            return progress.totalBytesTransferred();
        }

        @Override
        public Duration timePassed() {
            return progress.timePassed();
        }

        @Override
        public Duration totalTimePassed() {
            return progress.totalTimePassed();
        }

        @Override
        public long contentLength() {
            if (progress.contentLength() > 0) {
                return progress.contentLength();
            }

            if (downloadFileInfo.getSize() > 0) {
                return downloadFileInfo.getSize();
            }

            return 0;
        }

        @Override
        public boolean done() {
            return progress.done();
        }
    }
}
