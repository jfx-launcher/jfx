package dk.xakeps.jfx.downloader;

import java.net.http.HttpResponse;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Flow;

public class MessageDigestBodySubscriber<T> implements HttpResponse.BodySubscriber<BiResponse<MessageDigest, T>> {
    private final HttpResponse.BodySubscriber<T> downstream;
    private final MessageDigest messageDigest;
    private final CompletableFuture<MessageDigest> mdResult = new CompletableFuture<>();
    private final CompletableFuture<BiResponse<MessageDigest, T>> result;

    private MessageDigestBodySubscriber(HttpResponse.BodySubscriber<T> downstream, MessageDigest messageDigest) {
        this.downstream = Objects.requireNonNull(downstream, "downstream");
        this.messageDigest = Objects.requireNonNull(messageDigest, "messageDigest");
        this.result = mdResult.thenCompose(md -> downstream.getBody().thenApply(t -> new BiResponse<>(md, t)));
    }

    @Override
    public CompletionStage<BiResponse<MessageDigest, T>> getBody() {
        return result;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        downstream.onSubscribe(subscription);
    }

    @Override
    public void onNext(List<ByteBuffer> item) {
        for (ByteBuffer byteBuffer : item) {
            messageDigest.update(byteBuffer);
            byteBuffer.flip();
        }
        downstream.onNext(item);
    }

    @Override
    public void onError(Throwable throwable) {
        result.completeExceptionally(throwable);
        downstream.onError(throwable);
    }

    @Override
    public void onComplete() {
        mdResult.complete(messageDigest);
        downstream.onComplete();
    }

    public static <T> HttpResponse.BodySubscriber<BiResponse<MessageDigest, T>> ofAlgorithm(
            HttpResponse.BodySubscriber<T> downstream, MessageDigest messageDigest) {
        return new MessageDigestBodySubscriber<>(downstream,messageDigest);
    }

    static <T> HttpResponse.BodyHandler<BiResponse<MessageDigest, T>>
    ofAlgorithm(HttpResponse.BodyHandler<T> bodyHandler, String algorithm) throws NoSuchAlgorithmException {
        MessageDigest instance = MessageDigest.getInstance(algorithm);
        return responseInfo -> MessageDigestBodySubscriber.ofAlgorithm(bodyHandler.apply(responseInfo), instance);
    }
}
