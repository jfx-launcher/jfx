module dk.xakeps.jfx.downloader {
    requires transitive dk.xakeps.jfx.file;
    requires transitive dk.xakeps.jfx.network;

    requires methanol;

    exports dk.xakeps.jfx.downloader;
}