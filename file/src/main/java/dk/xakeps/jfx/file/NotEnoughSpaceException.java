/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.file;

/**
 * Exception thrown when not enough disk space
 */
public class NotEnoughSpaceException extends RuntimeException {
    private final String storeName;
    private final long availableSpace;
    private final long requiredSpace;
    
    public NotEnoughSpaceException(String storeName, long availableSpace, long requiredSpace) {
        super(generateMessage("", storeName, availableSpace, requiredSpace));
        this.storeName = storeName;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public NotEnoughSpaceException(String message, String storeName, long availableSpace, long requiredSpace) {
        super(generateMessage(message, storeName, availableSpace, requiredSpace));
        this.storeName = storeName;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public NotEnoughSpaceException(String message, Throwable cause, String storeName, long availableSpace, long requiredSpace) {
        super(generateMessage(message, storeName, availableSpace, requiredSpace), cause);
        this.storeName = storeName;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public NotEnoughSpaceException(Throwable cause, String storeName, long availableSpace, long requiredSpace) {
        super(generateMessage("", storeName, availableSpace, requiredSpace), cause);
        this.storeName = storeName;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public NotEnoughSpaceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String storeName, long availableSpace, long requiredSpace) {
        super(generateMessage(message, storeName, availableSpace, requiredSpace), cause, enableSuppression, writableStackTrace);
        this.storeName = storeName;
        this.availableSpace = availableSpace;
        this.requiredSpace = requiredSpace;
    }

    public String getStoreName() {
        return storeName;
    }

    public long getAvailableSpace() {
        return availableSpace;
    }

    public long getRequiredSpace() {
        return requiredSpace;
    }

    private static String generateMessage(String message, String storeName, long availableSpace, long requiredSpace) {
        return String.format(
                "Store name: %s, available (%d) / (%d) required, not enough (%d)",
                storeName,
                availableSpace,
                requiredSpace,
                requiredSpace - availableSpace
        ) + "\n" + message;
    }
}
