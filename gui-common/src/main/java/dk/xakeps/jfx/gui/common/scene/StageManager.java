/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.gui.common.scene;

import dk.xakeps.jfx.gui.common.scene.overlay.Overlay;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayHandle;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayManager;
import dk.xakeps.jfx.gui.common.scene.overlay.RootPane;
import dk.xakeps.jfx.gui.common.view.View;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class StageManager {
    private final Stage stage;

    private final ObjectProperty<View> currentView = new SimpleObjectProperty<>(this, "currentView");

    private final RootPane rootPane = new RootPane();
    private final OverlayManager overlayManager = new OverlayManager(rootPane);

    public StageManager(Stage stage) {
        this.stage = Objects.requireNonNull(stage, "stage");
        Scene scene = new Scene(rootPane);
        stage.setScene(scene);
    }

    public StageManager(Stage stage, double width, double height) {
        this.stage = Objects.requireNonNull(stage, "stage");
        Scene scene = new Scene(rootPane, width, height);
        stage.setScene(scene);
    }

    public View getCurrentView() {
        return currentView.getValue();
    }

    public <T extends View> T setCurrentView(T view) {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException("Called from non-main thread");
        }
        rootPane.setContent(view.getContent());
        currentView.set(view);
        return view;
    }

    public OverlayManager getOverlayManager() {
        return overlayManager;
    }

    // Maybe something better?
    public <T> CompletableFuture<T> runWithOverlay(Overlay overlay, CompletableFuture<T> task) {
        OverlayHandle overlayHandle = overlayManager.addOverlay(overlay);
        return task.whenCompleteAsync((t, throwable) -> {
            overlayManager.disableOverlay(overlayHandle);
            if (throwable != null) throwable.printStackTrace();
        }, Platform::runLater);
    }

    public ObservableValue<View> currentViewProperty() {
        return currentView;
    }

    public void close() {
        stage.close();
    }

    public Stage getStage() {
        return stage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StageManager that = (StageManager) o;
        return stage.equals(that.stage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stage);
    }
}
