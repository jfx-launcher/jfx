/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.gui.common.scene;

import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class StageManagerInjector implements InjectionSource<StageManager> {
    private final StageManager stageManager;

    public StageManagerInjector(StageManager stageManager) {
        this.stageManager = Objects.requireNonNull(stageManager, "stageManager");
    }

    @Override
    public Class<StageManager> getInjectableClass() {
        return StageManager.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof StageManagerAware) {
            ((StageManagerAware) o).setStageManager(stageManager);
        }
    }
}
