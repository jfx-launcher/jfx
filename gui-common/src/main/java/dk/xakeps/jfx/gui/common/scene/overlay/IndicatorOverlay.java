/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.gui.common.scene.overlay;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class IndicatorOverlay implements Overlay {
    public static final Overlay INSTANCE = new IndicatorOverlay();

    private final Node content = new Indicator();

    @Override
    public Node getContent() {
        return content;
    }

    private static final class Indicator extends TilePane {
        public Indicator() {
            setAlignment(Pos.CENTER);
            StackPane pane = new StackPane();
            pane.setAlignment(Pos.CENTER);
            pane.setPadding(new Insets(30));
            pane.setBackground(new Background(new BackgroundFill(new Color(1, 1, 1, 0.8), new CornerRadii(5), new Insets(10))));
            pane.getChildren().add(new ProgressIndicator());
            getChildren().add(pane);
        }
    }
}
