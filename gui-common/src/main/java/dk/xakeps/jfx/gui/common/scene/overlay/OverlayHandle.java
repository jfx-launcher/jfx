package dk.xakeps.jfx.gui.common.scene.overlay;

import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.util.Objects;

public class OverlayHandle implements AutoCloseable {
    private final OverlayManager overlayManager;
    private final Overlay overlay;
    private final Node overlayNode;

    public OverlayHandle(OverlayManager overlayManager, Overlay overlay) {
        this.overlayManager = Objects.requireNonNull(overlayManager, "overlayManager");
        Node content = overlay.getContent();
        AnchorPane overlayNode = new AnchorPane(content);
        AnchorPane.setTopAnchor(content, 0d);
        AnchorPane.setRightAnchor(content, 0d);
        AnchorPane.setBottomAnchor(content, 0d);
        AnchorPane.setLeftAnchor(content, 0d);
        this.overlay = Objects.requireNonNull(overlay, "overlay");
        this.overlayNode = Objects.requireNonNull(overlayNode, "overlayNode");
    }

    Node getOverlayNode() {
        return overlayNode;
    }

    public void disable() {
        close();
    }

    @Override
    public void close() {
        overlayManager.disableOverlay(this);
    }
}
