package dk.xakeps.jfx.gui.common.view;

import javafx.scene.Node;

@FunctionalInterface
public interface View {
    Node getContent();
}
