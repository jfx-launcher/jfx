package dk.xakeps.jfx.gui.core;

import dk.xakeps.jfx.authlibremapper.Remapper;
import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import dk.xakeps.jfx.launcher.Library;
import dk.xakeps.jfx.launcher.LibraryReplacer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

public class AuthlibRemapperReplacer implements LibraryReplacer {
    private static final System.Logger LOGGER = System.getLogger(AuthlibRemapperReplacer.class.getName());

    private final RemapperConfig remapperConfig;
    private final Remapper remapper;

    public AuthlibRemapperReplacer(RemapperConfig remapperConfig) {
        this.remapperConfig = remapperConfig;
        this.remapper = new Remapper(remapperConfig);
        Path outputPath = remapperConfig.getOutputPath();
        if (Files.notExists(outputPath)) {
            try {
                Files.createDirectories(outputPath);
            } catch (IOException e) {
                throw new RuntimeException("Can't create remapper output directories", e);
            }
        }
    }

    @Override
    public boolean test(Library library) {
        return library.getName().contains("com.mojang:authlib");
    }

    @Override
    public Optional<Path> replaceWith(Library library, Path artifactPath) {
        try {
            return Optional.of(remapper.remap(artifactPath));
        } catch (IOException e) {
            LOGGER.log(System.Logger.Level.ERROR, "Failed to remap authlib", e);
            throw new RuntimeException("Failed to remap authlib", e);
        }
    }
}
