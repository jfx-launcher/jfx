package dk.xakeps.jfx.gui.core;

import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.authlibremapper.RemapperConfig;
import dk.xakeps.jfx.gui.common.scene.StageManager;
import dk.xakeps.jfx.gui.common.scene.StageManagerInjector;
import dk.xakeps.jfx.gui.common.scene.overlay.IndicatorOverlay;
import dk.xakeps.jfx.gui.common.scene.overlay.OverlayHandle;
import dk.xakeps.jfx.gui.common.view.View;
import dk.xakeps.jfx.gui.core.config.ConfigHolder;
import dk.xakeps.jfx.gui.core.inject.*;
import dk.xakeps.jfx.gui.core.launcher.GameLauncherBean;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.gui.core.profile.ProfileManagerFactory;
import dk.xakeps.jfx.gui.core.settings.SettingsProvider;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.user.User;
import javafx.application.Platform;
import javafx.util.Pair;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.prefs.Preferences;

public class Core {
    private static final System.Logger LOGGER = System.getLogger(Core.class.getName());

    private final ConfigHolder configHolder;
    private final ModuleProvider moduleProvider;
    private final GuiEntryPoint guiEntryPoint;

    private final DirectoryManager directoryManager;
    private final SettingsProvider settingsProvider;
    private final RemapperConfig remapperConfig;
    private StageManager stageManager;
    private Injector injector;

    public Core(ConfigHolder configHolder,
                ModuleProvider moduleProvider,
                GuiEntryPoint guiEntryPoint) {
        this.configHolder = Objects.requireNonNull(configHolder, "configHolder");
        this.moduleProvider = Objects.requireNonNull(moduleProvider, "moduleProvider");
        this.guiEntryPoint = Objects.requireNonNull(guiEntryPoint, "guiEntryPoint");

        String launcherId = configHolder.getLauncherId();
        // todo: per platform search, linux - xdg, mac - app data, windows - app data
        String directory = Preferences.userNodeForPackage(getClass()).node(launcherId).get("directory", null);
        if (directory == null) {
            directory = Paths.get(System.getProperty("user.home"), launcherId).toString();
        }
        Path rootPath = Paths.get(directory);
        this.directoryManager = new DirectoryManager(rootPath);
        this.settingsProvider = new SettingsProvider(directoryManager.getConfigDirectory());
        this.remapperConfig = configHolder.getRemapperConfigDto().fill(RemapperConfig.newBuilder())
                .pubKey(configHolder.getAuthlibPubKey())
                .outputPath(directoryManager.getLauncherDirectory().resolve("remapped"))
                .build();
    }

    public void start(StageManager stageManager) {
        if (this.stageManager != null) {
            throw new IllegalStateException("Already started");
        }

        LOGGER.log(System.Logger.Level.TRACE, "Creating injector");
        this.stageManager = stageManager;
        this.injector = Injector.newBuilder()
                .addInjector(new StageManagerInjector(stageManager))
                .addInjector(new DirectoryManagerInjector(directoryManager))
                .addInjector(new SettingsLoaderInjector(settingsProvider))
                .build();

        LOGGER.log(System.Logger.Level.DEBUG, "Starting the launcher");
        moduleProvider.loadAuthModule(injector).authenticate()
                .thenApplyAsync(this::enableWaitOverlay, Platform::runLater)
                .thenApplyAsync(this::createProfileManager)
                .thenComposeAsync(this::loadMainView)
                .thenAcceptAsync(this::enableMainView, Platform::runLater)
                .exceptionally(throwable -> {
                    LOGGER.log(System.Logger.Level.ERROR, "Unknown error during launcher startup sequence", throwable);
                    return null;
                });
    }

    private Pair<AuthInfo, OverlayHandle> enableWaitOverlay(AuthInfo authInfo) {
        LOGGER.log(System.Logger.Level.TRACE, "Enabling wait overlay");
        OverlayHandle overlayHandle = stageManager.getOverlayManager().addOverlay(IndicatorOverlay.INSTANCE);
        return new Pair<>(authInfo, overlayHandle);
    }

    private Pair<ProfileManager, OverlayHandle> createProfileManager(Pair<AuthInfo, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.DEBUG, "Creating profile manager");
        User user = moduleProvider.loadUserInfoModule().loadUser(pair.getKey().getAccessToken());
        ProfileManagerFactory factory = new ProfileManagerFactory(settingsProvider);
        ProfileManager profileManager = factory.createProfileManager(user, pair.getKey());
        try {
            profileManager.save();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Pair<>(profileManager, pair.getValue());
    }

    private CompletableFuture<Pair<View, OverlayHandle>> loadMainView(Pair<ProfileManager, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.DEBUG, "Loading main view");
        AuthInfo authInfo = pair.getKey().getAuthInfo();
        AccountInfoModule accountInfoModule = moduleProvider.loadAccountInfoModule(authInfo);

        LOGGER.log(System.Logger.Level.TRACE, "Loading manager wrappers");
        VersionManagerWrapper versionManagerWrapper = VersionManagerWrapper.create(configHolder.getConfig(), authInfo,
                directoryManager.getLauncherDirectory().resolve("versions"));
        ModPackManagerWrapper modPackManagerWrapper = ModPackManagerWrapper.create(authInfo, configHolder.getConfig(),
                directoryManager.getLauncherDirectory().resolve("modpacks"));
        UpdateManagerWrapper updateManagerWrapper = UpdateManagerWrapper.create(authInfo, configHolder.getConfig(),
                directoryManager.getLauncherDirectory().resolve("java"));

        GameLauncherBean gameLauncherBean = new GameLauncherBean(versionManagerWrapper,
                modPackManagerWrapper, updateManagerWrapper, directoryManager, pair.getKey(),
                new AuthlibRemapperReplacer(remapperConfig));

        LOGGER.log(System.Logger.Level.TRACE, "Creating injector");
        Injector injectorCopy = injector.copy()
                .addInjector(new ProfileManagerInjector(pair.getKey()))
                .addInjector(new ModPackManagerWrapperInjector(modPackManagerWrapper))
                .addInjector(new VersionManagerWrapperInjector(versionManagerWrapper))
                .addInjector(new UpdateManagerWrapperInjector(updateManagerWrapper))
                .addInjector(new AccountInfoModuleInjector(accountInfoModule))
                .addInjector(new GameLauncherBeanInjector(gameLauncherBean))
                .build();
        injectorCopy.inject(guiEntryPoint);
        return guiEntryPoint.loadMainGui().thenApply(view -> new Pair<>(view, pair.getValue()));
    }

    private void enableMainView(Pair<View, OverlayHandle> pair) {
        LOGGER.log(System.Logger.Level.DEBUG, "Showing main view and disabling wait overlay");
        stageManager.setCurrentView(pair.getKey());
        stageManager.getOverlayManager().disableOverlay(pair.getValue());
    }
}
