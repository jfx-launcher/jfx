package dk.xakeps.jfx.gui.core;

import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;

import java.nio.file.Path;
import java.util.Objects;

public final class DirectoryProviderImpl implements DirectoryProvider {
    private final DirectoryManager directoryManager;
    private final ProfileManager profileManager;
    private final GameVersion  gameVersion;
    private final ModPackVersion modPackVersion;

    public DirectoryProviderImpl(DirectoryManager directoryManager, ProfileManager profileManager, GameVersion gameVersion) {
        this(directoryManager, profileManager, gameVersion, null);
    }

    public DirectoryProviderImpl(DirectoryManager directoryManager,
                                 ProfileManager profileManager,
                                 GameVersion gameVersion,
                                 ModPackVersion modPackVersion) {
        this.directoryManager = Objects.requireNonNull(directoryManager, "directoryManager");
        this.profileManager = Objects.requireNonNull(profileManager, "profileManager");
        this.gameVersion = Objects.requireNonNull(gameVersion, "gameVersion");
        this.modPackVersion = modPackVersion;
    }

    @Override
    public Path getGameDir() {
        Path rootDir = directoryManager.getLauncherDirectory().resolve("run");

        Path gameDirectory = profileManager.getSelectedProfile().getGameDirectory();
        if (gameDirectory != null) {
            rootDir = gameDirectory;
        }

        if (modPackVersion != null) {
            return rootDir.resolve("modpack").resolve(modPackVersion.getModPackId());
        }

        return rootDir.resolve("version").resolve(gameVersion.getId());
    }

    @Override
    public Path getAssetsDir() {
        return directoryManager.getLauncherDirectory().resolve("assets");
    }

    @Override
    public Path getLibrariesDir() {
        return directoryManager.getLauncherDirectory().resolve("libraries");
    }

    @Override
    public Path getTempDir() {
        return directoryManager.getLauncherDirectory().resolve("temp");
    }

    @Override
    public Path getResourcePackDir() {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Path getDataPackDir() {
        throw new UnsupportedOperationException("not implemented");
    }
}
