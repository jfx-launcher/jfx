package dk.xakeps.jfx.gui.core;

import dk.xakeps.jfx.gui.common.view.View;

import java.util.concurrent.CompletableFuture;

public interface GuiEntryPoint {
    CompletableFuture<View> loadMainGui();
}
