package dk.xakeps.jfx.gui.core;

import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.auth.AuthModule;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.user.UserInfoModule;

public interface ModuleProvider {
    AuthModule loadAuthModule(Injector injector);
    UserInfoModule loadUserInfoModule();
    AccountInfoModule loadAccountInfoModule(AuthInfo authInfo);
}
