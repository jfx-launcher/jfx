package dk.xakeps.jfx.gui.core.config;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;

public class Config {
    @JsonProperty("modManifestUri")
    private final URI modManifestUri;
    @JsonProperty("modPackAdminUri")
    private final URI modPackAdminUri;
    @JsonProperty("versionManifestUri")
    private final URI versionManifestUri;
    @JsonProperty("versionManagerAdminUri")
    private final URI versionManagerAdminUri;
    @JsonProperty("updateServiceUri")
    private final URI updateServiceUri;

    @JsonCreator
    public Config(@JsonProperty("modManifestUri") URI modManifestUri,
                  @JsonProperty("modPackAdminUri") URI modPackAdminUri,
                  @JsonProperty("versionManifestUri") URI versionManifestUri,
                  @JsonProperty("versionManagerAdminUri") URI versionManagerAdminUri,
                  @JsonProperty("updateServiceUri") URI updateServiceUri) {
        this.modManifestUri = modManifestUri;
        this.modPackAdminUri = modPackAdminUri;
        this.versionManifestUri = versionManifestUri;
        this.versionManagerAdminUri = versionManagerAdminUri;
        this.updateServiceUri = updateServiceUri;
    }

    public URI getModManifestUri() {
        return modManifestUri;
    }

    public URI getModPackAdminUri() {
        return modPackAdminUri;
    }

    public URI getVersionManifestUri() {
        return versionManifestUri;
    }

    public URI getVersionManagerAdminUri() {
        return versionManagerAdminUri;
    }

    public URI getUpdateServiceUri() {
        return updateServiceUri;
    }
}
