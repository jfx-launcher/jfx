package dk.xakeps.jfx.gui.core.config;

import java.util.Objects;

public class ConfigHolder {
    private final String launcherId;
    private final RemapperConfigDto remapperConfigDto;
    private final Config config;
    private final byte[] authlibPubKey;

    private ConfigHolder(Builder builder) {
        this.launcherId = Objects.requireNonNull(builder.launcherId, "launcherId");
        this.remapperConfigDto = Objects.requireNonNull(builder.remapperConfigDto, "remapperConfigDto");
        this.config = Objects.requireNonNull(builder.config, "config");
        this.authlibPubKey = Objects.requireNonNull(builder.authlibPubKey, "authlibPubKey");
    }

    public String getLauncherId() {
        return launcherId;
    }

    public RemapperConfigDto getRemapperConfigDto() {
        return remapperConfigDto;
    }

    public Config getConfig() {
        return config;
    }

    public byte[] getAuthlibPubKey() {
        return authlibPubKey;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private String launcherId;
        private RemapperConfigDto remapperConfigDto;
        private Config config;
        private byte[] authlibPubKey;

        private Builder() {
        }

        public Builder launcherId(String launcherId) {
            this.launcherId = Objects.requireNonNull(launcherId, "launcherId");
            return this;
        }

        public Builder remapperConfigDto(RemapperConfigDto remapperConfigDto) {
            this.remapperConfigDto = Objects.requireNonNull(remapperConfigDto, "remapperConfigDto");
            return this;
        }

        public Builder config(Config config) {
            this.config = Objects.requireNonNull(config, "config");
            return this;
        }

        public Builder authlibPubKey(byte[] authlibPubKey) {
            this.authlibPubKey = Objects.requireNonNull(authlibPubKey, "authlibPubKey");
            return this;
        }

        public ConfigHolder build() {
            return new ConfigHolder(this);
        }
    }
}
