package dk.xakeps.jfx.gui.core.config;

import dk.xakeps.jfx.authlibremapper.RemapperConfig;

import java.util.List;

public record RemapperConfigDto(String joinUrl, String hasJoinedUrl, String profilesUrl,
                                List<String> allowedDomains,
                                List<String> blockedDomains, String profilesRepoUrl,
                                String privilegesUrl, String blockListUrl,
                                String legacyJoinUrl, String legacyHasJoinedUrl) {
    public RemapperConfig.Builder fill(RemapperConfig.Builder builder) {
        return builder.joinUrl(joinUrl)
                .hasJoinedUrl(hasJoinedUrl)
                .profilesUrl(profilesUrl)
                .allowedDomains(allowedDomains)
                .blockedDomains(blockedDomains)
                .profilesRepoUrl(profilesRepoUrl)
                .privilegesUrl(privilegesUrl)
                .blockListUrl(blockListUrl)
                .legacyJoinUrl(legacyJoinUrl)
                .legacyHasJoinedUrl(legacyHasJoinedUrl);
    }
}
