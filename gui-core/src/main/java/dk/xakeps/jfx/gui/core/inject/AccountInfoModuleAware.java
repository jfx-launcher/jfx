package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.account.AccountInfoModule;

public interface AccountInfoModuleAware {
    void setAccountInfoModule(AccountInfoModule accountInfoModule);
}
