package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.account.AccountInfoModule;
import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class AccountInfoModuleInjector implements InjectionSource<AccountInfoModule> {
    private final AccountInfoModule accountInfoModule;

    public AccountInfoModuleInjector(AccountInfoModule accountInfoModule) {
        this.accountInfoModule = Objects.requireNonNull(accountInfoModule, "accountInfoModule");
    }

    @Override
    public Class<AccountInfoModule> getInjectableClass() {
        return AccountInfoModule.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof AccountInfoModuleAware) {
            ((AccountInfoModuleAware) o).setAccountInfoModule(accountInfoModule);
        }
    }
}
