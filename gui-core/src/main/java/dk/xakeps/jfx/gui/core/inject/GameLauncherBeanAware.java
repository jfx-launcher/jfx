package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.launcher.GameLauncherBean;

public interface GameLauncherBeanAware {
    void setGameLauncherBean(GameLauncherBean gameLauncherBean);
}
