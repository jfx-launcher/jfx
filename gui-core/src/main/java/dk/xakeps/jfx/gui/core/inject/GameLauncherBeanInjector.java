package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.launcher.GameLauncherBean;
import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class GameLauncherBeanInjector implements InjectionSource<GameLauncherBean> {
    private final GameLauncherBean gameLauncherBean;

    public GameLauncherBeanInjector(GameLauncherBean gameLauncherBean) {
        this.gameLauncherBean = Objects.requireNonNull(gameLauncherBean, "gameLauncherBean");
    }

    @Override
    public Class<GameLauncherBean> getInjectableClass() {
        return GameLauncherBean.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof GameLauncherBeanAware) {
            ((GameLauncherBeanAware) o).setGameLauncherBean(gameLauncherBean);
        }
    }
}
