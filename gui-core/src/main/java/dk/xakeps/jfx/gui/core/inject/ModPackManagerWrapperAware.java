package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;

public interface ModPackManagerWrapperAware {
    void setModPackManagerWrapper(ModPackManagerWrapper modPackManagerWrapper);
}