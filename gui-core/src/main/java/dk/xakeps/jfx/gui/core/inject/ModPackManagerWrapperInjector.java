package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class ModPackManagerWrapperInjector implements InjectionSource<ModPackManagerWrapper> {
    private final ModPackManagerWrapper modPackManagerWrapper;

    public ModPackManagerWrapperInjector(ModPackManagerWrapper modPackManagerWrapper) {
        this.modPackManagerWrapper = Objects.requireNonNull(modPackManagerWrapper, "modPackManagerWrapper");
    }

    @Override
    public Class<ModPackManagerWrapper> getInjectableClass() {
        return ModPackManagerWrapper.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof ModPackManagerWrapperAware) {
            ((ModPackManagerWrapperAware) o).setModPackManagerWrapper(modPackManagerWrapper);
        }
    }
}
