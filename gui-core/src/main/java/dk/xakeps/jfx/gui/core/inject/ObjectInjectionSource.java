package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;
import java.util.Optional;

public class ObjectInjectionSource<A, T> implements InjectionSource<T> {
    private final Class<T> injectableClass;
    private final T instance;
    private final AwareExtractor<A> awareExtractor;
    private final SpecialInjector<A, T> specialInjector;

    private ObjectInjectionSource(Class<T> injectableClass,
                                  T instance,
                                  Class<A> awareClass,
                                  SpecialInjector<A, T> specialInjector) {
        this.injectableClass = Objects.requireNonNull(injectableClass, "injectableClass");
        this.instance = Objects.requireNonNull(instance, "instance");
        this.awareExtractor = new AwareExtractor<>(
                Objects.requireNonNull(awareClass, "awareExtractor"));
        this.specialInjector = Objects.requireNonNull(specialInjector, "specialInjector");
    }

    @Override
    public Class<T> getInjectableClass() {
        return injectableClass;
    }

    @Override
    public void inject(Object o) {
        awareExtractor.extract(o)
                .ifPresent(a -> specialInjector.inject(a, instance));
    }

    @FunctionalInterface
    interface SpecialInjector<A, T> {
        void inject(A aware, T object);
    }

    public static <A, T> InjectionSource<T> create(Class<T> injectableClass,
                                                   Class<A> awareClass,
                                                   T instance,
                                                   SpecialInjector<A, T> specialInjector) {
        Objects.requireNonNull(injectableClass, "injectableClass");
        Objects.requireNonNull(awareClass, "awareClass");
        Objects.requireNonNull(instance, "instance");
        Objects.requireNonNull(specialInjector, "specialInjector");
        return new ObjectInjectionSource<>(injectableClass, instance, awareClass, specialInjector);
    }

    private record AwareExtractor<A>(Class<A> awareClass) {
        private AwareExtractor {
            Objects.requireNonNull(awareClass, "awareClass");
        }

        public Optional<A> extract(Object o) {
            if (awareClass.isInstance(o)) {
                return Optional.of((A) o);
            } else {
                return Optional.empty();
            }
        }
    }
}
