package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.settings.SettingsProvider;
import dk.xakeps.jfx.injector.InjectionSource;
import dk.xakeps.jfx.settings.SettingsLoaderAware;
import dk.xakeps.jfx.settings.SettingsLoader;

import java.util.Objects;

public class SettingsLoaderInjector implements InjectionSource<SettingsLoader> {
    private final SettingsProvider settingsProvider;

    public SettingsLoaderInjector(SettingsProvider settingsProvider) {
        this.settingsProvider = Objects.requireNonNull(settingsProvider, "settingsProvider");
    }

    @Override
    public Class<SettingsLoader> getInjectableClass() {
        return SettingsLoader.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof SettingsLoaderAware) {
            SettingsLoader settingsLoader = settingsProvider.newLoader(o.getClass());
            ((SettingsLoaderAware) o).setSettingsLoader(settingsLoader);
        }
    }
}
