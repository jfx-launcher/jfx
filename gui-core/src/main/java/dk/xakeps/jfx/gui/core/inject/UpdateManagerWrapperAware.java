package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;

public interface UpdateManagerWrapperAware {
    void setUpdateManagerWrapper(UpdateManagerWrapper updateManagerWrapper);
}
