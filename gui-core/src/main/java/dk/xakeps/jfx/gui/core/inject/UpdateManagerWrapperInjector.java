package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class UpdateManagerWrapperInjector implements InjectionSource<UpdateManagerWrapper> {
    private final UpdateManagerWrapper updateManagerWrapper;

    public UpdateManagerWrapperInjector(UpdateManagerWrapper updateManagerWrapper) {
        this.updateManagerWrapper = Objects.requireNonNull(updateManagerWrapper, "updateManagerWrapper");
    }

    @Override
    public Class<UpdateManagerWrapper> getInjectableClass() {
        return UpdateManagerWrapper.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof UpdateManagerWrapperAware) {
            ((UpdateManagerWrapperAware) o).setUpdateManagerWrapper(updateManagerWrapper);
        }
    }
}
