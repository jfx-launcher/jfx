package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;

public interface VersionManagerWrapperAware {
    void setVersionManagerWrapper(VersionManagerWrapper versionManagerWrapper);
}
