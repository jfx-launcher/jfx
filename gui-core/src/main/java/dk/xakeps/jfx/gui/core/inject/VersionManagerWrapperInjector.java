package dk.xakeps.jfx.gui.core.inject;

import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.injector.InjectionSource;

import java.util.Objects;

public class VersionManagerWrapperInjector implements InjectionSource<VersionManagerWrapper> {
    private final VersionManagerWrapper versionManagerWrapper;

    public VersionManagerWrapperInjector(VersionManagerWrapper versionManagerWrapper) {
        this.versionManagerWrapper = Objects.requireNonNull(versionManagerWrapper, "versionManagerWrapper");
    }

    @Override
    public Class<VersionManagerWrapper> getInjectableClass() {
        return VersionManagerWrapper.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof VersionManagerWrapperAware) {
            ((VersionManagerWrapperAware) o).setVersionManagerWrapper(versionManagerWrapper);
        }
    }
}
