package dk.xakeps.jfx.gui.core.launcher;

import dk.xakeps.jfx.file.FileInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileCleaner {
    private final Set<Path> files;
    private final Set<String> ignoredPaths;

    public FileCleaner(Set<FileInfo> files, Set<String> ignoredPaths) {
        this.files = files.stream().map(FileInfo::getFile).collect(Collectors.toSet());
        this.ignoredPaths = ignoredPaths;
    }

    public void clean(Path cleanRoot) {
        try(Stream<Path> stream = Files.walk(cleanRoot)) {
            stream.filter(path -> !isIgnored(cleanRoot, path))
                    .filter(Predicate.not(this::modPackHasFile))
                    .sorted(FileCleaner::comparePaths)
                    .forEach(this::deleteFile);
        } catch (IOException e) {
            throw new RuntimeException("Error while cleaning paths", e);
        }
    }

    private boolean modPackHasFile(Path path) {
        if (Files.isRegularFile(path)) {
            return files.contains(path);
        }
        for (Path file : files) {
            if (file.startsWith(path)) {
                return true;
            }
        }
        return false;
    }

    private boolean isIgnored(Path root, Path path) {
        Path relativePath = root.relativize(path);
        for (String ignoredPath : ignoredPaths) {
            if (relativePath.startsWith(ignoredPath)) {
                return true;
            }
        }
        return false;
    }

    private void deleteFile(Path path) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            throw new RuntimeException("Can't delete path", e);
        }
    }

    private static int comparePaths(Path o1, Path o2) {
        if (Files.isDirectory(o1)) {
            return 1;
        } else if (Files.isDirectory(o2)) {
            return -1;
        }
        return 0;
    }
}
