package dk.xakeps.jfx.gui.core.launcher;

import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.gui.core.AuthlibRemapperReplacer;
import dk.xakeps.jfx.gui.core.Constants;
import dk.xakeps.jfx.gui.core.DirectoryManager;
import dk.xakeps.jfx.gui.core.DirectoryProviderImpl;
import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.gui.core.wrapper.ModPackManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.VersionManagerWrapper;
import dk.xakeps.jfx.gui.core.wrapper.update.UpdateManagerWrapper;
import dk.xakeps.jfx.updatemanager.UpdatePackage;
import dk.xakeps.jfx.updatemanager.UpdateVersion;
import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.GameLauncher;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackDownloader;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import dk.xakeps.jfx.platform.JavaExecutableNotFoundException;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class GameLauncherBean {
    private final VersionManagerWrapper versionManagerWrapper;
    private final ModPackManagerWrapper modPackManagerWrapper;
    private final UpdateManagerWrapper updateManagerWrapper;
    private final DirectoryManager directoryManager;
    private final ProfileManager profileManager;
    private final AuthlibRemapperReplacer authlibRemapperReplacer;
    private final PlatformInfoImpl platformInfo;

    public GameLauncherBean(VersionManagerWrapper versionManagerWrapper,
                            ModPackManagerWrapper modPackManagerWrapper,
                            UpdateManagerWrapper updateManagerWrapper,
                            DirectoryManager directoryManager,
                            ProfileManager profileManager,
                            AuthlibRemapperReplacer authlibRemapperReplacer) {
        this.versionManagerWrapper = Objects.requireNonNull(versionManagerWrapper, "versionManagerWrapper");
        this.modPackManagerWrapper = Objects.requireNonNull(modPackManagerWrapper, "modPackManagerWrapper");
        this.updateManagerWrapper = updateManagerWrapper;
        this.directoryManager = Objects.requireNonNull(directoryManager, "directoryManager");
        this.profileManager = Objects.requireNonNull(profileManager, "profileManager");
        this.authlibRemapperReplacer = Objects.requireNonNull(authlibRemapperReplacer, "authlibRemapperReplacer");
        this.platformInfo = new PlatformInfoImpl(profileManager);
    }

    public CompletableFuture<GameRunnable> prepareVersion(GameVersion gameVersion,
                                                          ProgressListenerFactory progressListenerFactory) {
        return CompletableFuture.supplyAsync(() -> prepareVersionInternal(
                gameVersion, progressListenerFactory)::start);
    }

    public CompletableFuture<GameRunnable> prepareModPack(ModPack modPack,
                                                          ProgressListenerFactory progressListenerFactory) {
        return CompletableFuture.supplyAsync(() -> prepareModPackInternal(
                modPack.getEnabledVersion().orElseThrow(), progressListenerFactory));
    }

    public CompletableFuture<GameRunnable> prepareModPack(ModPackVersion modPackVersion,
                                                          ProgressListenerFactory progressListenerFactory) {
        return CompletableFuture.supplyAsync(() -> prepareModPackInternal(modPackVersion, progressListenerFactory));
    }

    private GameRunnable prepareModPackInternal(ModPackVersion modPackVersion,
                                                ProgressListenerFactory progressListenerFactory) {
        GameVersion gameVersion = versionManagerWrapper.getVersion(modPackVersion.getMinecraftVersion()).orElseThrow();

        DirectoryProvider directoryProvider = new DirectoryProviderImpl(directoryManager, profileManager,
                gameVersion, modPackVersion);

        UpdateVersion javaVersion = getJavaVersion(modPackVersion);
        UpdateVersionDownloader.DownloadedUpdateVersion downloadedUpdateVersion =
                downloadJavaVersion(javaVersion, progressListenerFactory);
        Path installRoot = downloadedUpdateVersion.getInstallRoot();
        platformInfo.setJavaHome(tryExtractJavaHome(installRoot));

        ModPackDownloader.DownloadedModPack downloadedModPack = downloadModPack(directoryProvider,
                modPackVersion, progressListenerFactory);
        GameLauncher launcher = createLauncher(directoryProvider, gameVersion, progressListenerFactory);
        GameLauncher.PreparedGameLauncher preparedGameLauncher = prepareLauncher(launcher);
        FileCleaner cleaner = createCleaner(preparedGameLauncher, downloadedModPack, modPackVersion);
        cleaner.clean(directoryProvider.getGameDir());

        return preparedGameLauncher::start;
    }

    private GameLauncher.PreparedGameLauncher prepareVersionInternal(GameVersion gameVersion,
                                                                     ProgressListenerFactory progressListenerFactory) {
        updateManagerWrapper.getRemoteRegistry(Constants.JAVA_UPDATE_PACKAGE_ID).reload().join();
        UpdateVersion javaVersion = updateManagerWrapper.getUpdatePackage(
                Constants.JAVA_UPDATE_PACKAGE_ID, platformInfo.getArch(),
                platformInfo.getName()
        ).flatMap(UpdatePackage::getEnabledVersion).orElse(null);

        if (javaVersion == null) {
            throw new JavaExecutableNotFoundException(
                    String.format("Can't find default Java for platform: Arch=%s, System=%s",
                            platformInfo.getArch(), platformInfo.getName()));
        }

        UpdateVersionDownloader.DownloadedUpdateVersion downloadedUpdateVersion =
                downloadJavaVersion(javaVersion, progressListenerFactory);
        Path installRoot = downloadedUpdateVersion.getInstallRoot();
        platformInfo.setJavaHome(tryExtractJavaHome(installRoot));

        return prepareLauncher(
                createLauncher(new DirectoryProviderImpl(directoryManager, profileManager, gameVersion),
                        gameVersion, progressListenerFactory));
    }

    private GameLauncher.PreparedGameLauncher prepareLauncher(GameLauncher gameLauncher) {
        try {
            return gameLauncher.prepare();
        } catch (IOException e) {
            throw new RuntimeException("Can't prepare version", e);
        }
    }

    private GameLauncher createLauncher(DirectoryProvider directoryProvider,
                                        GameVersion gameVersion,
                                        ProgressListenerFactory progressListenerFactory) {
        return versionManagerWrapper.newLauncherBuilder()
                .directoryProvider(directoryProvider)
                .platformInfo(platformInfo)
                .gameVersion(gameVersion)
                .versionType("client")
                .user(profileManager.getAccountModel())
                .accessToken(profileManager.getAuthInfo().getAccessToken())
                .progressListenerFactory(progressListenerFactory)
                .libraryReplacer(authlibRemapperReplacer)
                .configureIO()
                .redirectErrorStream(true)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .redirectInput(ProcessBuilder.Redirect.INHERIT)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .finish()
                .build();
    }

    private UpdateVersionDownloader.DownloadedUpdateVersion downloadJavaVersion(UpdateVersion javaVersion,
                                                                                ProgressListenerFactory progressListenerFactory) {
        return updateManagerWrapper.newDownloaderBuilder()
                .updateVersion(javaVersion)
                .progressListenerFactory(progressListenerFactory)
                .build().download();
    }

    private ModPackDownloader.DownloadedModPack downloadModPack(DirectoryProvider directoryProvider,
                                                                ModPackVersion modPackVersion,
                                                                ProgressListenerFactory progressListenerFactory) {
        try {
            return modPackManagerWrapper.newDownloaderBuilder()
                    .downloadRoot(directoryProvider.getGameDir())
                    .progressListenerFactory(progressListenerFactory)
                    .modPackVersion(modPackVersion)
                    .build().download();
        } catch (IOException e) {
            throw new RuntimeException("Failed to download modpack", e);
        }
    }

    private FileCleaner createCleaner(GameLauncher.PreparedGameLauncher preparedGameLauncher,
                                      ModPackDownloader.DownloadedModPack downloadedModPack,
                                      ModPackVersion modPackVersion) {
        Set<FileInfo> gameFiles = preparedGameLauncher.getGameFiles();
        Set<FileInfo> modPackFiles = downloadedModPack.getFiles();

        Set<FileInfo> allFiles = new HashSet<>(gameFiles);
        allFiles.addAll(modPackFiles);

        return new FileCleaner(allFiles, modPackVersion.getIgnoredFiles());
    }

    private UpdateVersion getJavaVersion(ModPackVersion modPackVersion) {
        updateManagerWrapper.getRemoteRegistry(Constants.JAVA_UPDATE_PACKAGE_ID).reload().join();
        UpdateVersion javaVersion = updateManagerWrapper.getUpdateVersion(
                Constants.JAVA_UPDATE_PACKAGE_ID, platformInfo.getArch(),
                platformInfo.getName(), modPackVersion.getJavaVersion())
                .orElse(null);
        if (javaVersion == null) {
            throw new JavaExecutableNotFoundException(
                    String.format("Can't find Java for platform: Arch=%s, System=%s, Java=%s",
                            platformInfo.getArch(), platformInfo.getName(), modPackVersion.getJavaVersion()));
        }
        return javaVersion;
    }

    private Path tryExtractJavaHome(Path javaInstallPath) {
        try (DirectoryStream<Path> paths = Files.newDirectoryStream(javaInstallPath)) {
            Iterator<Path> iterator = paths.iterator();
            long count = 0;
            Path firstPath = null;
            if (iterator.hasNext()) {
                firstPath = iterator.next();
                count++;
            }
            if (count == 1) {
                return firstPath;
            }
            return javaInstallPath;
        } catch (IOException e) {
            throw new RuntimeException("Can't list java dir", e);
        }
    }
}
