package dk.xakeps.jfx.gui.core.launcher;

import java.io.IOException;

public interface GameRunnable {
    void start() throws IOException;
}
