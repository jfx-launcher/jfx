package dk.xakeps.jfx.gui.core.launcher;

import dk.xakeps.jfx.gui.core.profile.ProfileManager;
import dk.xakeps.jfx.platform.JavaExecutableNotFoundException;
import dk.xakeps.jfx.platform.PlatformInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.*;
import java.util.*;

public class PlatformInfoImpl implements PlatformInfo {
    private final ProfileManager profileManager;
    private Path javaExecutable;

    public PlatformInfoImpl(ProfileManager profileManager) {
        this.profileManager = profileManager;
    }

    @Override
    public Path getJavaExecutable() {
        if (this.javaExecutable != null) {
            return javaExecutable;
        }
        Path javaExecutable = profileManager.getSelectedProfile().getJavaExecutable();
        if (javaExecutable != null) {
            return javaExecutable;
        }
        return PlatformInfo.super.getJavaExecutable();
    }

    public void setJavaHome(Path javaHome) {
        this.javaExecutable = findJavaExecutable(javaHome);
        try {
            makeJavaExecutableExecutable(javaExecutable);
        } catch (IOException e) {
            throw new JavaExecutableNotFoundException("Can't make java executable", e);
        }
    }

    private Path findJavaExecutable(Path javaInstallRoot) {
        Path binDir = javaInstallRoot.toAbsolutePath().resolve("bin");
        if (WINDOWS.equals(getName())) {
            Path javawExe = binDir.resolve("javaw.exe");
            Path javaExe = binDir.resolve("java.exe");
            if (Files.isRegularFile(javawExe)) {
                return javawExe;
            } else if (Files.isRegularFile(javaExe)) {
                return javaExe;
            } else {
                throw new JavaExecutableNotFoundException("Executable not found. JAVA_HOME=" + javaInstallRoot);
            }
        }
        Path java = binDir.resolve("java");
        if (Files.isRegularFile(java)) {
            return java;
        } else {
            throw new JavaExecutableNotFoundException("Executable not found. JAVA_HOME=" + javaInstallRoot);
        }
    }

    public String getArch() {
        String property = Objects.requireNonNullElse(System.getenv("ARCH"), "").toLowerCase(Locale.ROOT);
        if (!property.isBlank()) {
            if (property.contains("amd64") || property.contains("x86_64")) {
                return x86_64;
            } else if (property.equals("x86")) {
                return x86;
            } else {
                return property;
            }
        } else {
            return PlatformInfo.super.getArch();
        }
    }

    private void makeJavaExecutableExecutable(Path javaExecutable) throws IOException {
        if (!Files.isExecutable(javaExecutable)) {
            PosixFileAttributeView posixAttributes = Files.getFileAttributeView(
                    javaExecutable, PosixFileAttributeView.class);
            if (posixAttributes != null) {
                PosixFileAttributes attributes = posixAttributes.readAttributes();
                Set<PosixFilePermission> permissions = attributes.permissions();
                permissions.add(PosixFilePermission.OWNER_EXECUTE);
                permissions.add(PosixFilePermission.GROUP_EXECUTE);
                permissions.add(PosixFilePermission.OTHERS_EXECUTE);
                posixAttributes.setPermissions(permissions);
            } else {
                if (!javaExecutable.toFile().setExecutable(true)) {
                    System.getLogger(PlatformInfoImpl.class.getName())
                            .log(System.Logger.Level.WARNING, "Can't make java runnable executable");
                }
            }
        }
    }
}
