/*
    This file is part of JFX Launcher
    Copyright (C) 2020 SHADOWDAN <chdanilpro@gmail.com>
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.gui.core.profile;

import com.fasterxml.jackson.core.type.TypeReference;
import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.gui.core.profile.dto.LauncherProfileModelDto;
import dk.xakeps.jfx.gui.core.profile.dto.LauncherProfilesDto;
import dk.xakeps.jfx.gui.core.settings.SettingsProvider;
import dk.xakeps.jfx.settings.Settings;
import dk.xakeps.jfx.settings.SettingsLoader;
import dk.xakeps.jfx.user.User;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class ProfileManagerFactory {
    private final SettingsLoader settingsLoader;

    public ProfileManagerFactory(SettingsProvider settingsProvider) {
        this.settingsLoader = settingsProvider.newLoader("profiles");
    }

    public ProfileManager createProfileManager(User accountModel, AuthInfo authInfo) {
        Settings<LauncherProfilesDto> settings = getSettings(accountModel);
        LauncherProfilesDto dto = settings.getObject();
        List<LauncherProfileModel> profiles = dto.getProfiles().stream()
                .map(LauncherProfileModelDto::asModel)
                .collect(Collectors.toList());
        LauncherProfileModel selectedProfile = profiles.stream()
                .filter(m -> m.getName().equals(dto.getSelectedProfile()))
                .findFirst().orElse(null);
        if (selectedProfile == null) {
            selectedProfile = new LauncherProfileModel();
            selectedProfile.setName(accountModel.getSelectedProfile().getName());
            profiles.add(selectedProfile);
        }
        ProfileManager profileManager = new ProfileManager(accountModel, authInfo, this);
        profileManager.getProfiles().addAll(profiles);
        profileManager.setSelectedProfile(selectedProfile);
        return profileManager;
    }

    void saveProfileManager(ProfileManager profileManager) throws IOException {
        LauncherProfilesDto dto = new LauncherProfilesDto(profileManager);
        Settings<LauncherProfilesDto> load = getSettings(profileManager.getAccountModel());
        load.setObject(dto);
        load.save();
    }

    private Settings<LauncherProfilesDto> getSettings(User accountModel) {
        Settings<LauncherProfilesDto> settings = getSettings(accountModel, new LauncherProfilesDto());
        try {
            settings.reload();
        } catch (IOException e) {
            /// todo: logger
            e.printStackTrace();
        }
        return settings;
    }

    private Settings<LauncherProfilesDto> getSettings(User accountModel, LauncherProfilesDto def) {
        return settingsLoader.load(
                Paths.get(accountModel.getSelectedProfile().getId()),
                def,
                new TypeReference<>() {}
        );
    }
}
