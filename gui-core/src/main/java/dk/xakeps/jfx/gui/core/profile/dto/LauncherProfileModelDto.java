/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.gui.core.profile.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.gui.core.profile.LauncherProfileModel;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class LauncherProfileModelDto {
    private final String name;
    private final Path gameDirectory;
    private final Path javaExecutable;
    private final List<String> enabledReleaseTypes;
    private final String lastPlayedVersion;

    public LauncherProfileModelDto(LauncherProfileModel profileModel) {
        this.name = profileModel.getName();
        this.gameDirectory = profileModel.getGameDirectory();
        this.javaExecutable = profileModel.getJavaExecutable();
        this.enabledReleaseTypes = new ArrayList<>(profileModel.getEnabledReleaseTypes());
        this.lastPlayedVersion = profileModel.getLastPlayedVersion();
    }

    @JsonCreator
    public LauncherProfileModelDto(@JsonProperty("name") String name,
                                   @JsonProperty("gameDirectory") Path gameDirectory,
                                   @JsonProperty("javaExecutable") Path javaExecutable,
                                   @JsonProperty("enabledReleaseTypes") List<String> enabledReleaseTypes,
                                   @JsonProperty("lastPlayedVersion") String lastPlayedVersion) {
        this.name = name;
        this.gameDirectory = gameDirectory;
        this.javaExecutable = javaExecutable;
        this.enabledReleaseTypes = Objects.requireNonNullElse(enabledReleaseTypes, Collections.emptyList());
        this.lastPlayedVersion = lastPlayedVersion;
    }

    public String getName() {
        return name;
    }

    public Path getGameDirectory() {
        return gameDirectory;
    }

    public Path getJavaExecutable() {
        return javaExecutable;
    }

    public List<String> getEnabledReleaseTypes() {
        return enabledReleaseTypes;
    }

    public String getLastPlayedVersion() {
        return lastPlayedVersion;
    }

    public LauncherProfileModel asModel() {
        LauncherProfileModel model = new LauncherProfileModel();
        model.setName(name);
        model.setGameDirectory(gameDirectory);
        model.setJavaExecutable(javaExecutable);
        model.getEnabledReleaseTypes().setAll(enabledReleaseTypes);
        model.setLastPlayedVersion(lastPlayedVersion);
        return model;
    }
}
