package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.modpackmanager.ModPackListener;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;

import java.util.Objects;

public class ModPackListenerImpl implements ModPackListener {
    private final ModPackManagerWrapper wrapper;

    public ModPackListenerImpl(ModPackManagerWrapper wrapper) {
        this.wrapper = Objects.requireNonNull(wrapper, "wrapper");
    }

    @Override
    public void onInstalled(ModPackVersion modPackVersion) {
        wrapper.getLocalRegistry().reload();
        wrapper.getRemoteRegistry().reload();
    }

    @Override
    public void onRemoved(ModPackVersion modPackVersion) {
        wrapper.getLocalRegistry().reload();
        wrapper.getRemoteRegistry().reload();
    }
}
