package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.gui.core.config.Config;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackAdmin;
import dk.xakeps.jfx.modpackmanager.ModPackDownloader;
import dk.xakeps.jfx.modpackmanager.ModPackManager;

import java.nio.file.Path;
import java.util.Optional;

public class ModPackManagerWrapper {
    private final ModPackManager modPackManager;
    private final ModPackAdmin modPackAdmin;

    private final ModPackRegistryWrapper remoteRegistry;
    private final ModPackRegistryWrapper localRegistry;

    private ModPackManagerWrapper(AuthInfo authInfo, Config config, Path metadataPath) {
        this.modPackManager = ModPackManager.newBuilder()
                .authInfo(authInfo)
                .modPackListener(new ModPackListenerImpl(this))
                .manifestUri(config.getModManifestUri())
                .metadataPath(metadataPath)
                .build();
        this.modPackAdmin = modPackManager.newModPackAdminBuilder()
                .serviceUri(config.getModPackAdminUri()).build();
        this.remoteRegistry = new ModPackRegistryWrapper(modPackManager.getRemoteRegistry());
        this.localRegistry = new ModPackRegistryWrapper(modPackManager.getLocalRegistry());
    }

    public ModPackRegistryWrapper getRemoteRegistry() {
        return remoteRegistry;
    }

    public ModPackRegistryWrapper getLocalRegistry() {
        return localRegistry;
    }

    public Optional<ModPack> getModPack(String modPackId) {
        return modPackManager.getRemoteRegistry().getModPackById(modPackId)
                .or(() -> modPackManager.getLocalRegistry().getModPackById(modPackId));
    }

    public ModPackDownloader.Builder newDownloaderBuilder() {
        return modPackManager.newDownloaderBuilder();
    }

    public ModPackAdmin getModPackAdmin() {
        return modPackAdmin;
    }

    public static ModPackManagerWrapper create(AuthInfo authInfo, Config config, Path metadataPath) {
        return new ModPackManagerWrapper(authInfo, config, metadataPath);
    }
}
