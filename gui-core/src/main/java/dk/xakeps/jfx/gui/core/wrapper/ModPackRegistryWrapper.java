package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackRegistry;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class ModPackRegistryWrapper {
    private final ModPackRegistry modPackRegistry;

    private final ListProperty<ModPack> modPacks;

    public ModPackRegistryWrapper(ModPackRegistry modPackRegistry) {
        this.modPackRegistry = Objects.requireNonNull(modPackRegistry, "modPackRegistry");
        this.modPacks = new SimpleListProperty<>(this, "versions", FXCollections.observableArrayList());
        reload();
    }

    public ObservableList<ModPack> getModPacks() {
        return modPacks.get();
    }

    public ListProperty<ModPack> modPacksProperty() {
        return modPacks;
    }

    public CompletableFuture<Void> reload() {
        return CompletableFuture.supplyAsync(this::reloadAndGetModPacks)
                .thenAcceptAsync(modPacks::setAll, Platform::runLater);
    }

    private List<ModPack> reloadAndGetModPacks() {
        modPackRegistry.reload();
        return modPackRegistry.getModPacks();
    }
}
