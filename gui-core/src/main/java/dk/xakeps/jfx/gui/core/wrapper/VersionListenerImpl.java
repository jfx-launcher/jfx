package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionListener;

import java.util.Objects;

public class VersionListenerImpl implements VersionListener {
    private final VersionManagerWrapper wrapper;

    public VersionListenerImpl(VersionManagerWrapper wrapper) {
        this.wrapper = Objects.requireNonNull(wrapper, "wrapper");
    }

    @Override
    public void onInstalled(GameVersion gameVersion) {
        wrapper.getLocalRegistry().reload();
        wrapper.getRemoteRegistry().reload();
    }

    @Override
    public void onRemoved(GameVersion gameVersion) {
        wrapper.getLocalRegistry().reload();
        wrapper.getRemoteRegistry().reload();
    }
}
