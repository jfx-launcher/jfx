package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.gui.core.config.Config;
import dk.xakeps.jfx.launcher.*;

import java.nio.file.Path;
import java.util.Optional;

public class VersionManagerWrapper {
    private final VersionManager versionManager;
    private final VersionManagerAdmin versionManagerAdmin;

    private final VersionRegistryWrapper remoteRegistry;
    private final VersionRegistryWrapper localRegistry;

    private VersionManagerWrapper(Config config, AuthInfo authInfo, Path versionsDir) {
        this.versionManager = VersionManager.newBuilder()
                .versionsDir(versionsDir)
                .versionListener(new VersionListenerImpl(this))
                .manifestsUri(config.getVersionManifestUri())
                .build();
        this.versionManagerAdmin = versionManager.newVersionManagerAdminBuilder()
                .serviceUri(config.getVersionManagerAdminUri())
                .accessToken(authInfo.getAccessToken())
                .build();
        this.remoteRegistry = new VersionRegistryWrapper(versionManager.getRemoteRegistry());
        this.localRegistry = new VersionRegistryWrapper(versionManager.getLocalRegistry());
    }

    public VersionRegistryWrapper getRemoteRegistry() {
        return remoteRegistry;
    }

    public VersionRegistryWrapper getLocalRegistry() {
        return localRegistry;
    }

    public Optional<GameVersion> getVersion(String id) {
        return versionManager.getVersion(id);
    }

    private static Optional<GameVersion> getVersion(VersionManager versionManager, String id) {
        return versionManager.getRemoteRegistry().getVersionById(id)
                .or(() -> versionManager.getLocalRegistry().getVersionById(id));
    }

    public GameLauncher.Builder newLauncherBuilder() {
        return versionManager.newLauncherBuilder();
    }

    public VersionManagerAdmin getVersionManagerAdmin() {
        return versionManagerAdmin;
    }

    public static VersionManagerWrapper create(Config config, AuthInfo authInfo, Path versionsDir) {
        return new VersionManagerWrapper(config, authInfo, versionsDir);
    }
}
