package dk.xakeps.jfx.gui.core.wrapper;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionRegistry;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class VersionRegistryWrapper {
    private final VersionRegistry versionRegistry;

    private final ListProperty<GameVersion> versions;

    public VersionRegistryWrapper(VersionRegistry versionRegistry) {
        this.versionRegistry = Objects.requireNonNull(versionRegistry, "versionRegistry");
        this.versions = new SimpleListProperty<>(this, "versions", FXCollections.observableArrayList());
        reload();
    }

    public ObservableList<GameVersion> getVersions() {
        return versions.get();
    }

    public ListProperty<GameVersion> versionsProperty() {
        return versions;
    }

    public CompletableFuture<Void> reload() {
        return CompletableFuture.supplyAsync(this::reloadAndGetVersions)
                .thenAcceptAsync(versions::setAll, Platform::runLater);
    }

    private List<GameVersion> reloadAndGetVersions() {
        versionRegistry.reload();
        return versionRegistry.getVersions();
    }
}