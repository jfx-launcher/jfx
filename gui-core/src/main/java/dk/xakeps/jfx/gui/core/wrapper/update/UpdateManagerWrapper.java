package dk.xakeps.jfx.gui.core.wrapper.update;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.gui.core.config.Config;
import dk.xakeps.jfx.updatemanager.*;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class UpdateManagerWrapper {
    private final UpdateManager updateManager;
    private final UpdateManagerAdmin updateManagerAdmin;

    private final Map<String, UpdateRegistryWrapper> remoteRegistries;
    private final Map<String, UpdateRegistryWrapper> localRegistries;

    private final ListProperty<String> updatePackageTypes;

    private UpdateManagerWrapper(AuthInfo authInfo, Config config, Path metadataPath) {
        this.updateManager = UpdateManager.newBuilder()
                .updateVersionListener(new UpdateVersionListenerImpl(this))
                .manifestUri(config.getUpdateServiceUri())
                .metadataPath(metadataPath)
                .build();
        this.updateManagerAdmin = updateManager.newUpdateManagerAdminBuilder()
                .accessToken(authInfo.getAccessToken())
                .serviceUri(config.getUpdateServiceUri())
                .build();

        this.updatePackageTypes = new SimpleListProperty<>(this, "updatePackageTypes",
                FXCollections.observableArrayList());

        this.remoteRegistries = new HashMap<>();
        this.localRegistries = new HashMap<>();
        reload().exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        });
    }

    public UpdateRegistryWrapper getRemoteRegistry(String updatePackageType) {
        if (!updatePackageTypes.contains(updatePackageType)) {
            throw new UpdateManagerException("Registry not found: " + updatePackageType, null);
        }
        return remoteRegistries.computeIfAbsent(updatePackageType,
                s -> new UpdateRegistryWrapper(updateManager.getRemoteRegistry(s)));
    }

    public UpdateRegistryWrapper getLocalRegistry(String updatePackageType) {
        if (!updatePackageTypes.contains(updatePackageType)) {
            throw new UpdateManagerException("Registry not found: " + updatePackageType, null);
        }
        return localRegistries.computeIfAbsent(updatePackageType,
                s -> new UpdateRegistryWrapper(updateManager.getLocalRegistry(s)));
    }

    public Optional<UpdateVersion> getUpdateVersion(String updatePackageType, String arch, String system, String version) {
        return updateManager.getRemoteRegistry(updatePackageType).getUpdateVersion(arch, system, version)
                .or(() -> updateManager.getLocalRegistry(updatePackageType).getUpdateVersion(arch, system, version));
    }

    public Optional<UpdatePackage> getUpdatePackage(String updatePackageType, String arch, String system) {
        return updateManager.getRemoteRegistry(updatePackageType).getUpdatePackage(arch, system)
                .or(() -> updateManager.getLocalRegistry(updatePackageType).getUpdatePackage(arch, system));
    }

    public UpdateVersionDownloader.Builder newDownloaderBuilder() {
        return updateManager.newDownloaderBuilder();
    }

    public UpdateManagerAdmin getUpdateManagerAdmin() {
        return updateManagerAdmin;
    }

    public ObservableList<String> getUpdatePackageTypes() {
        return updatePackageTypes.get();
    }

    public ListProperty<String> updatePackageTypesProperty() {
        return updatePackageTypes;
    }

    public CompletableFuture<Void> reload() {
        return CompletableFuture.runAsync(updateManager::reload)
                .thenRunAsync(this::updateProperties, Platform::runLater);
    }

    private void updateProperties() {
        this.updatePackageTypes.setAll(updateManager.getUpdatePackageTypes());
    }

    public static UpdateManagerWrapper create(AuthInfo authInfo, Config config, Path metadataPath) {
        return new UpdateManagerWrapper(authInfo, config, metadataPath);
    }
}
