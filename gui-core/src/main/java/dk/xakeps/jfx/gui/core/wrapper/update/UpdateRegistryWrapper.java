package dk.xakeps.jfx.gui.core.wrapper.update;

import dk.xakeps.jfx.updatemanager.UpdatePackage;
import dk.xakeps.jfx.updatemanager.UpdateRegistry;
import dk.xakeps.jfx.updatemanager.UpdateVersion;
import javafx.application.Platform;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class UpdateRegistryWrapper {
    private final UpdateRegistry updateRegistry;

    private final ListProperty<UpdateVersion> updateVersions;
    private final ListProperty<UpdatePackage> updatePackages;

    public UpdateRegistryWrapper(UpdateRegistry updateRegistry) {
        this.updateRegistry = Objects.requireNonNull(updateRegistry, "updateRegistry");
        this.updateVersions = new SimpleListProperty<>(this, "versions", FXCollections.observableArrayList());
        this.updatePackages = new SimpleListProperty<>(this, "updatePackages", FXCollections.observableArrayList());
        reload().exceptionally(throwable -> {
            throwable.printStackTrace();
            return null;
        });
    }

    public ObservableList<UpdateVersion> getUpdateVersions() {
        return updateVersions.get();
    }

    public ListProperty<UpdateVersion> updateVersionsProperty() {
        return updateVersions;
    }

    public ObservableList<UpdatePackage> getUpdatePackages() {
        return updatePackages.get();
    }

    public ListProperty<UpdatePackage> updatePackagesProperty() {
        return updatePackages;
    }

    public CompletableFuture<Void> reload() {
        return CompletableFuture.runAsync(updateRegistry::reload)
                .thenRunAsync(this::updateProperties, Platform::runLater);
    }

    private void updateProperties() {
        updateVersions.setAll(updateRegistry.getUpdateVersions());
        updatePackages.setAll(updateRegistry.getUpdatePackages());
    }
}
