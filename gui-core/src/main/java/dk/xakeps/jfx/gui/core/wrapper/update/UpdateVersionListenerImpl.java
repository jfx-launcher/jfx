package dk.xakeps.jfx.gui.core.wrapper.update;

import dk.xakeps.jfx.updatemanager.UpdateVersionListener;
import dk.xakeps.jfx.updatemanager.UpdateVersion;

import java.util.Objects;

public class UpdateVersionListenerImpl implements UpdateVersionListener {
    private final UpdateManagerWrapper wrapper;

    public UpdateVersionListenerImpl(UpdateManagerWrapper wrapper) {
        this.wrapper = Objects.requireNonNull(wrapper, "wrapper");
    }

    @Override
    public void onInstalled(UpdateVersion updateVersion) {
        wrapper.getLocalRegistry(updateVersion.getUpdatePackageType()).reload();
        wrapper.getRemoteRegistry(updateVersion.getUpdatePackageType()).reload();
    }

    @Override
    public void onRemoved(UpdateVersion updateVersion) {
        wrapper.getLocalRegistry(updateVersion.getUpdatePackageType()).reload();
        wrapper.getRemoteRegistry(updateVersion.getUpdatePackageType()).reload();
    }
}
