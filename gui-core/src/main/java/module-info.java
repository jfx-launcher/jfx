import dk.xakeps.jfx.account.AccountInfoModuleProvider;
import dk.xakeps.jfx.auth.AuthModuleProvider;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.modpackmanager.ModPackManager;
import dk.xakeps.jfx.settings.SettingsLoaderFactory;
import dk.xakeps.jfx.user.UserInfoModuleProvider;

module dk.xakeps.jfx.gui.core {
    requires transitive dk.xakeps.jfx.gui.common;
    requires transitive dk.xakeps.jfx.launcher;
    requires transitive dk.xakeps.jfx.modpackmanager;
    requires transitive dk.xakeps.jfx.updatemanager;
    requires transitive dk.xakeps.jfx.auth;
    requires transitive dk.xakeps.jfx.account;
    requires transitive dk.xakeps.jfx.user;
    requires transitive dk.xakeps.jfx.settings;
    requires transitive dk.xakeps.jfx.authlibremapper;
    requires transitive com.fasterxml.jackson.databind;

    requires java.prefs;

    exports dk.xakeps.jfx.gui.core;
    exports dk.xakeps.jfx.gui.core.config;
    exports dk.xakeps.jfx.gui.core.inject;
    exports dk.xakeps.jfx.gui.core.launcher;
    exports dk.xakeps.jfx.gui.core.profile;
    exports dk.xakeps.jfx.gui.core.settings;
    exports dk.xakeps.jfx.gui.core.wrapper;
    exports dk.xakeps.jfx.gui.core.wrapper.update;

    opens dk.xakeps.jfx.gui.core.config to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.gui.core.profile.dto to com.fasterxml.jackson.databind;

    uses AuthModuleProvider;
    uses AccountInfoModuleProvider;
    uses UserInfoModuleProvider;
    uses VersionManager.Builder;
    uses ModPackManager.Builder;
    uses SettingsLoaderFactory;
}