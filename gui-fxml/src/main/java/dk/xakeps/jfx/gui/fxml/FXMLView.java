package dk.xakeps.jfx.gui.fxml;

import dk.xakeps.jfx.gui.common.view.View;
import dk.xakeps.jfx.injector.Injector;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;

public interface FXMLView extends View {
    Object getController();

    static CompletableFuture<FXMLView> load(URL viewUrl, ResourceBundle bundle, Injector injector) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                FXMLLoader loader = new FXMLLoader(viewUrl, bundle, null, param -> {
                    Object controller;
                    try {
                        controller = param.getDeclaredConstructor().newInstance();
                    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | InstantiationException e) {
                        throw new ViewLoadException("Can't create controller", e);
                    }

                    injector.inject(controller);
                    return controller;
                });

                Node content = loader.load();
                Object controller = loader.getController();
                return new FXMLView() {
                    @Override
                    public Object getController() {
                        return controller;
                    }

                    @Override
                    public Node getContent() {
                        return content;
                    }
                };
            } catch (IOException e) {
                throw new ViewLoadException("Error loading FXML", e);
            }
        });
    }
}
