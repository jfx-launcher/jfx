

module dk.xakeps.jfx.gui.fxml {
    requires transitive dk.xakeps.jfx.gui.common;
    requires transitive javafx.fxml;

    exports dk.xakeps.jfx.gui.fxml;
}