package dk.xakeps.jfx.httpclient.utils;

import java.net.http.HttpResponse;
import java.util.Objects;
import java.util.function.Predicate;

public class ExtraBodyHandlers {
    public static <R, E> HttpResponse.BodyHandler<Result<R, E>> ofResponseOrError(
            Predicate<HttpResponse.ResponseInfo> errorPredicate,
            HttpResponse.BodyHandler<R> responseHandler,
            HttpResponse.BodyHandler<E> errorHandler) {
        Objects.requireNonNull(errorPredicate);
        Objects.requireNonNull(responseHandler);
        Objects.requireNonNull(errorHandler);

        return responseInfo -> {
            if (errorPredicate.test(responseInfo)) {
                return HttpResponse.BodySubscribers.mapping(errorHandler.apply(responseInfo), Result::ofError);
            } else {
                return HttpResponse.BodySubscribers.mapping(responseHandler.apply(responseInfo), Result::ofResponse);
            }
        };
    }
}
