package dk.xakeps.jfx.httpclient.utils;

import java.util.Optional;

public final class Result<R, E> {
    private final R response;
    private final E error;

    private Result(R response, E error) {
        this.response = response;
        this.error = error;
    }

    public Optional<R> response() {
        return Optional.ofNullable(response);
    }

    public Optional<E> error() {
        return Optional.ofNullable(error);
    }

    public static <T, E> Result<T, E> ofError(E e) {
        return new Result<>(null, e);
    }

    public static <T, E> Result<T, E> ofResponse(T t) {
        return new Result<>(t, null);
    }
}
