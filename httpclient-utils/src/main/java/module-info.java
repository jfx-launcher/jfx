module dk.xakeps.jfx.httpclient.utils {
    requires transitive java.net.http;

    exports dk.xakeps.jfx.httpclient.utils;
}