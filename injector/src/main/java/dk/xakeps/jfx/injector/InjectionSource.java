package dk.xakeps.jfx.injector;

public interface InjectionSource<T> {
    Class<T> getInjectableClass();
    void inject(Object o);
}
