package dk.xakeps.jfx.injector;

import java.util.ServiceLoader;

public interface Injector {
    void inject(Object o);

    Builder copy();

    static Builder newBuilder() {
        return ServiceLoader.load(Builder.class)
                .findFirst()
                .orElseThrow();
    }

    interface Builder {
        <T> Builder addInjector(InjectionSource<T> injectionSource);
        Injector build();
    }
}