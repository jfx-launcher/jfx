package dk.xakeps.jfx.injector;

public interface InjectorAware {
    void setInjector(Injector injector);
}
