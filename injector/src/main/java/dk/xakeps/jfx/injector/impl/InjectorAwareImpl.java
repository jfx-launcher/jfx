package dk.xakeps.jfx.injector.impl;

import dk.xakeps.jfx.injector.InjectionSource;
import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.InjectorAware;

import java.util.Objects;
import java.util.function.Consumer;

public class InjectorAwareImpl implements InjectionSource<Injector> {
    private final Injector injector;

    public InjectorAwareImpl(Injector injector) {
        this.injector = Objects.requireNonNull(injector, "injector");
    }

    @Override
    public Class<Injector> getInjectableClass() {
        return Injector.class;
    }

    @Override
    public void inject(Object o) {
        if (o instanceof InjectorAware) {
            ((InjectorAware) o).setInjector(injector);
        }
    }
}
