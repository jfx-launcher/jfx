package dk.xakeps.jfx.injector.impl;

import dk.xakeps.jfx.injector.InjectionSource;
import dk.xakeps.jfx.injector.Injector;

import java.util.*;
import java.util.function.Consumer;

public class InjectorBuilderImpl implements Injector.Builder {
    private final Map<Class<?>, InjectionSource<?>> injectors = new HashMap<>();

    public InjectorBuilderImpl() {
    }

    public InjectorBuilderImpl(Map<Class<?>, InjectionSource<?>> injectors) {
        this.injectors.putAll(injectors);
    }

    @Override
    public <T> Injector.Builder addInjector(InjectionSource<T> injectionSource) {
        Objects.requireNonNull(injectionSource, "injectionSource");
        Objects.requireNonNull(injectionSource.getInjectableClass(), "injectableClass");
        this.injectors.put(injectionSource.getInjectableClass(), injectionSource);
        return this;
    }

    @Override
    public Injector build() {
        return new InjectorImpl(injectors);
    }
}
