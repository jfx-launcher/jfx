package dk.xakeps.jfx.injector.impl;

import dk.xakeps.jfx.injector.InjectionSource;
import dk.xakeps.jfx.injector.Injector;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class InjectorImpl implements Injector {
    private final Map<Class<?>, InjectionSource<?>> injectors;

    public InjectorImpl(Map<Class<?>, InjectionSource<?>> injectors) {
        Map<Class<?>, InjectionSource<?>> copy = new HashMap<>(injectors);
        copy.put(Injector.class, new InjectorAwareImpl(this));
        this.injectors = Map.copyOf(copy);
    }

    @Override
    public void inject(Object o) {
        for (InjectionSource<?> injector : injectors.values()) {
            injector.inject(o);
        }
    }

    @Override
    public Builder copy() {
        return new InjectorBuilderImpl(injectors);
    }
}
