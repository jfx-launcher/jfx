import dk.xakeps.jfx.injector.Injector;
import dk.xakeps.jfx.injector.impl.InjectorBuilderImpl;

module dk.xakeps.jfx.injector {
    exports dk.xakeps.jfx.injector;

    uses Injector.Builder;

    provides Injector.Builder with InjectorBuilderImpl;
}