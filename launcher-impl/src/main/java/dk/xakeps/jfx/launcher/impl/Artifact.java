/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Objects;

public class Artifact {
    @JsonProperty("sha1")
    private final String sha1;
    @JsonProperty("size")
    private final long size;
    @JsonProperty("url")
    private final URI uri;

    @JsonCreator
    public Artifact(@JsonProperty("sha1") String sha1,
                    @JsonProperty("size") long size,
                    @JsonProperty("url") URI uri) {
        this.sha1 = Objects.requireNonNullElse(sha1, "");
        this.size = size;
        this.uri = uri;
    }

    public String getSha1() {
        return sha1;
    }

    public long getSize() {
        return size;
    }

    public URI getUri() {
        return uri;
    }
}
