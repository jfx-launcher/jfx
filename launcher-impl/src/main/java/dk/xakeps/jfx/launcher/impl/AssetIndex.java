/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.downloader.DownloadFileInfo;

import java.net.URI;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class AssetIndex {
    @JsonProperty("objects")
    private final Map<String, Asset> objects;
    @JsonProperty("map_to_resources")
    private final boolean mapToResources;
    @JsonProperty("virtual")
    private final boolean virtual;
    @JsonIgnore
    private Set<DownloadFileInfo> downloads = new HashSet<>();

    @JsonCreator
    public AssetIndex(@JsonProperty("objects") Map<String, Asset> objects,
                      @JsonProperty("map_to_resources") boolean mapToResources,
                      @JsonProperty("virtual") boolean virtual) {
        this.objects = objects;
        this.mapToResources = mapToResources;
        this.virtual = virtual;
    }

    public Map<String, Asset> getObjects() {
        return objects;
    }

    public boolean isMapToResources() {
        return mapToResources;
    }

    public boolean isVirtual() {
        return virtual;
    }

    public Set<DownloadFileInfo> collectDownloads(Path assetsDir, URI resourcesUri) {
        if (!downloads.isEmpty()) {
            return downloads;
        }
        Path objectsDir = assetsDir.resolve("objects");
        this.objects.values().stream()
                .distinct()
                .map(a -> new AssetDownloadFileInfo(a, resourcesUri, objectsDir))
                .forEach(downloads::add);
        return downloads;
    }

    public static class Asset {
        private final String hash;
        private final long size;

        @JsonCreator
        public Asset(@JsonProperty("hash") String hash,
                     @JsonProperty("size") long size) {
            this.hash = hash;
            this.size = size;
        }

        public String getHash() {
            return hash;
        }

        public long getSize() {
            return size;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Asset asset = (Asset) o;
            return size == asset.size &&
                    Objects.equals(hash, asset.hash);
        }

        @Override
        public int hashCode() {
            return Objects.hash(hash, size);
        }
    }

    private static class AssetDownloadFileInfo implements DownloadFileInfo {
        private final Asset asset;
        private final URI source;
        private final Path file;

        public AssetDownloadFileInfo(Asset asset, URI resourcesUri, Path objectsDir) {
            this.asset = asset;
            String path = calculatePath(asset);
            this.source = resourcesUri.resolve(path);
            this.file = objectsDir.resolve(path);
        }

        @Override
        public String getName() {
            return "Asset";
        }

        @Override
        public URI getSource() {
            return source;
        }

        @Override
        public Path getFile() {
            return file;
        }

        @Override
        public long getSize() {
            return asset.getSize();
        }

        @Override
        public String getHash() {
            return asset.getHash();
        }

        @Override
        public String getHashAlgorithm() {
            return "SHA-1";
        }

        private static String calculatePath(Asset asset) {
            return asset.getHash().substring(0, 2) + "/" + asset.getHash();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AssetDownloadFileInfo that = (AssetDownloadFileInfo) o;
            return Objects.equals(file, that.file);
        }

        @Override
        public int hashCode() {
            return Objects.hash(file);
        }
    }
}
