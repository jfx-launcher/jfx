/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;

public class AssetIndexArtifact extends Artifact {
    @JsonProperty("id")
    private final String id;
    @JsonProperty("totalSize")
    private final long totalSize;

    @JsonCreator
    public AssetIndexArtifact(@JsonProperty("id") String id,
                              @JsonProperty("sha1") String sha1,
                              @JsonProperty("size") long size,
                              @JsonProperty("totalSize") long totalSize,
                              @JsonProperty("url") URI uri) {
        super(sha1, size, uri);
        this.id = id;
        this.totalSize = totalSize;
    }

    public String getId() {
        return id;
    }

    public long getTotalSize() {
        return totalSize;
    }
}
