/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import java.net.URI;

/**
 * Some Constants used in launcher
 */
public class Constants {
    public static final String LAUNCHER_ID_PROPERTY = "launcher.impl.id";
    private static final String DEFAULT_LAUNCHER_ID = "JFX Launcher";
    public static final String LAUNCHER_ID = System.getProperty(LAUNCHER_ID_PROPERTY, DEFAULT_LAUNCHER_ID);

    public static final String LAUNCHER_VERSION_PROPERTY = "launcher.impl.version";
    private static final String DEFAULT_LAUNCHER_VERSION = "1.0";
    public static final String LAUNCHER_VERSION = System.getProperty(LAUNCHER_VERSION_PROPERTY, DEFAULT_LAUNCHER_VERSION);

    public static final String RESOURCES_URI_PROPERTY = "launcher.impl.download.resources_uri";
    private static final String DEFAULT_RESOURCES_URI = "http://resources.download.minecraft.net/";
    public static final URI RESOURCE_URI = URI.create(System.getProperty(RESOURCES_URI_PROPERTY, DEFAULT_RESOURCES_URI));

    public static final String MAVEN_REPO_URI_PROPERTY = "launcher.impl.maven_repo_uri";
    private static final String DEFAULT_MAVEN_REPO_URI = "https://libraries.minecraft.net/";
    public static final URI MAVEN_REPO_URI = URI.create(System.getProperty(MAVEN_REPO_URI_PROPERTY, DEFAULT_MAVEN_REPO_URI));

    public static final String MANIFESTS_URI_PROPERTY = "launcher.impl.versions_manifest_uri";
    private static final String DEFAULT_MANIFESTS_URI = "https://launchermeta.mojang.com/mc/game/version_manifest.json";
    public static final URI MANIFESTS_URI = URI.create(System.getProperty(MANIFESTS_URI_PROPERTY, DEFAULT_MANIFESTS_URI));
}
