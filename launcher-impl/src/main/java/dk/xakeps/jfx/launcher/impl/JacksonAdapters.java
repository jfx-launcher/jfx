package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mizosoft.methanol.adapter.ForwardingDecoder;
import com.github.mizosoft.methanol.adapter.ForwardingEncoder;
import com.github.mizosoft.methanol.adapter.jackson.JacksonAdapterFactory;

public class JacksonAdapters {
    private static final ObjectMapper mapper = ObjectMapperHolder.OBJECT_MAPPER;

    public static class JacksonEncoder extends ForwardingEncoder {
        public JacksonEncoder() {
            super(JacksonAdapterFactory.createEncoder(mapper));
        }
    }

    public static class JacksonDecoder extends ForwardingDecoder {
        public JacksonDecoder() {
            super(JacksonAdapterFactory.createDecoder(mapper));
        }
    }
}