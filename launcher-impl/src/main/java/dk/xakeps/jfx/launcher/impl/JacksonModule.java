/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.databind.module.SimpleModule;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.argument.ArgumentDeserializer;
import dk.xakeps.jfx.launcher.impl.argument.ArgumentTargetDeserializer;
import dk.xakeps.jfx.launcher.impl.compat.*;
import dk.xakeps.jfx.user.Property;

public class JacksonModule extends SimpleModule {
    {
        addDeserializer(Argument.class, new ArgumentDeserializer());
        addSerializer(ListCompatRule.class, new ListCompatRuleSerializer());
        addDeserializer(ListCompatRule.class, new ListCompatRuleDeserializer());
        addSerializer(EmptyCompatRule.class, new EmptyCompatRuleSerializer());
        addSerializer(Property.class, new PropertySerializer());
        addDeserializer(ArgumentTarget.class, new ArgumentTargetDeserializer());
    }
}
