/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.argument;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.impl.compat.EmptyCompatRule;
import dk.xakeps.jfx.launcher.impl.compat.ListCompatRule;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ArgumentDeserializer extends JsonDeserializer<Argument> {
    @Override
    public Argument deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonToken currentToken = p.currentToken();
        if (currentToken == JsonToken.VALUE_STRING) { // argument may be plain string
            return new Argument(Collections.singletonList(p.getText()), EmptyCompatRule.INSTANCE);
        } else if (currentToken == JsonToken.START_OBJECT) { // argument also may be an object
            CompatRule rule = EmptyCompatRule.INSTANCE;
            List<String> arguments = Collections.emptyList();

            while (p.nextToken() == JsonToken.FIELD_NAME) {
                String fieldName = p.currentName();
                if (fieldName.equals("rules")) {
                    if (p.nextToken() != JsonToken.START_ARRAY) {
                        throw ctxt.wrongTokenException(p, Argument.class, JsonToken.START_ARRAY, "Expected rules array start");
                    }
                    rule = p.readValueAs(ListCompatRule.class);
                } else if (fieldName.equals("value")) {
                    arguments = readArgs(p, ctxt);
                } else {
                    ctxt.handleUnknownProperty(p, this, Argument.class, fieldName);
                }
            }

            return new Argument(arguments, rule);
        } else {
            throw ctxt.wrongTokenException(p, Argument.class, JsonToken.START_OBJECT, "Expected VALUE_STRING or START_OBJECT");
        }
    }

    // reads field "value" of an argument
    private List<String> readArgs(JsonParser p, DeserializationContext ctxt) throws IOException {
        List<String> arguments;
        JsonToken token = p.nextToken();
        if (token == JsonToken.VALUE_STRING) { // value may be single string
            arguments = Collections.singletonList(p.getText());
        } else if (token == JsonToken.START_ARRAY) { // value may be array of strings
            if (p.nextToken() != JsonToken.VALUE_STRING) {
                throw ctxt.wrongTokenException(p, Argument.class, JsonToken.VALUE_STRING, "Expected VALUE_STRING while reading value array");
            }
            arguments = new ArrayList<>(4);
            Iterator<String> args = p.readValuesAs(String.class);
            while (args.hasNext()) {
                arguments.add(args.next());
            }
        } else {
            throw ctxt.wrongTokenException(p, Argument.class, JsonToken.START_ARRAY, "Expected START_ARRAY or VALUE_STRING object start");
        }
        return arguments;
    }
}
