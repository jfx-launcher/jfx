/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.argument;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;

import java.io.IOException;
import java.util.Locale;

/**
 * Jackson databind 2.12 fixes enum as map key deserialization
 * this will not be required with jackson databind 2.12
 */
public class ArgumentTargetDeserializer extends JsonDeserializer<ArgumentTarget> {
    @Override
    public ArgumentTarget deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return ArgumentTarget.valueOf(p.getText().toUpperCase(Locale.ROOT));
    }
}
