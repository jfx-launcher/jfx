/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.CompatInfo;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class CompatRuleImpl implements CompatRule {
    @JsonProperty("action")
    private final Action action;
    @JsonProperty("os")
    private final SystemMatcher os;
    @JsonProperty("features")
    private final Map<String, Boolean> features;

    @JsonCreator
    public CompatRuleImpl(@JsonProperty("action") Action action,
                          @JsonProperty("os") SystemMatcher os,
                          @JsonProperty("features") Map<String, Boolean> features) {
        this.action = Objects.requireNonNullElse(action, Action.DEFAULT);
        this.os = os;
        this.features = Objects.requireNonNullElse(features, Collections.emptyMap());
    }

    /**
     * We should return action field only if os and all features match
     * otherwise DEFAULT should be returned
     * @param compatInfo represents information that may be
     *                   needed by this {@link CompatRule}.
     *                   Can't be null.
     * @return computed action
     */
    @Override
    public Action getAppliedAction(CompatInfo compatInfo) {
        if (os != null && !os.matches(compatInfo)) {
            return Action.DEFAULT;
        }
        for (Map.Entry<String, Boolean> entry : features.entrySet()) {
            if (!compatInfo.hasFeature(entry.getKey(), entry.getValue())) {
                return Action.DEFAULT;
            }
        }
        return action;
    }

    public Action getAction() {
        return action;
    }

    public SystemMatcher getOs() {
        return os;
    }

    public Map<String, Boolean> getFeatures() {
        return features;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompatRuleImpl that = (CompatRuleImpl) o;
        return action == that.action &&
                Objects.equals(os, that.os) &&
                Objects.equals(features, that.features);
    }

    @Override
    public int hashCode() {
        return Objects.hash(action, os, features);
    }
}
