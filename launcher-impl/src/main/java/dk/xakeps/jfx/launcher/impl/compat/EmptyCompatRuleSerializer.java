/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * Serializer, that prevents EmptyCompatRule from begin serialized
 */
public class EmptyCompatRuleSerializer extends JsonSerializer<EmptyCompatRule> {
    // should never be called
    @Override
    public void serialize(EmptyCompatRule value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        throw new JsonGenerationException("Should never be called", gen);
    }

    // return true, we don't have any rules
    @Override
    public boolean isEmpty(SerializerProvider provider, EmptyCompatRule value) {
        return true;
    }
}
