/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.launcher.CompatRule;

import java.util.List;
import java.util.Objects;

public class ListCompatRule implements CompatRule {
    private final List<CompatRule> rules;

    public ListCompatRule(List<CompatRule> rules) {
        this.rules = Objects.requireNonNull(rules, "rules");
    }

    /**
     * Disallow by default
     * iterate over ordered list of rules
     * and return last non-default action
     * @param compatInfo represents information that may be
     *                   needed by this {@link CompatRule}.
     *                   Can't be null.
     * @return computed action
     */
    @Override
    public Action getAppliedAction(CompatInfo compatInfo) {
        Action lastAction = Action.DISALLOW;
        for (CompatRule compatRule : rules) {
            CompatRule.Action action = compatRule.getAppliedAction(compatInfo);
            if (action != Action.DEFAULT) {
                lastAction = action;
            }
        }
        return lastAction;
    }

    public List<CompatRule> getRules() {
        return rules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListCompatRule that = (ListCompatRule) o;
        return rules.equals(that.rules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rules);
    }
}
