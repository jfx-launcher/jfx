/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.argument.Argument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This deserializer will deserialize {@link List} of {@link CompatRule} as {@link ListCompatRule}
 */
public class ListCompatRuleDeserializer extends JsonDeserializer<ListCompatRule> {
    @Override
    public ListCompatRule deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (p.nextToken() != JsonToken.START_OBJECT) { // read first [ token
            throw ctxt.wrongTokenException(p, Argument.class, JsonToken.START_OBJECT, "Error while reading CompatRuleImpl");
        }
        List<CompatRule> rules = new ArrayList<>();
        Iterator<CompatRuleImpl> compatRuleIterator = p.readValuesAs(CompatRuleImpl.class);
        while (compatRuleIterator.hasNext()) {
            rules.add(compatRuleIterator.next());
        }
        // iterator will read last ] token
        return new ListCompatRule(rules);
    }
}
