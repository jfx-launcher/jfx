/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import dk.xakeps.jfx.launcher.CompatRule;

import java.io.IOException;

/**
 * This class will serialize {@link ListCompatRule} as an array of {@link CompatRule}s
 */
public class ListCompatRuleSerializer extends JsonSerializer<ListCompatRule> {
    @Override
    public void serialize(ListCompatRule value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();
        for (CompatRule rule : value.getRules()) {
            gen.writeObject(rule);
        }
        gen.writeEndArray();
    }

    // so we won't see "rule: []" in serialized json
    @Override
    public boolean isEmpty(SerializerProvider provider, ListCompatRule value) {
        return value.getRules().isEmpty();
    }
}
