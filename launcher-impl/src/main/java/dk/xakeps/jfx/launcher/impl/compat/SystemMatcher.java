/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.platform.PlatformInfo;

import java.util.Objects;

public class SystemMatcher {
    @JsonProperty("name")
    private final String name;
    @JsonProperty("version")
    private final String version;
    @JsonProperty("arch")
    private final String arch;

    @JsonCreator
    public SystemMatcher(@JsonProperty("name") String name,
                         @JsonProperty("version") String version,
                         @JsonProperty("arch") String arch) {
        this.name = Objects.requireNonNullElse(name, "");
        this.version = Objects.requireNonNullElse(version, "");
        this.arch = Objects.requireNonNullElse(arch, "");
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }

    public String getArch() {
        return arch;
    }

    /**
     * Check name, version and arch of provided platform
     * name -> not empty? Check it, if check fails, then return false
     * version -> not empty? Check it, if check fails, then return false
     * arch -> not empty? Check it, if check fails, then return false
     * if all match or all empty, return true
     * @param compatInfo compatibility info
     * @return true if system matches
     */
    public boolean matches(CompatInfo compatInfo) {
        PlatformInfo platformInfo = compatInfo.getPlatformInfo();
        if (!name.isEmpty() && !platformInfo.getName().equalsIgnoreCase(name)) {
            return false;
        }
        if (!version.isEmpty() && !platformInfo.getVersion().matches(version)) {
            return false;
        }
        if (!arch.isEmpty() && !platformInfo.getArch().matches(arch)) {
            return false;
        }
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemMatcher that = (SystemMatcher) o;
        return name.equals(that.name) &&
                version.equals(that.version) &&
                arch.equals(that.arch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, version, arch);
    }
}
