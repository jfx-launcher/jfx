/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.download;

import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.launcher.impl.Artifact;

import java.net.URI;

/**
 * Represents abstract {@link DownloadFileInfo} that is backed by {@link Artifact}
 */
public abstract class ArtifactDownloadFileInfo implements DownloadFileInfo {
    protected Artifact artifact;

    public ArtifactDownloadFileInfo(Artifact artifact) {
        this.artifact = artifact;
    }

    @Override
    public URI getSource() {
        return artifact.getUri();
    }

    @Override
    public long getSize() {
        return artifact.getSize();
    }

    @Override
    public String getHash() {
        return artifact.getSha1();
    }

    @Override
    public String getHashAlgorithm() {
        return "SHA-1";
    }
}
