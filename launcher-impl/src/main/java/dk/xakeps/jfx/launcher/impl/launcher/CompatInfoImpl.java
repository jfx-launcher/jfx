/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.platform.PlatformInfo;

/**
 * basic implementation of CompatInfo
 */
class CompatInfoImpl implements CompatInfo {
    private static final System.Logger LOGGER = System.getLogger(CompatInfoImpl.class.getName());

    private final GameLauncherImpl gameLauncher;

    public CompatInfoImpl(GameLauncherImpl gameLauncher) {
        this.gameLauncher = gameLauncher;
        LOGGER.log(System.Logger.Level.TRACE, "CompatInfoImpl created");
    }

    @Override
    public boolean hasFeature(String name, boolean value) {
        LOGGER.log(System.Logger.Level.TRACE, "hasFeature({0}, {1})", name, value);
        GameLauncherBuilderImpl.FeatureInfo featureInfo = gameLauncher.builder.features.get(name);
        boolean b = featureInfo != null && featureInfo.getValue() == value;
        LOGGER.log(System.Logger.Level.TRACE, "hasFeature({0}, {1}) = {2}", name, value, b);
        return b;
    }

    @Override
    public PlatformInfo getPlatformInfo() {
        return gameLauncher.builder.platformInfo;
    }
}
