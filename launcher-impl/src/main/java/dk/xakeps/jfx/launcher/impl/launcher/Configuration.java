/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import dk.xakeps.jfx.launcher.Library;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.Artifact;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.library.ExtractRule;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.logging.LoggingArtifact;
import dk.xakeps.jfx.launcher.impl.version.VersionData;

import java.util.*;

/**
 * Holds launch configuration of {@link VersionData}
 */
public class Configuration {
    private final VersionData versionData;
    private final Map<ArgumentTarget, List<Argument>> arguments = new HashMap<>();
    private final List<LibrariesInfo> librariesInfos = new ArrayList<>();
    private Artifact mainArtifact;
    private AssetsInfo assetsInfo;
    private LoggingInfo loggingInfo;
    private String mainClass;
    private boolean inheritArguments;

    public Configuration(VersionData versionData) {
        this.versionData = versionData;
    }

    public VersionData getVersionData() {
        return versionData;
    }

    public List<Argument> getArguments(ArgumentTarget argumentTarget) {
        return arguments.computeIfAbsent(argumentTarget, k -> new ArrayList<>(4));
    }

    public List<LibrariesInfo> getLibrariesInfos() {
        return librariesInfos;
    }

    public Artifact getMainArtifact() {
        return mainArtifact;
    }

    public void setMainArtifact(Artifact mainArtifact) {
        this.mainArtifact = mainArtifact;
    }

    public AssetsInfo getAssetsInfo() {
        return assetsInfo;
    }

    public void setAssetsInfo(AssetsInfo assetsInfo) {
        this.assetsInfo = assetsInfo;
    }

    public LoggingInfo getLoggingInfo() {
        return loggingInfo;
    }

    public void setLoggingInfo(LoggingInfo loggingInfo) {
        this.loggingInfo = loggingInfo;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public boolean isInheritArguments() {
        return inheritArguments;
    }

    public void setInheritArguments(boolean inheritArguments) {
        this.inheritArguments = inheritArguments;
    }

    public static final class AssetsInfo {
        private final AssetIndexArtifact assetIndexArtifact;
        private final String assetsId;

        public AssetsInfo(AssetIndexArtifact assetIndexArtifact, String assetsId) {
            this.assetIndexArtifact = assetIndexArtifact;
            this.assetsId = assetsId;
        }

        public AssetIndexArtifact getAssetIndexArtifact() {
            return assetIndexArtifact;
        }

        public String getAssetsId() {
            return assetsId;
        }
    }
    public static final class LoggingInfo {
        private final Argument argument;
        private final LoggingArtifact artifact;

        public LoggingInfo(Argument argument, LoggingArtifact artifact) {
            this.argument = argument;
            this.artifact = artifact;
        }

        public Argument getArgument() {
            return argument;
        }

        public LoggingArtifact getArtifact() {
            return artifact;
        }
    }
    public static final class LibrariesInfo implements Library {
        private final String name;
        private final Map<ExtractRule, List<String>> extractRules = new HashMap<>();
        private final List<LibraryArtifact> libraries = new ArrayList<>(1);
        private final List<LibraryArtifact> natives = new ArrayList<>(0);

        public LibrariesInfo(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        public Map<ExtractRule, List<String>> getExtractRules() {
            return extractRules;
        }

        public List<LibraryArtifact> getLibraries() {
            return libraries;
        }

        public List<LibraryArtifact> getNatives() {
            return natives;
        }

        public Set<LibraryArtifact> getDownloads() {
            Set<LibraryArtifact> artifacts = new HashSet<>(libraries);
            artifacts.addAll(natives);
            return artifacts;
        }
    }
}
