/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.launcher.LauncherException;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.Artifact;
import dk.xakeps.jfx.launcher.impl.AssetIndex;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.compat.EmptyCompatRule;
import dk.xakeps.jfx.launcher.impl.download.ArtifactDownloadFileInfo;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.logging.LoggingArtifact;
import dk.xakeps.jfx.launcher.impl.version.VersionData;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Used to calculate right set of arguments based on one or many {@link Configuration}s
 */
public class ConfigurationProcessor {
    private static final System.Logger LOGGER = System.getLogger(ConfigurationProcessor.class.getName());

    private final GameLauncherBuilderImpl builder;
    /**
     * Contains list of configurations.
     * Last parent inserted first
     */
    private final List<Configuration> configurations;
    private final Set<Path> classpathEntries = new LinkedHashSet<>();
    private final List<Configuration.LibrariesInfo> librariesInfos = new ArrayList<>();
    private final Set<DownloadFileInfo> downloads = new HashSet<>();
    private final Set<VersionJsonFileInfo> versionJsonFiles = new HashSet<>();
    private final Map<ArgumentTarget, List<Argument>> arguments = new HashMap<>();
    private MainArtifactInfo mainArtifactInfo;
    private AssetIndexInfo assetIndexInfo;

    public ConfigurationProcessor(GameLauncherBuilderImpl builder, List<Configuration> configurations) {
        this.builder = builder;
        this.configurations = configurations;
    }

    /**
     * Finds asset index id
     * @return asset index id
     */
    private String findAssetIndexId() {
        LOGGER.log(System.Logger.Level.TRACE, "Finding asset index id");
        // find most latest override of asset index id
        for (Configuration configuration : configurations) {
            if (configuration.getAssetsInfo() != null) {
                String assetsId = configuration.getAssetsInfo().getAssetsId();
                if (assetsId != null && !assetsId.isEmpty()) {
                    LOGGER.log(System.Logger.Level.TRACE, "Asset index id found, {0}", assetsId);
                    return assetsId;
                }
            }
        }
        LOGGER.log(System.Logger.Level.ERROR, "Asset index id not found");
        throw new LauncherException("Asset index id not found");
    }

    /**
     * Finds {@link AssetIndexArtifact}
     * @return asset index artifact
     */
    private AssetIndexArtifact findAssetIndexArtifact() {
        LOGGER.log(System.Logger.Level.TRACE, "Finding asset index artifact");
        // find most latest override of asset index artifact
        for (Configuration configuration : configurations) {
            Configuration.AssetsInfo assetsInfo = configuration.getAssetsInfo();
            if (assetsInfo == null) {
                continue;
            }
            AssetIndexArtifact assetIndexArtifact = assetsInfo.getAssetIndexArtifact();
            if (assetIndexArtifact != null) {
                LOGGER.log(System.Logger.Level.TRACE, "Asset index artifact found");
                return assetIndexArtifact;
            }
        }
        LOGGER.log(System.Logger.Level.ERROR, "asset index artifact not found");
        throw new LauncherException("Asset index not found");
    }

    /**
     * Calculate and store {@link AssetIndexInfo}
     * @return {@link AssetIndexInfo}
     */
    public AssetIndexInfo getAssetIndexInfo() {
        LOGGER.log(System.Logger.Level.TRACE, "Getting asset index info");
        if (assetIndexInfo != null) {
            return assetIndexInfo;
        }
        LOGGER.log(System.Logger.Level.TRACE, "Getting asset index info created");
        this.assetIndexInfo = new AssetIndexInfo(findAssetIndexId(), findAssetIndexArtifact());
        return assetIndexInfo;
    }

    /**
     * Collect arguments
     * @param argumentTarget target of arguments
     * @return list of arguments
     */
    public List<Argument> getArguments(ArgumentTarget argumentTarget) {
        LOGGER.log(System.Logger.Level.TRACE, "Collecting arguments for {0}", argumentTarget);
        List<Argument> args = arguments.getOrDefault(argumentTarget, Collections.emptyList());
        if (!args.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Returning calculated arguments for {0}", argumentTarget);
            return args;
        }
        args = new ArrayList<>();
        boolean inheritArguments = true;
        for (Configuration configuration : configurations) {
            if (!inheritArguments) {
                args.clear();
            }
            inheritArguments = configuration.isInheritArguments();
            args.addAll(configuration.getArguments(argumentTarget));
        }
        processArgs(argumentTarget, args);
        arguments.put(argumentTarget, args);
        LOGGER.log(System.Logger.Level.TRACE, "Calculated arguments for {0}", argumentTarget);
        return args;
    }

    /**
     * Adds additional args if required
     * @param argumentTarget argument target
     * @param arguments checked args
     */
    private void processArgs(ArgumentTarget argumentTarget, List<Argument> arguments) {
        LOGGER.log(System.Logger.Level.TRACE, "Additional processing for arguments, target {0}", argumentTarget);
        if (argumentTarget == ArgumentTarget.JVM && arguments.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Adding default JVM arguments");
            arguments.add(new Argument(Collections.singletonList("-Djava.library.path=${natives_directory}"), EmptyCompatRule.INSTANCE));
            arguments.add(new Argument(Collections.singletonList("-Dminecraft.launcher.brand=${launcher_name}"), EmptyCompatRule.INSTANCE));
            arguments.add(new Argument(Collections.singletonList("-Dminecraft.launcher.version=${launcher_version}"), EmptyCompatRule.INSTANCE));
            arguments.add(new Argument(Collections.singletonList("-Dminecraft.client.jar=${main_jar}"), EmptyCompatRule.INSTANCE));
            arguments.add(new Argument(Arrays.asList("-cp", "${classpath}"), EmptyCompatRule.INSTANCE));
        }

        if (builder.advancedLogger && argumentTarget == ArgumentTarget.JVM) {
            LOGGER.log(System.Logger.Level.TRACE, "Advanced logger requested, trying enabling it");
            if (getLoggingInfo().isPresent()) {
                LOGGER.log(System.Logger.Level.TRACE, "Advanced logger found");
                Configuration.LoggingInfo loggingInfo = getLoggingInfo().get();
                Argument argument = loggingInfo.getArgument();
                LoggingArtifact config = loggingInfo.getArtifact();
                String configFile = builder.directoryProvider.getAssetsDir()
                        .resolve("log_configs")
                        .resolve(config.getId())
                        .toAbsolutePath().toString();
                List<String> collect = argument.getValue().stream()
                        .map(s -> s.replace("${path}", configFile))
                        .collect(Collectors.toList());
                Argument loggerArg = new Argument(collect, argument.getRules());
                arguments.add(0, loggerArg);
                LOGGER.log(System.Logger.Level.TRACE, "Advanced logger activated");
            }
        }
    }

    /**
     * Calculates libraries
     * @return calculated libraries
     */
    public List<Configuration.LibrariesInfo> getLibrariesInfos() {
        LOGGER.log(System.Logger.Level.TRACE, "Collecting libraries");
        if (!librariesInfos.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Returning calculated libraries");
            return librariesInfos;
        }
        // must be in reverse order, so parent libraries come after child libraries
        for (int i = configurations.size() - 1; i >= 0; i--) {
            Configuration configuration = configurations.get(i);
            configuration.getLibrariesInfos().stream()
                    .filter(builder.libraryFilter)
                    .forEach(librariesInfos::add);
        }
        LOGGER.log(System.Logger.Level.TRACE, "Libraries calculated");
        return librariesInfos;
    }

    /**
     * Calculate classpath entries
     * @return classpath entries
     */
    public Set<Path> getClasspathEntries() {
        LOGGER.log(System.Logger.Level.TRACE, "Calculating classpath entries");
        if (!classpathEntries.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Returning calculated classpath entries");
            return classpathEntries;
        }
        Path librariesDir = builder.directoryProvider.getLibrariesDir();
        // classpath entries are ordered!
        // so parent libraries come after child libraries
        for (Configuration.LibrariesInfo librariesInfo : getLibrariesInfos()) {
            for (LibraryArtifact library : librariesInfo.getLibraries()) {
                Path libraryPath = librariesDir.resolve(library.getPath());
                if (!libraryPath.startsWith(librariesDir)) {
                    LOGGER.log(System.Logger.Level.TRACE, "Out of libraries directory scope, library {0}, library path {1}, root {2}",
                            librariesInfo.getName(), library.getPath(), librariesDir);
                    throw new IllegalArgumentException(String.format(
                            "Out of libraries directory scope, library %s, library path %s, root %s",
                            librariesInfo.getName(), library.getPath(), librariesDir

                    ));
                }
                if (builder.libraryReplacer.test(librariesInfo)) {
                    builder.libraryReplacer.replaceWith(librariesInfo, libraryPath)
                            .ifPresent(classpathEntries::add);
                } else {
                    classpathEntries.add(libraryPath);
                }
            }
        }
        MainArtifactInfo mainArtifactInfo = getMainArtifactInfo();
        classpathEntries.add(mainArtifactInfo.getFile());
        LOGGER.log(System.Logger.Level.TRACE, "Libraries calculated");
        return classpathEntries;
    }

    /**
     * Calculate main artifact info
     * @return main artifact info
     */
    public MainArtifactInfo getMainArtifactInfo() {
        LOGGER.log(System.Logger.Level.TRACE, "Calculating main artifact info");
        if (mainArtifactInfo != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Returning calculated main artifact info");
            return mainArtifactInfo;
        }
        String mainClass = null;
        for (int i = configurations.size() - 1; i >= 0; i--) {
            Configuration configuration = configurations.get(i);
            if (mainClass == null) {
                LOGGER.log(System.Logger.Level.TRACE, "Main class found, {0}", configuration.getMainClass());
                mainClass = configuration.getMainClass();
            }
            Artifact mainArtifact = configuration.getMainArtifact();
            if (mainArtifact != null) {
                LOGGER.log(System.Logger.Level.TRACE, "Main artifact found");
                mainArtifactInfo = new MainArtifactInfo(configuration.getVersionData(), mainArtifact, mainClass);
            }
            if (mainArtifactInfo != null && mainClass != null) {
                LOGGER.log(System.Logger.Level.TRACE, "Main artifact and main class found");
                break;
            }
        }
        if (mainArtifactInfo == null) {
            LOGGER.log(System.Logger.Level.ERROR, "Main artifact not found");
            throw new LauncherException("Main artifact not found");
        }
        LOGGER.log(System.Logger.Level.TRACE, "Calculated main artifact");
        return mainArtifactInfo;
    }

    /**
     * Gets logging info
     * @return logging info
     */
    public Optional<Configuration.LoggingInfo> getLoggingInfo() {
        LOGGER.log(System.Logger.Level.TRACE, "Getting logging info");
        // we should get logging info of last processed configuration
        Configuration configuration = configurations.get(configurations.size() - 1);
        return Optional.ofNullable(configuration.getLoggingInfo());
    }

    /**
     *
     * @return list of {@link FileInfo} of saved json files
     * @throws IOException if error happens
     */
    public Set<VersionJsonFileInfo> saveVersionJsons() throws IOException {
        LOGGER.log(System.Logger.Level.TRACE, "Saving version jsons");
        versionJsonFiles.clear();
        for (Configuration configuration : configurations) {
            VersionData versionData = configuration.getVersionData();
            Path path = saveVersionJson(versionData);
            versionJsonFiles.add(new VersionJsonFileInfo(path, Files.size(path)));
            if (builder.versionManager.getLocalRegistry().addInstalled(versionData)) {
                if (builder.versionManager.getVersionListener().isPresent()) {
                    builder.versionManager.getVersionListener().get().onInstalled(versionData);
                }
            }
        }
        LOGGER.log(System.Logger.Level.TRACE, "Versions saved");
        return versionJsonFiles;
    }

    /**
     * Saves provided {@link VersionData} to a disk
     * @param versionData version data to save
     * @return path to saved version data
     * @throws IOException if error happens
     */
    private Path saveVersionJson(VersionData versionData) throws IOException {
        LOGGER.log(System.Logger.Level.TRACE, "Saving version json {0}", versionData.getId());
        Path resolve = builder.versionsDir
                .resolve(versionData.getId())
                .resolve(versionData.getId() + ".json");
        LOGGER.log(System.Logger.Level.TRACE, "Version {0}, Save path {1}", versionData.getId(), resolve);
        Files.createDirectories(resolve.getParent());
        try (BufferedWriter writer = Files.newBufferedWriter(resolve)) {
            ObjectMapperHolder.OBJECT_MAPPER.writeValue(writer, versionData);
        } catch (IOException e) {
            Files.deleteIfExists(resolve);
            LOGGER.log(System.Logger.Level.ERROR, "Failed to save version {0}", versionData.getId(), e);
            throw e;
        }
        LOGGER.log(System.Logger.Level.TRACE, "Version saved, {0}", versionData.getId());
        return resolve;
    }

    /**
     * Calculates assets to download
     * @return set of assets to download
     */
    public Set<DownloadFileInfo> collectDownloadsExceptAssets() {
        LOGGER.log(System.Logger.Level.TRACE, "Collecting downloads");
        if (!downloads.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Returning collected downloads");
            return downloads;
        }
        Set<DownloadFileInfo> downloads = new HashSet<>();
        getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getDownloads)
                .flatMap(Collection::stream)
                .map(LibraryFileInfo::new)
                .forEach(downloads::add);
        downloads.add(getMainArtifactInfo());
        downloads.add(getAssetIndexInfo());
        getLoggingInfo().map(Configuration.LoggingInfo::getArtifact)
                .map(LoggingFileInfo::new).ifPresent(downloads::add);
        LOGGER.log(System.Logger.Level.TRACE, "Downloads collected");
        return downloads;
    }

    public class MainArtifactInfo extends ArtifactDownloadFileInfo {
        private final VersionData versionData;
        private final Artifact artifact;
        private final String mainClass;
        private final Path file;

        public MainArtifactInfo(VersionData versionData, Artifact artifact, String mainClass) {
            super(artifact);
            this.versionData = versionData;
            this.artifact = artifact;
            this.mainClass = mainClass;
            this.file = builder.versionsDir
                    .resolve(getId())
                    .resolve(getId() + ".jar");
        }

        public String getId() {
            return versionData.getId();
        }

        public Artifact getArtifact() {
            return artifact;
        }

        public String getMainClass() {
            return mainClass;
        }

        @Override
        public Path getFile() {
            return file;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MainArtifactInfo that = (MainArtifactInfo) o;
            return Objects.equals(file, that.file);
        }

        @Override
        public int hashCode() {
            return Objects.hash(file);
        }

        @Override
        public String getName() {
            return "Version JAR";
        }
    }
    public class AssetIndexInfo extends ArtifactDownloadFileInfo {
        private final String assetIndexId;
        private final AssetIndexArtifact assetIndexArtifact;
        private final Path path;
        private AssetIndex assetIndex;

        public AssetIndexInfo(String assetIndexId, AssetIndexArtifact assetIndexArtifact) {
            super(assetIndexArtifact);
            this.assetIndexId = assetIndexId;
            this.assetIndexArtifact = assetIndexArtifact;
            this.path = calculatePath(assetIndexId);
        }

        public String getAssetIndexId() {
            return assetIndexId;
        }

        public AssetIndexArtifact getAssetIndexArtifact() {
            return assetIndexArtifact;
        }

        public Optional<AssetIndex> getAssetIndex() throws IOException {
            if (assetIndex != null) {
                return Optional.of(assetIndex);
            }
            if (Files.notExists(getFile())) {
                return Optional.empty();
            }
            try (BufferedReader reader = Files.newBufferedReader(getFile(), StandardCharsets.UTF_8)) {
                assetIndex = ObjectMapperHolder.OBJECT_MAPPER.readValue(reader, AssetIndex.class);
            }
            return Optional.of(assetIndex);
        }

        @Override
        public Path getFile() {
            return path;
        }

        private Path calculatePath(String assetIndexId) {
            Path indexesDir = builder.directoryProvider.getAssetsDir().resolve("indexes");
            Path resolve = indexesDir.resolve(assetIndexId + ".json");
            if (!resolve.startsWith(indexesDir)) {
                throw new IllegalArgumentException(String.format(
                        "Out of libraries directory scope, library path %s, root %s",
                        resolve, indexesDir
                ));
            }
            return resolve;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AssetIndexInfo that = (AssetIndexInfo) o;
            return Objects.equals(path, that.path);
        }

        @Override
        public int hashCode() {
            return Objects.hash(path);
        }

        @Override
        public String getName() {
            return "Asset Index";
        }
    }
    private final class LibraryFileInfo extends ArtifactDownloadFileInfo {
        private final Path path;

        public LibraryFileInfo(LibraryArtifact libraryArtifact) {
            super(libraryArtifact);
            this.path = calculatePath(libraryArtifact);
        }

        @Override
        public Path getFile() {
            return path;
        }

        private Path calculatePath(LibraryArtifact artifact) {
            Path librariesDir = builder.directoryProvider.getLibrariesDir();
            Path resolve = librariesDir.resolve(artifact.getPath());
            if (!resolve.startsWith(librariesDir)) {
                LOGGER.log(System.Logger.Level.ERROR, "Out of libraries directory scope, library path {0}, root {1}",
                        resolve, librariesDir);
                throw new IllegalArgumentException(String.format(
                        "Out of libraries directory scope, library path %s, root %s",
                        resolve, librariesDir
                ));
            }
            return resolve;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LibraryFileInfo that = (LibraryFileInfo) o;
            return Objects.equals(path, that.path);
        }

        @Override
        public int hashCode() {
            return Objects.hash(path);
        }

        @Override
        public String getName() {
            return "Library";
        }
    }
    private final class LoggingFileInfo extends ArtifactDownloadFileInfo {
        private final Path path;

        public LoggingFileInfo(LoggingArtifact artifact) {
            super(artifact);
            this.path = calculatePath(artifact);
        }

        @Override
        public Path getFile() {
            return path;
        }

        private Path calculatePath(LoggingArtifact artifact) {
            Path assetsDir = builder.directoryProvider.getAssetsDir();
            Path resolve = assetsDir
                    .resolve("log_configs").resolve(artifact.getId());
            if (!resolve.startsWith(assetsDir)) {
                throw new IllegalArgumentException(String.format(
                        "Out of libraries directory scope, library path %s, root %s",
                        resolve, assetsDir
                ));
            }
            return resolve;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LoggingFileInfo that = (LoggingFileInfo) o;
            return Objects.equals(path, that.path);
        }

        @Override
        public int hashCode() {
            return Objects.hash(path);
        }

        @Override
        public String getName() {
            return "Logger config";
        }
    }
    public static final class VersionJsonFileInfo implements FileInfo {
        private final Path file;
        private final long size;

        public VersionJsonFileInfo(Path file, long size) {
            this.file = file;
            this.size = size;
        }

        @Override
        public Path getFile() {
            return file;
        }

        @Override
        public long getSize() {
            return size;
        }

        @Override
        public String getHash() {
            return "";
        }

        @Override
        public String getHashAlgorithm() {
            return "";
        }
    }
}
