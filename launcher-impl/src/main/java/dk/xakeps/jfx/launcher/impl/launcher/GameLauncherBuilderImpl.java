/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import dk.xakeps.jfx.launcher.*;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.Constants;
import dk.xakeps.jfx.launcher.impl.version.InternalGameVersion;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerImpl;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import dk.xakeps.jfx.platform.PlatformInfo;
import dk.xakeps.jfx.user.User;

import java.net.InetSocketAddress;
import java.net.URI;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Builds {@link GameLauncher}
 */
public class GameLauncherBuilderImpl implements GameLauncher.Builder {
    private static final System.Logger LOGGER = System.getLogger(GameLauncherBuilderImpl.class.getName());

    final VersionManagerImpl versionManager;
    final Path versionsDir;
    DirectoryProvider directoryProvider;
    List<Path> classpath = Collections.emptyList();
    PlatformInfo platformInfo;
    InternalGameVersion gameVersion;
    String versionType;
    User user;
    String accessToken = "";
    Predicate<Library> libraryFilter = library -> true;
    LibraryReplacer libraryReplacer = NoOpLibraryReplacer.INSTANCE;
    final Map<ArgumentTarget, List<Argument>> arguments = new HashMap<>(0);
    InetSocketAddress connectTo;
    final Map<String, FeatureInfo> features = new HashMap<>(4);
    ProgressListenerFactory progressListenerFactory;
    boolean advancedLogger;
    IOConfigImpl ioConfig;
    URI defaultMavenRepo = Constants.MAVEN_REPO_URI;
    URI resourcesUri = Constants.RESOURCE_URI;
    String launcherId = Constants.LAUNCHER_ID;
    String launcherVersion = Constants.LAUNCHER_VERSION;

    public GameLauncherBuilderImpl(VersionManagerImpl versionManager, Path versionsDir) {
        this.versionManager = Objects.requireNonNull(versionManager, "versionManager");
        this.versionsDir = Objects.requireNonNull(versionsDir, "versionsDir");
        LOGGER.log(System.Logger.Level.TRACE, "GameLauncher Builder created");
    }

    @Override
    public GameLauncher.Builder directoryProvider(DirectoryProvider directoryProvider) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting directoryProvider");
        this.directoryProvider = Objects.requireNonNull(directoryProvider, "directoryProvider");
        LOGGER.log(System.Logger.Level.TRACE, "directoryProvider set");
        return this;
    }

    @Override
    public GameLauncher.Builder classpath(List<Path> classpath) {
        LOGGER.log(System.Logger.Level.TRACE, "Settings classpath entries");
        if (classpath != null) {
            for (Path path : classpath) {
                if (!path.isAbsolute()) {
                    LOGGER.log(System.Logger.Level.ERROR, "Classpath entry is not absolute {0}", path);
                    throw new IllegalArgumentException("classpath paths must be absolute");
                }
            }
            LOGGER.log(System.Logger.Level.TRACE, "classpath entries set");
            this.classpath = new ArrayList<>(classpath);
        } else {
            LOGGER.log(System.Logger.Level.TRACE, "classpath entries cleared");
            this.classpath = Collections.emptyList();
        }
        return this;
    }

    @Override
    public GameLauncher.Builder platformInfo(PlatformInfo platformInfo) {
        LOGGER.log(System.Logger.Level.TRACE, "changing platformInfo");
        this.platformInfo = Objects.requireNonNull(platformInfo, "platformInfo");
        LOGGER.log(System.Logger.Level.TRACE, "platformInfo set");
        return this;
    }

    @Override
    public GameLauncher.Builder feature(String name, boolean value, UnaryOperator<String> argReplacer) {
        LOGGER.log(System.Logger.Level.TRACE, "setting feature {0} with arg replacer", name);
        Objects.requireNonNull(argReplacer, "argReplacer");
        features.put(name, new FeatureInfo(name, value, argReplacer));
        LOGGER.log(System.Logger.Level.TRACE, "feature {0} with arg replacer added", name);
        return this;
    }

    @Override
    public GameLauncher.Builder feature(String name, boolean value) {
        features.put(name, new FeatureInfo(name, value, null));
        LOGGER.log(System.Logger.Level.TRACE, "feature {0} added", name);
        return this;
    }

    @Override
    public GameLauncher.Builder gameVersion(GameVersion gameVersion) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting gameVersion");
        if (!(gameVersion instanceof InternalGameVersion)) {
            LOGGER.log(System.Logger.Level.ERROR, "Wrong GameVersion instance");
            throw new IllegalArgumentException("Wrong game version type. Must implement InternalGameVersion");
        }
        this.gameVersion = (InternalGameVersion) Objects.requireNonNull(gameVersion, "gameVersion");
        LOGGER.log(System.Logger.Level.TRACE, "gameVersion set");
        return this;
    }

    @Override
    public GameLauncher.Builder versionType(String versionType) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting versionType");
        this.versionType = Objects.requireNonNull(versionType, "versionType");
        LOGGER.log(System.Logger.Level.TRACE, "versionType set");
        return this;
    }

    @Override
    public GameLauncher.Builder user(User user) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting user");
        this.user = Objects.requireNonNull(user, "user");
        LOGGER.log(System.Logger.Level.TRACE, "user set");
        return this;
    }

    @Override
    public GameLauncher.Builder accessToken(String accessToken) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting accessToken");
        this.accessToken = Objects.requireNonNull(accessToken, "accessToken");
        LOGGER.log(System.Logger.Level.TRACE, "accessToken set");
        return this;
    }

    @Override
    public GameLauncher.Builder libraryFilter(Predicate<Library> libraryFilter) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting libraryFilter. isNull == {0}", libraryFilter == null);
        this.libraryFilter = Objects.requireNonNullElse(libraryFilter, library -> true);
        return this;
    }

    @Override
    public GameLauncher.Builder libraryReplacer(LibraryReplacer libraryReplacer) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting libraryReplacer");
        this.libraryReplacer = Objects.requireNonNullElse(libraryReplacer, NoOpLibraryReplacer.INSTANCE);
        LOGGER.log(System.Logger.Level.TRACE, "libraryReplacer set");
        return this;
    }

    @Override
    public GameLauncher.Builder arguments(ArgumentTarget argumentTarget, List<Argument> arguments) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting arguments for {0}", argumentTarget);
        this.arguments.put(argumentTarget, arguments);
        LOGGER.log(System.Logger.Level.TRACE, "Arguments for {0} set", argumentTarget);
        return this;
    }

    @Override
    public GameLauncher.Builder connectTo(InetSocketAddress server) {
        this.connectTo = server;
        LOGGER.log(System.Logger.Level.TRACE, "connectTo set, {0}", connectTo);
        return this;
    }

    @Override
    public GameLauncher.Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting progressListenerFactory");
        this.progressListenerFactory = Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        LOGGER.log(System.Logger.Level.TRACE, "progressListenerFactory set");
        return this;
    }

    @Override
    public GameLauncher.Builder advancedLogger(boolean advancedLogger) {
        this.advancedLogger = advancedLogger;
        LOGGER.log(System.Logger.Level.TRACE, "advancedLogger set, advancedLogger={0}", advancedLogger);
        return this;
    }

    @Override
    public IOConfig configureIO() {
        LOGGER.log(System.Logger.Level.TRACE, "Configuring IO");
        if (ioConfig == null) {
            LOGGER.log(System.Logger.Level.TRACE, "Creating new IOConfig");
            ioConfig = new IOConfigImpl();
        }
        return ioConfig;
    }

    /**
     * @param defaultMavenRepo default maven repository uri
     * @return itself, for chaining
     */
    public GameLauncher.Builder defaultMavenRepo(URI defaultMavenRepo) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting defaultMavenRepo");
        this.defaultMavenRepo = Objects.requireNonNull(defaultMavenRepo, "defaultMavenRepo");
        LOGGER.log(System.Logger.Level.TRACE, "defaultMavenRepo set");
        return this;
    }

    /**
     * @param resourcesUri default resources uri
     * @return itself, for chaining
     */
    public GameLauncher.Builder resourcesUri(URI resourcesUri) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting resourcesUri");
        this.resourcesUri = Objects.requireNonNull(resourcesUri, "resourcesUri");
        LOGGER.log(System.Logger.Level.TRACE, "resourcesUri set");
        return this;
    }

    /**
     * @param launcherId launcher id
     * @return itself, for chaining
     */
    public GameLauncher.Builder launcherId(String launcherId) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting launcherId");
        this.launcherId = Objects.requireNonNull(launcherId, "launcherId");
        LOGGER.log(System.Logger.Level.TRACE, "launcherId set");
        return this;
    }

    /**
     * @param launcherVersion launcher version
     * @return itself, for chaining
     */
    public GameLauncher.Builder launcherVersion(String launcherVersion) {
        LOGGER.log(System.Logger.Level.TRACE, "Setting launcherVersion");
        this.launcherVersion = Objects.requireNonNull(launcherVersion, "launcherVersion");
        LOGGER.log(System.Logger.Level.TRACE, "launcherVersion set");
        return this;
    }

    @Override
    public GameLauncher build() {
        LOGGER.log(System.Logger.Level.TRACE, "Building GameLauncher");
        Objects.requireNonNull(directoryProvider, "directoryProvider");
        Objects.requireNonNull(platformInfo, "platformInfo");
        Objects.requireNonNull(gameVersion, "gameVersion");
        Objects.requireNonNull(versionType, "versionType");
        Objects.requireNonNull(user, "user");
        Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        return new GameLauncherImpl(this);
    }

    class IOConfigImpl implements IOConfig {
        Boolean redirectErrorStream;
        ProcessBuilder.Redirect redirectError;
        ProcessBuilder.Redirect redirectInput;
        ProcessBuilder.Redirect redirectOutput;
        StreamsConsumer streamsConsumer;

        @Override
        public IOConfig redirectErrorStream(boolean redirectErrorStream) {
            this.redirectErrorStream = redirectErrorStream;
            LOGGER.log(System.Logger.Level.TRACE, "redirectErrorStream set");
            return this;
        }

        @Override
        public IOConfig redirectError(ProcessBuilder.Redirect redirectError) {
            this.redirectError = redirectError;
            LOGGER.log(System.Logger.Level.TRACE, "redirectErrorStream set");
            return this;
        }

        @Override
        public IOConfig redirectInput(ProcessBuilder.Redirect redirectInput) {
            this.redirectInput = redirectInput;
            LOGGER.log(System.Logger.Level.TRACE, "redirectInput set");
            return this;
        }

        @Override
        public IOConfig redirectOutput(ProcessBuilder.Redirect redirectOutput) {
            this.redirectOutput = redirectOutput;
            LOGGER.log(System.Logger.Level.TRACE, "redirectOutput set");
            return this;
        }

        @Override
        public IOConfig streamsConsumer(StreamsConsumer streamsConsumer) {
            this.streamsConsumer = streamsConsumer;
            LOGGER.log(System.Logger.Level.TRACE, "streamsConsumer set");
            return this;
        }

        public void applyTo(ProcessBuilder processBuilder) {
            LOGGER.log(System.Logger.Level.TRACE, "Applying IOConfig");
            if (redirectOutput != null) {
                LOGGER.log(System.Logger.Level.TRACE, "setting redirectOutput, {0}", redirectOutput);
                processBuilder.redirectOutput(redirectOutput);
            }
            if (redirectError != null) {
                LOGGER.log(System.Logger.Level.TRACE, "setting redirectError, {0}", redirectError);
                processBuilder.redirectError(redirectError);
            }
            if (redirectInput != null) {
                LOGGER.log(System.Logger.Level.TRACE, "setting redirectInput, {0}", redirectInput);
                processBuilder.redirectInput(redirectInput);
            }
            if (redirectErrorStream != null) {
                LOGGER.log(System.Logger.Level.TRACE, "setting redirectErrorStream, {0}", redirectErrorStream);
                processBuilder.redirectErrorStream(redirectErrorStream);
            }
            LOGGER.log(System.Logger.Level.TRACE, "IOConfig applied");
        }

        @Override
        public GameLauncher.Builder finish() {
            LOGGER.log(System.Logger.Level.TRACE, "IOConfig finished");
            return GameLauncherBuilderImpl.this;
        }
    }

    public static final class FeatureInfo {
        private final String name;
        private final boolean value;
        private final UnaryOperator<String> argReplacer;

        public FeatureInfo(String name, boolean value, UnaryOperator<String> argReplacer) {
            this.name = name;
            this.value = value;
            this.argReplacer = argReplacer;
        }

        public String getName() {
            return name;
        }

        public boolean getValue() {
            return value;
        }

        public UnaryOperator<String> getArgReplacer() {
            return argReplacer;
        }
    }

    private static final class NoOpLibraryReplacer implements LibraryReplacer {
        private static final LibraryReplacer INSTANCE = new NoOpLibraryReplacer();

        private NoOpLibraryReplacer() {
        }

        @Override
        public boolean test(Library library) {
            return false;
        }

        @Override
        public Optional<Path> replaceWith(Library library, Path artifactPath) {
            return Optional.empty();
        }
    }
}
