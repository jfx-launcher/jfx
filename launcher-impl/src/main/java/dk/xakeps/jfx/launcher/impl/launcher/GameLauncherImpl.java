/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.downloader.Downloader;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.launcher.GameLauncher;
import dk.xakeps.jfx.launcher.LauncherException;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.AssetIndex;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.library.ExtractRule;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.version.InternalGameVersion;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;


public class GameLauncherImpl implements GameLauncher, GameLauncher.PreparedGameLauncher {
    private static final System.Logger LOGGER = System.getLogger(GameLauncherImpl.class.getName());

    private final UUID runId = UUID.randomUUID();
    final GameLauncherBuilderImpl builder;
    final CompatInfo compatInfo;
    final ConfigurationProcessor processor;
    private final Map<String, String> placeholders = new HashMap<>();
    private final Set<FileInfo> gameFiles = new HashSet<>();

    public GameLauncherImpl(GameLauncherBuilderImpl builder) {
        this.builder = builder;
        this.compatInfo = new CompatInfoImpl(this);
        InternalGameVersion gameVersion = builder.gameVersion;
        LauncherInfo launcherInfo = new LauncherInfo(this);
        List<Configuration> configurations = gameVersion.prepareLauncher(launcherInfo);
        this.processor = new ConfigurationProcessor(builder, configurations);
        LOGGER.log(System.Logger.Level.TRACE, "GameLauncher created");
    }

    /**
     * Collects list of files and downloads all of them
     * Unpacks assets if required
     * Unpacks natives
     * @return this
     * @throws IOException if error happens
     */
    @Override
    public PreparedGameLauncher prepare() throws IOException {
        LOGGER.log(System.Logger.Level.INFO, "Downloading game files");
        try (Downloader downloader = new Downloader(builder.progressListenerFactory)) {

            Set<DownloadFileInfo> downloadFileInfos = processor.collectDownloadsExceptAssets();
            gameFiles.addAll(downloadFileInfos);
            ConfigurationProcessor.AssetIndexInfo assetIndexInfo = processor.getAssetIndexInfo();
            gameFiles.add(assetIndexInfo);
            LOGGER.log(System.Logger.Level.INFO, "Downloading asset index");
            downloader.download("Asset Index", Collections.singleton(assetIndexInfo)).join();
            LOGGER.log(System.Logger.Level.INFO, "Requesting download of game files");
            CompletableFuture<Void> mainFiles = downloader.download("Version files", downloadFileInfos);
            // asset index will be available only after download
            AssetIndex assetIndex = assetIndexInfo.getAssetIndex()
                    .orElseThrow(() -> new IllegalStateException("Asset Index not found!"));
            Set<DownloadFileInfo> assets = assetIndex.collectDownloads(builder.directoryProvider.getAssetsDir(), builder.resourcesUri);
            gameFiles.addAll(assets);
            LOGGER.log(System.Logger.Level.INFO, "Downloading assets");
            CompletableFuture<Void> assetsFiles = downloader.download("Assets", assets);
            LOGGER.log(System.Logger.Level.INFO, "Joining download futures");
            mainFiles.join();
            assetsFiles.join();

            LOGGER.log(System.Logger.Level.INFO, "Saving version jsons");
            gameFiles.addAll(processor.saveVersionJsons());

            Path assetsPath = builder.directoryProvider.getAssetsDir();
            // old versions may not have sound
            // block http://www.minecraft.net/resources/ in hosts, sounds will work
            String assetIndexId = processor.getAssetIndexInfo().getAssetIndexId();
            if (assetIndex.isMapToResources()) {
                // fix options.txt lang: options
                // older versions crash if they can't find lang
                LOGGER.log(System.Logger.Level.INFO, "Mapping assets to resources");
                fixOptionsForOldVersion();
                Path resourcesDir = builder.directoryProvider.getGameDir().resolve("resources");
                unpackAssets(assetIndex, resourcesDir);
            }
            if (assetIndex.isVirtual() || assetIndex.isMapToResources()) {
                // here we are unpacking legacy asset index in to virtual directory in assets
                assetsPath = unpackAssets(assetIndex, assetsPath.resolve("virtual").resolve(assetIndexId));
            }

            unpackNatives();

            fillPlaceholders(assetIndexId, assetsPath);

            return this;
        }
    }

    @Override
    public void start() {
        try {
            startUnsafe();
        } catch (Exception e) {
            throw new LauncherException("Error while starting the game", e);
        }
    }

    /**
     * Starts the game
     * @throws IOException if error happens
     */
    private void startUnsafe() throws IOException {
        LOGGER.log(System.Logger.Level.TRACE, "Preparing command line");
        // prepare arguments
        List<String> args = new ArrayList<>();
        Path javaExecutable = builder.platformInfo.getJavaExecutable();
        args.add(javaExecutable.toAbsolutePath().toString()); // java executable path
        args.addAll(getJavaArguments()); // java args
        args.add(processor.getMainArtifactInfo().getMainClass()); // main class
        args.addAll(getGameArguments()); // game args
        if (builder.connectTo != null) { // auto-connect feature
            LOGGER.log(System.Logger.Level.TRACE, "Auto-connect to {0}", builder.connectTo);
            args.add("--server");
            args.add(builder.connectTo.getHostString());
            if (builder.connectTo.getPort() > 0) {
                args.add("--port");
                args.add(Integer.toString(builder.connectTo.getPort()));
            }
        }

        // ensure working directory is created
        Files.createDirectories(builder.directoryProvider.getGameDir());
        ProcessBuilder processBuilder = new ProcessBuilder()
                .command(args)
                .directory(builder.directoryProvider.getGameDir().toFile());

        // apply io redirect settings
        if (builder.ioConfig != null) {
            builder.ioConfig.applyTo(processBuilder);
        }

        // start game
        LOGGER.log(System.Logger.Level.INFO, "Starting the game");
        Process start = processBuilder.start();

        // consume game streams
        if (builder.ioConfig != null && builder.ioConfig.streamsConsumer != null) {
            builder.ioConfig.streamsConsumer.consume(
                    start.getInputStream(),
                    start.getOutputStream(),
                    start.getErrorStream()
            );
        }

        // clean natives after game closes
        start.onExit().thenRun(this::cleanNatives).join();
    }

    @Override
    public Set<FileInfo> getGameFiles() {
        return Collections.unmodifiableSet(gameFiles);
    }

    /**
     * Old versions may not run because they can't parse
     * lang line from newer options.txt file
     * @throws IOException if error happens
     */
    private void fixOptionsForOldVersion() throws IOException {
        LOGGER.log(System.Logger.Level.TRACE, "Fixing options file for older versions");
        Path optionsFile = builder.directoryProvider.getGameDir().resolve("options.txt");
        if (Files.notExists(optionsFile)) {
            return;
        }
        List<String> strings = Files.readAllLines(optionsFile, StandardCharsets.UTF_8);
        ListIterator<String> iterator = strings.listIterator();
        while (iterator.hasNext()) {
            String line = iterator.next();
            if (!line.startsWith("lang:")) {
                continue;
            }
            String langId = line.substring(5);
            String[] parts = langId.split("_");
            if (parts.length == 2) {
                // remove language info, newer versions may have more langs
                // so instead of fixing lang case, we just remove it
                // but only if second part is lower case
                if (parts[1].equalsIgnoreCase(parts[1].toLowerCase(Locale.ROOT))) {
                    iterator.remove();
                    Files.write(optionsFile, strings, StandardCharsets.UTF_8);
                }
                break;
            }
        }
    }

    private List<String> getGameArguments() {
        return getArguments(ArgumentTarget.GAME);
    }

    private List<String> getJavaArguments() {
        return getArguments(ArgumentTarget.JVM);
    }

    private List<String> getArguments(ArgumentTarget argumentTarget) {
        List<Argument> extArguments = builder.arguments.getOrDefault(argumentTarget, Collections.emptyList());
        List<Argument> arguments = processor.getArguments(argumentTarget);
        if (!extArguments.isEmpty()) {
            arguments = new ArrayList<>(arguments);
            arguments.addAll(extArguments);
        }
        return arguments.stream()
                .map(Argument::getValue)
                .flatMap(Collection::stream)
                .map(this::replacePlaceholders)
                .collect(Collectors.toList());
    }

    private Path unpackAssets(AssetIndex assetIndex, Path target) throws IOException {
        LOGGER.log(System.Logger.Level.INFO, "Unpacking assets");
        Path objectsDir = builder.directoryProvider.getAssetsDir().resolve("objects");
        for (Map.Entry<String, AssetIndex.Asset> entry : assetIndex.getObjects().entrySet()) {
            Path assetFile = objectsDir.resolve(entry.getValue().getHash().substring(0, 2))
                    .resolve(entry.getValue().getHash());
            Path targetFile = target.resolve(entry.getKey());
            Files.createDirectories(targetFile.getParent());
            gameFiles.add(new UnpackedAssetInfo(entry.getValue(), targetFile));
            if (Files.notExists(targetFile)) {
                Files.copy(assetFile, targetFile);
            }
        }
        return target;
    }

    private void unpackNatives() throws IOException {
        LOGGER.log(System.Logger.Level.INFO, "Unpacking natives");
        Path nativesDirectory = getNativesDirectory();
        Files.createDirectories(nativesDirectory);
        for (Configuration.LibrariesInfo librariesInfo : processor.getLibrariesInfos()) {
            for (LibraryArtifact aNative : librariesInfo.getNatives()) {
                unpackNative(librariesInfo, aNative);
            }
        }
    }

    private void unpackNative(Configuration.LibrariesInfo info, LibraryArtifact artifact) throws IOException {
        Path nativesDirectory = getNativesDirectory();
        Path librariesDir = builder.directoryProvider.getLibrariesDir();
        Path libraryFile = librariesDir.resolve(artifact.getPath());
        try (FileSystem fileSystem = FileSystems.newFileSystem(libraryFile, (ClassLoader) null)) {
            Path root = fileSystem.getPath("/");
            List<Path> files = Files.find(root, Integer.MAX_VALUE, GameLauncherImpl::filterNative).collect(Collectors.toList());
            for (Path file : files) {
                List<String> rules = info.getExtractRules().getOrDefault(ExtractRule.EXCLUDE, Collections.emptyList());
                if (!canUnpack(file, rules)) {
                    continue;
                }
                String lf = file.toAbsolutePath().toString();
                Path realFsPath = nativesDirectory.resolve(lf.substring(1));

                gameFiles.add(new UnpackedNativeInfo(realFsPath, Files.size(file)));

                if (Files.exists(realFsPath)) {
                    continue;
                }
                Files.createDirectories(realFsPath.getParent());
                Files.copy(file, realFsPath);
            }
        }
    }

    static boolean canUnpack(Path file, List<String> extractRules) {
        String filePath = file.toString();
        for (String extractRule : extractRules) {
            if (filePath.startsWith(extractRule) || filePath.startsWith("/" + extractRule)) {
                return false;
            }
        }
        return true;
    }

    private static boolean filterNative(Path path, BasicFileAttributes attributes) {
        return attributes.isRegularFile();
    }

    String replacePlaceholders(String arg) {
        if (arg.startsWith("${") && arg.endsWith("}") && !arg.contains(" ")) {
            String ret = placeholders.get(arg);
            if (ret != null) {
                return ret;
            }
            return replaceFeature(arg);
        }
        for (Map.Entry<String, String> entry : placeholders.entrySet()) {
            arg = arg.replace(entry.getKey(), entry.getValue());
        }
        return replaceFeature(arg);
    }

    private String replaceFeature(String arg) {
        for (GameLauncherBuilderImpl.FeatureInfo value : builder.features.values()) {
            if (value.getArgReplacer() != null) {
                arg = value.getArgReplacer().apply(arg);
            }
        }
        return arg;
    }

    private void fillPlaceholders(String assetIndexId, Path assetIndexPath) {
        LOGGER.log(System.Logger.Level.TRACE, "Filling placeholders");
        ObjectMapper mapper = ObjectMapperHolder.OBJECT_MAPPER;
        placeholders.put("${auth_player_name}", builder.user.getSelectedProfile().getName());
        placeholders.put("${version_name}", builder.gameVersion.getId());
        placeholders.put("${game_directory}", builder.directoryProvider.getGameDir().toAbsolutePath().toString());
        placeholders.put("${assets_root}", assetIndexPath.toAbsolutePath().toString());
        placeholders.put("${game_assets}", assetIndexPath.toAbsolutePath().toString());
        placeholders.put("${assets_index_name}", assetIndexId);
        placeholders.put("${auth_uuid}", builder.user.getSelectedProfile().getId());
        placeholders.put("${auth_access_token}", builder.accessToken);
        placeholders.put("${user_type}", UserType.MOJANG.toString());
        placeholders.put("${version_type}", builder.gameVersion.getReleaseType());

        try {
            placeholders.put("${user_properties}", mapper.writeValueAsString(builder.user.getProperties()));
        } catch (JsonProcessingException e) {
            LOGGER.log(System.Logger.Level.WARNING, "Can't serialize user properties", e);
        }
        try {
            placeholders.put("${profile_properties}", mapper.writeValueAsString(builder.user.getSelectedProfile().getProperties())); // not actually used in any version json file
        } catch (JsonProcessingException e) {
            LOGGER.log(System.Logger.Level.WARNING, "Can't serialize profile properties", e);
        }

        String sessionStr = "-";
        if (!builder.accessToken.isEmpty()) {
            sessionStr = String.format(
                    "token:%s:%s",
                    builder.accessToken,
                    builder.user.getSelectedProfile().getId()
            ); // format: token:${accessToken}:${profileId} or if no access token - otherwise
        }
        placeholders.put("${auth_session}", sessionStr);

        placeholders.put("${natives_directory}", getNativesDirectory().toAbsolutePath().toString());

        placeholders.put("${launcher_name}", builder.launcherId);
        placeholders.put("${launcher_version}", builder.launcherVersion);

        String mainJarPath = processor.getMainArtifactInfo().getFile().toAbsolutePath().toString();
        placeholders.put("${main_jar}", mainJarPath);

        placeholders.put("${classpath}", generateClasspath(true));
    }

    private Path getNativesDirectory() {
        return builder.directoryProvider.getTempDir()
                .resolve("bin")
                .resolve(runId.toString());
    }

    String generateClasspath(boolean failIfNotFound) {
        LOGGER.log(System.Logger.Level.TRACE, "Generating classpath");
        StringJoiner joiner = new StringJoiner(compatInfo.getPlatformInfo().getPathSeparator());
        for (Path path : builder.classpath) {
            joiner.add(path.toAbsolutePath().toString());
        }
        for (Path classpathEntry : processor.getClasspathEntries()) {
            if (failIfNotFound && Files.notExists(classpathEntry)) {
                throw new LauncherException("Library not found: " + classpathEntry.toAbsolutePath().toString());
            }
            joiner.add(classpathEntry.toAbsolutePath().toString());
        }
        return joiner.toString();
    }

    private void cleanNatives() {
        LOGGER.log(System.Logger.Level.TRACE, "Cleaning natives");
        try {
            Files.walkFileTree(getNativesDirectory(), new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.deleteIfExists(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.deleteIfExists(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            LOGGER.log(System.Logger.Level.WARNING, "Error when removing native file", e);
        }
    }
}