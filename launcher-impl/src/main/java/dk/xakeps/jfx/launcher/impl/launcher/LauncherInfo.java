/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.launcher.impl.version.InternalGameVersion;

import java.net.URI;
import java.util.Optional;

/**
 * Contains some info and helper methods for {@link InternalGameVersion#prepareLauncher(LauncherInfo)}
 */
public class LauncherInfo {
    private static final System.Logger LOGGER = System.getLogger(LauncherInfo.class.getName());

    private final CompatInfo compatInfo;
    private final String versionType;
    private final URI defaultMavenUri;
    private final VersionManager versionManager;

    public LauncherInfo(GameLauncherImpl gameLauncher) {
        LOGGER.log(System.Logger.Level.TRACE, "LauncherInfo created");
        this.compatInfo = gameLauncher.compatInfo;
        this.versionType = gameLauncher.builder.versionType;
        this.defaultMavenUri = gameLauncher.builder.defaultMavenRepo;
        this.versionManager = gameLauncher.builder.versionManager;
    }

    public CompatInfo getCompatInfo() {
        return compatInfo;
    }

    public String getVersionType() {
        return versionType;
    }

    public URI getDefaultMavenUri() {
        return defaultMavenUri;
    }

    public Optional<InternalGameVersion> getVersion(String id) {
        LOGGER.log(System.Logger.Level.TRACE, "getVersion({0})", id);
        Optional<InternalGameVersion> version = versionManager.getVersion(id)
                .map(gameVersion -> (InternalGameVersion) gameVersion);
        LOGGER.log(System.Logger.Level.TRACE, "getVersion({0}) = present({1})", id, version.isPresent());
        return version;
    }
}
