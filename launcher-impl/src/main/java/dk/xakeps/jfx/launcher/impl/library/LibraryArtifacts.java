/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class LibraryArtifacts {
    @JsonProperty("artifact")
    private final LibraryArtifact artifact;
    @JsonProperty("classifiers")
    private final Map<String, LibraryArtifact> classifiers;

    @JsonCreator
    public LibraryArtifacts(@JsonProperty(value = "artifact") LibraryArtifact artifact,
                            @JsonProperty("classifiers") Map<String, LibraryArtifact> classifiers) {
        this.artifact = artifact;
        this.classifiers = Objects.requireNonNullElse(classifiers, Collections.emptyMap());
    }

    public LibraryArtifact getArtifact() {
        return artifact;
    }

    public Map<String, LibraryArtifact> getClassifiers() {
        return classifiers;
    }
}
