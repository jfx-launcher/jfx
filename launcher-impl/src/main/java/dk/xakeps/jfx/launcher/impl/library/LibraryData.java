/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.library;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.LauncherException;
import dk.xakeps.jfx.launcher.impl.compat.EmptyCompatRule;
import dk.xakeps.jfx.launcher.impl.compat.ListCompatRule;
import dk.xakeps.jfx.launcher.impl.launcher.Configuration;
import dk.xakeps.jfx.launcher.impl.launcher.LauncherInfo;
import dk.xakeps.jfx.platform.PlatformInfo;

import java.net.URI;
import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LibraryData {
    @JsonProperty("name")
    private final String name;
    @JsonProperty("downloads")
    private final LibraryArtifacts downloads;
    @JsonProperty("natives")
    private final Map<String, String> natives;
    @JsonProperty("rules")
    private final CompatRule rules;
    @JsonProperty("extract")
    private final Map<ExtractRule, List<String>> extractRules;
    @JsonProperty("url")
    private final URI url;

    @JsonCreator
    public LibraryData(@JsonProperty("name") String name,
                       @JsonProperty("downloads") LibraryArtifacts downloads,
                       @JsonProperty("natives") Map<String, String> natives,
                       @JsonProperty("rules") ListCompatRule rules,
                       @JsonProperty("extract") Map<ExtractRule, List<String>> extractRules,
                       @JsonProperty("url") URI url) {
        this.name = name;
        this.downloads = downloads;
        this.natives = Objects.requireNonNullElse(natives, Collections.emptyMap());
        this.rules = Objects.requireNonNullElse(rules, EmptyCompatRule.INSTANCE);
        this.extractRules = Objects.requireNonNullElse(extractRules, Collections.emptyMap());
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public LibraryArtifacts getDownloads() {
        return downloads;
    }

    public Map<String, String> getNatives() {
        return natives;
    }

    public CompatRule getRules() {
        return rules;
    }

    public Map<ExtractRule, List<String>> getExtractRules() {
        return extractRules;
    }

    /**
     * Original launcher checks for natives first
     * if natives are found, then collects natives only and returns
     * without collecting artifact itself
     *
     * should we behave like original launcher?
     * @param launcherInfo launcher info
     * @return libraries info
     */
    public Optional<Configuration.LibrariesInfo> getLibrariesInfo(LauncherInfo launcherInfo) {
        if (!rules.shouldApply(launcherInfo.getCompatInfo())) {
            return Optional.empty();
        }
        Configuration.LibrariesInfo info = new Configuration.LibrariesInfo(name);
        if (downloads != null && downloads.getArtifact() != null) {
            info.getLibraries().add(downloads.getArtifact());
        }

        LibraryArtifact nativeLib = getNativeForPlatform(launcherInfo.getCompatInfo().getPlatformInfo());
        if (nativeLib != null) {
            info.getNatives().add(nativeLib);
            info.getExtractRules().putAll(extractRules);
        }

        // support for old json file
        if (downloads == null) {
            // org.lwjgl:lwjgl-tinyfd:3.2.1
            // groupId:artifactId[:extension[:classifier]]:version
            // resolve maven artifact url/maven-path
            // if url is null, use default (central? mojang?)
            URI repoUri = url != null ? url : launcherInfo.getDefaultMavenUri();
            if (repoUri == null) {
                throw new LauncherException(
                        String.format("Library %s has no artifacts or maven repo URL specified", name));
            }
            MavenData parse = MavenData.parse(name);
            String path = parse.getPath(true);
            URI artifactUri = repoUri.resolve(path);
            LibraryArtifact libraryArtifact = new LibraryArtifact(null, 0, artifactUri, path);
            info.getLibraries().add(libraryArtifact);
        }
        return Optional.of(info);
    }

    private LibraryArtifact getNativeForPlatform(PlatformInfo platformInfo) {
        String nativeId = natives.get(platformInfo.getName());
        if (downloads == null || nativeId == null) {
            return null;
        }

        String nativeIdWithArch = nativeId.replace("${arch}", platformInfo.getArch());
        LibraryArtifact libraryArtifact = downloads.getClassifiers().get(nativeIdWithArch);
        if (libraryArtifact != null) {
            return libraryArtifact;
        }

        // compatibility with some Mojang version jsons
        if (platformInfo.getArch().equalsIgnoreCase(PlatformInfo.x86)) {
            nativeIdWithArch = nativeId.replace("${arch}", "32");
        } else if (platformInfo.getArch().equalsIgnoreCase(PlatformInfo.x86_64)) {
            nativeIdWithArch = nativeId.replace("${arch}", "64");
        }

        return downloads.getClassifiers().get(nativeIdWithArch);
    }
}
