/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.library;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class to parse maven artifact string
 */
public class MavenData {
    private static final Pattern MAVEN_COORDS_PATTERN = Pattern.compile("([^: ]+):([^: ]+)(:([^: ]*)(:([^: ]+))?)?:([^: ]+)");
    private static final Pattern SNAPSHOT_TIMESTAMP_PATTERN = Pattern.compile("^(.*-)?([0-9]{8}\\.[0-9]{6}-[0-9]+)$");
    private static final String SNAPSHOT = "SNAPSHOT";

    final String coordinates;

    final String groupId;
    final String artifactId;
    final String extension;
    final String classifier;
    final String version;

    final boolean snapshot;
    final String rootVersion;

    private MavenData(String coordinates,
                      String groupId,
                      String artifactId,
                      String extension,
                      String classifier,
                      String version,
                      boolean snapshot,
                      String rootVersion) {
        this.coordinates = coordinates;
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.extension = extension;
        this.classifier = classifier;
        this.version = version;
        this.snapshot = snapshot;
        this.rootVersion = rootVersion;
    }

    static MavenData parse(String coords) {
        Matcher matcher = MAVEN_COORDS_PATTERN.matcher(coords);
        if (!matcher.matches()) {
            throw new IllegalArgumentException( "Bad artifact coordinates " + coords
                    + ", expected format is <groupId>:<artifactId>[:<extension>[:<classifier>]]:<version>" );
        }

        String groupId = matcher.group(1);
        String artifactId = matcher.group(2);
        String extension = Optional.ofNullable(matcher.group(4)).orElse("jar");
        String classifier = matcher.group(6);
        String version = matcher.group(7);

        boolean snapshot = isSnapshot(version);
        String rootVersion = version;
        if (snapshot) {
            rootVersion = getRootVersion(version);
        }

        return new MavenData(
                coords,
                groupId,
                artifactId,
                extension,
                classifier,
                version,
                snapshot,
                rootVersion);
    }

    static boolean isSnapshot(String version) {
        return endsWithIgnoreCase(version, SNAPSHOT) || SNAPSHOT_TIMESTAMP_PATTERN.matcher(version).matches();
    }

    /**
     * Removes timestamp form version string
     * @param version version string
     * @return version string without timestamp
     */
    static String getRootVersion(String version) {
        Matcher matcher = SNAPSHOT_TIMESTAMP_PATTERN.matcher(version);
        if (matcher.matches()) {
            String group;
            if ((group = matcher.group(1)) != null) {
                return group + SNAPSHOT;
            } else {
                return SNAPSHOT;
            }
        }
        return version;
    }

    /**
     * groupId/artifactId/rootVersion/artifactId-ver[-classifier].extension
     * @param local if it's local repo
     * @return repo path
     */
    public String getPath(boolean local) {
        StringBuilder builder = new StringBuilder();
        builder.append(groupId.replace('.', '/')).append('/')
                .append(artifactId).append('/')
                .append(rootVersion).append('/')
                .append(artifactId).append('-');
        if (local) {
            builder.append(rootVersion);
        } else {
            builder.append(version);
        }
        if (classifier != null && !classifier.isEmpty()) {
            builder.append('-').append(classifier);
        }
        builder.append('.').append(extension);

        return builder.toString();
    }

    private static boolean endsWithIgnoreCase(String str, String suffix) {
        int suffixLength = suffix.length();
        int matchFrom = str.length() - suffixLength;
        return str.regionMatches(true, matchFrom, suffix, 0, suffixLength);
    }
}
