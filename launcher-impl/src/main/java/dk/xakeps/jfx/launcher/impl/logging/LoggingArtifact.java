/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.logging;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.impl.Artifact;

import java.net.URI;

public class LoggingArtifact extends Artifact {
    private final String id;

    @JsonCreator
    public LoggingArtifact(@JsonProperty("sha1") String sha1,
                           @JsonProperty("size") long size,
                           @JsonProperty("url") URI uri,
                           @JsonProperty("id") String id) {
        super(sha1, size, uri);
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
