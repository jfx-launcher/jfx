/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.logging;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.argument.Argument;

public class LoggingData {
    @JsonProperty("argument")
    private final Argument argument;
    @JsonProperty("file")
    private final LoggingArtifact file;
    @JsonProperty("type")
    private final String type;

    @JsonCreator
    public LoggingData(@JsonProperty("argument") Argument argument,
                       @JsonProperty("file") LoggingArtifact file,
                       @JsonProperty("type") String type) {
        this.argument = argument;
        this.file = file;
        this.type = type;
    }

    public Argument getArgument() {
        return argument;
    }

    public LoggingArtifact getFile() {
        return file;
    }

    public String getType() {
        return type;
    }
}
