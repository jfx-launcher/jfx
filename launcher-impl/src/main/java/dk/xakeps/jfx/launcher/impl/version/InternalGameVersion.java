/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.impl.launcher.Configuration;
import dk.xakeps.jfx.launcher.impl.launcher.LauncherInfo;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Used to prepare launcher
 */
public interface InternalGameVersion extends GameVersion {
    List<Configuration> prepareLauncher(LauncherInfo launcherInfo, Set<String> prepared);

    default List<Configuration> prepareLauncher(LauncherInfo launcherInfo) {
        return prepareLauncher(launcherInfo, new LinkedHashSet<>());
    }
}
