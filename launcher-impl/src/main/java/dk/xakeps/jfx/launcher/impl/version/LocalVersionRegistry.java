/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionRegistry;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LocalVersionRegistry implements VersionRegistry {
    private static final System.Logger LOGGER = System.getLogger(LocalVersionRegistry.class.getName());
    private final Path root;
    private List<VersionData> versions;

    private LocalVersionRegistry(Path root, List<VersionData> versions) {
        this.root = root;
        LOGGER.log(System.Logger.Level.INFO, "Local version registry loaded");
        this.versions = versions;
    }

    @Override
    public List<GameVersion> getVersions() {
        return Collections.unmodifiableList(versions);
    }

    @Override
    public void reload() {
        this.versions = loadVersions(root);
    }

    public boolean addInstalled(VersionData versionData) {
        LOGGER.log(System.Logger.Level.TRACE, "Adding installed version {0}", versionData.getId());
        if (getVersionById(versionData.getId()).isEmpty()) {
            this.versions.add(versionData);
            LOGGER.log(System.Logger.Level.TRACE, "Version {0} installed", versionData.getId());
            return true;
        }
        LOGGER.log(System.Logger.Level.TRACE, "Version already {0} installed", versionData.getId());
        return false;
    }

    static LocalVersionRegistry load(Path root) {
        return new LocalVersionRegistry(root, loadVersions(root));
    }

    static LocalVersionRegistry empty(Path root) {
        return new LocalVersionRegistry(root, new ArrayList<>());
    }

    private static List<VersionData> loadVersions(Path root) {
        LOGGER.log(System.Logger.Level.INFO, "Loading local version registry");
        try {
            if (Files.notExists(root)) {
                LOGGER.log(System.Logger.Level.INFO, "Local versions directory is empty");
                Files.createDirectories(root);
                return new ArrayList<>(0);
            }
            LOGGER.log(System.Logger.Level.INFO, "Parsing versions from directory {0}", root);
            return Files.find(root, Integer.MAX_VALUE, LocalVersionRegistry::filterPath)
                    .map(LocalVersionRegistry::loadJson)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.log(System.Logger.Level.WARNING, "Local version registry not available", e);
            return new ArrayList<>(1);
        }
    }

    private static boolean filterPath(Path path, BasicFileAttributes attributes) {
        return path.getFileName().toString().endsWith(".json") && attributes.isRegularFile();
    }

    private static VersionData loadJson(Path path) {
        LOGGER.log(System.Logger.Level.TRACE, "Loading local version, path {0}", path);
        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            return ObjectMapperHolder.OBJECT_MAPPER.readValue(br, VersionData.class);
        } catch (IOException e) {
            try {
                Files.deleteIfExists(path);
            } catch (IOException ioException) {
                LOGGER.log(System.Logger.Level.WARNING, "Can't remove broken version file {0}", path, ioException);
            }
            LOGGER.log(System.Logger.Level.WARNING, "Can't load local version, path {0}", path, e);
            return null;
        }
    }
}
