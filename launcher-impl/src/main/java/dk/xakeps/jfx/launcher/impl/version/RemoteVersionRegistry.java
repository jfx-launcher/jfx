/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionRegistry;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;

import java.io.IOException;
import java.net.URI;
import java.util.Collections;
import java.util.List;

public class RemoteVersionRegistry implements VersionRegistry {
    private static final System.Logger LOGGER = System.getLogger(RemoteVersionRegistry.class.getName());

    private final URI source;
    private VersionManifest manifest;

    private RemoteVersionRegistry(URI source, VersionManifest manifest) {
        this.source = source;
        LOGGER.log(System.Logger.Level.INFO, "Remote version registry loaded");
        this.manifest = manifest;
    }

    @Override
    public List<GameVersion> getVersions() {
        return Collections.unmodifiableList(manifest.getVersions());
    }

    @Override
    public void reload() {
        this.manifest = loadManifest(source);
    }

    static RemoteVersionRegistry load(URI source) {
        return new RemoteVersionRegistry(source, loadManifest(source));
    }

    static RemoteVersionRegistry empty(URI source) {
        return new RemoteVersionRegistry(source, new VersionManifest(Collections.emptyMap(), Collections.emptyList()));
    }

    private static VersionManifest loadManifest(URI source) {
        LOGGER.log(System.Logger.Level.INFO, "Loading remote version registry");
        try {
            return ObjectMapperHolder.OBJECT_MAPPER.readValue(source.toURL(), VersionManifest.class);
        } catch (IOException e) {
            LOGGER.log(System.Logger.Level.WARNING, "Remote version registry not available", e);
            return new VersionManifest(Collections.emptyMap(), Collections.emptyList());
        }
    }
}