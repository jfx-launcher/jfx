/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionException;
import dk.xakeps.jfx.launcher.impl.launcher.Configuration;
import dk.xakeps.jfx.launcher.impl.launcher.LauncherInfo;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Represents version entry from version_manifest.json file
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShortGameVersion implements GameVersion, InternalGameVersion {
    private static final System.Logger LOGGER = System.getLogger(ShortGameVersion.class.getName());

    @JsonProperty("id")
    private final String id;
    @JsonProperty("time")
    private final OffsetDateTime updateTime;
    @JsonProperty("releaseTime")
    private final OffsetDateTime releaseTime;
    @JsonProperty("type")
    private final String releaseType;
    @JsonProperty("url")
    private final URI url;
    @JsonProperty("_versionConfig")
    private final ShortGameVersionConfig versionConfig;

    @JsonIgnore
    private VersionData versionData;

    @JsonCreator
    public ShortGameVersion(@JsonProperty(value = "id", required = true) String id,
                            @JsonProperty("time") OffsetDateTime updateTime,
                            @JsonProperty("releaseTime") OffsetDateTime releaseTime,
                            @JsonProperty("type") String releaseType,
                            @JsonProperty(value = "url", required = true) URI url,
                            @JsonProperty("_versionConfig") ShortGameVersionConfig versionConfig) {
        this.id = Objects.requireNonNull(id, "id");
        this.updateTime = Objects.requireNonNullElse(updateTime, OffsetDateTime.now());
        this.releaseTime = Objects.requireNonNullElse(releaseTime, OffsetDateTime.now());
        this.releaseType = Objects.requireNonNullElse(releaseType, "");
        this.url = Objects.requireNonNull(url, "url");
        this.versionConfig = versionConfig;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public OffsetDateTime getUpdateTime() {
        return updateTime;
    }

    @Override
    public OffsetDateTime getReleaseTime() {
        return releaseTime;
    }

    @Override
    public String getReleaseType() {
        return releaseType;
    }

    @Override
    public Set<String> getVersionTypes() {
        return getRemoteVersion().getVersionTypes();
    }

    public URI getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShortGameVersion that = (ShortGameVersion) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public List<Configuration> prepareLauncher(LauncherInfo launcherInfo, Set<String> prepared) {
        LOGGER.log(System.Logger.Level.TRACE, "Preparing version {0}", id);

        if (versionConfig != null) {
            if (versionConfig.installType() == ShortGameVersionInstallType.FORGE_MODERN) {
                // todo:
                // 1. Run installer (?)
                // 1.1. How to give install feedback?
                // 1.2. Custom Configuration? Feels dirty
                // 2. Find and load forge JSON
                // 3. Profit!
                throw new UnsupportedOperationException("modern forge not supported yet");
            }
        }

        return getRemoteVersion().prepareLauncher(launcherInfo, prepared);
    }

    private VersionData getRemoteVersion() {
        LOGGER.log(System.Logger.Level.TRACE, "Getting full version json...");
        if (versionData != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Full version json already loaded");
            return versionData;
        }

        LOGGER.log(System.Logger.Level.TRACE, "Loading full version json");
        HttpRequest request = HttpRequest.newBuilder(url).build();
        try {
            HttpResponse<Result<VersionData, String>> response = Methanol.create().send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(VersionData.class),
                            HttpResponse.BodyHandlers.ofString()));
            String versionError = response.body().error().orElse(null);
            if (versionError != null) {
                LOGGER.log(System.Logger.Level.ERROR, "Can't load full version json, error: {0}. id {1}", versionError, id);
                throw new VersionException("Can't load remote version. Error: " + versionError);
            }
            versionData = response.body().response()
                    .orElseThrow(() -> new VersionException("Failed to load remote VersionData. Version: " + id));
        } catch (IOException | InterruptedException e) {
            LOGGER.log(System.Logger.Level.ERROR, "Can't load full version json. id {0}", id);
            throw new VersionException("Failed to load remote VersionData", e);
        }

        return versionData;
    }
}
