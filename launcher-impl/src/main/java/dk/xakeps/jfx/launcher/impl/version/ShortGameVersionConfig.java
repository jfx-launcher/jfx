package dk.xakeps.jfx.launcher.impl.version;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ShortGameVersionConfig(@JsonProperty("installType") ShortGameVersionInstallType installType) {
}
