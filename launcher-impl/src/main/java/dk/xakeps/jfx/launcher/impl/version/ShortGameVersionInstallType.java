package dk.xakeps.jfx.launcher.impl.version;

public enum ShortGameVersionInstallType {
    FORGE_OLD, FORGE_MODERN
}
