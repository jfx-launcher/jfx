/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.Artifact;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.compat.EmptyCompatRule;
import dk.xakeps.jfx.launcher.impl.launcher.Configuration;
import dk.xakeps.jfx.launcher.impl.launcher.LauncherInfo;
import dk.xakeps.jfx.launcher.impl.library.LibraryData;
import dk.xakeps.jfx.launcher.impl.logging.LoggingData;

import java.time.OffsetDateTime;
import java.util.*;

/**
 * Represents full version json file
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionData implements GameVersion, InternalGameVersion {
    private static final System.Logger LOGGER = System.getLogger(VersionData.class.getName());

    @JsonProperty("id")
    private String id;
    @JsonProperty("inheritsFrom")
    private String inheritsFrom;
    @JsonProperty("assets")
    private String assets;
    @JsonProperty("mainClass")
    private String mainClass;
    @JsonProperty("minecraftArguments")
    private String minecraftArguments;
    @JsonProperty("arguments")
    private Map<ArgumentTarget, List<Argument>> arguments;
    @JsonProperty("assetIndex")
    private AssetIndexArtifact assetIndex;
    @JsonProperty("downloads")
    private Map<String, Artifact> downloads;
    @JsonProperty("libraries")
    private List<LibraryData> libraries;
    @JsonProperty("logging")
    private Map<String, LoggingData> logging;
    @JsonProperty("minimumLauncherVersion")
    private int minimumLauncherVersion;
    @JsonProperty("releaseTime")
    private OffsetDateTime releaseTime;
    @JsonProperty("time")
    private OffsetDateTime updateTime;
    @JsonProperty("type")
    private String releaseType;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInheritsFrom() {
        return inheritsFrom;
    }

    public void setInheritsFrom(String inheritsFrom) {
        this.inheritsFrom = inheritsFrom;
    }

    public String getAssets() {
        return assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getMinecraftArguments() {
        return minecraftArguments;
    }

    public void setMinecraftArguments(String minecraftArguments) {
        this.minecraftArguments = minecraftArguments;
    }

    public Map<ArgumentTarget, List<Argument>> getArguments() {
        return arguments;
    }

    public void setArguments(Map<ArgumentTarget, List<Argument>> arguments) {
        this.arguments = arguments;
    }

    public AssetIndexArtifact getAssetIndex() {
        return assetIndex;
    }

    public void setAssetIndex(AssetIndexArtifact assetIndex) {
        this.assetIndex = assetIndex;
    }

    public Map<String, Artifact> getDownloads() {
        return downloads;
    }

    public void setDownloads(Map<String, Artifact> downloads) {
        this.downloads = downloads;
    }

    public List<LibraryData> getLibraries() {
        return libraries;
    }

    public void setLibraries(List<LibraryData> libraries) {
        this.libraries = libraries;
    }

    public Map<String, LoggingData> getLogging() {
        return logging;
    }

    public void setLogging(Map<String, LoggingData> logging) {
        this.logging = logging;
    }

    public int getMinimumLauncherVersion() {
        return minimumLauncherVersion;
    }

    public void setMinimumLauncherVersion(int minimumLauncherVersion) {
        this.minimumLauncherVersion = minimumLauncherVersion;
    }

    @Override
    public OffsetDateTime getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(OffsetDateTime releaseTime) {
        this.releaseTime = releaseTime;
    }

    @Override
    public OffsetDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(OffsetDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String getReleaseType() {
        return releaseType;
    }

    @JsonIgnore
    @Override
    public Set<String> getVersionTypes() {
        return downloads.keySet();
    }

    public void setReleaseType(String releaseType) {
        this.releaseType = releaseType;
    }

    @Override
    public List<Configuration> prepareLauncher(LauncherInfo launcherInfo, Set<String> prepared) {
        LOGGER.log(System.Logger.Level.INFO, "Preparing version {0}", id);
        List<Configuration> configs = new ArrayList<>(1);
        if (inheritsFrom != null && !inheritsFrom.isEmpty()) {
            LOGGER.log(System.Logger.Level.INFO, "Parent not empty, {0}", inheritsFrom);
            if (!prepared.add(inheritsFrom)) {
                LOGGER.log(System.Logger.Level.ERROR, "Circular inheritance detected! Child id: {0}, Inheritance tree: {1}", id, prepared);
                throw new IllegalStateException(String.format(
                        "Circular inheritance detected! Child id: %s, Inheritance tree: %s",
                        id,
                        prepared
                ));
            }
            LOGGER.log(System.Logger.Level.INFO, "Preparing parent");
            Optional<InternalGameVersion> parent = launcherInfo.getVersion(inheritsFrom);
            if (parent.isEmpty()) {
                LOGGER.log(System.Logger.Level.ERROR, "Parent version not found, child {0}, parent {1}", id, inheritsFrom);
                throw new IllegalArgumentException(
                        String.format("Parent version not found. Child: %s, parent: %s", id, inheritsFrom)
                );
            }
            configs = parent.get().prepareLauncher(launcherInfo, prepared);
        }
        CompatInfo compatInfo = launcherInfo.getCompatInfo();
        String versionType = launcherInfo.getVersionType();

        Configuration configuration = new Configuration(this);
        configs.add(configuration);
        if (downloads != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Adding main artifact");
            configuration.setMainArtifact(downloads.get(versionType));
        }

        if (libraries != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Adding libraries");
            for (LibraryData library : libraries) {
                library.getLibrariesInfo(launcherInfo)
                        .ifPresent(configuration.getLibrariesInfos()::add);
            }
        }

        if (logging != null) {
            LoggingData loggingData = logging.get(versionType);
            if (loggingData != null) {
                LOGGER.log(System.Logger.Level.TRACE, "Setting logging data");
                if (loggingData.getArgument().getRules().shouldApply(compatInfo)) {
                    Configuration.LoggingInfo loggingInfo = new Configuration.LoggingInfo(
                            loggingData.getArgument(),
                            loggingData.getFile()
                    );
                    configuration.setLoggingInfo(loggingInfo);
                }
            }
        }

        LOGGER.log(System.Logger.Level.TRACE, "Setting main class");
        configuration.setMainClass(mainClass);
        if (minecraftArguments != null && !minecraftArguments.isEmpty()) {
            LOGGER.log(System.Logger.Level.TRACE, "Setting minecraftArguments string");
            configuration.setInheritArguments(false);
            List<Argument> arguments = configuration.getArguments(ArgumentTarget.GAME);
            String[] args = minecraftArguments.split(" ");
            Argument argument = new Argument(Arrays.asList(args), EmptyCompatRule.INSTANCE);
            arguments.add(argument);
        } else if (arguments != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Setting arguments map");
            configuration.setInheritArguments(true);
            for (Map.Entry<ArgumentTarget, List<Argument>> entry : arguments.entrySet()) {
                for (Argument argument : entry.getValue()) {
                    if (argument.getRules().shouldApply(compatInfo)) {
                        configuration.getArguments(entry.getKey()).add(argument);
                    }
                }
            }
        }

        if (assets != null || assetIndex != null) {
            LOGGER.log(System.Logger.Level.TRACE, "Setting assets");
            configuration.setAssetsInfo(new Configuration.AssetsInfo(assetIndex, assets));
        }

        LOGGER.log(System.Logger.Level.INFO, "Version {0} prepared", id);
        return configs;
    }
}
