/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.VersionListener;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.launcher.impl.Constants;

import java.net.URI;
import java.nio.file.Path;
import java.util.Objects;

public class VersionManagerBuilderImpl implements VersionManager.Builder {
    private static final System.Logger LOGGER = System.getLogger(VersionManagerBuilderImpl.class.getName());

    Path versionsDir;
    VersionListener versionListener;
    URI manifestsUri = Constants.MANIFESTS_URI;

    @Override
    public VersionManager.Builder versionsDir(Path versionsDir) {
        LOGGER.log(System.Logger.Level.TRACE, "versionsDir set");
        this.versionsDir = Objects.requireNonNull(versionsDir, "versionsDir");
        return this;
    }

    @Override
    public VersionManager.Builder versionListener(VersionListener versionListener) {
        LOGGER.log(System.Logger.Level.TRACE, "versionListener set");
        this.versionListener = versionListener;
        return this;
    }

    /**
     * @param manifestsUri uri to version_manifests.json file
     * @return itself, for chaining
     */
    @Override
    public VersionManager.Builder manifestsUri(URI manifestsUri) {
        LOGGER.log(System.Logger.Level.TRACE, "manifestsUri set");
        this.manifestsUri = Objects.requireNonNull(manifestsUri, "manifestsUri");
        return this;
    }

    @Override
    public VersionManager build() {
        LOGGER.log(System.Logger.Level.TRACE, "building version manager");
        Objects.requireNonNull(versionsDir, "versionsDir");
        return VersionManagerImpl.create(this);
    }
}
