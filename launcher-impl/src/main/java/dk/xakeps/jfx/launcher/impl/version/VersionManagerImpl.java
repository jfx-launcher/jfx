/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.GameLauncher;
import dk.xakeps.jfx.launcher.VersionListener;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.launcher.VersionManagerAdmin;
import dk.xakeps.jfx.launcher.impl.launcher.GameLauncherBuilderImpl;
import dk.xakeps.jfx.launcher.impl.version.admin.VersionManagerAdminBuilderImpl;

import java.util.Optional;

public class VersionManagerImpl implements VersionManager {
    private static final System.Logger LOGGER = System.getLogger(VersionManagerImpl.class.getName());

    private final VersionManagerBuilderImpl builder;
    private final RemoteVersionRegistry remoteRegistry;
    private final LocalVersionRegistry localRegistry;

    public VersionManagerImpl(VersionManagerBuilderImpl builder, RemoteVersionRegistry remoteRegistry,
                              LocalVersionRegistry localRegistry) {
        LOGGER.log(System.Logger.Level.TRACE, "VersionManager created");
        this.builder = builder;
        this.remoteRegistry = remoteRegistry;
        this.localRegistry = localRegistry;
    }

    @Override
    public RemoteVersionRegistry getRemoteRegistry() {
        return remoteRegistry;
    }

    @Override
    public LocalVersionRegistry getLocalRegistry() {
        return localRegistry;
    }

    @Override
    public GameLauncher.Builder newLauncherBuilder() {
        LOGGER.log(System.Logger.Level.TRACE, "Creating GameLauncher Builder");
        return new GameLauncherBuilderImpl(this, builder.versionsDir);
    }

    @Override
    public VersionManagerAdmin.Builder newVersionManagerAdminBuilder() {
        LOGGER.log(System.Logger.Level.TRACE, "Creating VersionManagerAdmin Builder");
        return new VersionManagerAdminBuilderImpl();
    }

    public Optional<VersionListener> getVersionListener() {
        return Optional.ofNullable(builder.versionListener);
    }

    static VersionManager create(VersionManagerBuilderImpl builder) {
        return new VersionManagerImpl(builder,
                RemoteVersionRegistry.empty(builder.manifestsUri),
                LocalVersionRegistry.empty(builder.versionsDir));
    }
}
