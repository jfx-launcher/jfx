/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class VersionManifest {
    @JsonProperty("latest")
    private final Map<String, String> latest;
    @JsonProperty("versions")
    private final List<ShortGameVersion> versions;

    @JsonCreator
    public VersionManifest(@JsonProperty("latest") Map<String, String> latest,
                           @JsonProperty("versions") List<ShortGameVersion> versions) {
        this.latest = latest;
        this.versions = versions;
    }

    public Map<String, String> getLatest() {
        return latest;
    }

    public List<ShortGameVersion> getVersions() {
        return versions;
    }
}
