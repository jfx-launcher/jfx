package dk.xakeps.jfx.launcher.impl.version.admin;

import dk.xakeps.jfx.launcher.VersionManagerAdmin;

import java.net.URI;
import java.util.Objects;

public class VersionManagerAdminBuilderImpl implements VersionManagerAdmin.Builder {
    private static final System.Logger LOGGER = System.getLogger(VersionManagerAdminBuilderImpl.class.getName());

    String accessToken;
    URI serviceUri;

    @Override
    public VersionManagerAdmin.Builder accessToken(String accessToken) {
        LOGGER.log(System.Logger.Level.TRACE, "accessToken set");
        this.accessToken = Objects.requireNonNull(accessToken, "accessToken");
        return this;
    }

    @Override
    public VersionManagerAdmin.Builder serviceUri(URI serviceUri) {
        LOGGER.log(System.Logger.Level.TRACE, "serviceUri set");
        this.serviceUri = Objects.requireNonNull(serviceUri, "serviceUri");
        return this;
    }

    @Override
    public VersionManagerAdmin build() {
        Objects.requireNonNull(accessToken, "accessToken");
        Objects.requireNonNull(serviceUri, "serviceUri");
        return new VersionManagerAdminImpl(this);
    }
}
