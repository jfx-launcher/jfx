package dk.xakeps.jfx.launcher.impl.version.admin;

import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.MediaType;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MultipartBodyPublisher;
import dk.xakeps.jfx.launcher.VersionException;
import dk.xakeps.jfx.launcher.VersionManagerAdmin;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;

public class VersionManagerAdminImpl implements VersionManagerAdmin {
    private final Methanol methanol;
    private final URI uploadForgeEndpoint;

    public VersionManagerAdminImpl(VersionManagerAdminBuilderImpl builder) {
        this.methanol = Methanol.newBuilder()
                .defaultHeader("Authorization",
                        "Bearer " + builder.accessToken)
                .build();
        this.uploadForgeEndpoint = builder.serviceUri.resolve("manage/upload/forge");
    }

    @Override
    public void uploadForge(Path file) {
        HttpRequest request;
        try {
            request = HttpRequest.newBuilder(uploadForgeEndpoint)
                    .version(HttpClient.Version.HTTP_1_1)
                    .POST(MultipartBodyPublisher.newBuilder()
                            .filePart("file", file, MediaType.APPLICATION_OCTET_STREAM)
                            .build())
                    .build();
        } catch (FileNotFoundException e) {
            throw new VersionException("Forge installer JAR not found", e);
        }
        try {
            HttpResponse<String> response = methanol.send(request, HttpResponse.BodyHandlers.ofString());
            if (!HttpStatus.isSuccessful(response.statusCode())) {
                throw new VersionException("Can't upload forge installer. Response body: " + response.body());
            }
        } catch (IOException | InterruptedException e) {
            throw new VersionException("Can't upload forge installer", e);
        }
    }
}
