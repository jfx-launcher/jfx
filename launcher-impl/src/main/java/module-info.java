/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import com.github.mizosoft.methanol.BodyAdapter;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.launcher.impl.JacksonAdapters;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerBuilderImpl;

module dk.xakeps.jfx.launcher.impl {
    requires dk.xakeps.jfx.launcher;
    requires dk.xakeps.jfx.downloader;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.fasterxml.jackson.datatype.jdk8;
    requires methanol;
    requires methanol.adapter.jackson;
    requires dk.xakeps.jfx.httpclient.utils;

    opens dk.xakeps.jfx.launcher.impl to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.launcher.impl.compat to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.launcher.impl.library to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.launcher.impl.logging to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.launcher.impl.version to com.fasterxml.jackson.databind;

    provides BodyAdapter.Encoder with JacksonAdapters.JacksonEncoder;
    provides BodyAdapter.Decoder with JacksonAdapters.JacksonDecoder;
    provides VersionManager.Builder with VersionManagerBuilderImpl;
}