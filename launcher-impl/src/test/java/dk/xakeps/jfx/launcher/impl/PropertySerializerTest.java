/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.user.Property;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PropertySerializerTest {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER
                .enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @ParameterizedTest
    @MethodSource("testProperties")
    public void testSerializeProperty(Property property) throws JsonProcessingException {
        String s = mapper.writeValueAsString(property);
        TestProperty testProperty = ObjectMapperHolder.OBJECT_MAPPER.readValue(s, TestProperty.class);
        assertEquals(property.getName(), testProperty.name);
        assertEquals(property.getValue(), testProperty.value);
        assertEquals(property.getSignature().orElse(null), testProperty.signature);
    }

    @Test
    public void testSerializePropertyMap() throws JsonProcessingException {
        Map<String, Property> properties = new HashMap<>();
        properties.put("textures", new TestProperty("texturesProperty", "texturesValue", "texturesSignature"));
        String s = mapper.writeValueAsString(properties);
        Map<String, TestProperty> deser = mapper.readValue(s, new TypeReference<>() {});
        assertEquals(properties, deser);
    }

    public static Stream<Property> testProperties() {
        return Stream.of(
                new TestProperty("name", "value", null),
                new TestProperty("name", "value", "signature"),
                new TestProperty() {
                    private final String garbage = "garbage";
                }
        );
    }

    public static class TestProperty implements Property {
        public String name = "name";
        public String value = "value";
        public String signature;

        public TestProperty() {
        }

        public TestProperty(String name, String value, String signature) {
            this.name = Objects.requireNonNullElse(name, this.name);
            this.value = Objects.requireNonNullElse(value, this.value);
            this.signature = signature;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public Optional<String> getSignature() {
            return Optional.ofNullable(signature);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestProperty that = (TestProperty) o;
            return Objects.equals(name, that.name) &&
                    Objects.equals(value, that.value) &&
                    Objects.equals(signature, that.signature);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, value, signature);
        }
    }
}
