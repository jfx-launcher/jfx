/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl;

import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionManager;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerBuilderImpl;
import dk.xakeps.jfx.platform.PlatformInfo;
import dk.xakeps.jfx.network.ProgressInfo;
import dk.xakeps.jfx.network.ProgressListener;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import dk.xakeps.jfx.user.GameProfile;
import dk.xakeps.jfx.user.Property;
import dk.xakeps.jfx.user.User;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {
    public static void main(String[] args) throws IOException {
        DirectoryProviderImpl provider = new DirectoryProviderImpl(Paths.get("launcher_data").toAbsolutePath());
        VersionManager.Builder versionManagerBuilder = new VersionManagerBuilderImpl();
        VersionManager versionManager = versionManagerBuilder.versionsDir(provider.getVersionsDir()).build();
        GameVersion gameVersion = versionManager.getVersion("1.16.3").orElseThrow();
        versionManager.newLauncherBuilder()
                .directoryProvider(provider)
                .platformInfo(new PlatformInfo() {
                    @Override
                    public Path getJavaExecutable() {
                        return Paths.get("/usr/lib/jvm/java-8-openjdk/bin/java");
                    }
                })
                .gameVersion(gameVersion)
                .versionType("client")
                .user(new UserImpl())
                .progressListenerFactory(new ProgressListenerFactoryImpl()).build().prepare().start();
    }

    private static final class DirectoryProviderImpl implements DirectoryProvider {
        private final Path root;

        public DirectoryProviderImpl(Path root) {
            this.root = root;
        }

        public Path getVersionsDir() {
            return root.resolve("versions");
        }

        @Override
        public Path getGameDir() {
            return root.resolve("game");
        }

        @Override
        public Path getAssetsDir() {
            return root.resolve("assets");
        }

        @Override
        public Path getResourcePackDir() {
            return getGameDir().resolve("resourcepacks");
        }

        @Override
        public Path getDataPackDir() {
            return getGameDir().resolve("datapacks");
        }

        @Override
        public Path getLibrariesDir() {
            return root.resolve("libraries");
        }

        @Override
        public Path getTempDir() {
            return root.resolve("temp");
        }
    }

    private static final class ProgressListenerFactoryImpl implements ProgressListenerFactory {
        private final Map<String, List<ProgressListener>> listeners = new HashMap<>();

        @Override
        public ProgressListener newListener(String listenedId, long size) {
            return new ProgressListenerImpl();
        }
    }

    private static final class ProgressListenerImpl implements ProgressListener {
        @Override
        public void notify(ProgressInfo progressInfo) {

        }

        @Override
        public void close() throws IOException {

        }
    }

    private static final class UserImpl implements User {

        @Override
        public List<Property> getProperties() {
            return Collections.emptyList();
        }

        @Override
        public GameProfile getSelectedProfile() {
            return new GameProfileImpl();
        }

        @Override
        public List<GameProfile> getAvailableProfiles() {
            return Collections.singletonList(getSelectedProfile());
        }
    }

    private static final class GameProfileImpl implements GameProfile {

        @Override
        public String getId() {
            return "";
        }

        @Override
        public String getName() {
            return "Player";
        }

        @Override
        public List<Property> getProperties() {
            return Collections.emptyList();
        }
    }
}
