/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.argument;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.compat.CompatRuleImpl;
import dk.xakeps.jfx.launcher.impl.compat.ListCompatRule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ArgumentDeserializerTest {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    @Test
    public void testArgumentDeserializer() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_arguments.json"),
                new TypeReference<List<Argument>>() {}
                );

        Iterator<Argument> iterator = arguments.iterator();
        Argument arg = iterator.next();
        assertEquals(1, arg.getValue().size());
    }

    @Test
    public void testArgumentDeserializerRuleFeaturesNoArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_features_no_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(0, arg.getValue().size());
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertEquals(1, rule.getFeatures().size());
        assertTrue(rule.getFeatures().containsKey("is_demo_user"));
        assertEquals(true, rule.getFeatures().get("is_demo_user"));
    }

    @Test
    public void testArgumentDeserializerRuleFeaturesFieldArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_features_field_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(1, arg.getValue().size());
        assertEquals("--demo", arg.getValue().get(0));
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertEquals(1, rule.getFeatures().size());
        assertTrue(rule.getFeatures().containsKey("is_demo_user"));
        assertEquals(true, rule.getFeatures().get("is_demo_user"));

    }

    @Test
    public void testArgumentDeserializerRuleFeaturesArrArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_features_arr_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(2, arg.getValue().size());
        assertEquals("--demo", arg.getValue().get(0));
        assertEquals("--demo2", arg.getValue().get(1));
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertEquals(1, rule.getFeatures().size());
        assertTrue(rule.getFeatures().containsKey("is_demo_user"));
        assertEquals(true, rule.getFeatures().get("is_demo_user"));

    }

    @Test
    public void testArgumentDeserializerRuleOsNoArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_os_no_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(0, arg.getValue().size());
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertNotNull(rule.getOs());
        assertEquals("windows", rule.getOs().getName());
        assertEquals("10", rule.getOs().getVersion());
        assertEquals("x86", rule.getOs().getArch());
    }

    @Test
    public void testArgumentDeserializerRuleOsFieldArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_os_field_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(1, arg.getValue().size());
        assertEquals("--win-arg", arg.getValue().get(0));
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertNotNull(rule.getOs());
        assertEquals("windows", rule.getOs().getName());
        assertEquals("10", rule.getOs().getVersion());
        assertEquals("x86", rule.getOs().getArch());
    }

    @Test
    public void testArgumentDeserializerRuleOsArrArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_os_arr_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(2, arg.getValue().size());
        assertEquals("--win-arg", arg.getValue().get(0));
        assertEquals("--win-arg2", arg.getValue().get(1));
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertNotNull(rule.getOs());
        assertEquals("windows", rule.getOs().getName());
        assertEquals("10", rule.getOs().getVersion());
        assertEquals("x86", rule.getOs().getArch());
    }

    @Test
    public void testArgumentDeserializerRuleOsAndFeaturesNoArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_os_and_features_no_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(0, arg.getValue().size());
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertNotNull(rule.getOs());
        assertEquals("windows", rule.getOs().getName());
        assertEquals("10", rule.getOs().getVersion());
        assertEquals("x86", rule.getOs().getArch());
        assertEquals(1, rule.getFeatures().size());
        assertTrue(rule.getFeatures().containsKey("is_demo_user"));
        assertEquals(true, rule.getFeatures().get("is_demo_user"));
    }

    @Test
    public void testArgumentDeserializerRuleOsAndFeaturesFieldArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_rule_os_and_features_arr_arg.json"),
                new TypeReference<List<Argument>>() {}
        );
        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(2, arg.getValue().size());
        assertEquals("--only-on-win-with-feat", arg.getValue().get(0));
        assertEquals("--only-on-win-with-feat2", arg.getValue().get(1));
        CompatRule rules = arg.getRules();
        ListCompatRule list = (ListCompatRule) rules;
        assertEquals(1, list.getRules().size());
        CompatRuleImpl rule = (CompatRuleImpl) list.getRules().get(0);
        assertEquals(CompatRule.Action.ALLOW, rule.getAction());
        assertNotNull(rule.getOs());
        assertEquals("windows", rule.getOs().getName());
        assertEquals("10", rule.getOs().getVersion());
        assertEquals("x86", rule.getOs().getArch());
        assertEquals(1, rule.getFeatures().size());
        assertTrue(rule.getFeatures().containsKey("is_demo_user"));
        assertEquals(true, rule.getFeatures().get("is_demo_user"));
    }

    @Test
    public void testArgumentDeserializerFieldArgument() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_field_argument.json"),
                new TypeReference<List<Argument>>() {}
        );

        assertEquals(4, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(1, arg.getValue().size());
        assertEquals("--username", arg.getValue().get(0));

        arg = arguments.get(1);
        assertEquals(1, arg.getValue().size());
        assertEquals("${auth_player_name}", arg.getValue().get(0));

        arg = arguments.get(2);
        assertEquals(1, arg.getValue().size());
        assertEquals("--version", arg.getValue().get(0));

        arg = arguments.get(3);
        assertEquals(1, arg.getValue().size());
        assertEquals("${version_name}", arg.getValue().get(0));
    }

    @Test
    public void testArgumentDeserializerObjectArgumentFieldArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_object_argument_arg_field.json"),
                new TypeReference<List<Argument>>() {}
        );

        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(1, arg.getValue().size());
        assertEquals("--demo", arg.getValue().get(0));
    }

    @Test
    public void testArgumentDeserializerObjectArgumentArrArg() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_object_argument_arg_arr.json"),
                new TypeReference<List<Argument>>() {}
        );

        assertEquals(1, arguments.size());
        Argument arg = arguments.get(0);
        assertEquals(4, arg.getValue().size());
        assertEquals("--width", arg.getValue().get(0));
        assertEquals("${resolution_width}", arg.getValue().get(1));
        assertEquals("--height", arg.getValue().get(2));
        assertEquals("${resolution_height}", arg.getValue().get(3));
    }

    @Test
    public void testArgumentDeserializerWrongInput1() throws IOException {
        List<Argument> arguments2 = mapper.readValue(
                "[]",
                new TypeReference<List<Argument>>() {}
        );
    }
}