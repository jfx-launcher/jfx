/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.argument;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ArgumentSerializerTest {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    @Test
    public void test() throws IOException {
        List<Argument> arguments = mapper.readValue(
                getClass().getResource("/argument/test_arguments.json"),
                new TypeReference<List<Argument>>() {}
        );

        String json = mapper.writeValueAsString(arguments);
        List<Argument> result = mapper.readValue(json, new TypeReference<List<Argument>>() {});
        assertEquals(arguments, result);
    }
}
