/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.compat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.CompatInfo;
import dk.xakeps.jfx.launcher.CompatRule;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.platform.PlatformInfo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CompatRuleImplTest {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    @Test
    public void testFeature() throws IOException {
        CompatRuleImpl compatRule = mapper.readValue(
                getClass().getResource("/rules/test_rule_feature.json"),
                new TypeReference<>() {}
        );

        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.hasFeature("is_demo_user", true)).thenReturn(true);

        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testOs() throws IOException {
        CompatRuleImpl compatRule = mapper.readValue(
                getClass().getResource("/rules/test_rule_os.json"),
                new TypeReference<>() {}
        );

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("");
        when(platformInfo.getName()).thenReturn(PlatformInfo.WINDOWS);
        when(platformInfo.getVersion()).thenReturn("10.");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testArchAllow() throws IOException {
        CompatRuleImpl compatRule = mapper.readValue(
                getClass().getResource("/rules/test_rule_arch.json"),
                new TypeReference<>() {}
        );

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("x86");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testArchDisallow() throws IOException {
        CompatRuleImpl compatRule = mapper.readValue(
                getClass().getResource("/rules/test_rule_arch.json"),
                new TypeReference<>() {}
        );

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("x86_64");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        // ALLOW or DISALLOW must be returned only on exact match
        // otherwise DEFAULT should be returned
        // so ListCompatRule can save previous value
        assertEquals(CompatRule.Action.DEFAULT, appliedAction);
    }

    @Test
    public void testManyAllow() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many.json"),
                new TypeReference<>() {}
        );
        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("");
        when(platformInfo.getName()).thenReturn("windows");
        when(platformInfo.getVersion()).thenReturn("10.");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testManyDeny() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many.json"),
                new TypeReference<>() {}
        );

        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("");
        when(platformInfo.getName()).thenReturn("windows");
        when(platformInfo.getVersion()).thenReturn("weird");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.DISALLOW, appliedAction);
    }

    @Test
    public void testManyAllow2() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many_2.json"),
                new TypeReference<>() {}
        );
        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("x86");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testManyDeny2() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many_2.json"),
                new TypeReference<>() {}
        );

        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("x86_64");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.DISALLOW, appliedAction);
    }

    @Test
    public void testFeatureAndOsAllow() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many.json"),
                new TypeReference<>() {}
        );

        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("");
        when(platformInfo.getName()).thenReturn("windows");
        when(platformInfo.getVersion()).thenReturn("10.");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.hasFeature("is_demo_user", true)).thenReturn(true);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.ALLOW, appliedAction);
    }

    @Test
    public void testFeatureAndOsDeny() throws IOException {
        List<CompatRuleImpl> compatRuleList = mapper.readValue(
                getClass().getResource("/rules/test_rule_many.json"),
                new TypeReference<>() {}
        );

        ListCompatRule compatRule = new ListCompatRule(Collections.unmodifiableList(compatRuleList));

        PlatformInfo platformInfo = mock(PlatformInfo.class);
        when(platformInfo.getArch()).thenReturn("");
        when(platformInfo.getName()).thenReturn("windows");
        when(platformInfo.getVersion()).thenReturn("weird");
        CompatInfo compatInfo = mock(CompatInfo.class);
        when(compatInfo.hasFeature("is_demo_user", true)).thenReturn(true);
        when(compatInfo.getPlatformInfo()).thenReturn(platformInfo);
        CompatRule.Action appliedAction = compatRule.getAppliedAction(compatInfo);
        assertEquals(CompatRule.Action.DISALLOW, appliedAction);
    }
}