/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameLauncherImplTest {

    @Test
    public void canUnpack() throws URISyntaxException, IOException {
        List<String> excludes = Collections.singletonList("META-INF/");
        URL excludesZip = getClass().getResource("/excludes-test.zip");
        Assertions.assertNotNull(excludesZip);
        try (FileSystem fileSystem = FileSystems.newFileSystem(Path.of(excludesZip.toURI()))) {
            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("TEST-FILE"), excludes));
            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("TEST-PATH/FILE"), excludes));

            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("/TEST-FILE"), excludes));
            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("/TEST-PATH/FILE"), excludes));

            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("META-INF"), excludes));
            assertFalse(GameLauncherImpl.canUnpack(fileSystem.getPath("META-INF/FILE"), excludes));

            assertTrue(GameLauncherImpl.canUnpack(fileSystem.getPath("/META-INF"), excludes));
            assertFalse(GameLauncherImpl.canUnpack(fileSystem.getPath("/META-INF/FILE"), excludes));
        }
    }
}