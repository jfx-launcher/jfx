/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.version.LocalVersionRegistry;
import dk.xakeps.jfx.launcher.impl.version.RemoteVersionRegistry;
import dk.xakeps.jfx.launcher.impl.version.VersionData;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerImpl;
import dk.xakeps.jfx.platform.PlatformInfo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGameLauncherImpl_1_12_2_Forge {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    // no idea how to implement parametrized test
    @Test
    public void test() throws IOException, URISyntaxException {
        PlatformInfo platformInfoMock = mock(PlatformInfo.class);
        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86_64);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);

        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);
    }

    private void testWindows(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.WINDOWS);
        when(platformInfo.getPathSeparator()).thenReturn(";");
        when(platformInfo.getVersion()).thenReturn("10.0");
        test(platformInfo);
    }

    private void testMacOs(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.MACOS);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("11.0"); // big sur?
        test(platformInfo);
    }

    private void testLinux(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.LINUX);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("5.8");
        test(platformInfo);
    }

    private void test(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        testOld(platformInfo);
        testNew(platformInfo);
    }

    private void testOld(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        VersionData versionData = mapper.readValue(getClass().getResource("/version/1.12.2_forge_old.json"), VersionData.class);
        test(platformInfo, versionData);
    }

    private void testNew(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        VersionData versionData = mapper.readValue(getClass().getResource("/version/1.12.2_forge_new.json"), VersionData.class);
        test(platformInfo, versionData);
    }

    private void test(PlatformInfo platformInfo, VersionData versionData) throws IOException, URISyntaxException {
        VersionData parentVersion = mapper.readValue(getClass().getResource("/version/1.12.2.json"), VersionData.class);
        URL assetUrl = getClass().getResource("/version/assets/1.12.json");
        replaceAssetsUrl(parentVersion, assetUrl);

        DirectoryProvider directoryProvider = mock(DirectoryProvider.class);
        when(directoryProvider.getLibrariesDir()).thenReturn(Paths.get("libraries").toAbsolutePath());
        when(directoryProvider.getAssetsDir()).thenReturn(Paths.get("assets").toAbsolutePath());

        VersionManagerImpl versionManager = mock(VersionManagerImpl.class);
        RemoteVersionRegistry remoteRegistry = mock(RemoteVersionRegistry.class);
        LocalVersionRegistry localRegistry = mock(LocalVersionRegistry.class);
        when(localRegistry.getVersionById("1.12.2")).thenReturn(Optional.of(parentVersion));
        when(versionManager.getRemoteRegistry()).thenReturn(remoteRegistry);
        when(versionManager.getLocalRegistry()).thenReturn(localRegistry);
        when(versionManager.getVersion("1.12.2")).thenReturn(Optional.of(parentVersion));
        GameLauncherBuilderImpl builder = new GameLauncherBuilderImpl(versionManager, Paths.get("versions").toAbsolutePath());
        builder.directoryProvider = directoryProvider;
        builder.platformInfo = platformInfo;
        builder.gameVersion = versionData;
        builder.versionType = "client";

        GameLauncherImpl gameLauncher = new GameLauncherImpl(builder);

        ConfigurationProcessor.MainArtifactInfo mainArtifactInfo = gameLauncher.processor.getMainArtifactInfo();
        assertNotNull(mainArtifactInfo);
        assertEquals("net.minecraft.launchwrapper.Launch", mainArtifactInfo.getMainClass());
        assertNotNull(mainArtifactInfo.getArtifact());
        assertNotNull(gameLauncher.processor.getAssetIndexInfo().getAssetIndexArtifact());
        assertEquals(gameLauncher.processor.getAssetIndexInfo().getAssetIndexId(), "1.12");
        assertFalse(gameLauncher.processor.getLoggingInfo().isPresent());

        Set<LibraryArtifact> libraries = gameLauncher.processor.getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getLibraries)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        checkLibraries(platformInfo, libraries);
        checkArguments(platformInfo, gameLauncher);
        checkClassPath(platformInfo, gameLauncher);
    }

    private void checkLibraries(PlatformInfo platformInfo, Set<LibraryArtifact> libraries) {
        // 1.12.2 has total 32 libs for every platform
        // forge has 20 libs for every platform, but only 19 is added
        // because 1.12.2 and forge json both have jopt-simple library
        assertEquals(51, libraries.size());
    }

    private void checkArguments(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        List<Argument> gameArguments = gameLauncher.processor.getArguments(ArgumentTarget.GAME);
        assertEquals(1, gameArguments.size());
        assertEquals(20, gameArguments.get(0).getValue().size());
    }

    private void checkClassPath(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        Set<Path> classpathEntries = gameLauncher.processor.getClasspathEntries();
        Iterator<Path> iterator = classpathEntries.iterator();
        Path next = iterator.next();
        // parent libraries must be after child libraries
        // [
        //   [forge-libs],
        //   [minecraft-libs]
        // ]
        assertTrue(next.toString()
                .replace(next.getFileSystem().getSeparator(), "/")
                .contains("net/minecraftforge/forge"));
    }

    private void replaceAssetsUrl(VersionData versionData, URL assetsUrl) throws URISyntaxException {
        AssetIndexArtifact assetIndex = versionData.getAssetIndex();
        AssetIndexArtifact assetIndexArtifact = new AssetIndexArtifact(
                assetIndex.getId(),
                assetIndex.getSha1(),
                assetIndex.getSize(),
                assetIndex.getTotalSize(),
                assetsUrl.toURI()
        );
        versionData.setAssetIndex(assetIndexArtifact);
    }
}