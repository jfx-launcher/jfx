/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.LauncherException;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.version.VersionData;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerImpl;
import dk.xakeps.jfx.platform.PlatformInfo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGameLauncherImpl_1_16_3 {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    @Test
    public void test() throws IOException, URISyntaxException {
        PlatformInfo platformInfoMock = mock(PlatformInfo.class);
        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86_64);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);

        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);
    }

    private void testWindows(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.WINDOWS);
        when(platformInfo.getPathSeparator()).thenReturn(";");
        when(platformInfo.getVersion()).thenReturn("10.0");
        test(platformInfo);
    }

    private void testMacOs(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.MACOS);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("11.0"); // big sur?
        test(platformInfo);
    }

    private void testLinux(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.LINUX);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("5.8");
        test(platformInfo);
    }

    private void test(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        VersionData versionData = mapper.readValue(getClass().getResource("/version/1.16.3.json"), VersionData.class);
        URL assetUrl = getClass().getResource("/version/assets/1.16.json");
        replaceAssetsUrl(versionData, assetUrl);

        DirectoryProvider directoryProvider = mock(DirectoryProvider.class);
        when(directoryProvider.getLibrariesDir()).thenReturn(Paths.get("libraries").toAbsolutePath());
        when(directoryProvider.getAssetsDir()).thenReturn(Paths.get("assets").toAbsolutePath());

        VersionManagerImpl versionManager = mock(VersionManagerImpl.class);
        GameLauncherBuilderImpl builder = new GameLauncherBuilderImpl(versionManager, Paths.get("versions").toAbsolutePath());
        builder.directoryProvider = directoryProvider;
        builder.platformInfo = platformInfo;
        builder.gameVersion = versionData;
        builder.versionType = "client";

        GameLauncherImpl gameLauncher = new GameLauncherImpl(builder);
        ConfigurationProcessor.MainArtifactInfo mainArtifactInfo = gameLauncher.processor.getMainArtifactInfo();
        assertNotNull(mainArtifactInfo);
        assertNotNull(mainArtifactInfo.getMainClass());
        assertNotNull(mainArtifactInfo.getArtifact());
        assertNotNull(gameLauncher.processor.getAssetIndexInfo().getAssetIndexArtifact());
        assertEquals(gameLauncher.processor.getAssetIndexInfo().getAssetIndexId(), "1.16");
        assertTrue(gameLauncher.processor.getLoggingInfo().isPresent());

        Set<LibraryArtifact> natives = gameLauncher.processor.getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getNatives)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        checkLibraries(platformInfo, true, natives);
        Set<LibraryArtifact> libraries = gameLauncher.processor.getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getLibraries)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        checkLibraries(platformInfo, false, libraries);
        checkArguments(platformInfo, gameLauncher);
        checkClassPath(platformInfo, gameLauncher);
    }

    private void checkLibraries(PlatformInfo platformInfo, boolean natives, Set<LibraryArtifact> libraries) {
        // Mac total natives - 8
        // lwjgl, jemalloc, openal, opengl, glfw, stb, tinyfd, java-objc-bridge
        // Windows and linux total natives - 8
        // lwjgl, jemalloc, openal, opengl, glfw, stb, tinyfd, text2speech
        if (natives) {
            checkNatives(platformInfo, libraries);
        }
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.LINUX)) {
            if (!natives) {
                assertEquals(33, libraries.size());
            }
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            if (!natives) {
                assertEquals(34, libraries.size());
            }
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            if (!natives) {
                assertEquals(33, libraries.size());
            }
        }
    }

    private void checkNatives(PlatformInfo platformInfo, Set<LibraryArtifact> libraries) {
        boolean lwjgl = false, jemalloc = false, openal = false,
                opengl = false, glfw = false, stb = false, tinyFd = false,
                text2speech = false, javaObjCBridge = false;
        String lwjglVersion = "3.2.2";
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            lwjglVersion = "3.2.1";
        }
        String platformName = platformInfo.getName();
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            platformName = "macos";
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            platformName = "windows";
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.LINUX)) {
            platformName = "linux";
        }
        for (LibraryArtifact library : libraries) {
            if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl/${lwjglVersion}/lwjgl-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                lwjgl = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-jemalloc/${lwjglVersion}/lwjgl-jemalloc-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                jemalloc = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-openal/${lwjglVersion}/lwjgl-openal-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                openal = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-opengl/${lwjglVersion}/lwjgl-opengl-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                opengl = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-glfw/${lwjglVersion}/lwjgl-glfw-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                glfw = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-stb/${lwjglVersion}/lwjgl-stb-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                stb = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl-tinyfd/${lwjglVersion}/lwjgl-tinyfd-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                tinyFd = true;
            } else if (library.getPath().equalsIgnoreCase(replaceNative("com/mojang/text2speech/1.11.3/text2speech-1.11.3-natives-${platformName}.jar", lwjglVersion, platformName))) {
                text2speech = true;
            } else if (library.getPath().equalsIgnoreCase("ca/weblite/java-objc-bridge/1.0.0/java-objc-bridge-1.0.0-natives-osx.jar")) {
                javaObjCBridge = true;
            }
        }
        assertTrue(lwjgl, platformName + " lwjgl not found");
        assertTrue(jemalloc, platformName + " jemalloc not found");
        assertTrue(openal, platformName + " openal not found");
        assertTrue(opengl, platformName + " opengl not found");
        assertTrue(glfw, platformName + " glfw not found");
        assertTrue(stb, platformName + " stb not found");
        assertTrue(tinyFd, platformName + " tinyfd not found");
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            assertFalse(text2speech, "Text2Speech native must not be present on Mac OS");
            assertTrue(javaObjCBridge, "Java Obj-C Bridge must be present on Mac OS");
        } else {
            assertTrue(text2speech, "Text2Speech native must not be present on non-Mac OS");
            assertFalse(javaObjCBridge, "Java Obj-C Bridge must not be present on non-Mac OS");
        }
        assertEquals(8, libraries.size());
    }

    private void checkArguments(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        List<Argument> gameArguments = gameLauncher.processor.getArguments(ArgumentTarget.GAME);
        List<Argument> jvmArguments = gameLauncher.processor.getArguments(ArgumentTarget.JVM);
        assertFalse(gameArguments.isEmpty());
        assertFalse(jvmArguments.isEmpty());
        assertEquals(18, gameArguments.size());
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            if (platformInfo.getArch().equalsIgnoreCase(PlatformInfo.x86)) {
                assertEquals(7, jvmArguments.size());
            } else {
                assertEquals(6, jvmArguments.size());
            }
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            if (platformInfo.getArch().equalsIgnoreCase(PlatformInfo.x86)) {
                assertEquals(7, jvmArguments.size());
            } else {
                assertEquals(6, jvmArguments.size());
            }
        } else {
            if (platformInfo.getArch().equalsIgnoreCase(PlatformInfo.x86)) {
                assertEquals(6, jvmArguments.size());
            } else {
                assertEquals(5, jvmArguments.size());
            }
        }
    }

    private void checkClassPath(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        Set<Path> classpathEntries = gameLauncher.processor.getClasspathEntries();
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            assertEquals(35, classpathEntries.size());
        } else {
            assertEquals(34, classpathEntries.size());
        }
        assertThrows(LauncherException.class, () -> gameLauncher.generateClasspath(true));
        assertDoesNotThrow(() -> gameLauncher.generateClasspath(false));
    }

    private String replaceNative(String orig, String lwjglVer, String platformName) {
        return orig.replace("${lwjglVersion}", lwjglVer).replace("${platformName}", platformName);
    }

    private void replaceAssetsUrl(VersionData versionData, URL assetsUrl) throws URISyntaxException {
        AssetIndexArtifact assetIndex = versionData.getAssetIndex();
        AssetIndexArtifact assetIndexArtifact = new AssetIndexArtifact(
                assetIndex.getId(),
                assetIndex.getSha1(),
                assetIndex.getSize(),
                assetIndex.getTotalSize(),
                assetsUrl.toURI()
        );
        versionData.setAssetIndex(assetIndexArtifact);
    }
}