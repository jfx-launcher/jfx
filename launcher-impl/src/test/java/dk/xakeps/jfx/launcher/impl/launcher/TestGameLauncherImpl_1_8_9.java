/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.launcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.DirectoryProvider;
import dk.xakeps.jfx.launcher.LauncherException;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.launcher.impl.AssetIndexArtifact;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import dk.xakeps.jfx.launcher.impl.library.LibraryArtifact;
import dk.xakeps.jfx.launcher.impl.version.VersionData;
import dk.xakeps.jfx.launcher.impl.version.VersionManagerImpl;
import dk.xakeps.jfx.platform.PlatformInfo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGameLauncherImpl_1_8_9 {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }

    @Test
    public void test() throws IOException, URISyntaxException {
        PlatformInfo platformInfoMock = mock(PlatformInfo.class);
        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86_64);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);

        when(platformInfoMock.getArch()).thenReturn(PlatformInfo.x86);
        testWindows(platformInfoMock);
        testMacOs(platformInfoMock);
        testLinux(platformInfoMock);
    }

    private void testWindows(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.WINDOWS);
        when(platformInfo.getPathSeparator()).thenReturn(";");
        when(platformInfo.getVersion()).thenReturn("10.0");
        test(platformInfo);
    }

    private void testMacOs(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.MACOS);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("11.0"); // big sur?
        test(platformInfo);
    }

    private void testLinux(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        when(platformInfo.getName()).thenReturn(PlatformInfo.LINUX);
        when(platformInfo.getPathSeparator()).thenReturn(":");
        when(platformInfo.getVersion()).thenReturn("5.8");
        test(platformInfo);
    }

    private void test(PlatformInfo platformInfo) throws IOException, URISyntaxException {
        VersionData versionData = mapper.readValue(getClass().getResource("/version/1.8.9.json"), VersionData.class);
        URL assetUrl = getClass().getResource("/version/assets/1.8.json");
        replaceAssetsUrl(versionData, assetUrl);

        DirectoryProvider directoryProvider = mock(DirectoryProvider.class);
        when(directoryProvider.getLibrariesDir()).thenReturn(Paths.get("libraries").toAbsolutePath());
        when(directoryProvider.getAssetsDir()).thenReturn(Paths.get("assets").toAbsolutePath());

        VersionManagerImpl versionManager = mock(VersionManagerImpl.class);
        GameLauncherBuilderImpl builder = new GameLauncherBuilderImpl(versionManager, Paths.get("versions").toAbsolutePath());
        builder.directoryProvider = directoryProvider;
        builder.platformInfo = platformInfo;
        builder.gameVersion = versionData;
        builder.versionType = "client";

        GameLauncherImpl gameLauncher = new GameLauncherImpl(builder);

        Set<LibraryArtifact> natives = gameLauncher.processor.getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getNatives)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        checkLibraries(platformInfo, true, natives);
        Set<LibraryArtifact> libraries = gameLauncher.processor.getLibrariesInfos().stream()
                .map(Configuration.LibrariesInfo::getLibraries)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        checkLibraries(platformInfo, false, libraries);
        checkArguments(platformInfo, gameLauncher);
        checkClassPath(platformInfo, gameLauncher);
    }

    private void checkLibraries(PlatformInfo platformInfo, boolean natives, Set<LibraryArtifact> libraries) {
        // Mac total natives - 8
        // lwjgl, jemalloc, openal, opengl, glfw, stb, tinyfd, java-objc-bridge
        // Windows and linux total natives - 8
        // lwjgl, jemalloc, openal, opengl, glfw, stb, tinyfd, text2speech
        if (natives) {
            checkNatives(platformInfo, libraries);
        }
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            if (!natives) {
                assertEquals(31, libraries.size());
            }
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            if (!natives) {
                assertEquals(30, libraries.size());
            }
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.LINUX)) {
            if (!natives) {
                assertEquals(31, libraries.size());
            }
        }
    }

    private void checkNatives(PlatformInfo platformInfo, Set<LibraryArtifact> libraries) {
        boolean lwjglPlatform = false;
        String lwjglVersion = "2.9.4-nightly-20150209";
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            lwjglVersion = "2.9.2-nightly-20140822";
        }
        String platformName = platformInfo.getName();
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            platformName = "osx";
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            platformName = "windows";
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.LINUX)) {
            platformName = "linux";
        }
        for (LibraryArtifact library : libraries) {
            if (library.getPath().equalsIgnoreCase(replaceNative("org/lwjgl/lwjgl/lwjgl-platform/${lwjglVersion}/lwjgl-platform-${lwjglVersion}-natives-${platformName}.jar", lwjglVersion, platformName))) {
                lwjglPlatform = true;
            }
        }
        assertTrue(lwjglPlatform, platformName + " lwjgl platform not found");
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.WINDOWS)) {
            assertEquals(4, libraries.size());
        } else if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            assertEquals(3, libraries.size());
        } else {
            assertEquals(2, libraries.size());
        }
    }

    private void checkArguments(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        List<Argument> gameArguments = gameLauncher.processor.getArguments(ArgumentTarget.GAME);
        assertFalse(gameArguments.isEmpty());
        assertEquals(1, gameArguments.size());
        assertEquals(18, gameArguments.get(0).getValue().size());

        List<Argument> jvmArguments = gameLauncher.processor.getArguments(ArgumentTarget.JVM);
        assertFalse(jvmArguments.isEmpty());
        assertEquals(5, jvmArguments.size());

    }

    private void checkClassPath(PlatformInfo platformInfo, GameLauncherImpl gameLauncher) {
        Set<Path> classpathEntries = gameLauncher.processor.getClasspathEntries();
        if (platformInfo.getName().equalsIgnoreCase(PlatformInfo.MACOS)) {
            assertEquals(31, classpathEntries.size());
        } else {
            assertEquals(32, classpathEntries.size());
        }
        assertThrows(LauncherException.class, () -> gameLauncher.generateClasspath(true));
        assertDoesNotThrow(() -> gameLauncher.generateClasspath(false));
    }

    private String replaceNative(String orig, String lwjglVer, String platformName) {
        return orig.replace("${lwjglVersion}", lwjglVer).replace("${platformName}", platformName);
    }

    private void replaceAssetsUrl(VersionData versionData, URL assetsUrl) throws URISyntaxException {
        AssetIndexArtifact assetIndex = versionData.getAssetIndex();
        AssetIndexArtifact assetIndexArtifact = new AssetIndexArtifact(
                assetIndex.getId(),
                assetIndex.getSha1(),
                assetIndex.getSize(),
                assetIndex.getTotalSize(),
                assetsUrl.toURI()
        );
        versionData.setAssetIndex(assetIndexArtifact);
    }
}