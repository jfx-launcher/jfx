/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.library;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.xakeps.jfx.launcher.impl.ObjectMapperHolder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LibraryDataTest {
    private static ObjectMapper mapper;

    @BeforeAll
    public static void setup() {
        mapper = ObjectMapperHolder.OBJECT_MAPPER;
    }
    
    @Test
    public void test() throws IOException {
        List<LibraryData> libraryData = mapper.readValue(
                getClass().getResource("/libraries/test_library.json"),
                new TypeReference<>() {}
                );
        assertEquals(1, libraryData.size());
        LibraryData data = libraryData.get(0);
        assertEquals("net.minecraft:launchwrapper:1.5", data.getName());
        LibraryArtifacts downloads = data.getDownloads();
        assertNotNull(downloads);
        LibraryArtifact artifact = downloads.getArtifact();
        assertNotNull(artifact);
        assertEquals("net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar", artifact.getPath());
        assertEquals("5150b9c2951f0fde987ce9c33496e26add1de224", artifact.getSha1());
        assertEquals(27787, artifact.getSize());
        assertEquals(URI.create("https://libraries.minecraft.net/net/minecraft/launchwrapper/1.5/launchwrapper-1.5.jar"), artifact.getUri());
    }
}