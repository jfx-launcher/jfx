/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.library;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MavenDataTest {

    @Test
    public void parseSimple() {
        String testCoords = "org.example:artifact:1.0";
        MavenData parse = MavenData.parse(testCoords);
        assertEquals("org.example", parse.groupId);
        assertEquals("artifact", parse.artifactId);
        assertEquals("1.0", parse.version);
    }

    @Test
    public void parseExtension() {
        String testCoords = "org.example:artifact:ext:1.0";
        MavenData parse = MavenData.parse(testCoords);
        assertEquals("org.example", parse.groupId);
        assertEquals("artifact", parse.artifactId);
        assertEquals("ext", parse.extension);
        assertEquals("1.0", parse.version);
    }

    @Test
    public void parseClassifier() {
        String testCoords = "org.example:artifact:ext:classifier:1.0";
        MavenData parse = MavenData.parse(testCoords);
        assertEquals("org.example", parse.groupId);
        assertEquals("artifact", parse.artifactId);
        assertEquals("ext", parse.extension);
        assertEquals("classifier", parse.classifier);
        assertEquals("1.0", parse.version);
    }

    @Test
    public void isSnapshot() {
        assertFalse(MavenData.isSnapshot("1.0"));
        assertFalse(MavenData.isSnapshot("1.0-20201011o203501-1"));
        assertTrue(MavenData.isSnapshot("1.0-snapshot"));
        assertTrue(MavenData.isSnapshot("1.0-SNAPSHOT"));
        assertTrue(MavenData.isSnapshot("1.0-snapSHOT"));
        assertTrue(MavenData.isSnapshot("1.0-20201011.203501-1"));
    }

    @Test
    public void rootVersionTest() {
        assertEquals("1.0", MavenData.getRootVersion("1.0"));
        assertEquals("1.0-20201011o203501-1", MavenData.getRootVersion("1.0-20201011o203501-1"));
        assertEquals("1.0-snapshot", MavenData.getRootVersion("1.0-snapshot"));
        assertEquals("1.0-SNAPSHOT", MavenData.getRootVersion("1.0-SNAPSHOT"));
        assertEquals("1.0-snapSHOT", MavenData.getRootVersion("1.0-snapSHOT"));
        assertEquals("1.0-SNAPSHOT", MavenData.getRootVersion("1.0-20201011.203501-1"));
    }
}