/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.impl.version;

import dk.xakeps.jfx.launcher.GameVersion;
import dk.xakeps.jfx.launcher.VersionRegistry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class VersionManagerImplTest {
    @Test
    public void ensureRemoteManifestParsingWorks() throws IOException, URISyntaxException {
        VersionRegistry registry = RemoteVersionRegistry.load(getClass().getResource("/version_manifest.json").toURI());
        assertEquals(544, registry.getVersions().size());
        GameVersion gameVersion = registry.getVersionById("1.16.3").orElseThrow();
        assertEquals("release", gameVersion.getReleaseType());
        assertEquals(OffsetDateTime.parse("2020-09-10T13:42:37+00:00"), gameVersion.getReleaseTime());
    }
}