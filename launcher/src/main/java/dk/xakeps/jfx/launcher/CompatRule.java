/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher;

/**
 * Represents compatibility checker
 * that does implementation dependent checks
 * and returns applied action
 */
@FunctionalInterface
public interface CompatRule {
    /**
     * Calculates applied action
     * @param compatInfo represents information that may be
     *                   needed by this {@link CompatRule}.
     *                   Can't be null.
     * @return computed action
     */
    Action getAppliedAction(CompatInfo compatInfo);

    /**
     * Calculates, if compat-dependent class, e.g. {@link dk.xakeps.jfx.launcher.argument.Argument}
     * can be used
     * @param compatInfo represents information that may be
     *                   needed by this {@link CompatRule}.
     *                   Can't be null.
     * @return implementation dependent.
     * By default, returns false if
     * {@link #getAppliedAction(CompatInfo)} returns {@link Action#DISALLOW},
     * true otherwise.
     */
    default boolean shouldApply(CompatInfo compatInfo) {
        return getAppliedAction(compatInfo) != Action.DISALLOW;
    }

    /**
     * Represents applied action
     */
    enum Action {
        /**
         * Represents allow action.
         * Compat-dependent class, e.g. {@link dk.xakeps.jfx.launcher.argument.Argument}
         * may or not may be used, depending on usage site implementation
         */
        DEFAULT,

        /**
         * Represents allow action.
         * Compat-dependent class, e.g. {@link dk.xakeps.jfx.launcher.argument.Argument}
         * should be used if this returned
         */
        ALLOW,

        /**
         * Represents disallow action.
         * Compat-dependent class, e.g. {@link dk.xakeps.jfx.launcher.argument.Argument}
         * should not be used if this returned
         */
        DISALLOW
    }
}
