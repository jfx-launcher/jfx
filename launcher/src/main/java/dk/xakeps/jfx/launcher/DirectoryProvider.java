/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher;

import java.nio.file.Path;

/**
 * Provides directories that are used by launcher
 * depending on launcher implementation,
 * some or all methods of this class may be used or may not be used
 */
public interface DirectoryProvider {
    /**
     * Returns directory, where game instance files are or should be stored,
     * for example saved maps, game settings, mods
     * @return directory, where game instance files are or should be stored
     */
    Path getGameDir();

    /**
     * Returns directory, where game assets are or should be stored,
     * for example textures, sounds
     * @return directory, where game assets are or should be stored
     */
    Path getAssetsDir();

    /**
     * Returns directory, where game libraries are or should be stored,
     * for example log4j2
     * @return directory, where game libraries are or should be stored
     */
    Path getLibrariesDir();

    /**
     * Returns directory, where running game instance temporary files are or should be stored,
     * for example lwjgl natives or other natives
     * @return directory, where running game instance temporary files are or should be stored
     */
    Path getTempDir();

    /**
     * Returns directory, where resource packs are stored or should be stored
     * @return directory, where resource packs are stored or should be stored
     */
    Path getResourcePackDir();

    /**
     * Returns directory, where data packs are stored or should be stored
     * @return directory, where data packs are stored or should be stored
     */
    Path getDataPackDir();
}
