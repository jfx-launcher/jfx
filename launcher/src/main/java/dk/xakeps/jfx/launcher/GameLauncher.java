/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher;

import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.launcher.argument.Argument;
import dk.xakeps.jfx.launcher.argument.ArgumentTarget;
import dk.xakeps.jfx.platform.PlatformInfo;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import dk.xakeps.jfx.user.User;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Represents unprepared game launcher and it's state
 */
public interface GameLauncher {
    /**
     * Prepares game launcher to launch a game,
     * downloads required files if needed, unpacks files,
     * etc
     * Multiple calls of this method is undefined behavior
     * @return instance of {@link PreparedGameLauncher}
     * @throws IOException if error happens
     */
    PreparedGameLauncher prepare() throws IOException;

    /**
     * Represents prepared launcher
     */
    interface PreparedGameLauncher {
        /**
         * Launches game
         * @throws IOException if error happens
         */
        void start() throws IOException;

        /**
         * This set contains all files that are used by launching game version.
         * <p>Depending on implementation, this Set may also contain extra files
         * @return set of {@link FileInfo}s
         */
        Set<FileInfo> getGameFiles();
    }

    /**
     * Represents builder that creates an instance of {@link GameLauncher}
     */
    interface Builder {
        /**
         * Implementation of {@link DirectoryProvider}
         * @param directoryProvider implementation of {@link DirectoryProvider}
         * @return itself, for chaining
         */
        Builder directoryProvider(DirectoryProvider directoryProvider);

        /**
         * Used to add custom libraries to a classpath
         * @param classpath entries to prepend to a classpath
         * @return itself, for chaining
         */
        Builder classpath(List<Path> classpath);

        /**
         * Mandatory.
         * Used to set implementation of {@link PlatformInfo},
         * @param platformInfo an implementation of {@link PlatformInfo}
         * @return itself, for chaining
         */
        Builder platformInfo(PlatformInfo platformInfo);

        /**
         * Sets feature state
         * @param name feature name or id
         * @param value feature state
         * @return itself, for chaining
         */
        Builder feature(String name, boolean value);

        /**
         * Sets feature state and an arg replacer,
         * that is used to replace argument placeholder,
         * if {@link CompatInfo#hasFeature(String, boolean)} returned true
         * @param name feature name or id
         * @param value feature state
         * @param argReplacer function that replaces argument or returns
         *                    arguments itself, if nothing to replace
         * @return itself, for chaining
         */
        Builder feature(String name, boolean value, UnaryOperator<String> argReplacer);

        /**
         * Mandatory.
         * Game version, that should be started
         * @param version instance of {@link GameVersion},
         *                that was obtained using {@link VersionManager},
         *                that created this {@link Builder}
         * @return itself, for chaining
         */
        Builder gameVersion(GameVersion version);

        /**
         * Mandatory.
         * Version type, that should be started
         * @param versionType version type to launch,
         *                    that is obtained form {@link GameVersion#getVersionTypes()}
         * @return itself, for chaining
         */
        Builder versionType(String versionType);

        /**
         * Mandatory.
         * Information about user, that will be used in game
         * @param user implementation of {@link User}
         * @return itself, for chaining
         */
        Builder user(User user);

        /**
         * Access token to pass to a game, game may use it to access
         * special features
         * @param accessToken implementation dependent accessToken
         * @return itself, for chaining
         */
        Builder accessToken(String accessToken);

        /**
         * Predicate that is used to filter libraries.
         * If predicate returns true, library is <b>not</b> skipped
         * @param libraryFilter predicate
         * @return itself, for chaining
         */
        Builder libraryFilter(Predicate<Library> libraryFilter);

        /**
         * Removes given library from classpath and adds given
         * paths to classpath
         * @param libraryReplacer replacer implementation
         * @return itself, for chaining
         */
        Builder libraryReplacer(LibraryReplacer libraryReplacer);

        /**
         * Additional arguments that should be passed to a specified {@link ArgumentTarget}
         * @param argumentTarget target for arguments
         * @param arguments list of additional arguments
         * @return itself, for chaining
         */
        Builder arguments(ArgumentTarget argumentTarget, List<Argument> arguments);

        /**
         * If game should automatically connect to a server
         * after game started, specify this
         * <p>If {@link InetSocketAddress#getPort()} is 0,
         * then implementation dependent port is used
         * @param server server ip or host, optionally port
         *               that should be used for auto-connect feature
         * @return itself, for chaining
         */
        Builder connectTo(InetSocketAddress server);

        /**
         * Mandatory.
         * Implementation of {@link ProgressListenerFactory}
         * that is used to monitor downloads
         * @param progressListenerFactory implementation of {@link ProgressListenerFactory}
         * @return itself, for chaining
         */
        Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory);

        /**
         * Depending on game version, this may turn on
         * advanced game logger feature.
         * <p>Implementation of advanced logger depends on a game version
         * @param advancedLogger true, if advanced game logger should be used
         * @return itself, for chaining
         */
        Builder advancedLogger(boolean advancedLogger);

        /**
         * Configures input and output streams of the game to be started
         * @return instance of {@link IOConfig} used by this builder
         */
        IOConfig configureIO();

        /**
         * Builds instance of {@link GameLauncher}
         * Using this builder instance after this method was called
         * or calling this method multiple times
         * is undefined behavior
         * @return instance of {@link GameLauncher}
         */
        GameLauncher build();
    }
}