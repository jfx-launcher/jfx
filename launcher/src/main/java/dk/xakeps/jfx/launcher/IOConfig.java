/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Used to configure game input, output and error streams
 * Depending on implementation, this class may be used, may not be used
 * or may be used ignoring or altering meaning of configured properties
 */
public interface IOConfig {
    /**
     * @see ProcessBuilder#redirectErrorStream(boolean)
     * @param redirectErrorStream new value of redirectErrorStream property
     * @return itself, for chaining
     */
    IOConfig redirectErrorStream(boolean redirectErrorStream);

    /**
     * @see ProcessBuilder#redirectError(ProcessBuilder.Redirect)
     * @param redirectError new value of redirect property
     * @return itself, for chaining
     */
    IOConfig redirectError(ProcessBuilder.Redirect redirectError);

    /**
     * @see ProcessBuilder#redirectInput(ProcessBuilder.Redirect)
     * @param redirectInput new value of redirect property
     * @return itself, for chaining
     */
    IOConfig redirectInput(ProcessBuilder.Redirect redirectInput);

    /**
     * @see ProcessBuilder#redirectOutput(ProcessBuilder.Redirect)
     * @param redirectOutput new value of redirect property
     * @return itself, for chaining
     */
    IOConfig redirectOutput(ProcessBuilder.Redirect redirectOutput);

    /**
     * Implementation of {@link StreamsConsumer},
     * that will consume input, output and error streams
     * @param streamsConsumer implementation of {@link StreamsConsumer}
     * @return itself, for chaining
     */
    IOConfig streamsConsumer(StreamsConsumer streamsConsumer);

    /**
     * Finishes IO configuration
     * @return instance of {@link dk.xakeps.jfx.launcher.GameLauncher.Builder}
     * that that uses this {@link IOConfig}
     */
    GameLauncher.Builder finish();

    /**
     * Consumer, that will consume IO streams
     */
    @FunctionalInterface
    interface StreamsConsumer {
        /**
         * Provided parameters are depending on IO configuration
         * @param inputStream instance of {@link InputStream}
         * @param outputStream instance of {@link OutputStream}
         * @param errorStream instance of {@link InputStream}
         */
        void consume(InputStream inputStream, OutputStream outputStream, InputStream errorStream);
    }
}
