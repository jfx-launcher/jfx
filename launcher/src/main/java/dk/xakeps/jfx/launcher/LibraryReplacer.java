package dk.xakeps.jfx.launcher;

import java.nio.file.Path;
import java.util.Optional;

public interface LibraryReplacer {
    /**
     * If this method returns true, {@link #replaceWith(Library, Path)} will be called
     * and given library will be replaced with returned classpath paths
     * @param library library to test
     * @return if library should be replaced
     */
    boolean test(Library library);

    /**
     * Replaces given library classpath entries with returned paths.
     * Only called if {@link #test(Library)} returned true
     * @param library library to replace
     * @return replacing classpath entry
     */
    Optional<Path> replaceWith(Library library, Path artifactPath);
}
