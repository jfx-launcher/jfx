/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher;

import java.net.URI;
import java.nio.file.Path;
import java.util.Optional;
import java.util.ServiceLoader;

public interface VersionManager {
    /**
     * Returns {@link VersionRegistry} that contains
     * game versions available to install
     * @return instance of {@link VersionRegistry}
     */
    VersionRegistry getRemoteRegistry();

    /**
     * Returns {@link VersionRegistry} that contains
     * installed game versions
     * @return instance of {@link VersionRegistry}
     */
    VersionRegistry getLocalRegistry();

    /**
     * Finds given version.
     * Version search is implementation dependent
     * @param id version to find
     * @return found version
     */
    default Optional<GameVersion> getVersion(String id) {
        return getRemoteRegistry().getVersionById(id)
                .or(() -> getLocalRegistry().getVersionById(id));
    }

    /**
     * Creates new instance of {@link GameLauncher.Builder}
     * @return new instance of {@link GameLauncher.Builder}
     */
    GameLauncher.Builder newLauncherBuilder();

    /**
     * Creates new instance of {@link VersionManagerAdmin.Builder}
     * @return new instance of {@link VersionManagerAdmin.Builder}
     */
    VersionManagerAdmin.Builder newVersionManagerAdminBuilder();

    /**
     * Returns new builder
     * @return new builder instance
     */
    static Builder newBuilder() {
        return ServiceLoader.load(Builder.class)
                .findFirst()
                .orElseThrow();
    }

    interface Builder {
        /**
         * @param versionsDir directory where versions metadata is stored
         * @return itself, for chaining
         */
        Builder versionsDir(Path versionsDir);

        /**
         * Implementation of {@link VersionListener},
         * that is called, when version installed or removed
         * @param versionListener implementation of {@link VersionListener}
         * @return itself, for chaining
         */
        Builder versionListener(VersionListener versionListener);

        /**
         * @param manifestsUri uri to version_manifests.json file
         * @return itself, for chaining
         */
        VersionManager.Builder manifestsUri(URI manifestsUri);

        /**
         * Builds instance of {@link VersionManager}
         * Using this builder instance after this method was called
         * or calling this method multiple times
         * is undefined behavior
         * @return instance of {@link VersionManager}
         */
        VersionManager build();
    }
}
