package dk.xakeps.jfx.launcher;

import java.net.URI;
import java.nio.file.Path;

public interface VersionManagerAdmin {
    /**
     * Used to install forge version
     * @param file path to a forge installer
     */
    void uploadForge(Path file);

    interface Builder {
        /**
         * Used to authorize against admin endpoints
         * @param accessToken admin access token
         * @return itself, for chaining
         */
        Builder accessToken(String accessToken);

        /**
         * Root endpoint for game version management service
         * @param serviceUri uri of management service
         * @return itself, for chaining
         */
        Builder serviceUri(URI serviceUri);

        /**
         * Builds instance of {@link VersionManagerAdmin}
         * Using this builder instance after this method was called
         * or calling this method multiple times
         * is undefined behavior
         * @return instance of {@link VersionManagerAdmin}
         */
        VersionManagerAdmin build();
    }
}
