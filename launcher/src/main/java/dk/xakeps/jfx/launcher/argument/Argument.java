/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package dk.xakeps.jfx.launcher.argument;

import dk.xakeps.jfx.launcher.CompatRule;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Represents list of string arguments that are used for launch command line
 * This argument must be used only if rules allow this
 */
public class Argument {
    private final List<String> value;
    private final CompatRule rules;

    /**
     *
     * @param value list of string arguments or null
     * @param rules implementation of {@link CompatRule}
     */
    public Argument(List<String> value, CompatRule rules) {
        this.value = Objects.requireNonNullElse(value, Collections.emptyList());
        this.rules = Objects.requireNonNull(rules, "rules");
    }

    /**
     * This list may contain arguments and placeholders that should be replaced later
     * @return unmodifiable list of arguments
     */
    public List<String> getValue() {
        return Collections.unmodifiableList(value);
    }

    /**
     * This argument should be
     * @return {@link CompatRule} implementation
     */
    public CompatRule getRules() {
        return rules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Argument argument = (Argument) o;
        return value.equals(argument.value) &&
                rules.equals(argument.rules);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, rules);
    }
}
