/*
    This file is part of JFX Launcher
    Copyright (C) 2020 Xakep_SDK <admin@xakeps.dk>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

module dk.xakeps.jfx.launcher {
    requires transitive dk.xakeps.jfx.user;
    requires transitive dk.xakeps.jfx.file;
    requires transitive dk.xakeps.jfx.network;
    requires transitive dk.xakeps.jfx.platform;

    exports dk.xakeps.jfx.launcher;
    exports dk.xakeps.jfx.launcher.argument;

    uses dk.xakeps.jfx.launcher.VersionManager.Builder;
}