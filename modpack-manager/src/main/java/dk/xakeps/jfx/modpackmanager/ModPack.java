package dk.xakeps.jfx.modpackmanager;

import java.util.List;
import java.util.Optional;

public interface ModPack {
    String getModPackId();
    Optional<ModPackVersion> getEnabledVersion();
    List<ModPackVersion> getVersions();
}
