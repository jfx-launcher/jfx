package dk.xakeps.jfx.modpackmanager;

import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

public interface ModPackAdmin {
    void create(String modPackId);
    void upload(ProgressListenerFactory listenerFactory,
                ModPack modPack, String minecraftVersion, String javaVersion, Set<String> ignoredFiles, Path zip);
    void delete(ModPack modPack);
    void enable(ModPackVersion modPackVersion);
    List<Admin> getAdmins(ModPack modPack);
    void addAdmin(ModPack modPack, String id);
    void removeAdmin(ModPack modPack, String id);

    interface Builder {
        Builder serviceUri(URI serviceUri);
        ModPackAdmin build();
    }
}
