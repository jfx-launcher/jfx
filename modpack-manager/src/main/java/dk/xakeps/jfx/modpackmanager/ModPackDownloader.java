package dk.xakeps.jfx.modpackmanager;

import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

public interface ModPackDownloader {

    DownloadedModPack download() throws IOException;

    interface DownloadedModPack {
        Set<FileInfo> getFiles();
    }

    interface Builder {
        Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory);
        Builder downloadRoot(Path downloadRoot);
        Builder modPackVersion(ModPackVersion modPackVersion);
        ModPackDownloader build();
    }
}
