package dk.xakeps.jfx.modpackmanager;

public record ModPackError(String type, String message) {
}
