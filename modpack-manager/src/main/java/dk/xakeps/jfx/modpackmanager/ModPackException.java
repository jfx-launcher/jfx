package dk.xakeps.jfx.modpackmanager;

import java.util.Optional;

public class ModPackException extends ModPackManagerException {
    private final ModPackError modPackError;

    public ModPackException(ModPackError modPackError) {
        this.modPackError = modPackError;
    }

    public ModPackException(String message, ModPackError modPackError) {
        super(message);
        this.modPackError = modPackError;
    }

    public ModPackException(String message, Throwable cause, ModPackError modPackError) {
        super(message, cause);
        this.modPackError = modPackError;
    }

    public ModPackException(Throwable cause, ModPackError modPackError) {
        super(cause);
        this.modPackError = modPackError;
    }

    public ModPackException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, ModPackError modPackError) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.modPackError = modPackError;
    }

    public Optional<ModPackError> getModPackError() {
        return Optional.ofNullable(modPackError);
    }
}
