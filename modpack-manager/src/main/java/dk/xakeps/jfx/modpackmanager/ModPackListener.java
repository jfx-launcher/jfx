package dk.xakeps.jfx.modpackmanager;

public interface ModPackListener {
    void onInstalled(ModPackVersion modPackVersion);
    void onRemoved(ModPackVersion modPackVersion);
}
