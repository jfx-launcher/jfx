package dk.xakeps.jfx.modpackmanager;

import dk.xakeps.jfx.auth.AuthInfo;

import java.net.URI;
import java.nio.file.Path;
import java.util.ServiceLoader;

public interface ModPackManager {
    ModPackRegistry getRemoteRegistry();

    ModPackRegistry getLocalRegistry();

    ModPackDownloader.Builder newDownloaderBuilder();
    ModPackAdmin.Builder newModPackAdminBuilder();

    static Builder newBuilder() {
        return ServiceLoader.load(Builder.class)
                .findFirst()
                .orElseThrow();
    }

    interface Builder {
        Builder authInfo(AuthInfo authInfo);
        Builder modPackListener(ModPackListener modPackListener);
        Builder metadataPath(Path metadataPath);
        Builder manifestUri(URI manifestUri);
        ModPackManager build();
    }
}
