package dk.xakeps.jfx.modpackmanager;

public class ModPackManagerException extends RuntimeException {
    public ModPackManagerException() {
    }

    public ModPackManagerException(String message) {
        super(message);
    }

    public ModPackManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModPackManagerException(Throwable cause) {
        super(cause);
    }

    public ModPackManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
