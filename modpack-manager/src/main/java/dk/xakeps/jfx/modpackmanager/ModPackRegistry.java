package dk.xakeps.jfx.modpackmanager;

import java.util.List;
import java.util.Optional;

public interface ModPackRegistry {
    List<ModPack> getModPacks();

    default Optional<ModPack> getModPackById(String modPackId) {
        return getModPacks().stream()
                .filter(modPack -> modPack.getModPackId().equals(modPackId))
                .findFirst();
    }

    void reload();
}
