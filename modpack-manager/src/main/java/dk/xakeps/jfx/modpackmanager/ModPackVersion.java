package dk.xakeps.jfx.modpackmanager;

import java.util.Set;

public interface ModPackVersion extends Comparable<ModPackVersion> {
    String getModPackId();
    String getMinecraftVersion();
    String getJavaVersion();
    Set<String> getIgnoredFiles();
    String getVersion();

    @Override
    default int compareTo(ModPackVersion o) {
        return getVersion().compareTo(o.getVersion());
    }
}
