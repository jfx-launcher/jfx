package dk.xakeps.jfx.modpackmanager.impl;

import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import dk.xakeps.jfx.modpackmanager.impl.downloader.ModPackDownloaderImpl;

import java.io.IOException;
import java.util.Set;

public interface InternalModPackVersion extends ModPackVersion {
    Set<FileInfo> install(ModPackDownloaderImpl modPackDownloaderImpl) throws IOException;
}
