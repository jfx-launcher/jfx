package dk.xakeps.jfx.modpackmanager.impl;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;

public record ModArtifact(@JsonProperty("file") String file, @JsonProperty("size") long size,
                          @JsonProperty("hash") String hash, @JsonProperty("url") URI url) {
}
