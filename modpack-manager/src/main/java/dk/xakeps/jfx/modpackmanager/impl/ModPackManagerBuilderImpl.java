package dk.xakeps.jfx.modpackmanager.impl;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.modpackmanager.ModPackListener;
import dk.xakeps.jfx.modpackmanager.ModPackManager;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;

import java.net.URI;
import java.nio.file.Path;
import java.util.Objects;

public class ModPackManagerBuilderImpl implements ModPackManager.Builder {
    AuthInfo authInfo;
    ModPackListener modPackListener = NoOpModPackListener.INSTANCE;
    Path metadataPath;
    URI manifestUri;

    @Override
    public ModPackManager.Builder authInfo(AuthInfo authInfo) {
        this.authInfo = Objects.requireNonNull(authInfo, "authInfo");
        return this;
    }

    @Override
    public ModPackManager.Builder modPackListener(ModPackListener modPackListener) {
        this.modPackListener = Objects.requireNonNullElse(modPackListener, NoOpModPackListener.INSTANCE);
        return this;
    }

    @Override
    public ModPackManager.Builder metadataPath(Path metadataPath) {
        this.metadataPath = Objects.requireNonNull(metadataPath, "metadataPath");
        return this;
    }

    @Override
    public ModPackManager.Builder manifestUri(URI manifestUri) {
        this.manifestUri = Objects.requireNonNull(manifestUri, "manifestUri");
        return this;
    }

    @Override
    public ModPackManager build() {
        Objects.requireNonNull(authInfo, "authInfo");
        Objects.requireNonNull(modPackListener, "modPackListener");
        Objects.requireNonNull(metadataPath, "metadataPath");
        Objects.requireNonNull(manifestUri, "manifestUri");
        return ModPackManagerImpl.create(this);
    }

    private static class NoOpModPackListener implements ModPackListener {
        private static final ModPackListener INSTANCE = new NoOpModPackListener();

        @Override
        public void onInstalled(ModPackVersion modPackVersion) {
        }

        @Override
        public void onRemoved(ModPackVersion modPackVersion) {
        }
    }
}
