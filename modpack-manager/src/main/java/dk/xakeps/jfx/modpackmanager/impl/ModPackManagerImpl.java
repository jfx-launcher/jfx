package dk.xakeps.jfx.modpackmanager.impl;

import dk.xakeps.jfx.auth.AuthInfo;
import dk.xakeps.jfx.modpackmanager.*;
import dk.xakeps.jfx.modpackmanager.impl.admin.ModPackAdminBuilder;
import dk.xakeps.jfx.modpackmanager.impl.downloader.ModPackDownloaderBuilderImpl;
import dk.xakeps.jfx.modpackmanager.impl.local.LocalModPackRegistry;
import dk.xakeps.jfx.modpackmanager.impl.remote.RemoteModPackRegistry;

import java.nio.file.Path;
import java.util.Objects;
import java.util.Optional;

public class ModPackManagerImpl implements ModPackManager {
    final ModPackManagerBuilderImpl builder;
    private final RemoteModPackRegistry remoteModPackRegistry;
    private final LocalModPackRegistry localModPackRegistry;

    private ModPackManagerImpl(ModPackManagerBuilderImpl builder,
                               RemoteModPackRegistry remoteModPackRegistry,
                               LocalModPackRegistry localModPackRegistry) {
        this.builder = Objects.requireNonNull(builder, "builder");
        this.remoteModPackRegistry = Objects.requireNonNull(remoteModPackRegistry, "remoteModPackRegistry");
        this.localModPackRegistry = Objects.requireNonNull(localModPackRegistry, "remoteModPackRegistry");
    }

    @Override
    public ModPackRegistry getRemoteRegistry() {
        return remoteModPackRegistry;
    }

    @Override
    public ModPackRegistry getLocalRegistry() {
        return localModPackRegistry;
    }

    public ModPackListener getModPackListener() {
        return builder.modPackListener;
    }

    public Path getMetadataPath() {
        return builder.metadataPath;
    }

    public Optional<LocalModPackRegistry> getLocalRegistryInternal() {
        return Optional.ofNullable(localModPackRegistry);
    }

    public AuthInfo getAuthInfo() {
        return builder.authInfo;
    }

    static ModPackManager create(ModPackManagerBuilderImpl builder) {
        return new ModPackManagerImpl(builder, RemoteModPackRegistry.empty(builder.manifestUri),
                        LocalModPackRegistry.empty(builder.metadataPath));
    }

    @Override
    public ModPackDownloader.Builder newDownloaderBuilder() {
        return new ModPackDownloaderBuilderImpl(this);
    }

    @Override
    public ModPackAdmin.Builder newModPackAdminBuilder() {
        return new ModPackAdminBuilder(this);
    }
}
