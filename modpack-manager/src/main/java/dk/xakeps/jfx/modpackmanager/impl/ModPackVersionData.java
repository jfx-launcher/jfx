package dk.xakeps.jfx.modpackmanager.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.modpackmanager.impl.downloader.ModPackDownloaderImpl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ModPackVersionData implements InternalModPackVersion {
    @JsonProperty("modPackId")
    private final String modPackId;
    @JsonProperty("minecraftVersion")
    private final String minecraftVersion;
    @JsonProperty("javaVersion")
    private final String javaVersion;
    @JsonProperty("ignoredFiles")
    private final Set<String> ignoredFiles;
    @JsonProperty("version")
    private final String version;
    @JsonProperty("files")
    private final List<ModArtifact> files;

    @JsonCreator
    public ModPackVersionData(@JsonProperty("modPackId") String modPackId,
                              @JsonProperty("minecraftVersion") String minecraftVersion,
                              @JsonProperty("javaVersion") String javaVersion,
                              @JsonProperty("ignoredFiles") Set<String> ignoredFiles,
                              @JsonProperty("version") String version,
                              @JsonProperty("files") List<ModArtifact> files) {
        this.modPackId = Objects.requireNonNull(modPackId, "modPackId");
        this.minecraftVersion = Objects.requireNonNull(minecraftVersion, "minecraftVersion");
        this.javaVersion = Objects.requireNonNull(javaVersion, "javaVersion");
        this.ignoredFiles = Objects.requireNonNull(ignoredFiles, "ignoredFiles");
        this.version = Objects.requireNonNull(version, "version");
        this.files = Objects.requireNonNull(files, "files");
    }

    @Override
    public String getModPackId() {
        return modPackId;
    }

    @Override
    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    @Override
    public String getJavaVersion() {
        return javaVersion;
    }

    @Override
    public Set<String> getIgnoredFiles() {
        return ignoredFiles;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public List<ModArtifact> getArtifacts() {
        return files;
    }

    @Override
    public Set<FileInfo> install(ModPackDownloaderImpl modPackDownloaderImpl) throws IOException {
        return modPackDownloaderImpl.install(this);
    }
}
