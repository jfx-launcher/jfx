package dk.xakeps.jfx.modpackmanager.impl.admin;

import dk.xakeps.jfx.modpackmanager.Admin;

public record AdminImpl(String externalId) implements Admin {
    @Override
    public String getName() {
        return externalId;
    }
}
