package dk.xakeps.jfx.modpackmanager.impl.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

public record CreateModPackRequest(@JsonProperty("modPackId") String modPackId) {
}
