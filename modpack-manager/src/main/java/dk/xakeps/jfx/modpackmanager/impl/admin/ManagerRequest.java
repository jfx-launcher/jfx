package dk.xakeps.jfx.modpackmanager.impl.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ManagerRequest(@JsonProperty("externalId") String externalId) {
}
