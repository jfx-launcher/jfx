package dk.xakeps.jfx.modpackmanager.impl.admin;

import dk.xakeps.jfx.modpackmanager.ModPackAdmin;
import dk.xakeps.jfx.modpackmanager.impl.ModPackManagerImpl;

import java.net.URI;
import java.util.Objects;

public class ModPackAdminBuilder implements ModPackAdmin.Builder {
    final ModPackManagerImpl modPackManager;
    URI serviceUri;

    public ModPackAdminBuilder(ModPackManagerImpl modPackManager) {
        this.modPackManager = Objects.requireNonNull(modPackManager, "modPackManager");
    }

    @Override
    public ModPackAdmin.Builder serviceUri(URI serviceUri) {
        this.serviceUri = Objects.requireNonNull(serviceUri, "serviceUri");
        return this;
    }

    @Override
    public ModPackAdmin build() {
        Objects.requireNonNull(serviceUri, "serviceUri");
        return new ModPackAdminImpl(this);
    }
}
