package dk.xakeps.jfx.modpackmanager.impl.admin;

import com.github.mizosoft.methanol.*;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.modpackmanager.*;
import dk.xakeps.jfx.network.ProgressInfo;
import dk.xakeps.jfx.network.ProgressListener;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ModPackAdminImpl implements ModPackAdmin {
    private final ModPackAdminBuilder builder;
    private final Methanol methanol;
    private final URI createUri;
    private final URI uploadUri;
    private final URI deleteUri;
    private final URI enableUri;
    private final URI managerListUri;
    private final URI addManagerUri;
    private final URI removeManagerUri;

    public ModPackAdminImpl(ModPackAdminBuilder builder) {
        this.builder = Objects.requireNonNull(builder, "builder");
        this.methanol = Methanol.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .defaultHeader("Authorization",
                        "Bearer " + builder.modPackManager.getAuthInfo().getAccessToken())
                .build();
        this.createUri = builder.serviceUri.resolve("manage/create");
        this.uploadUri = builder.serviceUri.resolve("manage/");
        this.deleteUri = builder.serviceUri.resolve("manage/");
        this.enableUri = builder.serviceUri.resolve("manage/");
        this.managerListUri = builder.serviceUri.resolve("manage/");
        this.addManagerUri = builder.serviceUri.resolve("manage/");
        this.removeManagerUri = builder.serviceUri.resolve("manage/");
    }

    @Override
    public void create(String modPackId) {
        HttpRequest request = HttpRequest.newBuilder(createUri)
                .POST(MoreBodyPublishers.ofObject(
                        new CreateModPackRequest(modPackId), MediaType.APPLICATION_JSON))
                .build();
        try {
            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException(String.format("Can't create mod pack. Status code: '%d', Response body: %s",
                        response.statusCode(), response.body()), modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't create mod pack", e, null);
        }
    }

    @Override
    public void upload(ProgressListenerFactory listenerFactory,
                       ModPack modPack, String minecraftVersion, String javaVersion, Set<String> ignoredFiles, Path zip) {
        try (ProgressListener listener = listenerFactory.newListener(zip.getFileName().toString(), 0)) {
            MultipartBodyPublisher realPublisher = MultipartBodyPublisher.newBuilder()
                    .filePart("file", zip, MediaType.APPLICATION_OCTET_STREAM)
                    .textPart("minecraftVersion", minecraftVersion)
                    .textPart("javaVersion", javaVersion)
                    .formPart("ignoredFiles", MoreBodyPublishers.ofObject(ignoredFiles, MediaType.APPLICATION_JSON))
                    .build();
            HttpRequest.BodyPublisher trackingPublisher = ProgressTracker.create()
                    .trackingMultipart(realPublisher, item -> listener.notify(new ProgressInfoImpl(item)));

            HttpRequest request = HttpRequest.newBuilder(
                    uploadUri.resolve(
                            URLEncoder.encode(modPack.getModPackId(), StandardCharsets.UTF_8)
                                    .replace("+", "%20")
                            + "/versions"))
                    // https://github.com/mizosoft/methanol/issues/46
                    .POST(MoreBodyPublishers.ofMediaType(trackingPublisher, realPublisher.mediaType()))
                    .build();

            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't upload mod pack.", modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't upload mod pack", e, null);
        }
    }

    @Override
    public void delete(ModPack modPack) {
        HttpRequest request = HttpRequest.newBuilder(deleteUri.resolve(
                URLEncoder.encode(modPack.getModPackId(), StandardCharsets.UTF_8)
                        .replace("+", "%20")
            )).DELETE().build();
        try {
            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't delete mod pack.", modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't delete mod pack", e, null);
        }
    }

    @Override
    public void enable(ModPackVersion modPackVersion) {
        HttpRequest request = HttpRequest.newBuilder(enableUri.resolve(
                URLEncoder.encode(modPackVersion.getModPackId(), StandardCharsets.UTF_8)
                        .replace("+", "%20")
                        + "/versions/enabled"
                )).POST(MoreBodyPublishers.ofObject(new EnableModPackVersionRequest(modPackVersion.getVersion()),
                        MediaType.APPLICATION_JSON))
                .build();
        try {
            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't enable mod pack version.", modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't enable mod pack version", e, null);
        }
    }

    @Override
    public List<Admin> getAdmins(ModPack modPack) {
        HttpRequest request = HttpRequest.newBuilder(managerListUri.resolve(
                URLEncoder.encode(modPack.getModPackId(), StandardCharsets.UTF_8)
                        .replace("+", "%20") + "/managers"))
                .GET()
                .build();
        try {
            HttpResponse<Result<List<AdminImpl>, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(new TypeRef<List<AdminImpl>>() {}),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't read mod pack admins.", modPackError);
            }
            return Collections.unmodifiableList(response.body().response()
                    .orElseThrow(() -> new ModPackException("Can't get mod pack admins", null)));
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't get mod pack admins", e, null);
        }
    }

    @Override
    public void addAdmin(ModPack modPack, String id) {
        HttpRequest request = HttpRequest.newBuilder(addManagerUri.resolve(
                URLEncoder.encode(modPack.getModPackId(), StandardCharsets.UTF_8)
                        .replace("+", "%20") + "/managers"))
                .POST(MoreBodyPublishers.ofObject(
                        new ManagerRequest(id), MediaType.APPLICATION_JSON))
                .build();
        try {
            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't add mod pack manager", modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't add mod pack manager", e, null);
        }

    }

    @Override
    public void removeAdmin(ModPack modPack, String id) {
        HttpRequest request = HttpRequest.newBuilder(removeManagerUri.resolve(
                URLEncoder.encode(modPack.getModPackId(), StandardCharsets.UTF_8)
                        .replace("+", "%20") + "/managers/"
                        + URLEncoder.encode(id, StandardCharsets.UTF_8).replace("+", "%20")))
                .DELETE()
                .build();
        try {
            HttpResponse<Result<Void, ModPackError>> response = methanol.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't remove mod pack manager", modPackError);
            }
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't remove mod pack manager", e, null);
        }
    }

    private static class ProgressInfoImpl implements ProgressInfo {
        private final ProgressTracker.Progress progress;

        public ProgressInfoImpl(ProgressTracker.Progress progress) {
            this.progress = Objects.requireNonNull(progress, "progress");
        }

        @Override
        public long bytesTransferred() {
            return progress.bytesTransferred();
        }

        @Override
        public long totalBytesTransferred() {
            return progress.totalBytesTransferred();
        }

        @Override
        public Duration timePassed() {
            return progress.timePassed();
        }

        @Override
        public Duration totalTimePassed() {
            return progress.totalTimePassed();
        }

        @Override
        public long contentLength() {
            return progress.contentLength();
        }

        @Override
        public boolean done() {
            return progress.done();
        }
    }
}
