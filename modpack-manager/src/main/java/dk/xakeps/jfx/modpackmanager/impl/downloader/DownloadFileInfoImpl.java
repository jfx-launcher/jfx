package dk.xakeps.jfx.modpackmanager.impl.downloader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.modpackmanager.impl.ModArtifact;

import java.net.URI;
import java.nio.file.Path;

public class DownloadFileInfoImpl implements DownloadFileInfo {
    private final ModArtifact modArtifact;
    private final Path rootPath;

    public DownloadFileInfoImpl(ModArtifact modArtifact, Path rootPath) {
        this.modArtifact = modArtifact;
        this.rootPath = rootPath;
    }

    @Override
    public Path getFile() {
        return rootPath.resolve(modArtifact.file());
    }

    @Override
    public long getSize() {
        return modArtifact.size();
    }

    @Override
    public String getHash() {
        return modArtifact.hash();
    }

    @Override
    public String getName() {
        return modArtifact.file();
    }

    @Override
    public URI getSource() {
        return modArtifact.url();
    }

    @JsonIgnore
    @Override
    public String getHashAlgorithm() {
        return "SHA-1";
    }
}
