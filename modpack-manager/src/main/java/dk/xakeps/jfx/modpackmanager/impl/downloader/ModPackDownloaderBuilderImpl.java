package dk.xakeps.jfx.modpackmanager.impl.downloader;

import dk.xakeps.jfx.modpackmanager.ModPackDownloader;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import dk.xakeps.jfx.modpackmanager.impl.InternalModPackVersion;
import dk.xakeps.jfx.modpackmanager.impl.ModPackManagerImpl;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.nio.file.Path;
import java.util.Objects;

public class ModPackDownloaderBuilderImpl implements ModPackDownloader.Builder {
    final ModPackManagerImpl modPackManager;
    ProgressListenerFactory progressListenerFactory;
    Path downloadRoot;
    InternalModPackVersion modPackVersion;

    public ModPackDownloaderBuilderImpl(ModPackManagerImpl modPackManager) {
        this.modPackManager = modPackManager;
    }

    @Override
    public ModPackDownloader.Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory) {
        this.progressListenerFactory = Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        return this;
    }

    @Override
    public ModPackDownloader.Builder downloadRoot(Path downloadRoot) {
        this.downloadRoot = Objects.requireNonNull(downloadRoot, "downloadRoot");
        return this;
    }

    @Override
    public ModPackDownloader.Builder modPackVersion(ModPackVersion modPackVersion) {
        if (!(modPackVersion instanceof InternalModPackVersion)) {
            throw new IllegalArgumentException("ModPack must implement InternalModPack. ModPack: " + modPackVersion.getClass());
        }
        this.modPackVersion = (InternalModPackVersion) Objects.requireNonNull(modPackVersion, "modPackVersion");
        return this;
    }

    @Override
    public ModPackDownloader build() {
        Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        Objects.requireNonNull(downloadRoot, "downloadRoot");
        Objects.requireNonNull(modPackVersion, "modPackVersion");
        return new ModPackDownloaderImpl(this);
    }
}
