package dk.xakeps.jfx.modpackmanager.impl.downloader;

import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.downloader.Downloader;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.modpackmanager.ModPackDownloader;
import dk.xakeps.jfx.modpackmanager.ModPackException;
import dk.xakeps.jfx.modpackmanager.impl.ModPackVersionData;
import dk.xakeps.jfx.modpackmanager.impl.ObjectMapperHolder;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ModPackDownloaderImpl implements ModPackDownloader {
    private final ModPackDownloaderBuilderImpl builder;

    public ModPackDownloaderImpl(ModPackDownloaderBuilderImpl builder) {
        this.builder = Objects.requireNonNull(builder, "builder");
    }

    @Override
    public DownloadedModPack download() throws IOException {
        Set<FileInfo> files = builder.modPackVersion.install(this);
        return () -> files;
    }

    public Set<FileInfo> install(ModPackVersionData modPackVersionData) {
        try (Downloader downloader = new Downloader(builder.progressListenerFactory)) {
            Set<DownloadFileInfo> collect = modPackVersionData.getArtifacts().stream()
                    .map(modArtifact -> new DownloadFileInfoImpl(modArtifact, builder.downloadRoot))
                    .collect(Collectors.toSet());
            downloader.download(modPackVersionData.getModPackId(), collect).join();

            Path metadataPath = builder.modPackManager.getMetadataPath()
                    .resolve(modPackVersionData.getMinecraftVersion())
                    .resolve(modPackVersionData.getModPackId())
                    .resolve(modPackVersionData.getVersion() + ".json");
            try {
                Files.createDirectories(metadataPath.getParent());
            } catch (IOException e) {
                throw new ModPackException("Can't create ModPack metadata directory", e, null);
            }
            try (BufferedWriter writer = Files.newBufferedWriter(metadataPath)) {
                ObjectMapperHolder.OBJECT_MAPPER.writeValue(writer, modPackVersionData);
            } catch (IOException e) {
                throw new ModPackException("Can't save ModPack metadata", e, null);
            }

            builder.modPackManager.getLocalRegistryInternal()
                    .ifPresent(registry -> registry.addInstalled(modPackVersionData));
            builder.modPackManager.getModPackListener().onInstalled(modPackVersionData);

            return Collections.unmodifiableSet(collect);
        }
    }
}
