package dk.xakeps.jfx.modpackmanager.impl.local;

import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;
import dk.xakeps.jfx.modpackmanager.impl.ModPackVersionData;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class LocalModPack implements ModPack {
    private final String modPackId;
    private final String enabledVersion;
    private final List<ModPackVersionData> versions;

    public LocalModPack(String modPackId,
                        String enabledVersion,
                        List<ModPackVersionData> versions) {
        this.modPackId = modPackId;
        this.enabledVersion = enabledVersion;
        this.versions = versions;
    }

    @Override
    public String getModPackId() {
        return modPackId;
    }

    @Override
    public Optional<ModPackVersion> getEnabledVersion() {
        return versions.stream()
                .filter(v -> v.getVersion().equals(enabledVersion))
                .map(v -> (ModPackVersion) v)
                .findFirst();
    }

    @Override
    public List<ModPackVersion> getVersions() {
        return Collections.unmodifiableList(versions);
    }

    public List<ModPackVersionData> getVersionsImpl() {
        return versions;
    }
}
