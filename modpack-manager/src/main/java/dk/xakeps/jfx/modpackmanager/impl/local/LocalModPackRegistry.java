package dk.xakeps.jfx.modpackmanager.impl.local;

import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackException;
import dk.xakeps.jfx.modpackmanager.ModPackRegistry;
import dk.xakeps.jfx.modpackmanager.impl.ModPackVersionData;
import dk.xakeps.jfx.modpackmanager.impl.ObjectMapperHolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

public class LocalModPackRegistry implements ModPackRegistry {
    private final Path root;
    private List<LocalModPack> modPacks;

    private LocalModPackRegistry(Path root, List<LocalModPack> modPacks) {
        this.root = root;
        this.modPacks = Objects.requireNonNull(modPacks, "modPacks");
    }

    @Override
    public List<ModPack> getModPacks() {
        return Collections.unmodifiableList(modPacks);
    }

    @Override
    public void reload() {
        try {
            modPacks = loadModPacks(root);
        } catch (IOException e) {
            throw new ModPackException("Can't reload mod pack list", e, null);
        }
    }

    public boolean addInstalled(ModPackVersionData modPackVersionData) {
        LocalModPack modPack = modPacks.stream()
                .filter(localModPack -> localModPack.getModPackId().equals(modPackVersionData.getModPackId()))
                .findFirst().orElse(null);
        if (modPack == null) {
            modPack = new LocalModPack(
                    modPackVersionData.getModPackId(),
                    modPackVersionData.getVersion(),
                    new ArrayList<>()
            );
            modPacks.add(modPack);
        }
        if (!modPack.getVersionsImpl().contains(modPackVersionData)) {
            modPack.getVersionsImpl().add(modPackVersionData);
            Collections.sort(modPack.getVersionsImpl());
            return true;
        }
        return false;
    }

    public static LocalModPackRegistry load(Path root) {
        try {
            if (Files.notExists(root)) {
                Files.createDirectories(root);
                return new LocalModPackRegistry(root, new ArrayList<>(0));
            }
            List<LocalModPack> modPacks = loadModPacks(root);
            return new LocalModPackRegistry(root, modPacks);
        } catch (IOException e) {
            return new LocalModPackRegistry(root, new ArrayList<>(0));
        }
    }

    public static LocalModPackRegistry empty(Path root) {
        try {
            if (Files.notExists(root)) {
                Files.createDirectories(root);
                return new LocalModPackRegistry(root, new ArrayList<>(0));
            }
            return new LocalModPackRegistry(root, new ArrayList<>());
        } catch (IOException e) {
            return new LocalModPackRegistry(root, new ArrayList<>(0));
        }
    }

    private static List<LocalModPack> loadModPacks(Path root) throws IOException {
        Map<String, List<ModPackVersionData>> collect = Files.find(root,
                Integer.MAX_VALUE, LocalModPackRegistry::filterPath)
                .map(LocalModPackRegistry::loadJson)
                .filter(Objects::nonNull)
                .collect(Collectors.groupingBy(ModPackVersionData::getModPackId, Collectors.toList()));
        List<LocalModPack> modPacks = new ArrayList<>(collect.size());
        for (Map.Entry<String, List<ModPackVersionData>> entry : collect.entrySet()) {
            List<ModPackVersionData> versions = new ArrayList<>(entry.getValue());
            Collections.sort(versions);
            String enabledVersion = "";
            if (versions.size() > 0) {
                enabledVersion = versions.get(versions.size() - 1).getVersion();
            }
            modPacks.add(new LocalModPack(entry.getKey(), enabledVersion, versions));
        }
        return modPacks;
    }

    private static boolean filterPath(Path path, BasicFileAttributes attributes) {
        return path.getFileName().toString().endsWith(".json") && attributes.isRegularFile();
    }

    private static ModPackVersionData loadJson(Path path) {
        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            return ObjectMapperHolder.OBJECT_MAPPER.readValue(br, ModPackVersionData.class);
        } catch (IOException e) {
            try {
                Files.deleteIfExists(path);
            } catch (IOException ioException) {
                // todo: log
            }
            // todo: log
            return null;
        }
    }
}
