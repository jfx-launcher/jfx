package dk.xakeps.jfx.modpackmanager.impl.remote;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record ModPacksManifest(@JsonProperty("modPacks") List<RemoteModPack> modPacks) {
}
