package dk.xakeps.jfx.modpackmanager.impl.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackVersion;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class RemoteModPack implements ModPack {
    @JsonProperty("modPackId")
    private final String modPackId;
    @JsonProperty("versions")
    private final List<RemoteModPackVersionManifest> versions;
    @JsonProperty("enabledVersion")
    private final String enabledVersion;

    @JsonIgnore
    private final List<RemoteModPackVersion> wrappedVersions;

    @JsonCreator
    public RemoteModPack(@JsonProperty("modPackId") String modPackId,
                         @JsonProperty("versions") List<RemoteModPackVersionManifest> versions,
                         @JsonProperty("enabledVersion") String enabledVersion) {
        this.modPackId = Objects.requireNonNull(modPackId, "name");
        this.versions = Objects.requireNonNull(versions, "versions");
        this.enabledVersion = Objects.requireNonNull(enabledVersion, "enabledVersion");

        this.wrappedVersions = versions.stream()
                .map(v -> new RemoteModPackVersion(this, v))
                .sorted()
                .collect(Collectors.toList());
    }


    @Override
    public String getModPackId() {
        return modPackId;
    }

    @Override
    public Optional<ModPackVersion> getEnabledVersion() {
        return wrappedVersions.stream()
                .filter(v -> v.getVersion().equals(enabledVersion))
                .map(v -> (ModPackVersion) v)
                .findFirst();
    }

    @Override
    public List<ModPackVersion> getVersions() {
        return Collections.unmodifiableList(wrappedVersions);
    }
}
