package dk.xakeps.jfx.modpackmanager.impl.remote;

import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.modpackmanager.ModPack;
import dk.xakeps.jfx.modpackmanager.ModPackError;
import dk.xakeps.jfx.modpackmanager.ModPackException;
import dk.xakeps.jfx.modpackmanager.ModPackRegistry;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;

public class RemoteModPackRegistry implements ModPackRegistry {
    private final URI source;
    private ModPacksManifest modPacksManifest;

    private RemoteModPackRegistry(URI source, ModPacksManifest modPacksManifest) {
        this.source = source;
        this.modPacksManifest = modPacksManifest;
    }

    @Override
    public List<ModPack> getModPacks() {
        return Collections.unmodifiableList(modPacksManifest.modPacks());
    }

    @Override
    public void reload() {
        modPacksManifest = loadManifest(source);
    }

    public static RemoteModPackRegistry load(URI source) {
        try {
            ModPacksManifest modPacksManifest = loadManifest(source);
            return new RemoteModPackRegistry(source, modPacksManifest);
        } catch (ModPackException e) {
            return new RemoteModPackRegistry(source, new ModPacksManifest(Collections.emptyList()));
        }
    }

    public static RemoteModPackRegistry empty(URI source) {
        return new RemoteModPackRegistry(source, new ModPacksManifest(Collections.emptyList()));
    }
    
    private static ModPacksManifest loadManifest(URI source) {
        HttpRequest request = HttpRequest.newBuilder(source).build();
        try {
            HttpResponse<Result<ModPacksManifest, ModPackError>> response = Methanol.create().send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(ModPacksManifest.class),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Can't read mod packs list", modPackError);
            }
            return response.body().response().orElseThrow(() -> new ModPackException("Can't read mod packs list", null));
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Can't read mod packs list", e, null);
        }
    }
}
