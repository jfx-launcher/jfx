package dk.xakeps.jfx.modpackmanager.impl.remote;

import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.modpackmanager.ModPackError;
import dk.xakeps.jfx.modpackmanager.ModPackException;
import dk.xakeps.jfx.modpackmanager.impl.InternalModPackVersion;
import dk.xakeps.jfx.modpackmanager.impl.ModPackVersionData;
import dk.xakeps.jfx.modpackmanager.impl.downloader.ModPackDownloaderImpl;

import java.io.IOException;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Set;

public class RemoteModPackVersion implements InternalModPackVersion {
    private final RemoteModPack remoteModPack;
    private final RemoteModPackVersionManifest versionManifest;

    private ModPackVersionData modPackVersionData;

    public RemoteModPackVersion(RemoteModPack remoteModPack, RemoteModPackVersionManifest versionManifest) {
        this.remoteModPack = remoteModPack;
        this.versionManifest = versionManifest;
    }

    @Override
    public String getModPackId() {
        return remoteModPack.getModPackId();
    }

    @Override
    public String getMinecraftVersion() {
        return versionManifest.getMinecraftVersion();
    }

    @Override
    public String getJavaVersion() {
        return versionManifest.getJavaVersion();
    }

    @Override
    public Set<String> getIgnoredFiles() {
        return versionManifest.getIgnoredFiles();
    }

    @Override
    public String getVersion() {
        return versionManifest.getVersion();
    }


    @Override
    public Set<FileInfo> install(ModPackDownloaderImpl modPackDownloaderImpl) throws IOException {
        return getRemoteVersion().install(modPackDownloaderImpl);
    }

    private ModPackVersionData getRemoteVersion() {
        if (modPackVersionData != null) {
            return modPackVersionData;
        }

        HttpRequest request = HttpRequest.newBuilder(versionManifest.getUri()).build();
        try {
            HttpResponse<Result<ModPackVersionData, ModPackError>> response = Methanol.create().send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(ModPackVersionData.class),
                            MoreBodyHandlers.ofObject(ModPackError.class)));
            ModPackError modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new ModPackException("Failed to load remote ModPackData", modPackError);
            }
            modPackVersionData = response.body().response()
                    .orElseThrow(() -> new ModPackException("Failed to load remote ModPackData", null));
        } catch (IOException | InterruptedException e) {
            throw new ModPackException("Failed to load remote ModPackData", e, null);
        }

        return modPackVersionData;
    }
}
