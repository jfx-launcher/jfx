package dk.xakeps.jfx.modpackmanager.impl.remote;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.Set;

public final class RemoteModPackVersionManifest {
    @JsonProperty("version")
    private final String version;
    @JsonProperty("minecraftVersion")
    private final String minecraftVersion;
    @JsonProperty("javaVersion")
    private final String javaVersion;
    @JsonProperty("ignoredFiles")
    private final Set<String> ignoredFiles;
    @JsonProperty("url")
    private final URI uri;

    public RemoteModPackVersionManifest(@JsonProperty("version") String version,
                                        @JsonProperty("minecraftVersion") String minecraftVersion,
                                        @JsonProperty("javaVersion") String javaVersion,
                                        @JsonProperty("ignoredFiles") Set<String> ignoredFiles,
                                        @JsonProperty("url") URI uri) {
        this.version = version;
        this.minecraftVersion = minecraftVersion;
        this.javaVersion = javaVersion;
        this.ignoredFiles = ignoredFiles;
        this.uri = uri;
    }

    public String getVersion() {
        return version;
    }

    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public Set<String> getIgnoredFiles() {
        return ignoredFiles;
    }

    public URI getUri() {
        return uri;
    }
}
