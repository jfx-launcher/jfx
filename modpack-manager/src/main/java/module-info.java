import dk.xakeps.jfx.modpackmanager.ModPackManager;
import dk.xakeps.jfx.modpackmanager.impl.ModPackManagerBuilderImpl;

module dk.xakeps.jfx.modpackmanager {
    requires methanol;
    requires methanol.adapter.jackson;

    requires transitive dk.xakeps.jfx.auth;
    requires transitive dk.xakeps.jfx.file;
    requires transitive dk.xakeps.jfx.network;
    requires dk.xakeps.jfx.downloader;
    requires dk.xakeps.jfx.httpclient.utils;

    exports dk.xakeps.jfx.modpackmanager;

    requires com.fasterxml.jackson.databind;

    opens dk.xakeps.jfx.modpackmanager to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.modpackmanager.impl to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.modpackmanager.impl.admin to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.modpackmanager.impl.remote to com.fasterxml.jackson.databind;

    uses ModPackManager.Builder;
    provides ModPackManager.Builder with ModPackManagerBuilderImpl;
}