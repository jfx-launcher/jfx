package dk.xakeps.jfx.network;

import java.time.Duration;

public interface ProgressInfo {
    /** Returns the number of transferred bytes for this event. */
    long bytesTransferred();

    /** Returns the total number of bytes transferred so far. */
    long totalBytesTransferred();

    /** Returns the time passed between this and the previous progress events. */
    Duration timePassed();

    /** Returns the total time passed since the upload or download operation has begun. */
    Duration totalTimePassed();

    /** Returns content length, or a value less than zero if unknown. */
    long contentLength();

    /** Returns {@code true} if the upload or download operation is done. */
    boolean done();

    /**
     * Returns a double between {@code 0} and {@code 1} indicating progress, or {@link Double#NaN}
     * if not {@link #determinate()}.
     */
    default double value() {
        long length = contentLength();
        if (length <= 0L) {
            // Special case: length is 0 indicates no body and hence 100% progress
            return length == 0L ? 1.d : Double.NaN;
        }
        return 1.d * totalBytesTransferred() / length;
    }

    /** Returns {@code true} if this progress is determinate (i.e. content length is known). */
    default boolean determinate() {
        return contentLength() >= 0;
    }
}
