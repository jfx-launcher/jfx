package dk.xakeps.jfx.network;

import java.io.IOException;

public interface ProgressListener extends AutoCloseable {
    void notify(ProgressInfo progressInfo);

    @Override
    void close() throws IOException;
}
