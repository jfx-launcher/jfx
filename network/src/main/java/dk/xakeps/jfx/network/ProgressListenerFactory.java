package dk.xakeps.jfx.network;

public interface ProgressListenerFactory {
    ProgressListener newListener(String listenedId, long size);
}
