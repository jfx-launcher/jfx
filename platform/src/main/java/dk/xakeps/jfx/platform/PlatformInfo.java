package dk.xakeps.jfx.platform;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

/**
 * Represents info about platform
 */
public interface PlatformInfo {
    /**
     * Reserved name for Windows operating system
     */
    String WINDOWS = "windows";

    /**
     * Reserved name for systems based on Linux kernel
     */
    String LINUX = "linux";

    /**
     * Reserved name for Mac OS operating system
     */
    String MACOS = "osx";

    /**
     * Reserved name for unknown operating systems
     */
    String UNKNOWN = "unknown";

    /**
     * Reserved architecture id - x86
     */
    String x86 = "x86";

    /**
     * Reserved architecture id - x86_64
     */
    String x86_64 = "x86_64";

    /**
     * This method should return name of operating system
     * this method should return one of reserved names
     * or implementation-dependent name if no name reserved
     * or {@link PlatformInfo#UNKNOWN}
     * @return name of operating system
     */
    default String getName() {
        String property = System.getProperty("os.name").toLowerCase(Locale.ROOT);
        if (property.contains("win")) {
            return WINDOWS;
        } else if (property.contains("mac")) {
            return MACOS;
        } else if (property.contains("linux") || property.contains("unix")) {
            return LINUX;
        } else {
            return UNKNOWN;
        }
    }

    /**
     * This method should return version of operating system
     * or empty string, if version can't be determined
     * @return version of operating system
     */
    default String getVersion() {
        return System.getProperty("os.version");
    }

    /**
     * This method should return architecture of operating system
     * this method should return one of reserved names
     * or implementation-dependent name if no name reserved
     * or empty string, if architecture can't be determined
     * @return architecture of operating system
     */
    default String getArch() {
        String property = System.getProperty("os.arch").toLowerCase(Locale.ROOT);
        if (property.contains("amd64") || property.contains("x86_64")) {
            return x86_64;
        } else if (property.equals("x86")) {
            return x86;
        } else {
            return property;
        }
    }

    /**
     * This method should return PATH separator
     * @return path separator
     */
    default String getPathSeparator() {
        return switch (getName()) {
            case WINDOWS -> ";";
            case UNKNOWN -> File.pathSeparator;
            default -> ":";
        };
    }

    /**
     * This method should return absolute path to a java executable
     * @return absolute path to a java executable
     * @throws JavaExecutableNotFoundException if java executable can't be found
     */
    default Path getJavaExecutable() {
        String property = System.getProperty("java.home");
        Path binDir = Paths.get(property, "bin").toAbsolutePath();
        if (WINDOWS.equals(getName())) {
            Path javawExe = binDir.resolve("javaw.exe");
            Path javaExe = binDir.resolve("java.exe");
            if (Files.isRegularFile(javawExe)) {
                return javawExe;
            } else if (Files.isRegularFile(javaExe)) {
                return javaExe;
            } else {
                throw new JavaExecutableNotFoundException("Executable not found. JAVA_HOME=" + property);
            }
        }
        Path java = binDir.resolve("java");
        if (Files.isRegularFile(java)) {
            return java;
        } else {
            throw new JavaExecutableNotFoundException("Executable not found. JAVA_HOME=" + property);
        }
    }
}
