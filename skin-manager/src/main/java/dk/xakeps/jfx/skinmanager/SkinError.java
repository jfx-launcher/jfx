package dk.xakeps.jfx.skinmanager;

public record SkinError(String type, String message) {
}
