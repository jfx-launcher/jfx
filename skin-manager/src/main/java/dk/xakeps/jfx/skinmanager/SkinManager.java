package dk.xakeps.jfx.skinmanager;

import dk.xakeps.jfx.user.GameProfile;

import java.net.URI;
import java.nio.file.Path;
import java.util.ServiceLoader;

public interface SkinManager {
    void changeSkin(GameProfile profile, SkinType skinType, Path skinFile);
    void changeCape(GameProfile profile, Path skinFile);

    static Builder newBuilder() {
        return ServiceLoader.load(Builder.class)
                .findFirst()
                .orElseThrow();
    }

    interface Builder {
        Builder serviceUri(URI serviceUri);
        Builder accessToken(String accessToken);
        SkinManager build();
    }
}
