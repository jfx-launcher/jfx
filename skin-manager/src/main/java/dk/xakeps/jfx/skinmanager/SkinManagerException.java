package dk.xakeps.jfx.skinmanager;

public class SkinManagerException extends RuntimeException {
    public SkinManagerException() {
    }

    public SkinManagerException(String message) {
        super(message);
    }

    public SkinManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public SkinManagerException(Throwable cause) {
        super(cause);
    }

    public SkinManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
