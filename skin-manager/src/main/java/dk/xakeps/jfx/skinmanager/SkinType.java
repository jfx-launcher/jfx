package dk.xakeps.jfx.skinmanager;

public enum SkinType {
    CLASSIC, SLIM
}
