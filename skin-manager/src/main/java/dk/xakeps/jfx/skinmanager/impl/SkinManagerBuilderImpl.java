package dk.xakeps.jfx.skinmanager.impl;

import dk.xakeps.jfx.skinmanager.SkinManager;

import java.net.URI;
import java.util.Objects;

public class SkinManagerBuilderImpl implements SkinManager.Builder {
    URI serviceUri;
    String accessToken;

    @Override
    public SkinManager.Builder serviceUri(URI serviceUri) {
        this.serviceUri = Objects.requireNonNull(serviceUri, "serviceUri");
        return this;
    }

    @Override
    public SkinManager.Builder accessToken(String accessToken) {
        this.accessToken = Objects.requireNonNull(accessToken, "accessToken");
        return this;
    }

    @Override
    public SkinManager build() {
        Objects.requireNonNull(serviceUri, "serviceUri");
        Objects.requireNonNull(accessToken, "accessToken");
        return new SkinManagerImpl(this);
    }
}
