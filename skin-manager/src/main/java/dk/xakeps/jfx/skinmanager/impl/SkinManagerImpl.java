package dk.xakeps.jfx.skinmanager.impl;

import com.github.mizosoft.methanol.*;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.skinmanager.SkinError;
import dk.xakeps.jfx.skinmanager.SkinManager;
import dk.xakeps.jfx.skinmanager.SkinManagerException;
import dk.xakeps.jfx.skinmanager.SkinType;
import dk.xakeps.jfx.user.GameProfile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;

public class SkinManagerImpl implements SkinManager {
    private final HttpClient httpClient;
    private final URI serviceUri;

    public SkinManagerImpl(SkinManagerBuilderImpl builder) {
        this.httpClient = Methanol.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .defaultHeader("Authorization", "Bearer " + builder.accessToken)
                .build();
        this.serviceUri = builder.serviceUri;
    }

    @Override
    public void changeSkin(GameProfile profile, SkinType skinType, Path skinFile) {
        HttpRequest request;
        try {
            request = HttpRequest.newBuilder(serviceUri.resolve("skin"))
                    .POST(MultipartBodyPublisher.newBuilder()
                            .filePart("file", skinFile, MediaType.APPLICATION_OCTET_STREAM)
                            .textPart("variant", skinType.toString())
                            .textPart("profileId", profile.getId())
                            .build())
                    .build();
        } catch (FileNotFoundException e) {
            throw new SkinManagerException("Skin file not found", e);
        }

        runRequest(request);
    }

    @Override
    public void changeCape(GameProfile profile, Path skinFile) {
        HttpRequest request;
        try {
            request = HttpRequest.newBuilder(serviceUri.resolve("cape"))
                    .POST(MultipartBodyPublisher.newBuilder()
                            .filePart("file", skinFile, MediaType.APPLICATION_OCTET_STREAM)
                            .textPart("profileId", profile.getId())
                            .build())
                    .build();
        } catch (FileNotFoundException e) {
            throw new SkinManagerException("Skin file not found", e);
        }

        runRequest(request);
    }

    private void runRequest(HttpRequest request) {
        try {
            HttpResponse<Result<Void, SkinError>> response = httpClient.send(request, ExtraBodyHandlers.ofResponseOrError(
                    responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                    HttpResponse.BodyHandlers.discarding(),
                    MoreBodyHandlers.ofObject(SkinError.class)
            ));
            SkinError skinError = response.body().error().orElse(null);
            if (skinError != null) {
                throw new SkinManagerException("Can't upload skin or cape. Error: " + skinError);
            }
        } catch (IOException | InterruptedException e) {
            throw new SkinManagerException("Can't upload skin or cape", e);
        }
    }
}
