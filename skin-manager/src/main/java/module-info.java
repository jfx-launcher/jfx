import com.github.mizosoft.methanol.BodyAdapter;
import dk.xakeps.jfx.skinmanager.SkinManager;
import dk.xakeps.jfx.skinmanager.impl.JacksonAdapters;
import dk.xakeps.jfx.skinmanager.impl.SkinManagerBuilderImpl;

module dk.xakeps.jfx.skinmanager {
    requires methanol;
    requires methanol.adapter.jackson;
    requires dk.xakeps.jfx.httpclient.utils;
    requires com.fasterxml.jackson.databind;

    requires dk.xakeps.jfx.user;

    exports dk.xakeps.jfx.skinmanager;

    opens dk.xakeps.jfx.skinmanager to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.skinmanager.impl to com.fasterxml.jackson.databind;

    provides BodyAdapter.Encoder with JacksonAdapters.JacksonEncoder;
    provides BodyAdapter.Decoder with JacksonAdapters.JacksonDecoder;

    uses dk.xakeps.jfx.skinmanager.SkinManager.Builder;
    provides SkinManager.Builder with SkinManagerBuilderImpl;
}