package dk.xakeps.jfx.updatemanager;

import java.util.Set;

public interface PlatformSupportInfo {
    String getSystem();
    Set<String> getArchitectures();
}
