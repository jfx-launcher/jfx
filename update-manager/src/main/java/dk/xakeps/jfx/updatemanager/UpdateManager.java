package dk.xakeps.jfx.updatemanager;

import java.net.URI;
import java.nio.file.Path;
import java.util.ServiceLoader;
import java.util.Set;

public interface UpdateManager {
    UpdateRegistry getRemoteRegistry(String updatePackageType);
    UpdateRegistry getLocalRegistry(String updatePackageType);

    UpdateVersionDownloader.Builder newDownloaderBuilder();
    UpdateManagerAdmin.Builder newUpdateManagerAdminBuilder();

    Set<String> getUpdatePackageTypes();

    void reload();

    static Builder newBuilder() {
        return ServiceLoader.load(Builder.class)
                .findFirst()
                .orElseThrow();
    }

    interface Builder {
        Builder updateVersionListener(UpdateVersionListener updateVersionListener);
        Builder metadataPath(Path metadataPath);
        Builder manifestUri(URI manifestUri);
        UpdateManager build();
    }
}