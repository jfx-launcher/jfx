package dk.xakeps.jfx.updatemanager;

import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.net.URI;
import java.nio.file.Path;
import java.util.Set;

public interface UpdateManagerAdmin {
    void upload(ProgressListenerFactory listenerFactory,
                String updatePackageType, String arch, String system, String version, Path archive);
    void delete(UpdateVersion updateVersion);
    void enable(UpdateVersion updateVersion);

    Set<PlatformSupportInfo> getPlatformSupportInfos();

    interface Builder {
        Builder accessToken(String accessToken);
        Builder serviceUri(URI serviceUri);
        UpdateManagerAdmin build();
    }
}