package dk.xakeps.jfx.updatemanager;

public record UpdateManagerErrorMessage(String type, String message) {
}
