package dk.xakeps.jfx.updatemanager;

import java.util.Optional;

public class UpdateManagerException extends RuntimeException {
    private final UpdateManagerErrorMessage updateManagerErrorMessage;

    public UpdateManagerException(UpdateManagerErrorMessage updateManagerErrorMessage) {
        this.updateManagerErrorMessage = updateManagerErrorMessage;
    }

    public UpdateManagerException(String message, UpdateManagerErrorMessage updateManagerErrorMessage) {
        super(message);
        this.updateManagerErrorMessage = updateManagerErrorMessage;
    }

    public UpdateManagerException(String message, Throwable cause, UpdateManagerErrorMessage updateManagerErrorMessage) {
        super(message, cause);
        this.updateManagerErrorMessage = updateManagerErrorMessage;
    }

    public UpdateManagerException(Throwable cause, UpdateManagerErrorMessage updateManagerErrorMessage) {
        super(cause);
        this.updateManagerErrorMessage = updateManagerErrorMessage;
    }

    public UpdateManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, UpdateManagerErrorMessage updateManagerErrorMessage) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.updateManagerErrorMessage = updateManagerErrorMessage;
    }

    public Optional<UpdateManagerErrorMessage> getUpdateManagerErrorMessage() {
        return Optional.ofNullable(updateManagerErrorMessage);
    }
}
