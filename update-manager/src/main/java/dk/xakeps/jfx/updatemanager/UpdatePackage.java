package dk.xakeps.jfx.updatemanager;

import java.util.List;
import java.util.Optional;

public interface UpdatePackage {
    String getArch();
    String getSystem();
    Optional<UpdateVersion> getEnabledVersion();
    List<UpdateVersion> getVersions();
}
