package dk.xakeps.jfx.updatemanager;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface UpdateRegistry {
    List<UpdateVersion> getUpdateVersions();
    List<UpdatePackage> getUpdatePackages();

    default List<UpdateVersion> getUpdateVersion(String arch, String system) {
        return getUpdateVersions().stream().filter(updateVersion ->
                updateVersion.getArch().equals(arch)
                        && updateVersion.getSystem().equals(system))
                .collect(Collectors.toList());
    }

    default Optional<UpdateVersion> getUpdateVersion(String arch, String system, String version) {
        return getUpdateVersions().stream().filter(updateVersion ->
                updateVersion.getArch().equals(arch)
                        && updateVersion.getSystem().equals(system)
                        && updateVersion.getVersion().equals(version))
                .findFirst();
    }

    default Optional<UpdatePackage> getUpdatePackage(String arch, String system) {
        return getUpdatePackages().stream()
                .filter(updatePackage -> updatePackage.getArch().equals(arch) && updatePackage.getSystem().equals(system))
                .findFirst();
    }

    void reload();
}
