package dk.xakeps.jfx.updatemanager;

public interface UpdateVersion {
    String getUpdatePackageType();
    String getVersion();
    String getArch();
    String getSystem();
}