package dk.xakeps.jfx.updatemanager;

import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.nio.file.Path;
import java.util.Set;

public interface UpdateVersionDownloader {
    DownloadedUpdateVersion download();

    interface DownloadedUpdateVersion {
        Set<FileInfo> getFiles();
        Path getInstallRoot();
    }

    interface Builder {
        Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory);
        Builder updateVersion(UpdateVersion updateVersion);
        UpdateVersionDownloader build();
    }
}
