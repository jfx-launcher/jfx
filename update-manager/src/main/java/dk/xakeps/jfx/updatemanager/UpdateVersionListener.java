package dk.xakeps.jfx.updatemanager;

public interface UpdateVersionListener {
    void onInstalled(UpdateVersion updateVersion);
    void onRemoved(UpdateVersion updateVersion);
}
