package dk.xakeps.jfx.updatemanager.impl;

import dk.xakeps.jfx.updatemanager.UpdateVersion;
import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.updatemanager.impl.downloader.UpdateVersionDownloaderImpl;

public interface InternalUpdateVersion extends UpdateVersion {
    UpdateVersionDownloader.DownloadedUpdateVersion install(UpdateVersionDownloaderImpl updateVersionDownloader);
}
