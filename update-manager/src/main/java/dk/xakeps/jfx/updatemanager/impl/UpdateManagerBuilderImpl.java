package dk.xakeps.jfx.updatemanager.impl;

import dk.xakeps.jfx.updatemanager.UpdateVersionListener;
import dk.xakeps.jfx.updatemanager.UpdateManager;
import dk.xakeps.jfx.updatemanager.UpdateVersion;

import java.net.URI;
import java.nio.file.Path;
import java.util.Objects;

public class UpdateManagerBuilderImpl implements UpdateManager.Builder {
    UpdateVersionListener updateVersionListener = UpdateVersionListenerImpl.INSTANCE;
    Path metadataPath;
    URI manifestUri;

    @Override
    public UpdateManager.Builder updateVersionListener(UpdateVersionListener updateVersionListener) {
        this.updateVersionListener = Objects.requireNonNull(updateVersionListener, "updateVersionListener");
        return this;
    }

    @Override
    public UpdateManager.Builder metadataPath(Path metadataPath) {
        this.metadataPath = Objects.requireNonNull(metadataPath, "metadataPath");
        return this;
    }

    @Override
    public UpdateManager.Builder manifestUri(URI manifestUri) {
        this.manifestUri = Objects.requireNonNull(manifestUri, "manifestUri");
        return this;
    }

    @Override
    public UpdateManager build() {
        Objects.requireNonNull(updateVersionListener, "updateVersionListener");
        Objects.requireNonNull(metadataPath, "metadataPath");
        Objects.requireNonNull(manifestUri, "manifestUri");
        return new UpdateManagerImpl(this);
    }

    private static class UpdateVersionListenerImpl implements UpdateVersionListener {
        private static final UpdateVersionListener INSTANCE = new UpdateVersionListenerImpl();

        @Override
        public void onInstalled(UpdateVersion updateVersion) {

        }

        @Override
        public void onRemoved(UpdateVersion updateVersion) {

        }
    }
}
