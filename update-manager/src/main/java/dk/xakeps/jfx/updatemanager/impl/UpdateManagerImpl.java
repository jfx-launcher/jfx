package dk.xakeps.jfx.updatemanager.impl;

import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.updatemanager.*;
import dk.xakeps.jfx.updatemanager.impl.admin.UpdateManagerAdminBuilderImpl;
import dk.xakeps.jfx.updatemanager.impl.admin.PlatformManifest;
import dk.xakeps.jfx.updatemanager.impl.downloader.UpdateVersionDownloaderBuilderImpl;
import dk.xakeps.jfx.updatemanager.impl.local.LocalUpdateRegistry;
import dk.xakeps.jfx.updatemanager.impl.remote.RemoteUpdateRegistry;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.util.*;

public class UpdateManagerImpl implements UpdateManager {
    private final HttpClient httpClient = Methanol.create();

    final UpdateManagerBuilderImpl builder;

    private final Map<String, RemoteUpdateRegistry> remoteRegistries = new HashMap<>();
    private final Map<String, LocalUpdateRegistry> localRegistries = new HashMap<>();

    private Set<String> updatePackageTypes;

    public UpdateManagerImpl(UpdateManagerBuilderImpl builder) {
        this.builder = Objects.requireNonNull(builder, "builder");
        this.updatePackageTypes = Collections.emptySet();
    }

    @Override
    public UpdateRegistry getRemoteRegistry(String updatePackageType) {
        if (!updatePackageTypes.contains(updatePackageType)) {
            throw new UpdateManagerException("Registry not found: " + updatePackageType, null);
        }
        return remoteRegistries.computeIfAbsent(updatePackageType,
                s -> RemoteUpdateRegistry.empty(builder.manifestUri.resolve(updatePackageType + "/manifest.json"), updatePackageType));
    }

    @Override
    public UpdateRegistry getLocalRegistry(String updatePackageType) {
        if (!updatePackageTypes.contains(updatePackageType)) {
            throw new UpdateManagerException("Registry not found: " + updatePackageType, null);
        }
        return localRegistries.computeIfAbsent(updatePackageType,
                s -> LocalUpdateRegistry.empty(builder.metadataPath.resolve(s)));
    }

    @Override
    public UpdateVersionDownloader.Builder newDownloaderBuilder() {
        return new UpdateVersionDownloaderBuilderImpl(this);
    }

    @Override
    public UpdateManagerAdmin.Builder newUpdateManagerAdminBuilder() {
        return new UpdateManagerAdminBuilderImpl();
    }

    @Override
    public Set<String> getUpdatePackageTypes() {
        return updatePackageTypes;
    }

    @Override
    public void reload() {
        updatePackageTypes = loadUpdatePackageTypes();
        localRegistries.keySet().removeIf(next -> !updatePackageTypes.contains(next));
        remoteRegistries.keySet().removeIf(next -> !updatePackageTypes.contains(next));
    }


    public Optional<LocalUpdateRegistry> getLocalRegistryInternal(String updatePackageType) {
        return Optional.ofNullable(localRegistries.get(updatePackageType));
    }

    public Path getMetadataPath() {
        return builder.metadataPath;
    }

    public UpdateVersionListener getUpdateVersionListener() {
        return builder.updateVersionListener;
    }


    private Set<String> loadUpdatePackageTypes() {
        HttpRequest request = HttpRequest.newBuilder(builder.manifestUri).GET().build();
        try {
            HttpResponse<Result<PlatformManifest, UpdateManagerErrorMessage>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(PlatformManifest.class),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null || response.body().response().isEmpty()) {
                throw new UpdateManagerException("Can't read update package types", updateManagerErrorMessage);
            }
            return new HashSet<>(response.body().response().orElseThrow().updatePackageTypes());
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Can't read update package types", e, null);
        }
    }
}
