package dk.xakeps.jfx.updatemanager.impl.admin;

import com.fasterxml.jackson.annotation.JsonProperty;

public record EnableVersionRequest(@JsonProperty("version") String version) {
}
