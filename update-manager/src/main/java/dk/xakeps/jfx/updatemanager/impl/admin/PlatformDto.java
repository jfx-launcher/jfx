package dk.xakeps.jfx.updatemanager.impl.admin;

import dk.xakeps.jfx.updatemanager.PlatformSupportInfo;

import java.util.Collections;
import java.util.Set;

public record PlatformDto(String system, Set<String> architectures) implements PlatformSupportInfo {
    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public Set<String> getArchitectures() {
        return Collections.unmodifiableSet(architectures);
    }
}
