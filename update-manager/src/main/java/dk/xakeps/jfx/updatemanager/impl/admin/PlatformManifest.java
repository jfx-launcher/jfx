package dk.xakeps.jfx.updatemanager.impl.admin;

import java.util.List;
import java.util.Map;

public record PlatformManifest(Map<String, PlatformDto> platforms, List<String> updatePackageTypes) {
}
