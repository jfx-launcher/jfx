package dk.xakeps.jfx.updatemanager.impl.admin;

import dk.xakeps.jfx.updatemanager.UpdateManagerAdmin;

import java.net.URI;
import java.util.Objects;

public class UpdateManagerAdminBuilderImpl implements UpdateManagerAdmin.Builder {
    String accessToken;
    URI serviceUri;

    @Override
    public UpdateManagerAdmin.Builder accessToken(String accessToken) {
        this.accessToken = Objects.requireNonNull(accessToken, "accessToken");
        return this;
    }

    @Override
    public UpdateManagerAdmin.Builder serviceUri(URI serviceUri) {
        this.serviceUri = Objects.requireNonNull(serviceUri, "serviceUri");
        return this;
    }

    @Override
    public UpdateManagerAdmin build() {
        Objects.requireNonNull(accessToken, "accessToken");
        Objects.requireNonNull(serviceUri, "serviceUri");
        return new UpdateManagerAdminImpl(this);
    }
}
