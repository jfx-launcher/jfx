package dk.xakeps.jfx.updatemanager.impl.admin;

import com.github.mizosoft.methanol.*;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.network.ProgressInfo;
import dk.xakeps.jfx.network.ProgressListener;
import dk.xakeps.jfx.network.ProgressListenerFactory;
import dk.xakeps.jfx.updatemanager.*;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.Duration;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UpdateManagerAdminImpl implements UpdateManagerAdmin {
    private final HttpClient httpClient;
    private final URI serviceUri;

    public UpdateManagerAdminImpl(UpdateManagerAdminBuilderImpl builder) {
        this.httpClient = Methanol.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .defaultHeader("Authorization", "Bearer " + builder.accessToken)
                .build();
        this.serviceUri = builder.serviceUri;
    }

    @Override
    public void upload(ProgressListenerFactory listenerFactory,
                       String updatePackageType, String arch, String system, String version, Path archive) {
        try(ProgressListener listener = listenerFactory.newListener(archive.getFileName().toString(), 0)) {
            MultipartBodyPublisher realPublisher = MultipartBodyPublisher.newBuilder()
                    .textPart("version", version)
                    .filePart("file", archive, MediaType.APPLICATION_OCTET_STREAM).build();
            HttpRequest.BodyPublisher trackingPublisher = ProgressTracker.create()
                    .trackingMultipart(realPublisher, item -> listener.notify(new ProgressInfoImpl(item)));

            HttpRequest request = HttpRequest.newBuilder(serviceUri.resolve(updatePackageType + "/")
                    .resolve(URLEncoder.encode(system, StandardCharsets.UTF_8).replace("+", "%20")
                            + "/" + URLEncoder.encode(arch, StandardCharsets.UTF_8).replace("+", "%20")))
                    // https://github.com/mizosoft/methanol/issues/46
                    .POST(MoreBodyPublishers.ofMediaType(trackingPublisher, realPublisher.mediaType()))
                    .build();
            HttpResponse<Result<Void, UpdateManagerErrorMessage>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null) {
                throw new UpdateManagerException("Can't upload update version.", updateManagerErrorMessage);
            }
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Update version zip not found", e, null);
        }
    }

    @Override
    public void delete(UpdateVersion updateVersion) {
        HttpRequest request = HttpRequest.newBuilder(serviceUri.resolve(updateVersion.getUpdatePackageType() + "/").resolve(
                URLEncoder.encode(updateVersion.getSystem(), StandardCharsets.UTF_8).replace("+", "%20")
                        + "/" + URLEncoder.encode(updateVersion.getArch(), StandardCharsets.UTF_8).replace("+", "%20")
                        + "/" + URLEncoder.encode(updateVersion.getVersion(), StandardCharsets.UTF_8).replace("+", "%20")
        )).DELETE().build();
        try {
            HttpResponse<Result<Void, UpdateManagerErrorMessage>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null) {
                throw new UpdateManagerException("Can't delete update version.", updateManagerErrorMessage);
            }
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Can't delete update version", e, null);
        }
    }

    @Override
    public void enable(UpdateVersion updateVersion) {
        HttpRequest request = HttpRequest.newBuilder(serviceUri.resolve(updateVersion.getUpdatePackageType() + "/").resolve(
                URLEncoder.encode(updateVersion.getSystem(), StandardCharsets.UTF_8).replace("+", "%20")
                        + "/" + URLEncoder.encode(updateVersion.getArch(), StandardCharsets.UTF_8).replace("+", "%20")
                        + "/version/enabled"
        )).POST(MoreBodyPublishers.ofObject(
                new EnableVersionRequest(updateVersion.getVersion()), MediaType.APPLICATION_JSON))
                .build();
        try {
            HttpResponse<Result<Void, UpdateManagerErrorMessage>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            HttpResponse.BodyHandlers.discarding(),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null) {
                throw new UpdateManagerException("Can't enable update version.", updateManagerErrorMessage);
            }
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Can't enable update version", e, null);
        }
    }

    @Override
    public Set<PlatformSupportInfo> getPlatformSupportInfos() {
        HttpRequest request = HttpRequest.newBuilder(serviceUri).GET().build();
        try {
            HttpResponse<Result<PlatformManifest, UpdateManagerErrorMessage>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(PlatformManifest.class),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null || response.body().response().isEmpty()) {
                throw new UpdateManagerException("Can't delete update version.", updateManagerErrorMessage);
            }
            return new HashSet<>(response.body().response().get().platforms().values());
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Can't delete update version", e, null);
        }
    }

    private static class ProgressInfoImpl implements ProgressInfo {
        private final ProgressTracker.Progress progress;

        public ProgressInfoImpl(ProgressTracker.Progress progress) {
            this.progress = Objects.requireNonNull(progress, "progress");
        }

        @Override
        public long bytesTransferred() {
            return progress.bytesTransferred();
        }

        @Override
        public long totalBytesTransferred() {
            return progress.totalBytesTransferred();
        }

        @Override
        public Duration timePassed() {
            return progress.timePassed();
        }

        @Override
        public Duration totalTimePassed() {
            return progress.totalTimePassed();
        }

        @Override
        public long contentLength() {
            return progress.contentLength();
        }

        @Override
        public boolean done() {
            return progress.done();
        }
    }
}
