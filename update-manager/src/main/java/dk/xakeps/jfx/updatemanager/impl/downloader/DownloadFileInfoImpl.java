package dk.xakeps.jfx.updatemanager.impl.downloader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.updatemanager.impl.local.FileArtifact;

import java.net.URI;
import java.nio.file.Path;

public class DownloadFileInfoImpl implements DownloadFileInfo {
    private final FileArtifact fileArtifact;
    private final Path rootPath;

    public DownloadFileInfoImpl(FileArtifact fileArtifact, Path rootPath) {
        this.fileArtifact = fileArtifact;
        this.rootPath = rootPath;
    }

    @Override
    public Path getFile() {
        return rootPath.resolve(fileArtifact.file());
    }

    @Override
    public long getSize() {
        return fileArtifact.size();
    }

    @Override
    public String getHash() {
        return fileArtifact.hash();
    }

    @Override
    public String getName() {
        return fileArtifact.file();
    }

    @Override
    public URI getSource() {
        return fileArtifact.uri();
    }

    @JsonIgnore
    @Override
    public String getHashAlgorithm() {
        return "SHA-1";
    }
}
