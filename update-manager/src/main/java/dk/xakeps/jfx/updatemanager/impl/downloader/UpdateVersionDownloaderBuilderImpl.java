package dk.xakeps.jfx.updatemanager.impl.downloader;

import dk.xakeps.jfx.updatemanager.UpdateVersion;
import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.updatemanager.impl.InternalUpdateVersion;
import dk.xakeps.jfx.updatemanager.impl.UpdateManagerImpl;
import dk.xakeps.jfx.network.ProgressListenerFactory;

import java.util.Objects;

public class UpdateVersionDownloaderBuilderImpl implements UpdateVersionDownloader.Builder {
    final UpdateManagerImpl updateManager;

    ProgressListenerFactory progressListenerFactory;
    InternalUpdateVersion updateVersion;

    public UpdateVersionDownloaderBuilderImpl(UpdateManagerImpl updateManager) {
        this.updateManager = Objects.requireNonNull(updateManager, "updateManager");
    }

    @Override
    public UpdateVersionDownloader.Builder progressListenerFactory(ProgressListenerFactory progressListenerFactory) {
        this.progressListenerFactory = Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        return this;
    }

    @Override
    public UpdateVersionDownloader.Builder updateVersion(UpdateVersion updateVersion) {
        if (!(updateVersion instanceof InternalUpdateVersion)) {
            throw new IllegalArgumentException("UpdateVersion must implement InternalUpdateVersion. UpdateVersion: " + updateVersion.getClass());
        }
        this.updateVersion = (InternalUpdateVersion) Objects.requireNonNull(updateVersion, "updateVersion");
        return this;
    }

    @Override
    public UpdateVersionDownloader build() {
        Objects.requireNonNull(progressListenerFactory, "progressListenerFactory");
        Objects.requireNonNull(updateVersion, "UpdateVersion");
        return new UpdateVersionDownloaderImpl(this);
    }
}
