package dk.xakeps.jfx.updatemanager.impl.downloader;

import dk.xakeps.jfx.downloader.DownloadFileInfo;
import dk.xakeps.jfx.downloader.Downloader;
import dk.xakeps.jfx.file.FileInfo;
import dk.xakeps.jfx.updatemanager.UpdateManagerException;
import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.updatemanager.impl.ObjectMapperHolder;
import dk.xakeps.jfx.updatemanager.impl.local.UpdateVersionData;
import dk.xakeps.jfx.updatemanager.impl.local.SymlinkArtifact;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class UpdateVersionDownloaderImpl implements UpdateVersionDownloader {
    private final UpdateVersionDownloaderBuilderImpl builder;

    public UpdateVersionDownloaderImpl(UpdateVersionDownloaderBuilderImpl builder) {
        this.builder = Objects.requireNonNull(builder, "builder");
    }

    @Override
    public DownloadedUpdateVersion download() {
        return builder.updateVersion.install(this);
    }

    public DownloadedUpdateVersion install(UpdateVersionData updateVersionData) {
        Path downloadRoot = builder.updateManager.getMetadataPath()
                .resolve(updateVersionData.system())
                .resolve(updateVersionData.arch())
                .resolve(updateVersionData.version());
        try (Downloader downloader = new Downloader(builder.progressListenerFactory)) {
            Set<DownloadFileInfo> collect = updateVersionData.files().stream()
                    .map(modArtifact -> new DownloadFileInfoImpl(modArtifact, downloadRoot))
                    .collect(Collectors.toSet());
            downloader.download(updateVersionData.version(), collect).join();

            for (SymlinkArtifact symlink : updateVersionData.symlinks()) {
                try {
                    Files.createSymbolicLink(downloadRoot.resolve(symlink.source()), downloadRoot.resolve(symlink.target()));
                } catch (IOException e) {
                    throw new UpdateManagerException("Can't create symlink", e, null);
                }
            }

            Path metadataPath = downloadRoot.getParent()
                    .resolve(updateVersionData.version() + ".json");
            try {
                Files.createDirectories(metadataPath.getParent());
            } catch (IOException e) {
                throw new UpdateManagerException("Can't create UpdateVersion metadata directory", e, null);
            }
            try (BufferedWriter writer = Files.newBufferedWriter(metadataPath)) {
                ObjectMapperHolder.OBJECT_MAPPER.writeValue(writer, updateVersionData);
            } catch (IOException e) {
                throw new UpdateManagerException("Can't save UpdateVersion metadata", e, null);
            }

            builder.updateManager.getLocalRegistryInternal(updateVersionData.updatePackageType())
                    .ifPresent(r -> r.addInstalled(updateVersionData));
            builder.updateManager.getUpdateVersionListener().onInstalled(updateVersionData);

            return new DownloadedUpdateVersionImpl(Collections.unmodifiableSet(collect), downloadRoot);
        }
    }

    private static record DownloadedUpdateVersionImpl(Set<FileInfo> files, Path installRoot)
            implements DownloadedUpdateVersion {
        @Override
        public Set<FileInfo> getFiles() {
            return files;
        }

        @Override
        public Path getInstallRoot() {
            return installRoot;
        }
    }
}
