package dk.xakeps.jfx.updatemanager.impl.local;

import java.net.URI;

public record FileArtifact(String file, String hash, long size, URI uri) {
}
