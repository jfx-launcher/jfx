package dk.xakeps.jfx.updatemanager.impl.local;

import dk.xakeps.jfx.updatemanager.UpdatePackage;
import dk.xakeps.jfx.updatemanager.UpdateVersion;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public record LocalUpdatePackage(String arch, String system, UpdateVersion enabledVersion,
                                 List<UpdateVersionData> versions) implements UpdatePackage {
    @Override
    public String getArch() {
        return arch;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public Optional<UpdateVersion> getEnabledVersion() {
        return Optional.ofNullable(enabledVersion);
    }

    @Override
    public List<UpdateVersion> getVersions() {
        return Collections.unmodifiableList(versions);
    }
}
