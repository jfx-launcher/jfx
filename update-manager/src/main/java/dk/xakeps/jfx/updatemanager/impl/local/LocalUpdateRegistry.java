package dk.xakeps.jfx.updatemanager.impl.local;

import dk.xakeps.jfx.updatemanager.UpdateManagerException;
import dk.xakeps.jfx.updatemanager.UpdatePackage;
import dk.xakeps.jfx.updatemanager.UpdateRegistry;
import dk.xakeps.jfx.updatemanager.UpdateVersion;
import dk.xakeps.jfx.updatemanager.impl.ObjectMapperHolder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.stream.Collectors;

public class LocalUpdateRegistry implements UpdateRegistry {
    private final Path root;
    private Map<Key, LocalUpdatePackage> updatePackages;

    private LocalUpdateRegistry(Path root, Map<Key, LocalUpdatePackage> updatePackages) {
        this.root = Objects.requireNonNull(root, "root");
        this.updatePackages = updatePackages;
    }

    @Override
    public List<UpdateVersion> getUpdateVersions() {
        return updatePackages.values().stream().flatMap(localUpdatePackage -> localUpdatePackage.versions().stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<UpdatePackage> getUpdatePackages() {
        return List.copyOf(updatePackages.values());
    }

    @Override
    public void reload() {
        this.updatePackages = loadUpdateVersions(root);
    }

    public static LocalUpdateRegistry load(Path root) {
        try {
            if (Files.notExists(root)) {
                Files.createDirectories(root);
                return new LocalUpdateRegistry(root, new HashMap<>(0));
            }
            Map<Key, LocalUpdatePackage> updateVersions = loadUpdateVersions(root);
            return new LocalUpdateRegistry(root, updateVersions);
        } catch (IOException e) {
            return new LocalUpdateRegistry(root, new HashMap<>(0));
        }
    }

    public static LocalUpdateRegistry empty(Path root) {
        try {
            if (Files.notExists(root)) {
                Files.createDirectories(root);
                return new LocalUpdateRegistry(root, new HashMap<>(0));
            }
            return new LocalUpdateRegistry(root, new HashMap<>(0));
        } catch (IOException e) {
            return new LocalUpdateRegistry(root, new HashMap<>(0));
        }
    }

    private static Map<Key, LocalUpdatePackage> loadUpdateVersions(Path root) {
        List<UpdateVersionData> collect;
        try {
            collect = Files.find(root, Integer.MAX_VALUE, LocalUpdateRegistry::filterPath)
                    .map(LocalUpdateRegistry::loadJson)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UpdateManagerException("Can't load update versions", e, null);
        }

        Map<Key, List<UpdateVersionData>> mapped = collect.stream()
                .collect(Collectors.groupingBy(updateVersionData -> new Key(updateVersionData.arch(), updateVersionData.system())));

        Map<Key, LocalUpdatePackage> localUpdatePackages = new HashMap<>(mapped.size());
        for (Map.Entry<Key, List<UpdateVersionData>> entry : mapped.entrySet()) {
            Key key = entry.getKey();
            UpdateVersionData enabledVersion = entry.getValue().stream()
                    .min(Comparator.comparing(UpdateVersionData::getVersion))
                    .orElse(null);
            localUpdatePackages.put(key, new LocalUpdatePackage(key.arch(), key.system(), enabledVersion, entry.getValue()));
        }

        return localUpdatePackages;
    }

    private static boolean filterPath(Path path, BasicFileAttributes attributes) {
        return path.getFileName().toString().endsWith(".json") && attributes.isRegularFile();
    }

    private static UpdateVersionData loadJson(Path path) {
        try (BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            return ObjectMapperHolder.OBJECT_MAPPER.readValue(br, UpdateVersionData.class);
        } catch (IOException e) {
            try {
                Files.deleteIfExists(path);
            } catch (IOException ioException) {
                // todo: log
            }
            // todo: log
            return null;
        }
    }

    public void addInstalled(UpdateVersionData updateVersionData) {
        LocalUpdatePackage localUpdatePackage = updatePackages.get(new Key(updateVersionData.arch(), updateVersionData.system()));
        if (localUpdatePackage == null) {
            List<UpdateVersionData> list = new ArrayList<>();
            list.add(updateVersionData);
            localUpdatePackage = new LocalUpdatePackage(updateVersionData.arch(), updateVersionData.system(), updateVersionData, list);
            updatePackages.put(new Key(updateVersionData.arch(), updateVersionData.system()), localUpdatePackage);
        } else {
            localUpdatePackage.versions().add(updateVersionData);
        }
    }

    record Key(String arch, String system) {}
}
