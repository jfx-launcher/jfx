package dk.xakeps.jfx.updatemanager.impl.local;

public record SymlinkArtifact(String target, String source) {
}
