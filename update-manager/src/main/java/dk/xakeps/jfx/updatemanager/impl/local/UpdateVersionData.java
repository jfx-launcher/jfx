package dk.xakeps.jfx.updatemanager.impl.local;

import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.updatemanager.impl.InternalUpdateVersion;
import dk.xakeps.jfx.updatemanager.impl.downloader.UpdateVersionDownloaderImpl;

import java.util.List;

public record UpdateVersionData(String updatePackageType, String version, String arch, String system,
                                List<FileArtifact> files, List<SymlinkArtifact> symlinks) implements InternalUpdateVersion {
    @Override
    public String getUpdatePackageType() {
        return updatePackageType;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getArch() {
        return arch;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public UpdateVersionDownloader.DownloadedUpdateVersion install(UpdateVersionDownloaderImpl updateVersionDownloader) {
        return updateVersionDownloader.install(this);
    }
}
