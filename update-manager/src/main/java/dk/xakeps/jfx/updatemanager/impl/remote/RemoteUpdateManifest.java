package dk.xakeps.jfx.updatemanager.impl.remote;

import dk.xakeps.jfx.updatemanager.UpdatePackage;
import dk.xakeps.jfx.updatemanager.UpdateVersion;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public record RemoteUpdateManifest(String updatePackageType,
                                   String arch,
                                   String system,
                                   String enabledVersion,
                                   List<RemoteUpdateVersion> updates) implements UpdatePackage {
    @Override
    public String getArch() {
        return arch;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public Optional<UpdateVersion> getEnabledVersion() {
        return updates.stream().filter(remoteUpdateVersion -> remoteUpdateVersion.getVersion().equals(enabledVersion))
                .findFirst().map(Function.identity());
    }

    @Override
    public List<UpdateVersion> getVersions() {
        return Collections.unmodifiableList(updates);
    }
}
