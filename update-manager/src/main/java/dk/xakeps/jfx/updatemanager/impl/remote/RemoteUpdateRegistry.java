package dk.xakeps.jfx.updatemanager.impl.remote;

import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.updatemanager.*;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RemoteUpdateRegistry implements UpdateRegistry {
    private final URI source;
    private UpdatePackageList updatePackageList;

    private RemoteUpdateRegistry(URI source, UpdatePackageList updatePackageList) {
        this.source = Objects.requireNonNull(source, "source");
        this.updatePackageList = updatePackageList;
    }

    @Override
    public List<UpdateVersion> getUpdateVersions() {
        return updatePackageList.manifests().stream()
                .flatMap(remoteUpdateManifest -> remoteUpdateManifest.getVersions().stream())
                .collect(Collectors.toList());

    }

    @Override
    public List<UpdatePackage> getUpdatePackages() {
        return Collections.unmodifiableList(updatePackageList.manifests());
    }

    @Override
    public void reload() {
        this.updatePackageList = loadManifest(source);
    }

    public static RemoteUpdateRegistry load(URI source, String updatePackageType) {
        try {
            UpdatePackageList updatePackageList = loadManifest(source);
            return new RemoteUpdateRegistry(source, updatePackageList);
        } catch (UpdateManagerException e) {
            return new RemoteUpdateRegistry(source, new UpdatePackageList(updatePackageType, Collections.emptyList()));
        }
    }

    public static RemoteUpdateRegistry empty(URI source, String updatePackageType) {
        return new RemoteUpdateRegistry(source, new UpdatePackageList(updatePackageType, Collections.emptyList()));
    }

    private static UpdatePackageList loadManifest(URI source) {
        HttpRequest request = HttpRequest.newBuilder(source).build();
        try {
            HttpResponse<Result<UpdatePackageList, UpdateManagerErrorMessage>> response = Methanol.create().send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(UpdatePackageList.class),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage updateManagerErrorMessage = response.body().error().orElse(null);
            if (updateManagerErrorMessage != null) {
                throw new UpdateManagerException("Can't read update version list", updateManagerErrorMessage);
            }
            return response.body().response().orElseThrow(() -> new UpdateManagerException("Can't read update version list", null));
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Can't read update version list", e, null);
        }
    }
}
