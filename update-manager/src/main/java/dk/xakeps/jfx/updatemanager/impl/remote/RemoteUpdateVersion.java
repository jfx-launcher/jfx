package dk.xakeps.jfx.updatemanager.impl.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.mizosoft.methanol.HttpStatus;
import com.github.mizosoft.methanol.Methanol;
import com.github.mizosoft.methanol.MoreBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.updatemanager.UpdateManagerErrorMessage;
import dk.xakeps.jfx.updatemanager.UpdateManagerException;
import dk.xakeps.jfx.updatemanager.UpdateVersionDownloader;
import dk.xakeps.jfx.updatemanager.impl.InternalUpdateVersion;
import dk.xakeps.jfx.updatemanager.impl.downloader.UpdateVersionDownloaderImpl;
import dk.xakeps.jfx.updatemanager.impl.local.UpdateVersionData;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public final class RemoteUpdateVersion implements InternalUpdateVersion {
    @JsonProperty("updatePackageType")
    private final String updatePackageType;
    @JsonProperty("system")
    private final String system;
    @JsonProperty("arch")
    private final String arch;
    @JsonProperty("version")
    private final String version;
    @JsonProperty("url")
    private final URI uri;

    @JsonIgnore
    private UpdateVersionData updateVersionData;

    @JsonCreator
    public RemoteUpdateVersion(@JsonProperty("updatePackageType") String updatePackageType,
                               @JsonProperty("system") String system,
                               @JsonProperty("arch") String arch,
                               @JsonProperty("version") String version,
                               @JsonProperty("url") URI uri) {
        this.updatePackageType = updatePackageType;
        this.system = system;
        this.arch = arch;
        this.version = version;
        this.uri = uri;
    }

    @Override
    public String getUpdatePackageType() {
        return updatePackageType;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getArch() {
        return arch;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public UpdateVersionDownloader.DownloadedUpdateVersion install(UpdateVersionDownloaderImpl updateVersionDownloader) {
        return getUpdateVersionData().install(updateVersionDownloader);
    }

    private UpdateVersionData getUpdateVersionData() {
        if (updateVersionData != null) {
            return updateVersionData;
        }

        HttpRequest request = HttpRequest.newBuilder(uri).build();
        try {
            HttpResponse<Result<UpdateVersionData, UpdateManagerErrorMessage>> response = Methanol.create().send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(UpdateVersionData.class),
                            MoreBodyHandlers.ofObject(UpdateManagerErrorMessage.class)));
            UpdateManagerErrorMessage modPackError = response.body().error().orElse(null);
            if (modPackError != null) {
                throw new UpdateManagerException("Failed to load remote UpdateVersionData", modPackError);
            }
            updateVersionData = response.body().response()
                    .orElseThrow(() -> new UpdateManagerException("Failed to load remote UpdateVersionData", null));
        } catch (IOException | InterruptedException e) {
            throw new UpdateManagerException("Failed to load remote UpdateVersionData", e, null);
        }

        return updateVersionData;
    }
}
