package dk.xakeps.jfx.updatemanager.impl.remote;

import java.util.List;

public record UpdatePackageList(String updatePackageType, List<RemoteUpdateManifest> manifests) {
}
