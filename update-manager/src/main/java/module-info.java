import dk.xakeps.jfx.updatemanager.UpdateManager;
import dk.xakeps.jfx.updatemanager.impl.UpdateManagerBuilderImpl;

module dk.xakeps.jfx.updatemanager {
    requires transitive dk.xakeps.jfx.file;
    requires transitive dk.xakeps.jfx.network;

    requires dk.xakeps.jfx.downloader;
    requires dk.xakeps.jfx.httpclient.utils;
    requires methanol;
    requires methanol.adapter.jackson;

    exports dk.xakeps.jfx.updatemanager;

    opens dk.xakeps.jfx.updatemanager to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.updatemanager.impl.admin to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.updatemanager.impl.local to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.updatemanager.impl.remote to com.fasterxml.jackson.databind;

    uses UpdateManager.Builder;
    provides UpdateManager.Builder with UpdateManagerBuilderImpl;
}