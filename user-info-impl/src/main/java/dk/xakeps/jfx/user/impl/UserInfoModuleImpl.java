package dk.xakeps.jfx.user.impl;

import com.github.mizosoft.methanol.*;
import dk.xakeps.jfx.httpclient.utils.ExtraBodyHandlers;
import dk.xakeps.jfx.httpclient.utils.Result;
import dk.xakeps.jfx.user.User;
import dk.xakeps.jfx.user.UserInfoModule;
import dk.xakeps.jfx.user.UserModuleException;
import dk.xakeps.jfx.user.impl.http.ErrorResponse;
import dk.xakeps.jfx.user.impl.http.GetOrCreateRequest;
import dk.xakeps.jfx.user.impl.model.GameProfileModel;
import dk.xakeps.jfx.user.impl.model.ProfileListModel;
import dk.xakeps.jfx.user.impl.model.UserModel;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Collections;

public class UserInfoModuleImpl implements UserInfoModule {
    private final URI profileInfoUri;

    public UserInfoModuleImpl(URI profileInfoUri) {
        this.profileInfoUri = profileInfoUri;
    }

    @Override
    public User loadUser(String accessToken) {
        try {
            Methanol httpClient = Methanol.create();
            HttpRequest request = HttpRequest.newBuilder(profileInfoUri)
                    .header("Authorization", "Bearer " + accessToken)
                    .POST(MoreBodyPublishers.ofObject(new GetOrCreateRequest(true, true),
                            MediaType.APPLICATION_JSON))
                    .build();
            HttpResponse<Result<ProfileListModel, ErrorResponse>> response = httpClient.send(request,
                    ExtraBodyHandlers.ofResponseOrError(
                            responseInfo -> !HttpStatus.isSuccessful(responseInfo.statusCode()),
                            MoreBodyHandlers.ofObject(ProfileListModel.class),
                            MoreBodyHandlers.ofObject(ErrorResponse.class)
                    ));
            Result<ProfileListModel, ErrorResponse> result = response.body();
            ErrorResponse errorResponse = result.error().orElse(null);
            if (errorResponse != null) {
                throw new UserModuleException("Failed to get user info. Error: " + errorResponse);
            }

            ProfileListModel profileListModel = result.response()
                    .orElseThrow(() -> new UserModuleException("Failed to get user info"));
            String selectedProfileId = profileListModel.selectedProfile();
            GameProfileModel selectedProfile = null;
            for (GameProfileModel profile : profileListModel.profiles()) {
                if (selectedProfileId.equals(profile.getId())) {
                    selectedProfile = profile;
                }
            }
            if (selectedProfile == null) {
                throw new UserModuleException("selectedProfile is null");
            }
            return new UserModel(Collections.emptyList(), selectedProfile,
                    Collections.unmodifiableList(profileListModel.profiles()));
        } catch (IOException | InterruptedException e) {
            throw new UserModuleException(e);
        }
    }
}
