package dk.xakeps.jfx.user.impl;

import dk.xakeps.jfx.user.UserInfoModule;
import dk.xakeps.jfx.user.UserInfoModuleProvider;
import dk.xakeps.jfx.user.UserModuleException;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Map;
import java.util.Objects;

public class UserInfoModuleProviderImpl implements UserInfoModuleProvider {
    @Override
    public String getId() {
        return "DEFAULT";
    }

    @Override
    public UserInfoModule createModule(Map<String, String> settings) {
        String urlStr = settings.get("url");
        Objects.requireNonNull(urlStr, "url");

        return new UserInfoModuleImpl(URI.create(urlStr));
    }
}
