package dk.xakeps.jfx.user.impl.http;

public record ErrorResponse(String type, String message) {
}
