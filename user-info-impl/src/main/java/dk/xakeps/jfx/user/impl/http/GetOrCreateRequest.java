package dk.xakeps.jfx.user.impl.http;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetOrCreateRequest(@JsonProperty("full") boolean full, @JsonProperty("signed") boolean signed) {
}
