package dk.xakeps.jfx.user.impl.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.user.GameProfile;
import dk.xakeps.jfx.user.Property;

import java.util.Collections;
import java.util.List;

public record GameProfileModel(@JsonProperty("id") String id, @JsonProperty("name") String name,
                               @JsonProperty("properties") List<PropertyModel> properties) implements GameProfile {

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Property> getProperties() {
        return Collections.unmodifiableList(properties);
    }
}
