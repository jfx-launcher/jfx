package dk.xakeps.jfx.user.impl.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record ProfileListModel(@JsonProperty("profiles") List<GameProfileModel> profiles,
                               @JsonProperty("selectedProfile") String selectedProfile) {
}
