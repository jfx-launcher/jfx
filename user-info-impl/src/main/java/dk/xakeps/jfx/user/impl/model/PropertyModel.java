package dk.xakeps.jfx.user.impl.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.xakeps.jfx.user.Property;

import java.util.Optional;

public record PropertyModel(@JsonProperty("name") String name,
                            @JsonProperty("value") String value,
                            @JsonProperty("signature") String signature) implements Property {

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public Optional<String> getSignature() {
        return Optional.ofNullable(signature);
    }
}
