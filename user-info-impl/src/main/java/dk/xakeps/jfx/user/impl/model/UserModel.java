package dk.xakeps.jfx.user.impl.model;

import dk.xakeps.jfx.user.GameProfile;
import dk.xakeps.jfx.user.Property;
import dk.xakeps.jfx.user.User;

import java.util.List;

public record UserModel(List<Property> properties,
                        GameProfile selectedProfile, List<GameProfile> availableProfiles) implements User {
    @Override
    public List<Property> getProperties() {
        return properties;
    }

    @Override
    public GameProfile getSelectedProfile() {
        return selectedProfile;
    }

    @Override
    public List<GameProfile> getAvailableProfiles() {
        return availableProfiles;
    }
}
