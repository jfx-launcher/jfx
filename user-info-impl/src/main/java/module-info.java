import com.github.mizosoft.methanol.BodyAdapter;
import dk.xakeps.jfx.user.UserInfoModuleProvider;
import dk.xakeps.jfx.user.impl.JacksonAdapters;
import dk.xakeps.jfx.user.impl.UserInfoModuleProviderImpl;

module dk.xakeps.jfx.user.impl {
    requires methanol;
    requires methanol.adapter.jackson;

    requires dk.xakeps.jfx.httpclient.utils;
    requires dk.xakeps.jfx.user;

    requires com.fasterxml.jackson.databind;

    opens dk.xakeps.jfx.user.impl.http to com.fasterxml.jackson.databind;
    opens dk.xakeps.jfx.user.impl.model to com.fasterxml.jackson.databind;

    provides UserInfoModuleProvider with UserInfoModuleProviderImpl;

    provides BodyAdapter.Encoder with JacksonAdapters.JacksonEncoder;
    provides BodyAdapter.Decoder with JacksonAdapters.JacksonDecoder;
}