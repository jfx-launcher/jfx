package dk.xakeps.jfx.user;

public interface UserInfoModule {
    User loadUser(String accessToken);
}
