package dk.xakeps.jfx.user;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;

public interface UserInfoModuleProvider {
    String getId();

    default UserInfoModule createModule() {
        return createModule(Collections.emptyMap());
    }

    UserInfoModule createModule(Map<String, String> settings);

    static Optional<UserInfoModuleProvider> findAny() {
        return ServiceLoader.load(UserInfoModuleProvider.class).findFirst();
    }

    static Optional<UserInfoModuleProvider> find(String id) {
        return ServiceLoader.load(UserInfoModuleProvider.class).stream()
                .map(ServiceLoader.Provider::get)
                .filter(provider -> provider.getId().equals(id))
                .findAny();
    }
}
